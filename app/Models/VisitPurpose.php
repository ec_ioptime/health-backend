<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VisitPurpose extends Model
{
    use HasFactory;

    protected $table = "visitpurpose";
    protected $fillable =
    [
        'patient_id',
        'nurse_id',
        'purpose',
        'doc_name'
    ];
}
