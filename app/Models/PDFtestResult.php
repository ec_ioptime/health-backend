<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PDFtestResult extends Model
{
    use HasFactory;
    protected $table = 'pdftestr';
    protected $fillable = [
        'staff_id',
        'clinic_id',
        'testdate',
        'testtime',
        'testtype',
        'testresult',
        'sampleid',
        'patientid',
        'assay1',
        'testvalues1',
        'testvalues2',
        'testvalues3',
        'testvalues4',
        'testvalues5',
        'testvalues6',
        'statusreport',
        'statusreportcheck',
    ];
}
