<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Queue extends Model
{
    use HasFactory;

    protected $table = "queues";

    protected $fillable = [
        "queue_number",
        'currenNumber',
        "is_current",
        "clinic_id",
        "clinic_name",
        "department_id",
        "department_name",
        "user_id"
    ];
}
