<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VistsModel extends Model
{
    use HasFactory;
    protected $table = "vists";
    protected $fillable = [
        'parientID',
        'patientID',
        'status',
        'appointment',
        'doc_name',
    ];

    public function test()
    {
        return $this->hasMany(Test::class,'visit_id','id');
    }
}
