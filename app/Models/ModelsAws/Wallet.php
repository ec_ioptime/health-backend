<?php

namespace App\Models\ModelsAws;

use Rennokki\DynamoDb\DynamoDbModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Wallet extends  DynamoDbModel
{
    use HasFactory;
      public $table = "Wallet";


      public function getBalance($userId)
      {
          $wallet =   $this->where("WalletUserId", $userId)->first();
          return ($wallet != null ? $wallet->WalletBalance : 0);
        }
}
