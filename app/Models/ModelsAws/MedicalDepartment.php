<?php

namespace App\Models\ModelsAws;

use Rennokki\DynamoDb\DynamoDbModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MedicalDepartment extends DynamoDbModel
{
    use HasFactory;
    protected $table = "medical_departments";
    protected $primaryKey = 'id';
protected $compositeKey = ['id', 'departments_hash'];
    protected $dynamoDbIndexKeys = [
    'departments_hash' => [
        'hash' => 'id',
    ],
];
}
