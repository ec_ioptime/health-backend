<?php

namespace App\Models\ModelsAws;

use Rennokki\DynamoDb\DynamoDbModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Notifications\Notification;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class User extends DynamoDbModel
{

    use HasFactory,Notifiable;
    public $table = "Medical-Users";
    protected $primaryKey = 'UserId';
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */

    protected $hidden = [

    ];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */

    public function routeNotificationForSms(Notification $notification = null)
    {
        return $this->phone_number;
    }

     public function routeNotificationForMail($notification)
  {
    return $this->email;
}
}
