<?php

namespace App\Models\ModelsAws;

use Rennokki\DynamoDb\DynamoDbModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UhfCard extends DynamoDbModel
{

    use HasFactory;

    protected $table = 'uhf_cards';


}
