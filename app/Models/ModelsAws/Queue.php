<?php

namespace App\Models\ModelsAws;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Rennokki\DynamoDb\DynamoDbModel;

class Queue extends DynamoDbModel
{
    use HasFactory;
    public $table = "queues";
    protected $primaryKey = 'id';
    protected $compositeKey = ['id', 'departments_hash'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    public $fillable = [
        'queue_id',
    ];


    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];
}
