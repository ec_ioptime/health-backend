<?php

namespace App\Models\ModelsAws;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Rennokki\DynamoDb\DynamoDbModel;

class Transaction extends DynamoDbModel
{
    use HasFactory;
    public $table = "transactions";
    protected $guarded = [];
}

