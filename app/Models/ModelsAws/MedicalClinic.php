<?php

namespace App\Models\ModelsAws;

use Rennokki\DynamoDb\DynamoDbModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MedicalClinic extends DynamoDbModel
{
    use HasFactory;

    protected $table = "medical_clinics";
     protected $primaryKey = 'id';
     protected $compositeKey = ['id', 'clinics_hash'];

}
