<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Ratting extends Model
{
    use HasFactory;

    protected $table = "ratting";
    protected $fillable = [
        'id',
        'doc_id',
        'date',
        'description',
        'rate',
    ];

    public function doctors()
    {
        return $this->hasOne(User::class, 'id', 'doc_id');
    }

    public function usersdetail()
    {
        return $this->hasOne(UserDetail::class, 'user_id', 'doc_id');
    }

    public function reviewer()
    {
        return $this->hasMany(User::class, 'id', 'add_users_id');
    }
}
