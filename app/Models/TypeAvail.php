<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeAvail extends Model
{
    use HasFactory;
    protected $table = 'typeofavailbity';
    protected $fillable = ['name'];
}
