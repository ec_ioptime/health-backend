<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeofApp extends Model
{
    use HasFactory;
    protected $table = "typeof";
    protected $fillable = [
        'name',
        'image1'
    ];
}
