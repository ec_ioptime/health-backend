<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssignTests extends Model
{
    use HasFactory;
    protected $table = "assigntest";
    protected $fillable = [
        "patient_id",
        "staff_id",
        "test_id",
        "status"
    ];
}
