<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RishFactor extends Model
{
    use HasFactory;
    protected $table = "riskfactorans";
    protected $fillable = [
        "patient_id",
        "riskfactor_question",
        "ans",
        "risk_category_id",
        "risk_category_name",
    ];
}
