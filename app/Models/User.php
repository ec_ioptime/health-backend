<?php

namespace App\Models;

use App\Models\UserDetail;
use App\Models\VitalRecord;
use App\Models\DoctorCategory;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Jetstream\HasProfilePhoto;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id',
        'pin',
        'phone_number',
        'dialing_code'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'pin',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function questions()
    {
        return $this->hasMany(CovidQuestionRecord::class);
    }

    public function vaccination_records()
    {
        return $this->hasMany(VaccinationRecord::class);
    }

    public function wallet()
    {
        return $this->hasOne(Wallet::class);
    }

    public function clinic()
    {
        return $this->belongsTo(Clinic::class);
    }
    public function category()
    {
        return $this->belongsTo(DoctorCategory::class, 'doctor_category_id');
    }
    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function pcr_tests()
    {
        return $this->hasMany(PcrTest::class);
    }
    public function record()
    {
        return $this->hasMany(VitalRecord::class);
    }
    public function detail()
    {
        return $this->hasOne(UserDetail::class, 'user_id', 'id');
    }
    public function referral()
    {
        return $this->hasMany(Referral::class, 'patient_id', 'id');
    }

    public function usersdetail()
    {
        return $this->hasOne(UserDetail::class);
    }

    public function ratting()
    {
        return $this->hasMany(Ratting::class, 'doc_id', 'id');
    }

    public function checkInAndOut()
    {
        return $this->hasMany(PatientCheck::class, 'patient_id', 'id');
    }

    public function vists()
    {
        return $this->hasMany(VistsModel::class, 'patientID', 'id');
    }

    public function vitals()
    {
        return $this->hasMany(Test::class, 'patient_id', 'id');
    }
    public function test()
    {
        return $this->hasMany(Test::class, 'patient_id', 'id');
    }

    public function MedicalFamily()
    {
        return $this->hasMany(FamilyMedicalHistory::class, 'patientID', 'id');
    }

    public function depart()
    {
        return $this->hasOne(Department::class, 'id', 'department_id');
    }

    public function countrylist()
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }
}
