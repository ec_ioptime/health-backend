<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientCheck extends Model
{
    use HasFactory;
    protected $table = "patientcheck";
    protected $fillable =
    [
        'visit_id',
        'status',
        'department',
        'department_id',
        'patient_id',
        'staff_id',
        'check_in',
        'check_out',
        'type',
        'date'
    ];

    public function staff()
    {
        return $this->hasOne(User::class, 'id', 'staff_id');
    }

    public static function PatientCheckOutFunction($request, $dep)
    {
        $patientcheckin = PatientCheck::CHeckINProcessIN($request, $dep);
        $patientcheckin->check_out = date('Y-m-d H:i:s');
        $patientcheckin->status =  "checkout";
        return  $patientcheckin->save();
    }

    public static function QueueCurrentRuning($request)
    {
        $staffID = User::where('id', $request->staff_id)->first();
        return Queue::where('department_id', $request->department_id)
            ->where('clinic_id', $staffID->clinic_id)
            ->where('date', $request->todayDateUtc)
            ->orderBy('id', 'DESC')
            ->get();
    }
    public static function QueueCurrentGet($request, $visit = null, $dep = null)
    {
        $staffID = User::where('id', $request->staff_id)->first();
        if ($visit) {
            return Queue::where('department_id', $request->department_id)
                ->where('clinic_id', $staffID->clinic_id)
                ->where('date', $request->todayDateUtc)
                ->where('visit_id', $visit)
                ->orderBy('id', 'DESC')
                ->first();
        } else if ($dep) {
            return Queue::where('department_id', $dep)
                ->where('clinic_id', $staffID->clinic_id)
                ->where('date', $request->todayDateUtc)
                ->orderBy('id', 'DESC')
                ->first();
        } else {
            return Queue::where('department_id', $request->department_id)
                ->where('clinic_id', $staffID->clinic_id)
                ->where('date', $request->todayDateUtc)
                ->orderBy('id', 'DESC')
                ->first();
        }
    }

    public static function VisitModelGet($request, $visit_id = null)
    {
        if ($visit_id) {
            return  VistsModel::where('patientID', $request->patietnID)
                ->where('id', $request->visit_id)
                ->where('date', $request->todayDateUtc)
                ->where('status', '!=', 'complete')
                ->orderBy('id', 'DESC')
                ->first();
        } else {
            return VistsModel::where('patientID', $request->patietnID)
                ->where('date', $request->todayDateUtc)
                ->where('status', '!=', 'complete')
                ->orderBy('id', 'DESC')
                ->first();
        }
    }



    // CheckOUT
    public static function CHeckINProcessOUT($request, $dep, $checkVisit = null)
    {
        if ($checkVisit) {
            return PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('visit_id', $checkVisit->id)->where('date', $request->todayDateUtc)->where('status', 'checkout')->where('patient_id', $request->patietnID)->first();
        } else {
            return PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('visit_id', $request->id)->where('date', $request->todayDateUtc)->where('status', 'checkout')->where('patient_id', $request->patietnID)->first();
        }
    }
    // CheckIN
    public static function CHeckINProcessIN($request, $dep, $checkVisit = null)
    {
        if ($checkVisit) {
            return PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('visit_id', $checkVisit->id)->where('date', $request->todayDateUtc)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();
        } else {
            return PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('visit_id', $request->visit_id)->where('date', $request->todayDateUtc)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();
        }
    }

    public static function CreateVisit($request)
    {
        $modelVisit = null;
        if ($request->visit_id) {
            return  $vistID = $request->visit_id;
        } else {
            $modelVisit = new VistsModel();
            $use = User::where('id', $request->staff_id)->first();
            $modelVisit->patientID = $request->patietnID;
            $modelVisit->docID = $request->staff_id;
            $modelVisit->date = $request->todayDateUtc;
            $modelVisit->status = "new";
            $modelVisit->doc_name = $use->name;
            $modelVisit->save();
            return  $vistID = $modelVisit->id;
        }
    }

    public static function CreateCheckIN($request, $dep, $name, $vistID)
    {
        $patientcheckin = new PatientCheck();
        $patientcheckin->visit_id =  $vistID;
        $patientcheckin->status =  "checkin";
        $patientcheckin->department_id = $dep;
        $patientcheckin->department = $name;
        $patientcheckin->patient_id =  $request->patietnID;
        $patientcheckin->staff_id = $request->staff_id;
        $patientcheckin->check_in = date('Y-m-d H:i:s');
        $patientcheckin->type = $name;
        $patientcheckin->date = $request->todayDateUtc;
        return $patientcheckin->save();
    }

    public static function CheckVisit($request)
    {
        return VistsModel::where('patientID', $request->patietnID)
            ->where('date', $request->todayDateUtc)
            ->where('status', '!=', 'complete')
            ->orderBy('id', 'DESC')
            ->first();
    }
}
