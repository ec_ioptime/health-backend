<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmergencyContact extends Model
{
    use HasFactory;
    protected $table = "emergency_contact";

    public function user ()
    {
        return $this->belongsTo(User::class);
    }

    public function country ()
    {
        return $this->belongsTo(Country::class);
    }
}
