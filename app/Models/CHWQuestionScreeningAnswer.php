<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CHWQuestionScreeningAnswer extends Model
{
    use HasFactory;
    protected $table='chwquestionsanswer';
    protected $fillable=[
        'date',
        'category_id',
        'category',
        'question_id',
        'answer',
        'staff_id',
        'patient_id',
        'visit_id'
    ];

}
