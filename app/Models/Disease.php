<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use function PHPSTORM_META\map;

class Disease extends Model
{
    use HasFactory;

    protected $table="disease";
    protected $fillable=
    [
        "name",
        "answer",
        "is_familymedical"
    ];
    public function question()
    {
        return $this->hasOne(KnownChronicQuestion::class,'questions_id','id');
    }


}
