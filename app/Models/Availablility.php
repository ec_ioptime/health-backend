<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Availablility extends Model
{
    use HasFactory;

    protected $table = "availability";
    protected $fillable = [
        'price',
        'currency',
        'doc_id',
        'fromdate',
        'todate',
        'fromtime',
        'totime',
        'tyoeid',
        'feeid'
    ];
}
