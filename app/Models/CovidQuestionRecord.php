<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CovidQuestionRecord extends Model
{

    use HasFactory;
    protected $table = 'covid_question_records';

    public function user ()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function question ()
    {
        return $this->belongsTo(CovidQuestion::class, 'question_id');
    }

}
