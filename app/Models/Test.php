<?php

namespace App\Models;

use App\Models\VitalRecord;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Test extends Model
{
    use HasFactory;
    protected $table = "tests";
    public function records()
    {
        return $this->hasMany(VitalRecord::class);
    }
    public function assignTest()
    {
        return $this->hasMany(AssignTests::class, 'test_id', 'id');
    }
}
