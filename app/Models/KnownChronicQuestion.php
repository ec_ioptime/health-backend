<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KnownChronicQuestion extends Model
{
    use HasFactory;

    protected $table = "knowchronicquestion";

    protected $fillable =
    [
        'questions_id',
        'user_id',
        'answer',
        'options',
        'is_knowchronic',
    ];
    public function question()
    {
        return $this->belongsTo(Disease::class,'questions_id','id');
    }
}
