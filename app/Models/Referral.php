<?php

namespace App\Models;

use App\Models\User;
use App\Models\Clinic;
use App\Models\VitalRecord;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Referral extends Model
{
    use HasFactory;




    public function record()
    {
        return $this->belongsTo(VitalRecord::class, 'record_id');
    }
    // public function referredBy()
    // {
    //     return $this->belongsTo(Roles::class, 'referred_by');
    // }
    // public function referredTo()
    // {
    //     return $this->belongsTo(Roles::class, 'referred_to');
    // }



    public function patient()
    {
        return $this->belongsTo(User::class, 'patient_id');
    }

    public function nurse()
    {
        return $this->belongsTo(User::class, 'nurse_id');
    }


    public function referredBy()
    {
        return $this->belongsTo(Department::class, 'referred_by', 'id');
    }

    public function referredTo()
    {
        return $this->belongsTo(Department::class, 'referred_to', 'id');
    }
}
