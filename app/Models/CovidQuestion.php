<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CovidQuestion extends Model
{
    use HasFactory;

    public function questionRecords()
    {
        return $this->hasMany(CovidQuestionRecord::class);
    }
}
