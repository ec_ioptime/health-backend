<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RishFactorQuestions extends Model
{
    use HasFactory;

    protected $table = "riskfactor";
    protected $fillable = [
        "riskfactor",
        "question_type",
        "question_name",
        "answer",
    ];
}
