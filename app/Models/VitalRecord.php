<?php

namespace App\Models;

use App\Models\Test;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class VitalRecord extends Model
{
    use HasFactory;

    protected $table = "vital_records";
    protected $fillable = [
        "symptoms",
        "status",
        "nurse_id",
        "patient_id",
        "data",
        'visit_id',
        'questionId',
        'is_departmentRefer',
        'isCHW'
    ];

    public function patient()
    {
        return $this->belongsTo(User::class, 'patient_id');
    }
    public function nurse()
    {
        return $this->belongsTo(User::class, 'nurse_id');
    }
    public function test()
    {
        return $this->belongsTo(Test::class, 'test_id');
    }
    public function referral()
    {
        return $this->hasMany(Referral::class);
    }
}
