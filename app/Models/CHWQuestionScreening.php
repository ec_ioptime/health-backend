<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CHWQuestionScreening extends Model
{
    use HasFactory;
    protected $table = "chwquestions";
    protected $fillable = [
        'question',
        'answers',
        'category',
        'category_id'
    ];
}
