<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;

    public function DoctorDetail()
    {
        return $this->hasOne(User::class, 'id', 'doctor_id');
    }
    public function PatientDetail()
    {
        return $this->hasOne(User::class, 'id', 'patient_id');
    }
}
