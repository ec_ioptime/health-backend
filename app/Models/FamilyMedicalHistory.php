<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyMedicalHistory extends Model
{
    use HasFactory;
    protected $table = "familymedicalhistory";
    protected $fillable =
    [
        "patientID",
        "disease_id",
        "diseasen_name"
    ];
}
