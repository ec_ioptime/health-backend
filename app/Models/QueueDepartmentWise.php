<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QueueDepartmentWise extends Model
{
    use HasFactory;
    protected $table = "";
    protected $fillable = [
        'queue_number',
        'is_current',
        'device_id',
        'department_id',
        'department_name',
        'department_id',
        'user_id',
    ];
}
