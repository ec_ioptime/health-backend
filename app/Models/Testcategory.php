<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Testcategory extends Model
{
    use HasFactory;
    protected $table = "testcategory";

    public function tests()
    {
        return $this->hasMany(Listest::class, 'category_id', 'id');
    }
}
