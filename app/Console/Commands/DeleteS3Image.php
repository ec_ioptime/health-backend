<?php

namespace App\Console\Commands;

use Exception;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Aws\Rekognition\RekognitionClient;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class DeleteS3Image extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $config = Config::get('aws');
        $images  = Storage::disk('s3')->files();
        foreach ($images as $image ) {
            # code...
        $rekognitionClient = new RekognitionClient($config);
        try {
            $result = $rekognitionClient->searchFacesByImage([
                'CollectionId' => 'newCollection', // REQUIRED
                'Image' => [ // REQUIRED
                    'S3Object' => [
                        'Bucket' => 'photosbucketnewone',
                        'Name' => $image,
                    ],
                ],
            ]);
            $resultImage = $result->toArray();
            if (isset($resultImage["FaceMatches"][0]["Face"]["FaceId"])) {
                $user = User::where("face_id", $resultImage["FaceMatches"][0]["Face"]["FaceId"])->first();
                if (!isset($user)) {
                    Storage::disk('s3')->delete($image);
            Log::info($image.' Deleted');

                }
            } else {
                Storage::disk('s3')->delete($image);
                 Log::info($image.' Deleted');

            }
        } catch (Exception $th) {
            Log::info($th->getMessage());
            Storage::disk('s3')->delete($image);
            Log::info($image.' Deleted');
        }

        }

    }
}
