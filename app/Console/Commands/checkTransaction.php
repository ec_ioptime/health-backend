<?php

namespace App\Console\Commands;

use App\Models\ModelsAws\Transaction;
use App\Models\ModelsAws\UhfCard;
use App\Models\ModelsAws\User;
use App\Models\ModelsAws\Wallet;
use App\Notifications\SendSms;
use Aws\Laravel\AwsFacade;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class checkTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:transaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks for transaction in the ZK database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        Log::info("Checkng new transactions");
        $client = new Client();
        $apiURL = "http://3.233.102.40:8098/api/transaction/list?pageNo=1&pageSize=10&access_token=26E02472AF49D20271778A33B2310373";
        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ];

        $response = $client->request('GET', $apiURL, [
            // 'json' => $params,
            'headers' => $headers,
        ]);
        $transactions = json_decode($response->getBody(), true);
        print_r($transactions);
      die();

        // $now = strtotime(now()->setTimezone("UTC")->toDateTimeString());
        $config = Config::get('aws');
        $clientssss = AwsFacade::createClient('DynamoDb', $config);
        $now = strtotime("2022-02-25 15:01:04");
        // $now = strtotime(now()->setTimezone("UTC")->toDateTimeString());

        foreach ($transactions['data'] as $transaction => $value) {
            // foreach ($data as $transaction => $value) {

                if (str_contains($value["devName"], "U2")) {
                    $localTransactions = Transaction::all()->pluck("event_id")->toArray();
                    if ($localTransactions) {
                        if (in_array($value["id"], $localTransactions)) {
                            Log::info("Transaction already exists");
                            continue;
                        }
                    }
                    $event = strtotime($value['eventTime']);
                    $seconds = abs($now - $event);
                    try {
                        /* if new transaction is added in last 10 seconds. */
                        // if ($seconds <= 5) {
                        $cardNo = $value['cardNo'];
                        $userCard = UhfCard::where('card_number', $cardNo)->first();

                        if ($userCard) {
                            $user = User::where('ContactNumber', $userCard->phone_number)->first();

                            if ($user) {

                                $userWallet = Wallet::where("WalletUserId", (string) $user->UserId)->first();

                                if ($value["deptName"] == "Class E") {
                                    $amountToDeduct = 10;
                                } else if ($value["deptName"] == "Class A") {
                                    $amountToDeduct = 2;
                                } else if ($value["deptName"] == "Class B") {
                                    $amountToDeduct = 4;
                                } else if ($value["deptName"] == "Class C") {
                                    $amountToDeduct = 5;
                                } else if ($value["deptName"] == "Class D") {
                                    $amountToDeduct = 6;
                                } else {
                                    $amountToDeduct = 0;
                                    Log::info("No amount to deduct/No department detected.");
                                }
                                $urlAddAcess = "http://3.233.102.40:8098/api/accLevel/syncPerson?pin=" . $user->UserId . "&levelIds=2c9fd01a7e7b07be017f119c6f87257a&access_token=26E02472AF49D20271778A33B2310373";
                                if ($userWallet->WalletBalance >= $amountToDeduct) {
                                    $userWallet->WalletBalance = $userWallet->WalletBalance - $amountToDeduct;
                                    $userWallet->save();
                                    $transaction = new Transaction();
                                    $transaction->id = (string) rand(1, 1000000);
                                    $transaction->event_id = $value["id"];
                                    $transaction->user_id = $user->UserId;
                                    $transaction->amount_deducted = $amountToDeduct;
                                    $transaction->save();
                                    $user->notify(new SendSms("Your balance has been deducted. Amount of charge is $amountToDeduct"));
                                    $response = $client->request('POST', $urlAddAcess, [
                                        'headers' => $headers,
                                    ]);
                                    $RES = json_decode($response->getBody(), true);
                                    if ($RES['code'] == 0) {
                                        Log::info("Access Level Added to User" . $user->UserId);
                                    }
                                } else {
                                    $deleteLevelURl = "http://3.233.102.40:8098/api/accLevel/deleteLevel?pin=" . $user->UserId . "&levelIds=2c9fd01a7e7b07be017f119c6f87257a&access_token=26E02472AF49D20271778A33B2310373";
                                    $response = $client->request('POST', $deleteLevelURl, [
                                        'headers' => $headers,
                                    ]);
                                    $RES = json_decode($response->getBody(), true);
                                    if ($RES['code'] == 0) {
                                        Log::info("Access Level Removed for User." . $user->UserId);
                                    }
                                    $user->notify(new SendSms("Your wallet balance is Insufficent. Please recharge."));
                                }
                            } else {
                                Log::info("User not found");
                            } /* end if user */
                        } else {
                            Log::error("No Card Found in DB.");
                        } /* end if user card */
                        /* dynamo-db configuration */

                        // }

                    } catch (Exception $e) {
                        Log::error($e->getMessage());
                    }

                }
        }/* end foreach transactions */

    }
}

// Class A $2 , Class B $3 , Class C $4, Class D $5, Class E $10
//
