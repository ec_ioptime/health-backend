<?php

namespace App\Http\Traits;

use App\Models\UserDetail;
use Exception;
use Illuminate\Support\Facades\Log;

trait ApiResponseTrait
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result, $message)
    {
        $response = [
            'success' => true,
            'data' => $result,
            'message' => $message,
        ];
        return response()->json($response, 200);
    }
    public function sendResponseData($result, $message)
    {
        $response = [
            'success' => true,
            'responseData' => $result,
            'message' => $message,
        ];
        return response()->json($response, 200);
    }

    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];

        if (!empty($errorMessages)) {
            $response['data'] = $errorMessages;
        }

        return response()->json($response, $code);
    }

    public function SendErrorLog($error, $type)
    {
        $currentUrl = url()->current();
        Log::info(request()->userAgent());
        Log::info($currentUrl);

        if ($type == 'debug') {
            Log::debug($error);
        } else if ($type == 'info') {
            Log::info($error);
        } else if ($type == 'error') {
            Log::error($error);
        }
    }

    public function Expection($e, $msg, $type)
    {
        $this->SendErrorLog($e.$msg, $type);
        $message = $e->getMessage();
        return $this->sendError($msg,  $message);
    }
}
