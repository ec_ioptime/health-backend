<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponseTrait;
use Aws\Rekognition\RekognitionClient;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class AwsImageProcessingController extends Controller
{
    use ApiResponseTrait;
    /* inserts the face in collection after uploading it to S3 */
    public function insertImage(Request $request)
    {
        /* creates a collection */
        // $result = $rekognitionClient->createCollection([
        //     'CollectionId' =>'newCollection',
        // ]);
        // dd($result);
        Log::info(request()->userAgent());
        try {
            $config = Config::get('aws');
            $rekognitionClient = new RekognitionClient($config);

            $filenamewithextension = $request->file('image')->getClientOriginalName();
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $filenametostore = $filename . '_' . time() . '.' . $extension;
            //Upload File to s3
            Storage::disk('s3')->put($filenametostore, fopen($request->file('image'), 'r+'), 'public');
            /* inserts image in collection */
            $result = $rekognitionClient->indexFaces([
                'CollectionId' => 'newCollection',
                'Image' => [
                    'S3Object' => [
                        'Bucket' => 'photosbucketnewone',
                        'Name' => $filenametostore,
                    ],
                ],
            ]);

            return $this->sendResponse($result->toArray(), []);
        } catch (\Aws\Exception\AwsException $th) {
            return $this->sendResponse($th->getAwsErrorMessage(), []);
        }
    }

    public function searchImage(Request $request)
    {
        Log::info(request()->userAgent());
        $config = Config::get('aws');

        try {

            $rekognitionClient = new RekognitionClient($config);
            $filenamewithextension = $request->file('image')->getClientOriginalName();
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('image')->getClientOriginalExtension();
            //filename to store
            $filenametostore = $filename . '_' . time() . '.' . $extension;

            //Upload File to s3
            Storage::disk('s3')->put($filenametostore, fopen($request->file('image'), 'r+'), 'public');
            $result = $rekognitionClient->searchFacesByImage([
                'CollectionId' => 'newCollection', // REQUIRED
                'Image' => [ // REQUIRED
                    'S3Object' => [
                        'Bucket' => 'photosbucketnewone',
                        'Name' => $filenametostore,
                    ],
                ],
            ]);
            $resultImage = $result->toArray();
            if (isset($resultImage["FaceMatches"][0]["Face"]["FaceId"])) {
                $user = User::where("face_id", $resultImage["FaceMatches"][0]["Face"]["FaceId"])
                    ->where('clinic_id', $request->clinicId)
                    ->with('usersdetail')->first();
                if (isset($user)) {
                    if ($user->deactivated == 0) {
                        return $this->sendResponse(["user" => $user, "image-data" => $result->toArray()], []);
                    } else {
                        Log::info("User is deactivated");
                        return $this->sendError("User is deactivated", []);
                    }
                } else {
                    Log::info("No User Found for current image.");
                    return $this->sendError("No User Found for current image.", []);
                }
            } else {
                Log::info("No Face Matches.");
                return $this->sendResponse(["image-data" => $result->toArray()], []);
            }
        } catch (\Aws\Exception\AwsException $th) {
            Log::info($th->getAwsErrorMessage());
            return $this->sendResponse($th->getAwsErrorMessage(), []);
        }
    }
    public function searchImageBeforeReg(Request $request)
    {

        Log::info(request()->userAgent());
        $config = Config::get('aws');

        try {

            $rekognitionClient = new RekognitionClient($config);
            $filenamewithextension = $request->file('image')->getClientOriginalName();
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('image')->getClientOriginalExtension();
            //filename to store
            $filenametostore = $filename . '_' . time() . '.' . $extension;
            //Upload File to s3
            Storage::disk('s3')->put($filenametostore, fopen($request->file('image'), 'r+'), 'public');


            $result = $rekognitionClient->searchFacesByImage([
                'CollectionId' => 'newCollection', // REQUIRED
                'Image' => [ // REQUIRED
                    'S3Object' => [
                        'Bucket' => 'photosbucketnewone',
                        'Name' => $filenametostore,
                    ],
                ],
            ]);
            $resultImage = $result->toArray();

            $imageUrl =     Storage::disk('s3')->url($filenametostore);
            if (isset($resultImage["FaceMatches"][0]["Face"]["FaceId"])) {
                $user = User::where("face_id", $resultImage["FaceMatches"][0]["Face"]["FaceId"])->with('usersdetail')->first();
                if (isset($user)) {
                    if ($user->deactivated == 0) {
                        return $this->sendResponse('User already exists for current face.', ["user" => $user]);
                    } else {
                        Log::info("User is deactivated");
                        return $this->sendError("User is deactivated", []);
                    }
                } else {
                    Log::info("No User Found for current image.");
                    return $this->sendError("No User Found for current image.",  ["face-id" => $resultImage["FaceMatches"][0]["Face"]["FaceId"], 'imageUrl' => $imageUrl]);
                }
            } else {
                Log::info("No Face Matches.");
                $result = $rekognitionClient->indexFaces([
                    'CollectionId' => 'newCollection',
                    'Image' => [
                        'S3Object' => [
                            'Bucket' => 'photosbucketnewone',
                            'Name' => $filenametostore,
                        ],
                    ],
                ]);
                $resultImage = $result->toArray();
                if (isset($resultImage["FaceRecords"][0]["Face"]["FaceId"])) {
                    $id = $resultImage["FaceRecords"][0]["Face"]["FaceId"];
                } else {
                    $id = $resultImage["FaceMatches"][0]["Face"]["FaceId"];
                }
                Log::info("No User Found for current image.");
                return $this->sendError("No User Found for current image.",  ["face-id" => $id, 'imageUrl' => $imageUrl]);
            }
        } catch (\Aws\Exception\AwsException $th) {
            Log::info($th->getAwsErrorMessage());
            return $this->sendResponse($th->getAwsErrorMessage(), []);
        }
    }
}
