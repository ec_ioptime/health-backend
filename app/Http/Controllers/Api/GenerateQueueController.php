<?php

namespace App\Http\Controllers\Api;

use Exception;
use Carbon\Carbon;
use App\Models\Clinic;
use App\Models\Department;
use Illuminate\Http\Request;
use App\Models\Queue;
use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponseTrait;
use Illuminate\Support\Facades\Validator;

class GenerateQueueController extends Controller
{
    use ApiResponseTrait;
    public function generateQueue(Request $request)
    {

        $res = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'clinic_id' => 'required|exists:clinics,id',
            'department_id' => 'required|exists:departments,id',

        ]);
        foreach ($res->errors()->toArray() as $field => $message) {
            $errors[] = [
                'message' => $message[0],
            ];
        }
        if (isset($errors)) {
            return $this->sendError('Validation Error.', $errors);
        }
        try {

            $clinic = Clinic::find($request->clinic_id);
            $department = Department::find($request->department_id);

            $lastQueue = Queue::where('department_id', $request->department_id)->max('queue_number');
            $lastQueueNumber = (($lastQueue) ? $lastQueue : 0);
            $latestQueuNumber = ($lastQueueNumber + 1);
            $newQueueEntry = new Queue();
            $newQueueEntry->clinic_name = $clinic->name;
            $newQueueEntry->clinic_id = $clinic->id;
            $newQueueEntry->department_id = $department->id;
            $newQueueEntry->department_name = $department->department_name;
            $newQueueEntry->user_id = $request->user_id;
            if ($latestQueuNumber == 1) {
                $newQueueEntry->is_current = true;
            } else {
                $newQueueEntry->is_current = false;
            }
            $newQueueEntry->queue_number = $latestQueuNumber;
            $newQueueEntry->save();
            return $this->sendResponse(["queue_number" => $newQueueEntry->queue_number], "Queue generated successfully.");
        } catch (Exception $e) {
            return $this->sendError('Error', [], 500);
        }
    }

    public function getUserQueue($userId)
    {
        $queues = Queue::where(['user_id' => $userId, "is_current" => true])->first();
        return $this->sendResponse($queues, []);
    }

    public function queueReferGeneration(Request $request)
    {
        $res = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'clinic_id' => 'required|exists:clinics,id',
            'department_id' => 'required|exists:departments,id',

        ]);

        foreach ($res->errors()->toArray() as $field => $message) {
            $errors[] = [
                'message' => $message[0],
            ];
        }
        if (isset($errors)) {
            return $this->sendError('Validation Error.', $errors);
        }

        try {
            $clinic = Clinic::find($request->clinic_id);
            $department = Department::find($request->department_id);

            $lastQueue = Queue::where('department_id', $request->department_id)->max('queue_number');
            $lastQueueNumber = (($lastQueue) ? $lastQueue : 0);
            $latestQueuNumber = ($lastQueueNumber + 1);
            $newQueueEntry = new Queue();
            $newQueueEntry->clinic_name = $clinic->name;
            $newQueueEntry->clinic_id = $clinic->id;
            $newQueueEntry->department_id = $department->id;
            $newQueueEntry->department_name = $department->department_name;
            $newQueueEntry->user_id = $request->user_id;
            if ($latestQueuNumber == 1) {
                $newQueueEntry->is_current = true;
            } else {
                $newQueueEntry->is_current = false;
            }
            $newQueueEntry->queue_number = $latestQueuNumber;
            $newQueueEntry->save();
            return $this->sendResponse(["queue_number" => $newQueueEntry->queue_number], "Queue generated successfully.");
        } catch (Exception $e) {
            return $this->sendError('Error', [], 500);
        }
    }
}
