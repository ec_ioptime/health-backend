<?php

namespace App\Http\Controllers\Api\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\ApiResponseTrait;
use App\Models\CHWQuestionScreening;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Mockery\Expectation;

class LoginController extends Controller
{
    use ApiResponseTrait;
    /**
     * Registration
     */
    public function doct()
    {
        return 1;
    }
    public function register(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required|min:4',
                'email' => 'required|email',
                'password' => 'required',
            ]);
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);

            $token = $user->createToken('LaravelAuthApp')->accessToken;

            return $this->sendResponse([
                "token" => $token->token,
                "user" => $user
            ], "User registered successfully.");
        } catch (Expectation $e) {
            return $this->Expection($e, "Error register exception", "error");
        }
    }

    public function login(Request $request)
    {

        Log::info(request()->userAgent());
        if ($request->is_email == "true") {
            $validateUser = Validator::make(
                $request->all(),
                [
                    'email' => 'required|email',
                    'password' => 'required'
                ]
            );

            if ($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            if (!Auth::attempt($request->only(['email', 'password']))) {
                return response()->json([
                    'status' => false,
                    'message' => 'Email & Password does not match with our record.',
                ], 401);
            }

            $user = User::where('email', $request->email)->first();


            if ($user) {
                $token = auth()->user()->createToken('LaravelAuthApp')->plainTextToken;
                $token = explode("|", $token);
                Log::info(auth()->user()->email . " Logged in successfully");

                return response()->json([
                    'message' => 'User Logged In Successfully',
                    "user" => auth()->user(),
                    'user_details' => DB::table('user_details')->where('user_id', Auth::user()->id)->first(),
                    'token' => $token[1]
                ], 200);
            }
        } else {
            $validateUser = Validator::make(
                $request->all(),
                [
                    'phone_number' => 'required',
                    'password' => 'required'
                ]
            );

            if ($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            if (!Auth::attempt($request->only(['phone_number', 'password']))) {
                return response()->json([
                    'status' => false,
                    'message' => 'phone_number & Password does not match with our record.',
                ], 401);
            }

            $user = User::where('phone_number', $request->phone_number)->first();


            if ($user) {
                $token = auth()->user()->createToken('LaravelAuthApp')->plainTextToken;
                $token = explode("|", $token);
                Log::info(auth()->user()->email . " Logged in successfully");

                return response()->json([
                    'message' => 'User Logged In Successfully',
                    "user" => auth()->user(),
                    'user_details' => DB::table('user_details')->where('user_id', Auth::user()->id)->first(),
                    'token' => $token[1]
                ], 200);
            }
        }




        die();

        Log::info(request()->userAgent());
        if ($request->is_email == "true") {
            $res = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required',
            ]);
            if ($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $data = [
                'email' => $request->email,
                'password' => $request->password,
            ];
        } else {
            $validateUser = Validator::make(
                $request->all(),
                [
                    'phone_number' => 'required',
                    'password' => 'required'
                ]
            );

            if ($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            if (!Auth::attempt($request->only(['phone_number', 'password']))) {
                return response()->json([
                    'status' => false,
                    'message' => 'phone_number & Password does not match with our record.',
                ], 401);
            }

            $user = User::where('phone_number', $request->phone_number)->first();


            if ($user) {
                $token = auth()->user()->createToken('LaravelAuthApp')->plainTextToken;
                $token = explode("|", $token);
                Log::info(auth()->user()->email . " Logged in successfully");

                return response()->json([
                    'message' => 'User Logged In Successfully',
                    "user" => auth()->user(),
                    'user_details' => DB::table('user_details')->where('user_id', Auth::user()->id)->first(),
                    'token' => $token[1]
                ], 200);
            }
        }
    }


    public function staffLogin(Request $request)
    {
        Log::info(request()->userAgent());
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        $data = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if (auth()->attempt($data)) {

            if (auth()->user()->deactivated == 1) {
                return $this->sendError('User is deactivated.', [], 401);
            }

            if (auth()->user()->is_staff == 0) {
                return $this->sendError('Invalid Credentials.', [], 401);
            }

            auth()->user()->tokens()->delete();
            $token = auth()->user()->createToken('LaravelAuthApp')->plainTextToken;
            $token = explode("|", $token);
            Log::info(auth()->user()->email . " Logged in successfully");
            return $this->sendResponse(
                [
                    "token" => $token[1],
                    "user" => auth()->user(),
                    'user_details' => DB::table('user_details')->where('user_id', Auth::user()->id)->first(),
                ],
                'Login Successful'
            );
        } else {
            return $this->sendError('Incorrect Credentials.', [], 401);
        }
    }

    public function doctorLogin(Request $request)
    {
        Log::info(request()->userAgent());
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $data = [
            'email' => $request->username,
            'password' => $request->password,
        ];

        if (auth()->attempt($data)) {
            if (auth()->user()->is_doctor == 0 || auth()->user()->is_staff == 1) {
                return $this->sendError('Invalid Credentials.', [], 401);
            }
            if (auth()->user()->is_admin == 0 && auth()->user()->is_doctor == 1 && auth()->user()->is_staff == 0) {
                auth()->user()->tokens()->delete();
                $token = auth()->user()->createToken('LaravelAuthApp')->plainTextToken;
                $token = explode("|", $token);
                Log::info(auth()->user()->email . " Logged in successfully");
                return $this->sendResponse([
                    "token" => $token[1],
                    "user" => auth()->user(),
                    'user_details' => DB::table('user_details')->where('user_id', Auth::user()->id)->first(),
                ], 'Login Successful');
            } else {
                return $this->sendError('Incorrect Credentials.', [], 401);
            }
        } else {
            return $this->sendError('Incorrect Credentials.', [], 401);
        }
    }

    public function chwLogin(Request $request)
    {

        Log::info(request()->userAgent());
        if ($request->is_email == "true") {
            $validateUser = Validator::make(
                $request->all(),
                [
                    'email' => 'required|email',
                    'password' => 'required'
                ]
            );

            $userCheck = User::with('usersdetail')->where('email', $request->email)->where('role_id', '=', '7')->first();

            if (!$userCheck) {
                return $this->sendError('You are Not Allowed for Login', []);
            }


            if ($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            if (!Auth::attempt($request->only(['email', 'password']))) {
                return response()->json([
                    'status' => false,
                    'message' => 'Email & Password does not match with our record.',
                ], 401);
            }

            $user = User::where('email', $request->email)->where('role_id', '7')->first();


            if ($user) {
                $token = auth()->user()->createToken('LaravelAuthApp')->plainTextToken;
                $token = explode("|", $token);
                Log::info(auth()->user()->email . " Logged in successfully");

                return response()->json([
                    'message' => 'User Logged In Successfully',
                    "user" => auth()->user(),
                    'user_details' => DB::table('user_details')->where('user_id', Auth::user()->id)->first(),
                    'token' => $token[1]
                ], 200);
            }
        } else {
            $validateUser = Validator::make(
                $request->all(),
                [
                    'phone_number' => 'required',
                    'password' => 'required'
                ]
            );
            $userCheck = User::with('usersdetail')->where('phone_number', 'like', '%' . $request->phone_number . '%')->where('role_id', '=', '7')->first();
            if (!$userCheck) {
                return $this->sendError('You are Not Allowed for Login', []);
            }


            if ($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            if (!$userCheck || !Hash::check($request->password, $userCheck->password)) {
                return response()->json([
                    'status' => false,
                    'message' => 'Phone number & Password does not match with our record.',
                ], 401);
            }

            $credentials = $request->only('phone_number', 'password');

            if ($credentials) {
                $user = User::with('usersdetail')->where('phone_number', 'like', '%' . $request->phone_number . '%')->where('role_id', '=', '7')->first();
                if ($user) {
                    $token = $user->createToken('Token Name')->plainTextToken;
                    $token = explode("|", $token);
                    Log::info($user->name . " Logged in successfully");

                    return response()->json([
                        'message' => 'User Logged In Successfully',
                        "user" => $user,
                        'user_details' => DB::table('user_details')->where('user_id', $user->id)->first(),
                        'token' => $token[1]
                    ], 200);
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'Phone Number & Password does not match with our record.',
                    ], 401);
                }
            }
        }
    }

    public function PasswordResetCHW(Request $request)
    {
        if ($request->is_email == 'true') {
            $validateUser = Validator::make(
                $request->all(),
                [
                    'email' => 'required',
                    'password' => 'required|confirmed'
                ]
            );

            if ($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }
            $userEmail = User::where('email', $request->email)->where('role_id', '7')->first();
            if ($userEmail) {
                $userEmail->password = bcrypt($request->password);
                $userEmail->pin = $request->password;
                $userEmail->save();
                return $this->sendResponse($userEmail, "Password change sucessfully");
            } else {
                return $this->sendError('Incorrect Credentials.', [], 401);
            }
        } else {
            $validateUser = Validator::make(
                $request->all(),
                [
                    'phone_number' => 'required',
                    'password' => 'required|confirmed'
                ]
            );

            if ($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }
            $userEmail = User::where('phone_number', $request->phone_number)->where('role_id', '7')->first();
            if ($userEmail->dialing_code == $request->countryCode) {
                $userEmail->password = bcrypt($request->password);
                $userEmail->pin = $request->password;
                $userEmail->save();
                return $this->sendResponse($userEmail, "Password change sucessfully");
            } else {
                return $this->sendError('Incorrect Credentials.', [], 401);
            }
        }
    }

    public function PasswordForgetCHW(Request $request)
    {
        if ($request->is_email == 'true') {
            $validateUser = Validator::make(
                $request->all(),
                [
                    'email' => 'required',
                    'password' => 'required|confirmed'
                ]
            );

            if ($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }
            $userEmail = User::with('usersdetail')->where('email', $request->email)->where('role_id', '7')->first();
            if ($userEmail) {
                $userEmail->password = bcrypt($request->password);
                $userEmail->pin = $request->password;
                $userEmail->save();
                return $this->sendResponse($userEmail, "Password change sucessfully");
            } else {
                return $this->sendError('Incorrect Credentials.', [], 401);
            }
        } else {
            $validateUser = Validator::make(
                $request->all(),
                [
                    'phone_number' => 'required',
                    'password' => 'required|confirmed'
                ]
            );

            if ($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }
            $userEmail = User::with('usersdetail')->where('phone_number', $request->phone_number)->where('role_id', '7')->first();
            if ($userEmail->dialing_code == $request->countryCode) {
                $userEmail->password = bcrypt($request->password);
                $userEmail->pin = $request->password;
                $userEmail->save();
                return $this->sendResponse($userEmail, "Password change sucessfully");
            } else {
                return $this->sendError('Incorrect Credentials.', [], 401);
            }
        }
    }

    public function UserCheckCHW(Request $request)
    {
        if ($request->is_email == 'true') {
            $validateUser = Validator::make(
                $request->all(),
                [
                    'email' => 'required',
                ]
            );

            if ($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }
            $userEmail = User::with('usersdetail')->where('email', $request->email)->where('role_id', '7')->first();
            if ($userEmail) {
                return $this->sendResponse($userEmail, "User Exist");
            } else {
                return $this->sendError("User not exists", [], 401);
            }
        } else {
            $validateUser = Validator::make(
                $request->all(),
                [
                    'phone_number' => 'required',
                ]
            );

            if ($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }
            $userEmail = User::with('usersdetail')->where('phone_number', $request->phone_number)->where('role_id', '7')->first();
            if ($userEmail->dialing_code == $request->countryCode) {
                return $this->sendResponse($userEmail, "User Exist");
            } else {
                return $this->sendError("User not exists", [], 401);
            }
        }
    }
}
