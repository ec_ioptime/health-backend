<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponseTrait;
use App\Models\Parking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ManageParkingController extends Controller
{

    use ApiResponseTrait;

    public function addParkingRecord(Request $request)
    {
        $res = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'card_number' => 'required',
            'car_number' => 'required',
        ]);
        foreach ($res->errors()->toArray() as $field => $message) {
            $errors[] = [
                'message' => $message[0],
            ];
        }
        if (isset($errors)) {
            return $this->sendError('Validation Error.', $errors);
        }
        $newRecord = new  Parking();
        $newRecord->user_id = $request->user_id;
        $newRecord->card_number = $request->card_number;
        $newRecord->car_number = $request->car_number;
        $newRecord->save();

        return $this->sendResponse($newRecord, "Record added");
    }

    public function getUserParking($userId)
    {
        $records = Parking::where('user_id', $userId)->get();
        return $this->sendResponse($records, []);
    }
}
