<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Clinic;
use App\Http\Traits\ApiResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ClinicLocationController extends Controller
{
    use ApiResponseTrait;
    public function index($lon, $lat)
    {
        $data = DB::table("clinics")
            ->select(
                "clinics.*",
                DB::raw("6371 * acos(cos(radians(" . $lat . "))
                * cos(radians(clinics.latitude))
                * cos(radians(clinics.longitude) - radians(" . $lon . "))
                + sin(radians(" . $lat . "))
                * sin(radians(clinics.latitude))) AS distance")
            )
            ->having('distance', '<', 50)
            ->orderBy('distance', 'asc')
            ->get();

        // echo $this->distance($lat, $lon, $data[0]->latitude, $data[0]->longitude, "K") . " Miles<br>";
        // echo $this->distance($lat, $lon, $data[1]->latitude, $data[1]->longitude, "K") . " KM<br>";
        // echo $this->distance($lat, $lon, $data[2]->latitude, $data[2]->longitude, "K") . " KM<br>";
        // echo distance(32.9697, -96.80322, 29.46786, -98.53506, "K") . " Kilometers<br>";
        // echo distance(32.9697, -96.80322, 29.46786, -98.53506, "N") . " Nautical Miles<br>";
        if ($data) {
            return $this->sendResponse($data, "Near By Clinics");
        }
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit)
    {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }
}
