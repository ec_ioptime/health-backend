<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponseTrait;
use App\Models\CovidQuestion;
use App\Models\CovidQuestionRecord;
use App\Models\Listest;
use App\Models\PatientCheck;
use App\Models\Test;
use App\Models\User;
use App\Models\VisitPurpose;
use App\Models\VistsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ManageQuestionsController extends Controller
{

    use ApiResponseTrait;

    public function getQuestionsD()
    {
        $questions = CovidQuestion::where('is_patient', '=', '1')
            ->where('category_id', '1')
            ->orwhere('category_id', '2')
            ->orwhere('category_id', '3')
            ->orwhere('category_id', '4')
            ->get();
        return $this->sendResponse($questions, []);
    }
    public function getQuestionsT()
    {

        $questions = CovidQuestion::where('category_id', '1')
            ->orwhere('category_id', '2')
            ->get();
        return $this->sendResponse($questions, []);
    }

    public function getQuestionByCategory()
    {
        $category = request()->category;
        $questions = CovidQuestion::where('category', $category)->get();
        return $this->sendResponse($questions, []);
    }

    public function getUserQuestions($userId)
    {
        $userQuestions = CovidQuestionRecord::where('user_id', $userId)->get();
        return $this->sendResponse($userQuestions, []);
    }

    public function addRecord(Request $request)
    {

        $testsex = Test::where('patient_id', $request->user_id)
            ->where('status', 'pending')
            ->first();

        if ($testsex) {
            return $this->sendResponse($testsex, "Questionnaire Exists");
        }
        $res = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'covid_question_id' => 'required|exists:covid_questions,id',
            "answer" => 'required',
        ]);
        foreach ($res->errors()->toArray() as $field => $message) {
            $errors[] = [
                'message' => $message[0],
            ];
        }
        if (isset($errors)) {
            return $this->sendError('Validation Error.', $errors);
        }
        $newRecord = new CovidQuestionRecord();
        $newRecord->user_id = $request->user_id;
        $newRecord->covid_question_id = $request->covid_question_id;
        $covidquestions = CovidQuestion::where('id', $request->covid_question_id)->first();
        $newRecord->options = $request->options;
        $newRecord->answer = $request->answer;
        $newRecord->category_id = $covidquestions->category_id;
        $newRecord->category = $covidquestions->category;
        $newRecord->save();

        $test = Listest::where('category_id', $covidquestions->category_id)->first();

        $tests  = new Test();
        $tests->name = $test->name;
        $tests->patient_id = $request->user_id;
        $tests->patient_id = $request->user_id;
        $tests->is_screening = 1;
        $tests->category_id = $test->category_id;
        $tests->category = $test->category_name;
        $tests->status = "pending";
        $tests->save();


        $testlst = Test::where('patient_id', $request->user_id)->where('status', 'pending')->get();
        return $this->sendResponse($testlst, "Questionnaire complete and assign test");
    }

    public function addMultipleRecordN(Request $request)
    {
        $res = Validator::make($request->all(), [
            'user_id' => 'required',
            'question_id' => 'required',
            'answer' => 'required',

        ]);
        foreach ($res->errors()->toArray() as $field => $message) {
            $errors[] = [
                'message' => $message[0],
            ];
        }
        if (isset($errors)) {
            return $this->sendError('Validation Error.', $errors);
        }

        try {
            $testsex = Test::where('patient_id', $request->user_id)
                ->where('status', 'pending')
                ->where('nurse_id', '=', null)
                ->first();

            if ($testsex) {
                $testlst = Test::where('patient_id', $request->user_id)
                    ->where('status', 'pending')
                    ->where('nurse_id', '=', null)
                    ->get();
                return $this->sendResponse($testlst, "Questionnaire Exists");
            }

            foreach ($request->question_id as $key => $value) {
                $newRecord = new CovidQuestionRecord();
                $newRecord->covid_question_id = $request->question_id[$key];
                $newRecord->user_id = $request->user_id;
                $newRecord->answer = $request->answer[$key];
                $newRecord->options = $request->options[$key];
                $newRecord->date = Date('Y-m-d');
                $questions = CovidQuestion::where('id', $request->question_id[$key])->first();
                $newRecord->category_id = $questions->category_id;
                $newRecord->category = $questions->category;
                $newRecord->save();
            }

            $testlst = Test::where('patient_id', $request->user_id)->where('status', 'pending')->get();
            return $this->sendResponse($testlst, "Questionnaire complete");
        } catch (\Exception $th) {
            return $this->sendError($th->getMessage());
        }
    }

    public function addMultipleRecord(Request $request)
    {
        $res = Validator::make($request->all(), [
            'user_id' => 'required',
            'question_id' => 'required',
            'answer' => 'required',
            'purpose' => 'required',
            'nurse_id' => 'required',
        ]);

        foreach ($res->errors()->toArray() as $field => $message) {
            $errors[] = [
                'message' => $message[0],
            ];
        }
        if (isset($errors)) {
            return $this->sendError('Validation Error.', $errors);
        }

        try {

            $userNurse = User::where('id', $request->nurse_id)->first();

            $patientcheckinexk = PatientCheck::where('department_id', $userNurse->department_id)->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('patient_id', $request->user_id)->first();

            if ($patientcheckinexk == null) {
                return $this->sendError('Checkin first', []);
            }


            if ($request->purpose) {
                $vistP = new VisitPurpose();
                $use1 = User::where('id', $request->nurse_id)->first();
                $vistP->patient_id = $request->user_id;
                $vistP->nurse_id = $request->nurse_id;
                $vistP->purpose = $request->purpose;
                $vistP->visit_id =  $request->visit_id;
                $vistP->doc_name = $use1->name;
                $vistP->save();
            }

            foreach ($request->question_id as $key => $value) {
                $newRecord = new CovidQuestionRecord();
                $newRecord->covid_question_id = $request->question_id[$key];
                $newRecord->user_id = $request->user_id;
                $newRecord->nurse_id = $request->nurse_id;
                $newRecord->answer = $request->answer[$key];
                $newRecord->options = $request->options[$key];
                $newRecord->date = Date('Y-m-d');
                $questions = CovidQuestion::where('id', $request->question_id[$key])->first();
                $newRecord->category_id = $questions->category_id;
                $newRecord->category = $questions->category;
                $newRecord->visit_id = $request->id;
                $newRecord->save();
            }
            return $this->testAssign($request, $request->id);
        } catch (\Exception $th) {
            return $this->sendError($th->getMessage());
        }
    }


    public function testAssign($request, $vistsModel)
    {
        $questRecords = CovidQuestionRecord::where('user_id', $request->user_id)
            ->where('answer', '=', 'yes')
            ->where('visit_id', $request->visit_id)
            ->get()
            ->unique('category');

        foreach ($questRecords as $index => $items) {
            $test = DB::table('testcategory')->where('id', $items->category_id)->first();
            $tests  = new Test();
            $tests->name = $test->name;
            $tests->patient_id = $request->user_id;
            $tests->is_screening = 1;
            $tests->category_id = $test->id;
            $tests->category = $test->name;
            $tests->status = "pending";
            $tests->nurse_id = $request->nurse_id;
            $tests->visit_id =  $vistsModel;
            $tests->save();
        }

        $testlst = Test::where('patient_id', $request->user_id)->where('visit_id', $request->visit_id)->where('status', 'pending')->get();
        $visit = VistsModel::where('id', $vistsModel)->first();

        return response()->json(
            [
                'success' => true,
                'data' => [
                    'visit_id' => $visit,
                    'test_list' => $testlst,
                ],
                'message' => "Questionnaire complete and assign test",
            ]
        );
    }
}
