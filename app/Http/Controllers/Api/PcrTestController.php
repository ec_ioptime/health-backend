<?php

namespace App\Http\Controllers\Api;

use App\Models\PcrTest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponseTrait;
use Illuminate\Support\Facades\Validator;

class PcrTestController extends Controller
{
    use ApiResponseTrait;


    public function addPcrRecord(Request $request)
    {
          $res = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'user_name' => 'required',
        ]);

        foreach ($res->errors()->toArray() as $field => $message) {
            $errors[] = [
                'message' => $message[0],
            ];
        }
        if (isset($errors)) {
            return $this->sendError('Validation Error.', $errors);
        }

        $newRecord = new  PcrTest();
        $newRecord->user_id = $request->user_id;
        $newRecord->user_name = $request->user_name;
        $newRecord->pcr_status = "pending";
        $newRecord->save();

        return $this->sendResponse($newRecord, "Record added");
    }

    public function getPcrRecordForUser($userId)
    {
        $records = PcrTest::where('user_id', $userId)->first();
        return $this->sendResponse($records, []);
    }
}
