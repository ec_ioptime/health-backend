<?php

namespace App\Http\Controllers\Api;

use App\Models\Referral;
use App\Models\VitalRecord;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponseTrait;
use App\Models\VistsModel;

class ManageReferralController extends Controller
{
    use ApiResponseTrait;

    public function referRecord(Request $request, $recordId)
    {
        if ($request->visit_id) {
            $visit = VistsModel::where('id', $request->visit_id)->first();
            if (!$visit) {
                return $this->sendError("Visits not found");
            } else {

                $referral = new Referral();
                $referral->visit_id = $request->visit_id;
                $referral->patient_id = $request->patient_id;
                $referral->referred_by = $request->referred_by;
                $referral->referred_to = $request->referred_to;
                $referral->remarks = $request->remarks;
                $referral->status = $request->status;
                $referral->save();

                $referral = Referral::with(["referredBy", "referredTo"])->where('id', $referral->id)->get();
                return $this->sendResponseData($referral, "Record Reffered Successfully.");
            }
        }
    }
}
