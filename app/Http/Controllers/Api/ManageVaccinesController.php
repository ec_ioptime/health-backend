<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\VaccinationType;
use App\Models\VaccinationCenter;
use App\Models\VaccinationRecord;
use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponseTrait;
use Illuminate\Support\Facades\Validator;

class ManageVaccinesController extends Controller
{
    use ApiResponseTrait;
     public function getVaccineType()
    {
        $types = VaccinationType::all();
        return $this->sendResponse($types,[]);
    }

    public function getVaccinationCenter()
    {
        $centers = VaccinationCenter::all();
        return $this->sendResponse($centers,[]);
    }


    public function deleteVaccineRecord($userId)
    {
        $user = User::find($userId);
        $user->vaccination_records()->delete();
        $success = "sucess";
        return $this->sendResponse($success, "Records deleted");
    }


     public function addVaccinationRecord(Request $request)
    {
        $res = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'vaccination_center_id' => 'required|exists:vaccination_centers,id',
            'vaccination_type_id' => 'required|exists:vaccination_types,id',
            "total_doses" => 'required',
            "iot_number" => 'required',
        ]);
        foreach ($res->errors()->toArray() as $field => $message) {
            $errors[] = [
                'message' => $message[0],
            ];
        }
        if (isset($errors)) {
            return $this->sendError('Validation Error.', $errors);
        }
        $newRecord = new  VaccinationRecord();
        $newRecord->user_id = $request->user_id;
        $newRecord->vaccination_center_id = $request->vaccination_center_id;
        $newRecord->vaccination_type_id = $request->vaccination_type_id;
        $newRecord->total_doses = $request->total_doses;
        $newRecord->iot_number = $request->iot_number;
        $newRecord->save();

        return $this->sendResponse($newRecord, []);
    }

    public function getVaccinationRecordForUser($userId)
    {
        $record = VaccinationRecord::where('user_id', $userId)->get();
        return $this->sendResponse($record, []);

    }

    public function updateVaccinationRecord(Request $request, $recordId)
    {
        $record = VaccinationRecord::find($recordId);
        if(!$record){
            return $this->sendError('Record not found', []);
        }
      $res = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'vaccination_center_id' => 'required|exists:vaccination_centers,id',
            'vaccination_type_id' => 'required|exists:vaccination_types,id',
            "total_doses" => 'required',
            "iot_number" => 'required',
        ]);
        foreach ($res->errors()->toArray() as $field => $message) {
            $errors[] = [
                'message' => $message[0],
            ];
        }
        if (isset($errors)) {
            return $this->sendError('Validation Error.', $errors);
        }
        $record->user_id = $request->user_id;
        $record->vaccination_center_id = $request->vaccination_center_id;
        $record->vaccination_type_id = $request->vaccination_type_id;
        $record->total_doses = $request->total_doses;
        $record->iot_number = $request->iot_number;
        $record->save();
        return $this->sendResponse($record, "Record Updated Successfully");
    }
}
