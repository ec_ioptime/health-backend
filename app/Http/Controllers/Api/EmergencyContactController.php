<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\EmergencyContact;
use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponseTrait;
use Illuminate\Support\Facades\Validator;

class EmergencyContactController extends Controller
{

    use ApiResponseTrait;

    public function addEmergencyContact(Request $request)
    {

          $res = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'last_name' => 'required',
            'phone_number' => 'required',
            'first_name' => 'required',
            'dialing_code' => 'required',
            "country_id" => 'required',
        ]);
        foreach ($res->errors()->toArray() as $field => $message) {
            $errors[] = [
                'message' => $message[0],
            ];
        }
        if (isset($errors)) {
            return $this->sendError('Validation Error.', $errors);
        }
        $contact = new  EmergencyContact();
        $contact->first_name = $request->first_name;
        $contact->last_name = $request->last_name;
        $contact->phone_number = $request->phone_number;
        $contact->dialing_code = $request->dialing_code;
        $contact->country_id = $request->country_id;
        $contact->user_id = $request->user_id;
        $contact->save();
        return $this->sendResponse($contact, []);

    }

    public function updateContact(Request $request, $id)
    {
          $res = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'last_name' => 'required',
            'phone_number' => 'required',
            'first_name' => 'required',
            'dialing_code' => 'required',
            "country_id" => 'required',
        ]);
        foreach ($res->errors()->toArray() as $field => $message) {
            $errors[] = [
                'message' => $message[0],
            ];
        }
        if (isset($errors)) {
            return $this->sendError('Validation Error.', $errors);
        }
        $contact = EmergencyContact::find($id);
        if(!$contact){
            return $this->sendError('Contact not found.');
        }
            $contact->first_name = $request->first_name;
        $contact->last_name = $request->last_name;
        $contact->phone_number = $request->phone_number;
        $contact->dialing_code = $request->dialing_code;
        $contact->country_id = $request->country_id;
        $contact->user_id = $request->user_id;
        $contact->save();

        return $this->sendResponse($contact, "Contact updated successfully.");
    }
}
