<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Wallet;
use App\Models\UserDetail;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use App\Http\Traits\ApiResponseTrait;
use App\Models\Roles;
use Aws\Rekognition\RekognitionClient;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserRegisterController extends Controller
{
    use ApiResponseTrait;
    public function register(Request $request)
    {

        Log::info(request()->userAgent());
        $res = Validator::make($request->all(), [
            'name' => 'required|string',
            'surname' => 'required|string',
            'email' => 'required|string|email|unique:users|max:255',
            'pin' => 'required|string',
            'card' => 'required|string',
            'parent_user' => 'required|string',
            'date_of_birth' => 'required|string',
            'identity_type' => 'required|string',
            'document_id' => 'required|string',
            'gender' => 'required|string',
            'phone_number' => 'required',
            'country_id' => 'required|string',
            'dialing_code' => 'required|string',
            'document_type' => 'required|string',
            'VaccineVerified' => 'required',
            'next_of_kin_country_id' => 'required',
            'home_address_1' => 'required',
            'home_address_2' => 'required',
            'issue_state_country_id' => 'required',
            'personal_detail_country_id' => 'required',
            'age' => 'required|string',
        ]);
        $date_of_birth =  str_replace("-", "", $request->date_of_birth);
        $users = UserDetail::where('date_of_birth', '=', $request->date_of_birth)->get();
        if ($users) {
            $count = $users->count() + 1;
            $patient_id = 'ADG-' . $date_of_birth . '-' . $count;
        } else {
            $patient_id = 'ADG-' . $date_of_birth . '-' . '1';
        }
        foreach ($res->errors()->toArray() as $field => $message) {
            $errors[] = [
                'message' => $message[0],
            ];
        }
        if (isset($errors)) {
            return $this->sendError('Validation Error.', $errors);
        }
        $user = new User();
        $user->name = $request->name;
        $user->pin = $request->pin;
        $user->card = $request->card;
        $user->is_social_login = $request->is_social_login;
        $user->parent_user = $request->parent_user;

        if ($request->is_admin) {
            $user->is_admin = "1";
        } else if ($request->is_doctor) {
            $user->is_doctor = "1";
            $user->experience = $request->experience;
            $user->fee = $request->fee;
            $user->degree = $request->degree;
            $user->doctor_category_id = $request->doctor_category_id;
        } else if ($request->is_staff) {
            $user->is_staff = "1";
        }

        if (isset($request->face_id)) {
            $user->face_id = $request->face_id;
        }
        if (isset($request->email)) {
            $user->email = $request->email;
        }
        $user->dialing_code = $request->dialing_code;
        $user->phone_number = $request->dialing_code . $request->phone_number;
        if (isset($request->profile_pic)) {
            $base6412 = urldecode($request->profile_pic);
            $user->profile_pic = $base6412;
        }
        $user->password = bcrypt($request->pin);
        $user->role_id = $request->role_id;
        $user->clinic_id = $request->clinic_id;
        $user->save();


        $userDetail = new UserDetail();
        $userDetail->user_id = $user->id;
        $userDetail->surname = $request->surname;
        $userDetail->age = $request->age;
        $userDetail->patient_id = $patient_id;
        $userDetail->gender = $request->gender;
        $userDetail->identity_type = $request->identity_type;
        $userDetail->country_id = $request->country_id;
        $userDetail->dialing_code = $request->dialing_code;
        $userDetail->phone_number = $request->dialing_code . $request->phone_number;
        $userDetail->date_of_birth = $request->date_of_birth;
        $userDetail->home_address_1 = $request->home_address_1;
        $userDetail->home_address_2 = $request->home_address_2;
        $userDetail->issue_state_country_id = $request->issue_state_country_id;
        $userDetail->document_type = $request->document_type;
        $userDetail->document_id = $request->document_id;
        if (isset($request->expiry_date)) {
            $userDetail->expiry_date = $request->expiry_date;
        }
        if (isset($request->language)) {
            $userDetail->language = $request->language;
        }
        if (isset($request->race)) {
            $userDetail->race = $request->race;
        }
        if (isset($request->nationality)) {
            $userDetail->nationality = $request->nationality;
        }

        if (isset($request->department_id)) {
            $userDetail->department_id = $request->department_id;
        }
        if (isset($request->personal_detail_country_id)) {
            $userDetail->personal_detail_country_id = $request->personal_detail_country_id;
        }
        if (isset($request->next_of_kin_country_id)) {
            $userDetail->next_of_kin_country_id = $request->next_of_kin_country_id;
        }

        if (isset($request->clinic_id)) {
            $userDetail->clinic_id = $request->clinic_id;
        }


        if (isset($request->occupation_type)) {
            $userDetail->occupation_type = $request->occupation_type;
        }
        if (isset($request->martial_status)) {
            $userDetail->martial_status = $request->martial_status;
        }
        if (isset($request->home_address_3)) {
            $userDetail->home_address_3 = $request->home_address_3;
        }
        if (isset($request->designation)) {
            $userDetail->designation = $request->designation;
        }
        if (isset($request->employer_name)) {
            $userDetail->employer_name = $request->employer_name;
        }
        if (isset($request->employer_sur_name)) {
            $userDetail->employer_sur_name = $request->employer_sur_name;
        }
        if (isset($request->employer_country_id)) {
            $userDetail->employer_country_id = $request->employer_country_id;
        }
        if (isset($request->work_number)) {
            $userDetail->work_number = $request->work_number;
        }
        if (isset($request->work_address)) {
            $userDetail->work_address = $request->work_address;
        }
        if (isset($request->next_of_kin)) {
            $userDetail->next_of_kin = $request->next_of_kin;
        }
        if (isset($request->kin_surname)) {
            $userDetail->kin_surname = $request->kin_surname;
        }

        if (isset($request->kinRelation)) {
            $userDetail->kinRelation = $request->kinRelation;
        }
        if (isset($request->next_of_kin_number)) {
            $userDetail->next_of_kin_number = $request->next_of_kin_number;
        }

        if (!is_null($request->covidTestId)) {
            $userDetail->covidTestId = $request->covidTestId;
        }
        if (isset($request->longitude)) {
            $userDetail->longitude = $request->longitude;
        }
        if (isset($request->latitude)) {
            $userDetail->latitude = $request->latitude;
        }

        $userDetail->VaccineVerified = $request->VaccineVerified;
        $userDetail->save();
        Log::info($user->name . " Registered in successfully");
        $wallet = new Wallet();
        $wallet->user_id = $user->id;
        $wallet->balance = 0;
        $wallet->wallet_last_change = 0;
        $wallet->save();
        $token = $user->createToken('LaravelAuthApp')->plainTextToken;
        $token = explode("|", $token);
        try {
            $base64 = null;
            try {
                if (isset($request->profile_pic)) {
                    $imageUrl = urldecode($request->profile_pic);
                    // $image = $this->convertAndSaveImage($imageUrl);
                    $imageData = file_get_contents($imageUrl);
                    // Encode the image data to Base64
                    $base64 = base64_encode($imageData);
                    $base64 = stripslashes($base64);
                    //  $base64 = urldecode($request->profile_pic);
                    $base64 = preg_replace("/\r|\n/", "", html_entity_decode($base64));
                }
            } catch (\Throwable $th) {
                //throw $th;
                Log::info("Error loading image, setting it to null.");
                $base64 = null;
            }


            $urlAddAcess = "http://3.233.102.40:8098/api/person/add?access_token=26E02472AF49D20271778A33B2310373";
            if ($request->gender == 'male' || $request->gender == 'Male') {
                $gender = 'M';
            } else if ($request->gender == 'female' || $request->gender == 'Female') {
                $gender = 'F';
            }
            $body = array(
                "pin" => $user->id,
                "name" => $user->name,
                "lastName" => $userDetail->surname,
                "email" =>  $user->email,
                "gender" =>   $gender,
                "mobilePhone" =>  $user->phone_number,
                "deptCode" => 237,
                "cardNo" => $request->cardNo,
                "certNumber" => $request->certNumber,
                "certType" => $request->certType,
                "personPhoto" => $base64,
                "supplyCards" => $request->supplyCards,
            );
            Log::info($user->name . " added on dashboard, Adding User on ZK server");

            $response = Http::post($urlAddAcess, $body);
            $RES = json_decode($response->getBody(), true);
            Log::info($RES);

            if ($RES['code'] == 0) {
                Log::info("New User " . $user->email . " registered.");
                $token = $user->createToken('LaravelAuthApp')->plainTextToken;
                $token = explode("|", $token);
                $role = Roles::where('id', $user->role_id)->first();
                return $this->sendResponse(["token" => $token[1], "user" => $user, "detail" => $user->detail, 'Role' => $role->name], 'User registered successfully.');
            } else {
                Log::info($user->name . " registration failed.");
                $user->delete();
                $errors[] = [
                    'message' => $RES['message'],
                ];
                return $this->sendError('Error While Adding User.', $errors);
            }
        } catch (\Exception $th) {
            $user->delete();
            $message = $th->getMessage();
            if ($message == 'Undefined offset: 1') {
                $message = 'Write Full Name.';
            }
            $errors[] = [
                'message' => $message,
            ];
            return $this->sendError('Error While Adding User.',  $message);
        }
    }
    /* search face in users dashboard. */

    public function staffupdate(Request $request,$id){
        Log::info(request()->userAgent());
        $res = Validator::make($request->all(), [
            'name' => 'required|string',
            'dialing_code' => 'required|string',
            'document_type' => 'required|string',
            'card' => 'required|string',
            'parent_user' => 'required|string',
            'identity_type' => 'required|string',
            'document_id' => 'required|string',
            'gender' => 'required|string',
            'phone_number' => 'required',
            'country_id' => 'required|string',
            'VaccineVerified' => 'required',
            'next_of_kin_country_id' => 'required',
            'issue_state_country_id' => 'required',
            'personal_detail_country_id' => 'required',
        ]);
        foreach ($res->errors()->toArray() as $field => $message) {
            $errors[] = [
                'message' => $message[0],
            ];
        }
        if (isset($errors)) {
            return $this->sendError('Validation Error.', $errors);
        }

        $user = User::find($id);
        if (!is_null($request->name)) {
            $user->name = $request->name;
        }
        $user->card = $request->card;

        if (isset($request->profile_pic)) {
            $user->profile_pic = $request->profile_pic;
        }
        if (isset($request->email)) {
            $user->email = $request->email;
        }
        if (isset($request->clinic_id)) {
            $user->clinic_id = $request->clinic_id;
        }
        $user->dialing_code =  $request->dialing_code;
        $user->phone_number = $request->dialing_code . $request->phone_number;
        $user->save();
        $userDetail = UserDetail::where('user_id', $user->id)->first();
        $userDetail->document_id = $request->document_id;
        $userDetail->surname = $request->surname;
        $userDetail->age = $request->age;
        $userDetail->gender = $request->gender;
        $userDetail->country_id = $request->country_id;
        $userDetail->dialing_code = $request->dialing_code;
        $userDetail->phone_number = $request->dialing_code . $request->phone_number;
        $userDetail->date_of_birth = $request->date_of_birth;
        $userDetail->home_address_1 = $request->home_address_1;
        $userDetail->home_address_2 = $request->home_address_2;
        $userDetail->next_of_kin_country_id = $request->next_of_kin_country_id;
        $userDetail->issue_state_country_id = $request->issue_state_country_id;
        $userDetail->document_type = $request->document_type;
        if (isset($request->expiry_date)) {
            $userDetail->expiry_date = $request->expiry_date;
        }
        if (isset($request->clinic_id)) {
            $userDetail->clinic_id = $request->clinic_id;
        }
        if (isset($request->department_id)) {
            $userDetail->department_id = $request->department_id;
        }
        if (isset($request->language)) {
            $userDetail->language = $request->language;
        }
        if (isset($request->martial_status)) {
            $userDetail->martial_status = $request->martial_status;
        }
        if (isset($request->home_address_3)) {
            $userDetail->home_address_3 = $request->home_address_3;
        }
        if (isset($request->race)) {
            $userDetail->race = $request->race;
        }
        if (isset($request->nationality)) {
            $userDetail->nationality = $request->nationality;
        }
        if (isset($request->personal_detail_country_id)) {
            $userDetail->personal_detail_country_id = $request->personal_detail_country_id;
        }
        if (isset($request->next_of_kin_country_id)) {
            $userDetail->next_of_kin_country_id = $request->next_of_kin_country_id;
        }
        if (isset($request->occupation_type)) {
            $userDetail->occupation_type = $request->occupation_type;
        }
        if (isset($request->designation)) {
            $userDetail->designation = $request->designation;
        }
        if (isset($request->employer_name)) {
            $userDetail->employer_name = $request->employer_name;
        }
        if (isset($request->employer_sur_name)) {
            $userDetail->employer_sur_name = $request->employer_sur_name;
        }
        if (isset($request->employer_country_id)) {
            $userDetail->employer_country_id = $request->employer_country_id;
        }
        if (isset($request->work_number)) {
            $userDetail->work_number = $request->work_number;
        }
        if (isset($request->work_address)) {
            $userDetail->work_address = $request->work_address;
        }
        if (isset($request->next_of_kin)) {
            $userDetail->next_of_kin = $request->next_of_kin;
        }
        if (isset($request->kin_surname)) {
            $userDetail->kin_surname = $request->kin_surname;
        }
        if (isset($request->longitude)) {
            $userDetail->longitude = $request->longitude;
        }
        if (isset($request->latitude)) {
            $userDetail->latitude = $request->latitude;
        }

        if (isset($request->kinRelation)) {
            $userDetail->kinRelation = $request->kinRelation;
        }
        if (isset($request->next_of_kin_number)) {
            $userDetail->next_of_kin_number = $request->next_of_kin_number;
        }

        if (!is_null($request->covidTestId)) {
            $userDetail->covidTestId = $request->covidTestId;
        }
        $userDetail->VaccineVerified = $request->VaccineVerified;

        $userDetail->save();
        Log::info($user->name . " updated in successfully");
        return $this->sendResponse(["user" => $user, "detail" => $user->detail], 'User updated successfully.');
    }

    public function update(Request $request, $id)
    {
        Log::info(request()->userAgent());
        $res = Validator::make($request->all(), [
            'name' => 'required|string',
            'dialing_code' => 'required|string',
            'document_type' => 'required|string',
            'card' => 'required|string',
            'parent_user' => 'required|string',
            'date_of_birth' => 'required|string',
            'identity_type' => 'required|string',
            'document_id' => 'required|string',
            'gender' => 'required|string',
            'phone_number' => 'required',
            'country_id' => 'required|string',
            'VaccineVerified' => 'required',
            'next_of_kin_country_id' => 'required',
            'home_address_1' => 'required',
            'home_address_2' => 'required',
            'issue_state_country_id' => 'required',
            'personal_detail_country_id' => 'required',
            'age' => 'required|string',
        ]);
        foreach ($res->errors()->toArray() as $field => $message) {
            $errors[] = [
                'message' => $message[0],
            ];
        }
        if (isset($errors)) {
            return $this->sendError('Validation Error.', $errors);
        }

        $user = User::find($id);
        if (!is_null($request->name)) {
            $user->name = $request->name;
        }
        $user->card = $request->card;

        if (isset($request->profile_pic)) {
            $user->profile_pic = $request->profile_pic;
        }
        if (isset($request->email)) {
            $user->email = $request->email;
        }
        if (isset($request->clinic_id)) {
            $user->clinic_id = $request->clinic_id;
        }
        $user->dialing_code =  $request->dialing_code;
        $user->phone_number = $request->dialing_code . $request->phone_number;
        $user->save();
        $userDetail = UserDetail::where('user_id', $user->id)->first();
        $userDetail->document_id = $request->document_id;
        $userDetail->surname = $request->surname;
        $userDetail->age = $request->age;
        $userDetail->gender = $request->gender;
        $userDetail->country_id = $request->country_id;
        $userDetail->dialing_code = $request->dialing_code;
        $userDetail->phone_number = $request->dialing_code . $request->phone_number;
        $userDetail->date_of_birth = $request->date_of_birth;
        $userDetail->home_address_1 = $request->home_address_1;
        $userDetail->home_address_2 = $request->home_address_2;
        $userDetail->next_of_kin_country_id = $request->next_of_kin_country_id;
        $userDetail->issue_state_country_id = $request->issue_state_country_id;
        $userDetail->document_type = $request->document_type;
        if (isset($request->expiry_date)) {
            $userDetail->expiry_date = $request->expiry_date;
        }
        if (isset($request->clinic_id)) {
            $userDetail->clinic_id = $request->clinic_id;
        }
        if (isset($request->department_id)) {
            $userDetail->department_id = $request->department_id;
        }
        if (isset($request->language)) {
            $userDetail->language = $request->language;
        }
        if (isset($request->martial_status)) {
            $userDetail->martial_status = $request->martial_status;
        }
        if (isset($request->home_address_3)) {
            $userDetail->home_address_3 = $request->home_address_3;
        }
        if (isset($request->race)) {
            $userDetail->race = $request->race;
        }
        if (isset($request->nationality)) {
            $userDetail->nationality = $request->nationality;
        }
        if (isset($request->personal_detail_country_id)) {
            $userDetail->personal_detail_country_id = $request->personal_detail_country_id;
        }
        if (isset($request->next_of_kin_country_id)) {
            $userDetail->next_of_kin_country_id = $request->next_of_kin_country_id;
        }
        if (isset($request->occupation_type)) {
            $userDetail->occupation_type = $request->occupation_type;
        }
        if (isset($request->designation)) {
            $userDetail->designation = $request->designation;
        }
        if (isset($request->employer_name)) {
            $userDetail->employer_name = $request->employer_name;
        }
        if (isset($request->employer_sur_name)) {
            $userDetail->employer_sur_name = $request->employer_sur_name;
        }
        if (isset($request->employer_country_id)) {
            $userDetail->employer_country_id = $request->employer_country_id;
        }
        if (isset($request->work_number)) {
            $userDetail->work_number = $request->work_number;
        }
        if (isset($request->work_address)) {
            $userDetail->work_address = $request->work_address;
        }
        if (isset($request->next_of_kin)) {
            $userDetail->next_of_kin = $request->next_of_kin;
        }
        if (isset($request->kin_surname)) {
            $userDetail->kin_surname = $request->kin_surname;
        }
        if (isset($request->longitude)) {
            $userDetail->longitude = $request->longitude;
        }
        if (isset($request->latitude)) {
            $userDetail->latitude = $request->latitude;
        }

        if (isset($request->kinRelation)) {
            $userDetail->kinRelation = $request->kinRelation;
        }
        if (isset($request->next_of_kin_number)) {
            $userDetail->next_of_kin_number = $request->next_of_kin_number;
        }

        if (!is_null($request->covidTestId)) {
            $userDetail->covidTestId = $request->covidTestId;
        }
        $userDetail->VaccineVerified = $request->VaccineVerified;

        $userDetail->save();
        Log::info($user->name . " updated in successfully");
        return $this->sendResponse(["user" => $user, "detail" => $user->detail], 'User updated successfully.');
    }

    public function updatePin(Request $request)
    {
        Log::info(request()->userAgent());
        $res = Validator::make($request->all(), [
            'pin' => 'required|string',
        ]);
        foreach ($res->errors()->toArray() as $field => $message) {
            $errors[] = [
                'message' => $message[0],
            ];
        }
        if (isset($errors)) {
            return $this->sendError('Validation Error.', $errors);
        }

        $user = User::find($request->user_id);
        $user->pin = $request->pin;
        $user->password = bcrypt($user->pin);
        $user->save();
        Log::info($user->name . " Pin updated successfully.");
        return $this->sendResponse([], 'Pin updated successfully.');
    }

    public function getUser($userId)
    {
        $user = User::where(['id' => $userId, 'deactivated' => 0])->with('usersdetail')->first();
        return $this->sendResponse($user, 'User fetched successfully.');
    }
}
