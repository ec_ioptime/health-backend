<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponseTrait;
use App\Models\PatientCheck;
use App\Models\User;
use App\Models\VitalRecord;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use function PHPUnit\Framework\returnCallback;

class ManageVitalRecordsController extends Controller
{

    use ApiResponseTrait;

    public function addVitalRecord(Request $request)
    {
        $res = Validator::make($request->all(), [
            'symptoms' => 'required',
            'nurse_id' => 'required|exists:users,id',
            'patient_id' => 'required|exists:users,id',
            'value' => 'required',
            'unit' => 'required',
            'desciption' => 'required',

        ]);
        foreach ($res->errors()->toArray() as $field => $message) {
            $errors[] = [
                'message' => $message[0],
            ];
        }
        if (isset($errors)) {
            return $this->sendError('Validation Error.', $errors);
        }

        $userNurse = User::where('id', $request->nurse_id)->first();
        $patientcheckinexk = PatientCheck::where('department_id', $userNurse->department_id)->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('date', date('Y-m-d'))->where('patient_id', $request->patient_id)->first();
        $patientcheckinexk2 = PatientCheck::where('department_id', $userNurse->department_id)->where('visit_id', $request->visit_id)->where('status', 'checkout')->where('date', date('Y-m-d'))->where('patient_id', $request->patient_id)->first();
        if ($request->is_departmentRefer) {
            if ($patientcheckinexk2) {
            } else {
                if ($patientcheckinexk == null) {
                    return $this->sendError('Checkin first', []);
                }
            }
        }

        if (isset($request->isDeleted)) {
            if ($request->isDeleted == '1') {
                foreach ($request->deletedQuestionIdsArray as $key => $value) {
                    $record = VitalRecord::where('id', $request->deletedQuestionIdsArray[$key])->delete();
                }
            }
        }

        if (isset($request->isEdit)) {
            if ($request->isEdit == '1') {
                $record = VitalRecord::whereIn("questionId", $request->questionId)
                    ->where('visit_id', $request->visit_id)
                    ->get();
                $existingQuestionIds = $record->pluck('questionId')->toArray();

                $commonElements = array_intersect($existingQuestionIds, $request->questionId);
                $notInArray = array_diff($request->questionId, $existingQuestionIds);

                if (!empty($commonElements)) {
                    foreach ($commonElements as $commonElement) {
                        $recordToUpdate = $record->where('questionId', $commonElement)->first();
                        $key = array_search($commonElement, $request->questionId);
                        $recordToUpdate->symptoms = $request->symptoms;
                        $recordToUpdate->nurse_id = $request->nurse_id;
                        $recordToUpdate->patient_id = $request->patient_id;
                        $recordToUpdate->status = $request->status;
                        $recordToUpdate->desciption = $request->description[$key] ?? "";
                        $recordToUpdate->value = $request->value[$key] ?? "";
                        $recordToUpdate->unit = $request->unit[$key] ?? "";
                        $recordToUpdate->visit_id = $request->visit_id;

                        if ($request->is_departmentRefer) {
                            $recordToUpdate->is_departmentRefer = $request->is_departmentRefer;
                        }
                        if ($request->isCHW) {
                            $recordToUpdate->isCHW = $request->isCHW;
                        }
                        $recordToUpdate->save();
                    }
                }

                if (!empty($notInArray)) {
                    foreach ($notInArray as $notInArrayElement) {
                        $recordToInsert = new VitalRecord();
                        $key = array_search($notInArrayElement, $request->questionId);
                        $recordToInsert->symptoms = $request->symptoms;
                        $recordToInsert->nurse_id = $request->nurse_id;
                        $recordToInsert->patient_id = $request->patient_id;
                        $recordToInsert->status = $request->status;
                        $recordToInsert->desciption = $request->description[$key] ?? "";
                        $recordToInsert->value = $request->value[$key] ?? "";
                        $recordToInsert->unit = $request->unit[$key] ?? "";
                        $recordToInsert->visit_id = $request->visit_id;
                        $recordToInsert->questionId = $notInArrayElement;

                        if ($request->is_departmentRefer) {
                            $recordToInsert->is_departmentRefer = $request->is_departmentRefer;
                        }
                        if ($request->isCHW) {
                            $recordToInsert->isCHW = $request->isCHW;
                        }
                        $recordToInsert->save();
                    }
                }
            } else if ($request->isEdit == '0') {
                foreach ($request->questionId as $key => $index) {
                    $recordToInsert = new VitalRecord();
                    $recordToInsert->symptoms = $request->symptoms;
                    $recordToInsert->nurse_id = $request->nurse_id;
                    $recordToInsert->patient_id = $request->patient_id;
                    $recordToInsert->status = $request->status;
                    $recordToInsert->desciption = $request->description[$key] ?? "";
                    $recordToInsert->value = $request->value[$key] ?? "";
                    $recordToInsert->unit = $request->unit[$key] ?? "";
                    $recordToInsert->visit_id = $request->visit_id;
                    $recordToInsert->questionId = $request->questionId[$key];

                    if ($request->is_departmentRefer) {
                        $recordToInsert->is_departmentRefer = $request->is_departmentRefer;
                    }
                    if ($request->isCHW) {
                        $recordToInsert->isCHW = $request->isCHW;
                    }
                    $recordToInsert->save();
                }
            }
        }



        $record = VitalRecord::where('visit_id', $request->visit_id)
            ->where('patient_id', $request->patient_id)
            ->where('nurse_id', $request->nurse_id)
            ->get();

        return $this->sendResponseData($record, "Vital Record Added successfully.");
    }


    public function getRecord($patientID)
    {
        $exists = VitalRecord::where('id', $patientID)->where('status', 'referred')->get();
        return $this->sendResponseData($exists, []);
    }

    public function getPatientRecord($patientID)
    {
        $record = VitalRecord::where("patient_id", $patientID)->with(['nurse', 'patient'])->get()->transform(function ($item, $key) {
            $item->data = json_decode($item->data, true);
            return $item;
        });
        return $this->sendResponseData($record, []);
    }

    public function getLatestPendingRecord($patientID, $visitID)
    {

        if ($visitID) {
            $record = VitalRecord::where("patient_id", $patientID)->where('visit_id', $visitID)->with('patient', 'nurse')->orderBy('id', 'desc')->get();
        } else {
            $record = VitalRecord::where("patient_id", $patientID)->with('patient', 'nurse')->orderBy('id', 'desc')->get();
        }

        return $this->sendResponseData($record, []);
    }
    public function closeRecord($recordID)
    {
        $record = VitalRecord::where("id", $recordID)->first();
        if (!$record) {
            return $this->sendError("No record found");
        }
        $record->status = "closed";
        $record->save();
        return $this->sendResponseData($record, "Status Changed Successfully");
    }
}
