<?php

namespace App\Http\Controllers\Api;

use Exception;
use App\Models\Test;
use App\Models\User;
use App\Models\Clinic;
use App\Models\Country;
use App\Models\Department;
use App\Models\Appointment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponseTrait;
use App\Models\Availablility;
use App\Models\CHWQuestionScreening;
use App\Models\CHWQuestionScreeningAnswer;
use App\Models\CovidQuestionRecord;
use App\Models\Disease;
use App\Models\DoctorCategory;
use App\Models\FamilyMedicalHistory;
use App\Models\KnownChronicQuestion;
use App\Models\PatientCheck;
use App\Models\Queue;
use App\Models\Ratting;
use App\Models\Referral;
use App\Models\RishFactor;
use App\Models\RishFactorQuestions;
use App\Models\Roles;
use App\Models\TestAll;
use App\Models\TypeAvail;
use App\Models\VisitPurpose;
use App\Models\VistsModel;
use App\Models\VitalRecord;
// use Carbon\CarbonPeriod;
use DateInterval;
use DatePeriod;
use DateTime;
use FFI;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Laravel\Jetstream\Role;
use PDO;
use PhpParser\Node\Stmt\Return_;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

use function PHPUnit\Framework\isEmpty;
use function PHPUnit\Framework\returnSelf;

class ManageApiController extends Controller
{
    use ApiResponseTrait;
    public function UTCDate(Request $request)
    {
        try {
            $appointment  = User::where("id", '1999ss9')->get();
            if ($appointment->count() > 0) {
                return $this->sendResponse($appointment, "Appointments for doctor.");
            }
            $this->SendErrorLog("Error getAppointmentByDoctor Not Found", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error getAppointmentByDoctor exception", "error");
        }
    }

    public function getCountries()
    {
        try {
            $countries = Country::all();
            if ($countries->count() > 0) {
                return $this->sendResponse($countries, []);
            }
            $this->SendErrorLog("Error getCountries Not Found", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error getCountries exception", "error");
        }
    }
    public function getDoctors()
    {
        try {
            $doctors = User::where('is_doctor', true)->get();
            if ($doctors->count() > 0) {
                return $this->sendResponse($doctors,  "Doctors list.");
            }
            $this->SendErrorLog("Error getDoctors Not Found", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error getDoctors exception", "error");
        }
    }

    public function getDoctorsName($name)
    {
        try {
            $doctors = User::join('doctor_categories', 'users.doctor_category_id', '=', 'doctor_categories.id')
                ->join('clinics', 'users.clinic_id', '=', 'clinics.id')
                ->where(['is_staff' => 0, "is_admin" => 0, "deactivated" => 0])
                ->with('usersdetail')->where(function ($q) use ($name) {
                    $q->where('users.name', 'like', '%' . $name . '%');
                })->select('users.*', 'doctor_categories.name as doc_category_name', 'clinics.name as clinic_name')
                ->get();
            if ($doctors->count() > 0) {
                return $this->sendResponse($doctors,  "Doctors list.");
            }
            $this->SendErrorLog("Error getDoctors Not Found", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error getDoctorsName exception", "error");
        }
    }
    public function getDoctorByCategory($categoryId)
    {
        try {
            $doctors = User::join('doctor_categories', 'users.doctor_category_id', '=', 'doctor_categories.id')
                ->join('clinics', 'users.clinic_id', '=', 'clinics.id')
                ->where(['is_staff' => 0, "is_admin" => 0, "deactivated" => 0])
                ->with('usersdetail', 'ratting')->where(function ($q) use ($categoryId) {
                    $q->where('users.id', 'like', '%' . $categoryId . '%');
                })->select('users.*', 'doctor_categories.name as doc_category_name', 'clinics.name as clinic_name')
                ->get();

            if ($doctors->count() > 0) {
                return $this->sendResponse($doctors, "Doctors list by categories.");
            }

            $this->SendErrorLog("Error getDoctors Not Found", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error getDoctorbyCategory exception", "error");
        }
    }
    public function getUser(Request $request)
    {
        try {
            $key = $request->key;
            $user = User::where(['is_staff' => 0, "is_admin" => 0, "deactivated" => 0])
                ->where(function ($q) use ($key) {
                    $q->where('email', $key)->orWhere('phone_number', $key);
                })
                ->first();
            if ($user) {
                return $this->sendResponse($user, []);
            }
            $this->SendErrorLog("Error getUser Not Found", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error getUser exception", "error");
        }
    }
    public function getUserByName(Request $request)
    {
        try {
            $key = urldecode($request->key);
            if (isset($request->clinicId)) {
                $user = User::where(function ($query) use ($key) {
                    $query->where('email', 'like', '%' . $key . '%')
                        ->orWhere('name', 'like', '%' . $key . '%');
                })->where('clinic_id', '=', $request->clinicId)
                    ->where('role_id', '=', '8')
                    ->with('usersdetail')
                    ->get();
                if ($user->isEmpty()) {
                    $this->SendErrorLog("Error getDoctorsByName Not Found", "error");
                    return $this->sendError("Not Found", []);
                } else {
                    return $this->sendResponse($user, []);
                }
            } else {
                $user = User::where(function ($query) use ($key) {
                    $query->where('email', 'like', '%' . $key . '%')
                        ->orWhere('name', 'like', '%' . $key . '%');
                })->where('role_id', '=', '8')
                    ->with('usersdetail')
                    ->get();
                if ($user->isEmpty()) {
                    $this->SendErrorLog("Error getDoctorsByName Not Found", "error");
                    return $this->sendError("Not Found", []);
                } else {
                    return $this->sendResponse($user, []);
                }
            }
        } catch (Exception $e) {
            return $this->Expection($e, "Error getUserbyUsername exception", "error");
        }
    }

    public function getUserBalance($userId)
    {
        try {
            $user = User::find($userId);
            return $this->sendResponse($user->wallet, []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error getuserbalance exception", "error");
        }
    }

    public function getDepartments()
    {
        try {
            $departments = Department::all();
            if ($departments) {
                return $this->sendResponse($departments, []);
            }
            $this->SendErrorLog("Error getDepartments", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error getDepartment exception", "error");
        }
    }

    public function updateUserBalance(Request $request, $userId)
    {
        try {
            $res = Validator::make($request->all(), [
                'amount' => 'required|numeric',
            ]);
            foreach ($res->errors()->toArray() as $field => $message) {
                $errors[] = [
                    'message' => $message[0],
                ];
            }
            if (isset($errors)) {
                return $this->sendError('Validation Error.', $errors);
            }
            $user = User::find($userId);
            $user->wallet->balance += (int) $request->amount;
            $user->wallet->wallet_last_change = $request->amount;
            $user->wallet->save();
            return $this->sendResponse($user->wallet, "Balance updated successfully");
        } catch (Exception $e) {
            return $this->Expection($e, "Error updateuserbalance exception", "error");
        }
    }

    public function getGuests($parentUserId)
    {
        try {
            $guests = User::where(['parent_user' =>  $parentUserId, "deactivated" => 0])->get();
            if ($guests) {
                return $this->sendResponse($guests, []);
            }
            $this->SendErrorLog("Error getGuests", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error getQuest exception", "error");
        }
    }

    public function getClinics()
    {
        try {
            $clinics = Clinic::all();
            return $this->sendResponse($clinics, 'Clinic Data');
        } catch (Exception $e) {
            return $this->Expection($e, "Error getClinics exception", "error");
        }
    }

    public function getDepartmentForClinic($clinicId)
    {

        try {
            $usersDep = User::where('id', $clinicId)->first();
            $departments = Department::where('clinic_id',  $usersDep->clinic_id)->get();
            if ($departments->count() > 0) {
                return $this->sendResponse($departments, "Department Staff");
            }
            $this->SendErrorLog("Error getDepartmentsforClincis", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error getDepartmentforclinic exception", "error");
        }
    }


    public function getTests()
    {
        try {
            $tests  = Test::all();
            if ($tests->count() > 0) {
                return $this->sendResponse($tests, []);
            }
            $this->SendErrorLog("Error getTests", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error getTests exception", "error");
        }
    }
    public function getZkToken()
    {
        $token = '26E02472AF49D20271778A33B2310373';
        return $this->sendResponse($token, []);
    }
    public function addAppointment(Request $request)
    {
        try {
            $res = Validator::make($request->all(), [
                'doctor_id' => 'required|exists:users,id',
                'patient_id' => 'required|exists:users,id',
                'date' => 'required|date',
            ]);
            foreach ($res->errors()->toArray() as $field => $message) {
                $errors[] = [
                    'message' => $message[0],
                ];
            }
            if (isset($errors)) {
                return $this->sendError('Validation Error.', $errors);
            }

            if ($request->visitId) {

                $dep = User::where('id', $request->doctor_id)->first();
                $visitModel = VistsModel::where('id', $request->visitId)->first();
                $userApp = DB::table('appointments')
                    ->where('parientID', $visitModel->id)
                    ->where('patient_id', $request->patient_id)
                    ->where('doctor_id', $request->doctor_id)
                    ->where('status', 'new')
                    ->orderBy('id', 'desc')
                    ->first();

                if ($userApp) {
                    return $this->sendError('Already appointment added.', []);
                }
                $patientcheckinexk = PatientCheck::where('department_id', $dep->department_id)
                    ->orderBy('id', 'desc')->where('visit_id', $request->visitId)
                    ->where('date', date('Y-m-d'))
                    ->where('patient_id', $request->patient_id)
                    ->first();

                if ($patientcheckinexk->status != 'checkin' && $patientcheckinexk->check_out == null) {
                    return $this->sendError('Checkin first.', []);
                }

                $patientCHeckComplete = PatientCheck::where('visit_id', $request->visitId)->where('department_id', $dep->department_id)->where('date', date('Y-m-d'))->where('patient_id', $request->patient_id)->where('staff_id', $request->doctor_id)->where('status', 'checkout')->orderBy('id', 'desc')->first();
                if (isset($patientCHeckComplete)) {
                    $visit = $this->visitCreate($request);
                    $appointment = new Appointment();
                    $user = User::where('id', $request->doctor_id)->first();
                    $appointment->doctor_id = $request->doctor_id;
                    $appointment->doc_name = $user->name;
                    $appointment->patient_id = $request->patient_id;
                    $appointment->date = $request->date;
                    $appointment->visit_id =  $visit;
                    $appointment->clinic_id =   $user->clinic_id;
                    $appointment->status =   'new';
                    $appointment->parientID =   $request->visitId;
                    $appointment->save();
                    $appointment = Appointment::with('DoctorDetail', 'PatientDetail')->where('id', $appointment->id)->orderBy('id', 'desc')->first();
                    $visitModel = VistsModel::where('id',  $visit)->first();
                    $response = [
                        'appointment' => $appointment,
                        'visit' => $visitModel,
                    ];
                    return $this->sendResponse($response, "Appointment added");
                } else {
                    $this->SendErrorLog("Error addApointment", "error");
                    return $this->sendError('Checkout first.', []);
                }
            }
        } catch (Exception $e) {
            return $this->Expection($e, "Error addAppointment exception", "error");
        }
    }

    public function visitCreate($request)
    {
        try {
            $vists = new VistsModel();
            $vists->patientID = $request->patient_id ?? null;
            $vists->docID = $request->docID ?? null;
            $user = User::where('id', $request->doctor_id)->first();
            $vists->date = $request->date ?? null;
            $vists->desc = $request->description ?? null;
            $vists->doc_name = $user->name ?? null;
            $vists->date = $request->appointmentDate ?? null;
            $vists->docID = $user->id ?? null;
            $vists->parentId = $request->visitId ?? null;
            $vists->status = "new";
            $vists->typeid = $request->visttype ?? null;
            $vists->save();
            return $vists->id;
        } catch (Exception $e) {
            return $this->Expection($e, "Error visitCreate exception", "error");
        }
    }

    public function cancelAppointment($appointmentId)
    {
        try {
            $appointment  = VistsModel::where("id", $appointmentId)->first();
            if (!$appointment) {
                return $this->sendError('No cancelAppointment found.');
            }
            $appointment->status = 'canceled';
            $appointment->save();
            return $this->sendResponse($appointment, "Appointment canceled successfully.");
        } catch (Exception $e) {
            return $this->Expection($e, "Error cancelAppointment exception", "error");
        }
    }
    public function getAppointments()
    {
        try {
            $appointments  = Appointment::with('DoctorDetail', 'PatientDetail')->get();
            if ($appointments->count() > 0) {
                return $this->sendResponse($appointments, "Appointments List.");
            }
            $this->SendErrorLog("Error getAppointments", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error getAppointment exception", "error");
        }
    }
    public function getAppointmentsByDoctor($doctorId)
    {
        try {
            $appointment  = Appointment::where("doctor_id", $doctorId)->get();
            if ($appointment->count() > 0) {
                return $this->sendResponse($appointment, "Appointments for doctor.");
            }
            $this->SendErrorLog("Error getAppointmentByDoctor", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error getAppointmentByDoctor exception", "error");
        }
    }
    public function getAppointmentsByPatient($patientId)
    {
        try {
            $appointment  = Appointment::where("patient_id", $patientId)->get();
            if (!$appointment) {
                return $this->sendError('No appointment found for patient.');
            }
            return $this->sendResponse($appointment, "Appointments for patient.");
        } catch (Exception $e) {
            return $this->Expection($e, "Error getAppointmentsByPatient exception", "error");
        }
    }
    public function doctorscategories()
    {
        try {
            $doctorCategory = DoctorCategory::with('doctor')
                ->whereHas('doctor', function ($query) {
                    $query->where('is_doctor', '=', '1');
                })
                ->get();
            if (count($doctorCategory) > 0) {
                return $this->sendResponse($doctorCategory, "Doctor Category List.");
            }
            $this->SendErrorLog("Error doctorsCategories", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error doctorsCategories exception", "error");
        }
    }
    public function doctorscategoriesavailable()
    {
        try {
            $avaiableD = DB::table('doctor_categories')
                ->select('doctor_categories.id', 'doctor_categories.name', DB::raw("COUNT(users.doctor_category_id) as doct"))
                ->leftJoin('users', 'doctor_categories.id', '=', 'users.doctor_category_id')
                ->groupBy('doctor_categories.id', 'doctor_categories.name')
                ->get();
            if (count($avaiableD) > 0) {
                return $this->sendResponse($avaiableD, "Available Doctor List.");
            }
            $this->SendErrorLog("Error doctorsCategoriesAvailable", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error doctorsCategoriesAvailable exception", "error");
        }
    }

    public function ratting()
    {
        try {
            $collection = Ratting::with('usersdetail', 'doctors')
                ->get();
            if (count($collection) > 0) {
                return $this->sendResponse($collection, "Doctor Ratting List.");
            }
            $this->SendErrorLog("Error doctorRatting", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error doctorRatting exception", "error");
        }
    }

    public function getDoctorsbyid($id)
    {
        try {
            $collection = User::with('usersdetail', 'ratting')
                ->where(function ($q) use ($id) {
                    $q->where('id', 'like', '%' . $id . '%');
                })->get();

            if (count($collection) > 0) {
                return $this->sendResponse($collection, "Doctor Search By ID.");
            }
            $this->SendErrorLog("Error getDoctorbyID", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error getDoctorByID exception", "error");
        }
    }

    public function doctoronline()
    {
        try {

            $collection = User::with('usersdetail', 'ratting')
                ->where('is_doctor', '=', '1')
                ->get();
            if (count($collection) > 0) {
                return $this->sendResponse($collection, "Doctor Search By ID.");
            }
            $this->SendErrorLog("Error doctorOnline", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error doctorOnline exception", "error");
        }
    }

    public function addratting(Request $request)
    {
        try {
            $ratting = new Ratting();
            $ratting->doc_id = $request->doc_id;
            $ratting->date = $request->date;
            $ratting->description = $request->description;
            $ratting->rate = $request->rate;
            $ratting->add_users_id = Auth::user()->id;
            $ratting->name = Auth::user()->name;
            if ($ratting->save()) {
                return $this->sendResponse($ratting, "Ratting Addedd Successfully.");
            }
            $this->SendErrorLog("Error not save AddRatting", "error");
            return $this->sendError("Not Save", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error addReating exception", "error");
        }
    }

    public function GetPatient($id, $clinicId)
    {
        try {
            $patient = User::with('usersdetail')
                ->whereHas('usersdetail', function ($query) use ($id) {
                    $query->where('patient_id', 'LIKE', "%{$id}%");
                })
                ->where('role_id', '=', '8')
                ->where('clinic_id', '=', $clinicId)
                ->get();
            if ($patient) {
                return $this->sendResponse($patient, "Patient ID Available.");
            }
            $this->SendErrorLog("Error GetPatient", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error addReating exception", "error");
        }
    }

    public function gettimeapp(Request $request)
    {

        try {
            $avaiableD = Availablility::where('fromdate', $request->date)
                ->where('todate', $request->date)->where('doc_id', $request->doc_id)
                ->first();

            if ($avaiableD) {

                $from = date("h:i a", strtotime($avaiableD->fromtime));
                $to = date("h:i a", strtotime($avaiableD->totime));

                $minutesDifferentForTimeSlot = 30;
                $periods = CarbonPeriod::create($from, $minutesDifferentForTimeSlot . ' minutes', $to);

                $app = Appointment::where('date', $request->date)->where('doctor_id', $request->doc_id)->select('time')->get();
                $arr = [];
                foreach ($app as $index => $items) {
                    $arr[$index] = date('h:i a', strtotime($items->time));
                }

                $available_times = array_map(function ($period) {
                    return $period->format('h:i a');
                }, $periods->toArray());

                $result = array_diff($available_times, $arr);
                return $this->sendResponse($result, "Doctor avaiable Time.");
            } else {
                $this->SendErrorLog("Error getTimeApp", "error");
                return $this->sendError("Not Found", []);
            }
        } catch (Exception $e) {
            return $this->Expection($e, "Error getTimeApp exception", "error");
        }
    }

    public function available(Request $request)
    {
        return $request->all();
    }


    // type of docAvaiblity
    // get
    public function availabletypes(Request $request)
    {
        try {
            $type = TypeAvail::all();
            if ($type->count() > 0) {
                return $this->sendResponse($type, "Doctor Avaiable Type.");
            }
            $this->SendErrorLog("Error availableTypes", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error availableTypes exception", "error");
        }
    }
    // post
    public function availabletype(Request $request)
    {
        try {
            $res = Validator::make($request->all(), [
                'name' => 'required|unique:typeofavailbity,name',
            ]);
            foreach ($res->errors()->toArray() as $field => $message) {
                $errors[] = [
                    'message' => $message[0],
                ];
            }
            if (isset($errors)) {
                return $this->sendError('Validation Error.', $errors);
            }

            $docA = new TypeAvail();
            $docA->name = $request->name;
            if ($docA->save()) {
                return $this->sendResponse($docA, "Doctor avaiable Time.");
            }
            $this->SendErrorLog("Error availableType", "error");
            return $this->sendError("Not Save", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error availableType exception", "error");
        }
    }

    public function GetPatientDoument($id, $clinicId)
    {
        try {
            $patient = User::with('usersdetail')
                ->whereHas('usersdetail', function ($query) use ($id) {
                    $query->where('document_id', '=', $id);
                })
                ->where('clinic_id', '=', $clinicId)
                ->get();

            if ($patient->count() > 0) {
                return $this->sendResponse($patient, "Patient Document by ID.");
            }
            $this->SendErrorLog("Error GetPatientDoc", "error");
            return $this->sendError('Not Found Document ID.', $id);
        } catch (Exception $e) {
            return $this->Expection($e, "Error getPatientDocument exception", "error");
        }
    }


    // Not Using this function
    public function PatientRefer(Request $request, $recordId)
    {
        try {



            $checkInDesk = Department::where('id', $request->referred_by)->where('role_id', '1')->first();
            $checkInAdmin = Department::where('id', $request->referred_by)->where('role_id', '2')->first();

            if (isset($checkInDesk) == false && isset($checkInAdmin) == false) {
                $test = Test::where('visit_id', $recordId)->where('status', 'pending')->first();
                if ($test) {
                    return $this->sendError("Please add laboratory testd");
                }
            }
            $questions = CovidQuestionRecord::where('visit_id', $recordId)->first();
            if (!$questions) {
                return $this->sendError("No Screening question assign please add");
            }

            $exists = Referral::with(["referredBy", "referredTo"])->where('visit_id', $recordId)
                ->where('patient_id', $request->patient_id)
                ->where('referred_by', $request->referred_by)
                ->where('referred_to', $request->referred_to)
                ->where('status', 'referred')->first(); //last visit and check refer
            if ($exists) {
                return $this->sendError("Patient already referred");
            }

            $patientcheckinexk = PatientCheck::where('department_id', $request->referred_by)->where('visit_id', $recordId)->where('status', 'checkin')->where('patient_id', $request->patient_id)->first();

            if ($patientcheckinexk == null) {
                return $this->sendError('Checkin first', []);
            }
            if ($patientcheckinexk != null) {
                $patientrefer = PatientCheck::where('department_id', $request->referred_by)->orderBy('id', 'DESC')->where('visit_id', $recordId)->where('status', 'checkin')->where('patient_id', $request->patient_id)->first();
                $refer = Referral::with(["DepreferredBy", "DefreferredTo"])->where('checkinID', $patientrefer->id)->where('referred_by', $request->referred_by)->where('patient_id', $request->patient_id)->orderBy('id', 'DESC')->first();
                if ($refer) {
                    return $this->sendError('Already refer', []);
                } else {
                    $referral = new Referral();
                    $department = Department::where('id', $request->referred_by)->first();
                    $referral->checkinID = $patientrefer->id;
                    $referral->visit_id = $recordId;
                    $referral->patient_id = $request->patient_id;
                    $referral->referred_by = $request->referred_by;
                    $referral->referred_to = $request->referred_to;
                    $referral->clinic_id = $department->clinic_id;
                    $referral->is_departmentRefer = 1;

                    $referral->doc_id = $request->staff_id;
                    $user = User::where('id', $request->staff_id)->first();
                    $referral->doc_name = $user->name;

                    $referral->remarks = $request->remarks;
                    $referral->status = "referred";
                    if ($referral->save()) {
                        $refer  = Referral::with(["referredBy", "referredTo"])->where('visit_id', $recordId)
                            ->where('patient_id', $request->patient_id)
                            ->where('referred_by', $request->referred_by)
                            ->where('referred_to', $request->referred_to)
                            ->where('status', 'referred')->orderBy("id", "DESC")->first(); //last visit and check refer
                        return $this->sendResponseData($refer, "Record Reffered Successfully.");
                    }
                    $this->SendErrorLog("Error PatientRefer", "error");
                    return $this->sendError('Not Save Referral.', $request->all());
                }
            }
        } catch (Exception $e) {
            return $this->Expection($e, "Error patientRefer exception", "error");
        }
    }

    public function queueReferGeneration1($dep, $userId, $queueDateEq)
    {
        try {

            $department = Department::where('id', $dep)->first();
            $lastQueue = Queue::where('department_id', $dep)->where('user_id', $userId)->max('queue_number');

            if ($queueDateEq) {
                $lastQueueNumber = (($queueDateEq->queue_number));
                $latestQueuNumber = ($lastQueueNumber + 1);
                $newQueueEntry = new Queue();
                $newQueueEntry->department_id = $department->id;
                $departmentName = Department::where('id', $department->id)->first();
                $newQueueEntry->department_name = $departmentName->name;
                $newQueueEntry->user_id = $userId;
                if ($latestQueuNumber == 1) {
                    $newQueueEntry->is_current = true;
                } else {
                    $newQueueEntry->is_current = false;
                }
                $newQueueEntry->queue_number = $latestQueuNumber;
                $newQueueEntry->save();
            }

            if ($lastQueue) {
                $lastQueueNumber = (($lastQueue) ? $lastQueue : 0);
                $latestQueuNumber = ($lastQueueNumber + 1);
                $newQueueEntry = new Queue();
                $newQueueEntry->department_id = $department->id;
                $departmentName = Department::where('id', $department->id)->first();
                $newQueueEntry->department_name = $departmentName->name;
                $newQueueEntry->user_id = $userId;
                if ($latestQueuNumber == 1) {
                    $newQueueEntry->is_current = true;
                } else {
                    $newQueueEntry->is_current = false;
                }
                $newQueueEntry->queue_number = $latestQueuNumber;
                $newQueueEntry->save();
            } else {
                $lastQueue = Queue::where('department_id', $dep)->max('queue_number');
                $lastQueueNumber = (($lastQueue) ? $lastQueue : 0);
                $latestQueuNumber = ($lastQueueNumber + 1);
                $newQueueEntry = new Queue();
                $newQueueEntry->department_id = $department->id;
                $departmentName = Department::where('id', $dep)->first();
                $newQueueEntry->department_name = $departmentName->name;
                $newQueueEntry->user_id = $userId;
                if ($latestQueuNumber == 1) {
                    $newQueueEntry->is_current = true;
                } else {
                    $newQueueEntry->is_current = false;
                }
                $newQueueEntry->queue_number = $latestQueuNumber;
                $newQueueEntry->save();
            }

            return $this->sendResponse(["queue_number" => $newQueueEntry->queue_number], "Queue generated successfully.");
        } catch (Exception $e) {
            return $this->sendError('Error', [], 500);
        }
    }

    public function PatientCheckin(Request $request, $patietnID)
    {
        try {
            $patientID = User::where('id', $patietnID)->first();
            if (!$patientID) {
                return $this->sendError("no record found");
            }
            $patientcheckin = new PatientCheck();
            $departments = Department::where('id', $request->department_id)->first();
            $patientcheckin->department_id = $request->department_id;
            $patientcheckin->department = $departments->name;
            $patientcheckin->patient_id = $patietnID;
            $patientcheckin->staff_id = $request->staff_id;
            $patientcheckin->check_in = Carbon::now();
            $patientcheckin->type = $request->type;
            $patientcheckin->date = date('Y-m-d');
            if ($patientcheckin->save()) {
                return $this->sendResponseData($patientcheckin, "Checkin added Successfully.");
            } else {
                $this->SendErrorLog("Error patientCheckin not Save" . $patietnID, "error");
                return $this->sendError("Not Save", [], 404);
            }
        } catch (Exception $e) {
            return $this->Expection($e, "Error patientCheckin exception", "error");
        }
    }

    public function PatientCheckout(Request $request)
    {
        try {
            $patientcheckin =  PatientCheck::where('patient_id', $request->patietnID)->where('date', $request->date)->first();
            if ($patientcheckin->check_in) {
                $patientcheckin->check_out = Carbon::now();
                if ($patientcheckin->save()) {
                    return $this->sendResponseData($patientcheckin, "Checkout added Successfully.");
                }
                $this->SendErrorLog("Error patientCheckout not Save", "error");
                return $this->sendError("Not Save", [], 404);
            } else {
                $this->SendErrorLog("Error patientCheckout not found patient" . $request->patietnID, "error");
                return $this->sendError('Patient ID not found! ', $request->patietnID);
            }
        } catch (Exception $e) {
            return $this->Expection($e, "Error patientCheckin exception", "error");
        }
    }

    public function getCheckinCheckout(Request $request)
    {

        try {
            $user = User::where('id', $request->patient_id)->select('name')->first();
            $patientCheck = PatientCheck::where('patient_id', $request->patient_id)
                ->where('staff_id', $request->staff_id)
                ->where('department_id', $request->department_id)
                ->first();


            return response()->json(
                [
                    'success' => true,
                    'data' => [
                        'patient' => $user,
                        'checkinout' => $patientCheck,
                    ],
                    'message' => "Patient Check in/out",
                ]
            );
        } catch (Exception $e) {
            return $this->Expection($e, "Error patientCheckout exception", "error");
        }
    }

    public function DepartmenClinic(Request $request)
    {
        try {
            $role = Roles::all();
            if ($role->count() > 0) {
                return $this->sendResponseData($role, "Department Roles.");
            }
            $this->SendErrorLog("Error departmentClinic not found", "error");
            return $this->sendError('Department Clinic not found!',);
        } catch (Exception $e) {
            return $this->Expection($e, "Error patientCheckout exception", "error");
        }
    }

    public function DepartmenCheckInProcess(Request $request)
    {
        $userRole = User::where('id', $request->staff_id)->first();
        $userRoleName = Department::where('id', $userRole->department_id)->first();
        if ($userRoleName == null) {
            return $this->sendError('Department not exists.', []);
        }

        if ($request->check_in_process == 'true') {
            $this->validateionFeild($request, 'check_in_process');
            return $this->PatientCheckinDepartmentWise($request,  $userRoleName->id, $userRoleName->role_name);
        } else if ($request->check_in_admin == 'true') {
            $this->validateionFeild($request, 'check_in_admin');
            return $this->PatientCheckinDepartmentWise($request, $userRoleName->id, $userRoleName->role_name);
        } else if ($request->check_in_vital == 'true') {
            $this->validateionFeild($request, 'check_in_vital');
            return $this->PatientCheckinDepartmentWise($request, $userRoleName->id,  $userRoleName->role_name);
        } else if ($request->check_in_refer == 'true') {
            $this->validateionFeild($request, 'check_in_refer');
            return $this->PatientCheckinDepartmentWise($request, $userRoleName->id,  $userRoleName->role_name);
        }
    }

    public function validateionFeild($request, $msg)
    {
        $res = Validator::make($request->all(), [
            'patietnID' => 'required|exists:users,id',
            'staff_id' => 'required|exists:users,id',
            $msg => 'required',
            'visit_id' => 'required'
        ]);

        foreach ($res->errors()->toArray() as $field => $message) {
            $errors[] = [
                'message' => $message[0],
            ];
        }
        if (isset($errors)) {
            return $this->sendError('Validation ERROR.', $errors);
        }
    }

    // public function PatientCheckinDepartmentWise($request, $dep, $name)
    // {
    //     if ($request->check_in_process == 'true') {
    //         if ($request->ischeckout == 'true' && $request->check_in_process == 'true') {
    //             $checkVisit = PatientCheck::CheckVisit($request);
    //             $patientcheckinex = PatientCheck::CHeckINProcessOUT($request, $dep, $checkVisit);
    //             $patientcheckinexk = PatientCheck::CHeckINProcessIN($request, $dep, $checkVisit);
    //             if ($patientcheckinex) {
    //                 return $this->sendError('You Already Checkout from this Department.', []);
    //             } else if ($patientcheckinexk == null) {
    //                 return $this->sendError('Checkin first', []);
    //             }

    //             if ($patientcheckinex == null && $patientcheckinexk != null) {
    //                 $patientrefer = PatientCheck::CHeckINProcessIN($request, $dep, $checkVisit);
    //                 $refer = Referral::where('checkinID', $patientrefer->id)->first();
    //                 if (!$refer) {
    //                     return $this->sendError('Please refer first', []);
    //                 } else {
    //                     $patientcheckin = PatientCheck::PatientCheckOutFunction($request, $dep);
    //                     $staffID = User::where('id', $request->staff_id)->first();
    //                     $lastCurrenRunning = PatientCheck::QueueCurrentGet($request);
    //                     $lastCurrenRunningGet = PatientCheck::QueueCurrentGet($request);
    //                     foreach ($lastCurrenRunning as $item) {
    //                         $item->currenNumber = $lastCurrenRunningGet->currenNumber + 1;
    //                         $item->is_current = '0';
    //                         $item->save();
    //                     }
    //                     $modelVisit = PatientCheck::VisitModelGet($request);
    //                     return $this->ChcekQueueToken($request, $dep, $patientcheckin, $request->visit_id, $modelVisit);
    //                 }
    //             }
    //         } else {
    //             $checkVisit = PatientCheck::CheckVisit($request);
    //             // create Visit
    //             $vistID = PatientCheck::CreateVisit($request);
    //             $vistsModel = PatientCheck::CHeckINProcessIN($request, $dep, $checkVisit);
    //             if ($vistsModel) {
    //                 return $this->sendError('You Already checkin this Department..', []);
    //             }
    //             // get Model
    //             $modelVisit = PatientCheck::VisitModelGet($request);
    //             // Create CHECKIN
    //             $patientcheckin = PatientCheck::CreateCheckIN($request, $dep, $name, $vistID);
    //             // Generate Queue
    //             $staffID = User::where('id', $request->staff_id)->first();
    //             $this->queueReferGeneration($request, $vistID, $dep,  $request->patietnID, $staffID->clinic_id);
    //             $queue = PatientCheck::QueueCurrentGet($request, $vistID, $dep);

    //             $response = [
    //                 'success' => true,
    //                 'responseData' => $patientcheckin,
    //                 'modelVisit' => $modelVisit,
    //                 'queue' => $queue,
    //                 'message' => 'Checkin added Successfully.',
    //             ];
    //             return response()->json($response, 200);
    //         }
    //     } else if ($request->check_in_admin == 'true') {
    //         if ($request->ischeckout == 'true' && $request->check_in_admin == 'true') {
    //             $patientcheckinex = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('visit_id', $request->visit_id)->where('date', $request->todayDateUtc)->where('status', 'checkout')->where('patient_id', $request->patietnID)->first();
    //             $patientcheckinexk = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('visit_id', $request->visit_id)->where('date', $request->todayDateUtc)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();
    //             if ($patientcheckinex) {
    //                 return $this->sendError('You Already Checkout from this Department.', []);
    //             } else if ($patientcheckinexk == null) {
    //                 return $this->sendError('Checkin first', []);
    //             }
    //             if ($patientcheckinex == null && $patientcheckinexk != null) {
    //                 $patientrefer = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();
    //                 $refer = Referral::where('checkinID', $patientrefer->id)->first();
    //                 if (!$refer) {
    //                     return $this->sendError('Please refer first', []);
    //                 } else {

    //                     $patientcheckin = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();

    //                     $patientcheckin->check_out = Carbon::now('UTC');
    //                     $patientcheckin->status =  "checkout";
    //                     $patientcheckin->save();

    //                     $staffID = User::where('id', $request->staff_id)->first();

    //                     $lastCurrenRunning = Queue::where('department_id', $request->department_id)
    //                         ->where('clinic_id', $staffID->clinic_id)
    //                         // ->where('date', date('Y-m-d'))
    //                         ->where('date', $request->todayDateUtc)
    //                         ->orderBy('id', 'DESC')
    //                         ->get();

    //                     $lastCurrenRunningGet = Queue::where('department_id', $request->department_id)
    //                         ->where('clinic_id', $staffID->clinic_id)
    //                         // ->where('date', date('Y-m-d'))
    //                         ->where('date', $request->todayDateUtc)
    //                         ->orderBy('id', 'DESC')
    //                         ->first();

    //                     foreach ($lastCurrenRunning as $item) {
    //                         $item->currenNumber = $lastCurrenRunningGet->currenNumber + 1;
    //                         $item->is_current = '0';
    //                         $item->save();
    //                     }
    //                     // $this->queueReferGeneration($request->visit_id, $refer->referred_to,  $request->patietnID, $staffID->clinic_id);
    //                     // return $this->sendResponseData($patientcheckin, "CheckOut added Successfully.");
    //                     $modelVisit = VistsModel::where('patientID', $request->patietnID)
    //                         ->where('id', $request->visit_id)
    //                         ->where('date', $request->todayDateUtc)
    //                         ->where('status', '!=', 'complete')
    //                         ->orderBy('id', 'DESC')
    //                         ->first();
    //                     return  $this->ChcekQueueToken($request, $dep, $patientcheckin, $request->visit_id, $modelVisit);
    //                 }
    //             }
    //         } else {
    //             $vistsModel = VistsModel::where('patientID', $request->patietnID)->where('date', $request->todayDateUtc)->orderBy('id', 'DESC')->first();
    //             if (!$vistsModel) {
    //                 return $this->sendError('Visit not exist', []);
    //             }

    //             $patientcheckinExistin = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('status', 'checkin')->where('visit_id', $request->visit_id)->where('patient_id', $request->patietnID)->first();
    //             $patientcheckinExistin1 = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('status', 'checkout')->where('visit_id', $request->visit_id)->where('patient_id', $request->patietnID)->first();
    //             if ($patientcheckinExistin) {
    //                 return $this->sendError('You Already checkin in this Department..', []);
    //             } else if ($patientcheckinExistin1) {
    //                 return $this->sendError('You Already checkout from this Department.', []);
    //             }

    //             $ISpatientcheck = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('status', 'checkin')->where('visit_id', $request->visit_id)->where('patient_id', $request->patietnID)->first();
    //             if (!$ISpatientcheck) {
    //                 $patientcheckin = new PatientCheck();
    //                 $patientcheckin->visit_id =  $request->visit_id;
    //                 $patientcheckin->status =  "checkin";
    //                 $patientcheckin->department_id = $dep;
    //                 $patientcheckin->department = $name;
    //                 $patientcheckin->patient_id =  $request->patietnID;
    //                 $patientcheckin->staff_id = $request->staff_id;
    //                 $patientcheckin->check_in = Carbon::now('UTC');
    //                 $patientcheckin->type = $name;
    //                 $patientcheckin->date = $request->todayDateUtc;
    //                 $patientcheckin->save();

    //                 if (isset($request->visit_id)) {
    //                     $staffID = User::where('id', $request->staff_id)->first();
    //                     $this->queueReferGeneration($request, $request->visit_id, $request->department_id,  $request->patietnID, $staffID->clinic_id);
    //                 }

    //                 $modelVisit = VistsModel::where('patientID', $request->patietnID)
    //                     ->where('id', $request->visit_id)
    //                     ->where('date', $request->todayDateUtc)
    //                     ->where('status', '!=', 'complete')
    //                     ->orderBy('id', 'DESC')
    //                     ->first();
    //                 return  $this->ChcekQueueToken($request, $dep, $patientcheckin, $request->visit_id, $modelVisit);
    //                 // return $this->sendResponseData($patientcheckin, "Checkin added Successfully.");
    //             }

    //             $patientcheckin = new PatientCheck();
    //             $patientcheckin->visit_id =  $request->visit_id;
    //             $patientcheckin->status =  "checkin";
    //             $patientcheckin->department_id = $dep;
    //             $patientcheckin->department = $name;
    //             $patientcheckin->patient_id =  $request->patietnID;
    //             $patientcheckin->staff_id = $request->staff_id;
    //             $patientcheckin->check_in = Carbon::now('UTC');
    //             $patientcheckin->type = $name;
    //             $patientcheckin->date = Carbon::now('UTC');
    //             $patientcheckin->save();
    //             if (isset($request->visit_id)) {
    //                 $staffID = User::where('id', $request->staff_id)->first();
    //                 $this->queueReferGeneration($request, $request->visit_id, $request->department_id,  $request->patietnID, $staffID->clinic_id);
    //             }
    //             $modelVisit = VistsModel::where('patientID', $request->patietnID)
    //                 ->where('id', $request->visit_id)
    //                 ->where('date', $request->todayDateUtc)
    //                 ->where('status', '!=', 'complete')
    //                 ->orderBy('id', 'DESC')
    //                 ->first();
    //             return  $this->ChcekQueueToken($request, $dep, $patientcheckin, $request->visit_id, $modelVisit);

    //             // return $this->sendResponseData($patientcheckin, "Checkin added Successfully.");
    //         }
    //     } else if ($request->check_in_vital == 'true') {
    //         if ($request->ischeckout == 'true' && $request->check_in_vital == 'true') {
    //             $patientcheckinex = PatientCheck::where('department_id', $dep)->where('date', $request->todayDateUtc)->where('visit_id', $request->visit_id)->where('status', 'checkout')->where('patient_id', $request->patietnID)->first();
    //             $patientcheckinexk = PatientCheck::where('department_id', $dep)->where('date', $request->todayDateUtc)->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();
    //             if ($patientcheckinex) {
    //                 return $this->sendError('You Already Checkout from this Department.', []);
    //             } else if ($patientcheckinexk == null) {
    //                 return $this->sendError('Checkin first', []);
    //             }

    //             if ($patientcheckinex == null && $patientcheckinexk != null) {
    //                 $patientrefer = PatientCheck::where('department_id', $dep)->where('date', $request->todayDateUtc)->orderBy('id', 'desc')->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();
    //                 $refer = Referral::where('checkinID', $patientrefer->id)->first();
    //                 if (!$refer) {
    //                     return $this->sendError('Please refer first', []);
    //                 } else {

    //                     $patientcheckin = PatientCheck::where('department_id', $dep)->where('date', $request->todayDateUtc)->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();
    //                     $patientcheckin->check_out = Carbon::now('UTC');
    //                     $patientcheckin->status =  "checkout";
    //                     $patientcheckin->save();


    //                     $staffID = User::where('id', $request->staff_id)->first();
    //                     $lastCurrenRunning = Queue::where('department_id', $request->department_id)
    //                         ->where('clinic_id', $staffID->clinic_id)
    //                         ->where('date', $request->todayDateUtc)
    //                         ->orderBy('id', 'DESC')
    //                         ->get();

    //                     $lastCurrenRunningGet = Queue::where('department_id', $request->department_id)
    //                         ->where('clinic_id', $staffID->clinic_id)
    //                         ->where('date', $request->todayDateUtc)
    //                         ->orderBy('id', 'DESC')
    //                         ->first();

    //                     foreach ($lastCurrenRunning as $item) {
    //                         $item->currenNumber = $lastCurrenRunningGet->currenNumber + 1;
    //                         $item->is_current = '0';
    //                         $item->save();
    //                     }
    //                     // $this->queueReferGeneration($request->visit_id, $request->department_id,  $request->patietnID, $staffID->clinic_id);
    //                     // return $this->sendResponseData($patientcheckin, "CheckOut added Successfully.");
    //                     $modelVisit = VistsModel::where('patientID', $request->patietnID)
    //                         ->where('id', $request->visit_id)
    //                         ->where('date', $request->todayDateUtc)
    //                         ->where('status', '!=', 'complete')
    //                         ->orderBy('id', 'DESC')
    //                         ->first();

    //                     return $this->ChcekQueueToken($request, $dep, $patientcheckin, $request->visit_id, $modelVisit);
    //                 }
    //             }
    //         } else {

    //             $vistsModel = VistsModel::where('patientID', $request->patietnID)->orderBy('id', 'DESC')->first();
    //             if (!$vistsModel) {
    //                 return $this->sendError('Visit not exist', []);
    //             }

    //             $patientcheckinExistin = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('status', 'checkin')->where('visit_id', $request->visit_id)->where('patient_id', $request->patietnID)->first();
    //             $patientcheckinExistin1 = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('status', 'checkout')->where('visit_id', $request->visit_id)->where('patient_id', $request->patietnID)->first();
    //             if ($patientcheckinExistin) {
    //                 return $this->sendError('You Already checkin in this Department..', []);
    //             } else if ($patientcheckinExistin1) {
    //                 return $this->sendError('You Already checkout from this Department.', []);
    //             }

    //             $ISpatientcheck = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('status', 'checkin')->where('visit_id', $request->visit_id)->where('patient_id', $request->patietnID)->first();
    //             if (!$ISpatientcheck) {
    //                 $patientcheckin = new PatientCheck();
    //                 $patientcheckin->visit_id =  $request->visit_id;
    //                 $patientcheckin->status =  "checkin";
    //                 $patientcheckin->department_id = $dep;
    //                 $patientcheckin->department = $name;
    //                 $patientcheckin->patient_id =  $request->patietnID;
    //                 $patientcheckin->staff_id = $request->staff_id;
    //                 $patientcheckin->check_in = Carbon::now('UTC');
    //                 $patientcheckin->type = $name;
    //                 $patientcheckin->date = $request->todayDateUtc;
    //                 $patientcheckin->save();
    //                 if (isset($request->visit_id)) {
    //                     $staffID = User::where('id', $request->staff_id)->first();
    //                     $this->queueReferGeneration($request, $request->visit_id, $request->department_id,  $request->patietnID, $staffID->clinic_id);
    //                 }

    //                 $modelVisit = VistsModel::where('patientID', $request->patietnID)
    //                     ->where('id', $request->visit_id)
    //                     ->where('date', $request->todayDateUtc)
    //                     ->where('status', '!=', 'complete')
    //                     ->orderBy('id', 'DESC')
    //                     ->first();
    //                 return  $this->ChcekQueueToken($request, $dep, $patientcheckin, $request->visit_id, $modelVisit);
    //                 // return $this->sendResponseData($patientcheckin, "Checkin added Successfully.");
    //             }


    //             $patientcheckin = new PatientCheck();
    //             $patientcheckin->visit_id =  $request->visit_id;
    //             $patientcheckin->status =  "checkin";
    //             $patientcheckin->department_id = $dep;
    //             $patientcheckin->department = $name;
    //             $patientcheckin->patient_id =  $request->patietnID;
    //             $patientcheckin->staff_id = $request->staff_id;
    //             $patientcheckin->check_in = Carbon::now('UTC');
    //             $patientcheckin->type = $name;
    //             $patientcheckin->date = $request->todayDateUtc;
    //             $patientcheckin->save();
    //             $modelVisit = VistsModel::where('patientID', $request->patietnID)
    //                 ->where('id', $request->visit_id)
    //                 ->where('date', $request->todayDateUtc)
    //                 ->where('status', '!=', 'complete')
    //                 ->orderBy('id', 'DESC')
    //                 ->first();
    //             return  $this->ChcekQueueToken($request, $dep, $patientcheckin, $request->visit_id, $modelVisit);
    //             // return $this->sendResponseData($patientcheckin, "Checkin added Successfully.");
    //         }
    //     } else if ($request->check_in_refer == 'true') {
    //         if ($request->ischeckout == 'true' && $request->check_in_refer == 'true') {
    //             $patientcheckinex = PatientCheck::where('department_id', $dep)->where('date', $request->todayDateUtc)->orderBy('id', 'desc')->where('visit_id', $request->visit_id)->where('status', 'checkout')->where('patient_id', $request->patietnID)->first();
    //             $patientcheckinexk = PatientCheck::where('department_id', $dep)->where('date', $request->todayDateUtc)->orderBy('id', 'desc')->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();
    //             if ($patientcheckinex) {
    //                 return $this->sendError('You Already Checkout from this Department.', []);
    //             } else if ($patientcheckinexk == null) {
    //                 return $this->sendError('Checkin first', []);
    //             }

    //             $staffID = User::where('id', $request->staff_id)->first();

    //             $lastCurrenRunning = Queue::where('department_id', $request->department_id)
    //                 ->where('clinic_id', $staffID->clinic_id)
    //                 ->where('date', $request->todayDateUtc)
    //                 ->orderBy('id', 'DESC')
    //                 ->get();

    //             $lastCurrenRunningGet = Queue::where('department_id', $request->department_id)
    //                 ->where('clinic_id', $staffID->clinic_id)
    //                 ->where('date', $request->todayDateUtc)
    //                 ->orderBy('id', 'DESC')
    //                 ->first();

    //             foreach ($lastCurrenRunning as $item) {
    //                 $item->currenNumber = $lastCurrenRunningGet->currenNumber + 1;
    //                 $item->is_current = '0';
    //                 $item->save();
    //             }
    //             $patientcheckin = PatientCheck::where('department_id', $dep)->where('date', $request->todayDateUtc)->orderBy('id', 'desc')->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();
    //             $patientcheckin->check_out = Carbon::now('UTC');
    //             $patientcheckin->status =  "checkout";
    //             $patientcheckin->save();

    //             $vistCom = VistsModel::where('id', $request->visit_id)->first();
    //             $vistCom->status = 'complete';
    //             $vistCom->patientID = $request->patietnID;
    //             $vistCom->save();

    //             // return $this->sendResponseData($patientcheckin, "CheckOut added Successfully.");
    //             $modelVisit = VistsModel::where('patientID', $request->patietnID)
    //                 ->where('id', $request->visit_id)
    //                 ->where('date', $request->todayDateUtc)
    //                 ->where('status', '!=', 'complete')
    //                 ->orderBy('id', 'DESC')
    //                 ->first();
    //             return $this->ChcekQueueToken($request, $dep, $patientcheckin, $request->visit_id, $modelVisit);
    //         } else {
    //             // checkin
    //             $vistsModel = VistsModel::where('patientID', $request->patietnID)->where('date', $request->todayDateUtc)->orderBy('id', 'DESC')->first();
    //             if (!$vistsModel) {
    //                 return $this->sendError('Visit not exist', []);
    //             }
    //             $patientcheckinExistin = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('status', 'checkin')->where('visit_id', $request->visit_id)->where('patient_id', $request->patietnID)->first();
    //             $patientcheckinExistin1 = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('status', 'checkout')->where('visit_id', $request->visit_id)->where('patient_id', $request->patietnID)->first();
    //             if ($patientcheckinExistin) {
    //                 return $this->sendError('You Already checkin in this Department..', []);
    //             } else if ($patientcheckinExistin1) {
    //                 return $this->sendError('You Already checkout from this Department..', []);
    //             }

    //             $ISpatientcheck = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('status', 'checkin')->where('date', $request->todayDateUtc)->where('visit_id', $request->visit_id)->where('patient_id', $request->patietnID)->first();
    //             if (!$ISpatientcheck) {
    //                 $patientcheckin = new PatientCheck();
    //                 $patientcheckin->visit_id =  $request->visit_id;
    //                 $patientcheckin->status =  "checkin";
    //                 $patientcheckin->department_id = $dep;
    //                 $patientcheckin->department = $name;
    //                 $patientcheckin->patient_id =  $request->patietnID;
    //                 $patientcheckin->staff_id = $request->staff_id;
    //                 $patientcheckin->check_in = Carbon::now('UTC');
    //                 $patientcheckin->type = $name;
    //                 $patientcheckin->date = $request->todayDateUtc;
    //                 $patientcheckin->save();
    //                 if (isset($request->visit_id)) {
    //                     $staffID = User::where('id', $request->staff_id)->first();
    //                     $this->queueReferGeneration($request, $request->visit_id, $request->department_id,  $request->patietnID, $staffID->clinic_id);
    //                 } else {
    //                     $staffID = User::where('id', $request->staff_id)->first();
    //                     $visit = VistsModel::where('docID', $request->staff_id)->where('date', $request->todayDateUtc)->where('patientID', $request->patietnID)->where('status', '=', 'new')->orderBy('id', 'desc')->first();
    //                     $this->queueReferGeneration($request, $visit->id, $request->department_id,  $request->patietnID, $staffID->clinic_id);
    //                 }
    //                 $modelVisit = VistsModel::where('patientID', $request->patietnID)
    //                     ->where('id', $request->visit_id)
    //                     ->where('date', $request->todayDateUtc)
    //                     ->where('status', '!=', 'complete')
    //                     ->orderBy('id', 'DESC')
    //                     ->first();
    //                 return  $this->ChcekQueueToken($request, $dep, $patientcheckin, $request->visit_id, $modelVisit);
    //                 // return $this->sendResponseData($patientcheckin, "Checkin added Successfully.");
    //             }

    //             $patientcheckin = new PatientCheck();
    //             $patientcheckin->visit_id =  $request->visit_id;
    //             $patientcheckin->status =  "checkin";
    //             $patientcheckin->department_id = $dep;
    //             $patientcheckin->department = $name;
    //             $patientcheckin->patient_id =  $request->patietnID;
    //             $patientcheckin->staff_id = $request->staff_id;
    //             $patientcheckin->check_in =  Carbon::now('UTC');
    //             $patientcheckin->type = $name;
    //             $patientcheckin->date = $request->todayDateUtc;
    //             $patientcheckin->save();
    //             if (isset($request->visit_id)) {
    //                 $staffID = User::where('id', $request->staff_id)->first();
    //                 $this->queueReferGeneration($request, $request->visit_id, $request->department_id,  $request->patietnID, $staffID->clinic_id);
    //             } else {
    //                 $staffID = User::where('id', $request->staff_id)->first();
    //                 $visit = VistsModel::where('docID', $request->staff_id)->where('date', $request->todayDateUtc)->where('patientID', $request->patietnID)->where('status', '=', 'new')->orderBy('id', 'desc')->first();
    //                 $this->queueReferGeneration($request, $visit->id, $request->department_id,  $request->patietnID, $staffID->clinic_id);
    //             }
    //             $modelVisit = VistsModel::where('patientID', $request->patietnID)
    //                 ->where('id', $request->visit_id)
    //                 ->where('date', $request->todayDateUtc)
    //                 ->where('status', '!=', 'complete')
    //                 ->orderBy('id', 'DESC')
    //                 ->first();
    //             return  $this->ChcekQueueToken($request, $dep, $patientcheckin, $request->visit_id, $modelVisit);
    //             // return $this->sendResponseData($patientcheckin, "Checkin added Successfully.");
    //         }
    //     }
    // }


    public function PatientCheckinDepartmentWise($request, $dep, $name)
    {
        $patientID = User::where('id', $request->patietnID)->first();
        if (!$patientID) {
            return $this->sendError("Patient Record Not Found");
        }

        if ($request->check_in_process == 'true') {
            if ($request->ischeckout == 'true' && $request->check_in_process == 'true') {
                $patientcheckinex = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('visit_id', $request->visit_id)->where('date', $request->todayDateUtc)->where('status', 'checkout')->where('patient_id', $request->patietnID)->first();
                $patientcheckinexk = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('visit_id', $request->visit_id)->where('date', $request->todayDateUtc)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();
                if ($patientcheckinex) {
                    return $this->sendError('You Already Checkout from this Department.', []);
                } else if ($patientcheckinexk == null) {
                    return $this->sendError('Checkin first', []);
                }

                if ($patientcheckinex == null && $patientcheckinexk != null) {
                    $patientrefer = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();
                    $refer = Referral::where('checkinID', $patientrefer->id)->first();
                    if (!$refer) {
                        return $this->sendError('Please refer first', []);
                    } else {

                        $patientcheckin = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('patient_id', $request->patietnID)->where('date', $request->todayDateUtc)->first();
                        $patientcheckin->check_out = Carbon::now('UTC');
                        $patientcheckin->status =  "checkout";
                        $patientcheckin->save();

                        $departementGet = DB::table('departments')->where('id', $dep)->first();

                        $staffID = User::where('id', $request->staff_id)->first();

                        $lastCurrenRunning = Queue::where('department_id', $request->department_id)
                            ->where('clinic_id', $staffID->clinic_id)
                            ->where('date', $request->todayDateUtc)
                            ->orderBy('id', 'DESC')
                            ->get();

                        $lastCurrenRunningGet = Queue::where('department_id', $request->department_id)
                            ->where('clinic_id', $staffID->clinic_id)
                            ->where('date', $request->todayDateUtc)
                            ->orderBy('id', 'DESC')
                            ->first();

                        foreach ($lastCurrenRunning as $item) {
                            if ($item->currenNumber == $lastCurrenRunningGet->queue_number) {
                            } else {
                                $item->currenNumber = $lastCurrenRunningGet->currenNumber + 1;
                                $item->is_current = '0';
                                $item->save();
                            }
                        }

                        $userDepartment = User::where('id', $request->staff_id)->first();
                        // if (isset($request->visit_id)) {
                        //     $staffID = User::where('id', $request->staff_id)->first();
                        //     $this->queueReferGeneration($request, $request->visit_id, $dep,  $request->patietnID, $staffID->clinic_id);
                        // }

                        // $this->queueReferGeneration($request->visit_id, $refer->referred_to,  $request->patietnID, $staffID->clinic_id);
                        // return $this->sendResponseData($patientcheckin, "CheckOut added Successfully.");

                        $checkRefer = VistsModel::where('id', $request->visit_id)->first();
                        $queue = Queue::where('visit_id',  $request->visit_id)
                            ->where('user_id', $request->patietnID)
                            ->where('department_id', $dep)
                            ->where('date', $request->todayDateUtc)
                            ->where('clinic_id', $departementGet->clinic_id)
                            ->orderBy('id', 'desc')
                            ->first();

                        $referral = Referral::with(["referredBy", "referredTo"])
                            ->where('visit_id', $request->visit_id)
                            ->orderBy('id', 'DESC')
                            ->first();

                        $response = [
                            'success' => true,
                            'responseData' => $patientcheckin,
                            'modelVisit' => $checkRefer,
                            'queue' => $queue,
                            'currentToken' => $queue->currenNumber,
                            'message' => 'Checkin added Successfully.',
                        ];
                        return response()->json($response, 200);
                    }
                }
            } else {
                $vistsModel = VistsModel::where('patientID', $request->patietnID)
                    // ->where('id', $request->visit_id)
                    ->where('date', $request->todayDateUtc)
                    ->where('status', '!=', 'complete')
                    ->orderBy('id', 'DESC')
                    ->first();

                if ($vistsModel) {
                    return $this->sendError('You Already checkin in this Department..', []);
                }
                $modelVisit = null;
                if ($request->visit_id) {
                    $vistID = $request->visit_id;
                } else {
                    $modelVisit = new VistsModel();
                    $use = User::where('id', $request->staff_id)->first();
                    $modelVisit->patientID = $request->patietnID;
                    $modelVisit->docID = $request->staff_id;
                    $modelVisit->date = $request->todayDateUtc;
                    $modelVisit->status = "new";
                    $modelVisit->doc_name = $use->name;
                    $modelVisit->save();
                    $vistID = $modelVisit->id;
                }


                $ISpatientcheck = PatientCheck::where('department_id', $dep)->where('status', 'checkin')->where('visit_id', $vistID)->where('patient_id', $request->patietnID)->where('date', $request->todayDateUtc)->orderBy('id', 'desc')->first();
                if (!$ISpatientcheck) {
                    $patientcheckin = new PatientCheck();
                    $patientcheckin->visit_id =  $vistID;
                    $patientcheckin->status =  "checkin";
                    $patientcheckin->department_id = $dep;
                    $patientcheckin->department = $name;
                    $patientcheckin->patient_id =  $request->patietnID;
                    $patientcheckin->staff_id = $request->staff_id;
                    $patientcheckin->check_in = Carbon::now('UTC');
                    $patientcheckin->type = $name;
                    $patientcheckin->date = $request->todayDateUtc;
                    $patientcheckin->save();

                    $userDepartment = User::where('id', $request->staff_id)->first();
                    if (isset($vistID)) {
                        $staffID = User::where('id', $request->staff_id)->first();
                        $this->queueReferGeneration($request, $vistID, $userDepartment->department_id,  $request->patietnID, $staffID->clinic_id);
                    }

                    $queue = Queue::where('visit_id',  $vistID)
                        ->where('user_id', $request->patietnID)
                        ->where('department_id', $dep)
                        ->where('date', $request->todayDateUtc)
                        ->where('clinic_id', $userDepartment->clinic_id)
                        ->orderBy('id', 'desc')
                        ->first();

                    $referral = Referral::with(["referredBy", "referredTo"])
                        ->where('visit_id', $vistID)
                        ->orderBy('id', 'DESC')
                        ->first();

                    $response = [
                        'success' => true,
                        'responseData' => $patientcheckin,
                        'modelVisit' => $modelVisit,
                        'queue' => $queue,
                        'currentToken' => $queue->queue_number,
                        'message' => 'Checkin added Successfully.',
                    ];
                    return response()->json($response, 200);
                }

                $patientcheckinExistin = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('status', 'checkin')->where('visit_id', $vistID)->where('patient_id', $request->patietnID)->first();
                if ($patientcheckinExistin) {
                    return $this->sendError('You Already checkin in this Department..', []);
                }

                $patientcheckinExistin1 = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('status', 'checkout')->where('visit_id', $vistID)->where('patient_id', $request->patietnID)->first();
                if ($patientcheckinExistin1) {
                    return $this->sendError('You Already checkout from this Department.', []);
                }

                $patientcheckin = new PatientCheck();
                $patientcheckin->visit_id =  $vistID;
                $patientcheckin->status =  "checkin";
                $patientcheckin->department_id = $dep;
                $patientcheckin->department = $name;
                $patientcheckin->patient_id =  $request->patietnID;
                $patientcheckin->staff_id = $request->staff_id;
                $patientcheckin->check_in = Carbon::now('UTC');
                $patientcheckin->type = $name;
                $patientcheckin->date = $request->todayDateUtc;
                $patientcheckin->save();


                $userDepartment = User::where('id', $request->staff_id)->first();
                if (isset($request->visit_id)) {
                    $staffID = User::where('id', $request->staff_id)->first();
                    $this->queueReferGeneration($request, $request->visit_id, $dep,  $request->patietnID, $staffID->clinic_id);
                }
                $queue = Queue::where('visit_id',  $vistID)
                    ->where('user_id', $request->patietnID)
                    ->where('department_id', $dep)
                    ->where('date', $request->todayDateUtc)
                    ->where('clinic_id', $userDepartment->clinic_id)
                    ->orderBy('id', 'desc')
                    ->first();

                $referral = Referral::with(["referredBy", "referredTo"])
                    ->where('visit_id', $vistID)
                    ->orderBy('id', 'DESC')
                    ->first();

                $response = [
                    'success' => true,
                    'responseData' => $patientcheckin,
                    'modelVisit' => $modelVisit,
                    'queue' => $queue,
                    'currentToken' => $queue->queue_number,
                    'message' => 'Checkin added Successfully.',
                ];
                return response()->json($response, 200);
            }
        } else if ($request->check_in_admin == 'true') {
            if ($request->ischeckout == 'true' && $request->check_in_admin == 'true') {
                $patientcheckinex = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('visit_id', $request->visit_id)->where('status', 'checkout')->where('patient_id', $request->patietnID)->first();
                $patientcheckinexk = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();
                if ($patientcheckinex) {
                    return $this->sendError('You Already Checkout from this Department.', []);
                }
                if ($patientcheckinexk == null) {
                    return $this->sendError('Checkin first', []);
                }
                if ($patientcheckinex == null && $patientcheckinexk != null) {
                    $patientrefer = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();
                    $refer = Referral::where('checkinID', $patientrefer->id)->first();
                    if (!$refer) {
                        return $this->sendError('Please refer first', []);
                    } else {

                        $patientcheckin = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();

                        $patientcheckin->check_out = Carbon::now('UTC');
                        $patientcheckin->status =  "checkout";
                        $patientcheckin->save();

                        $staffID = User::where('id', $request->staff_id)->first();

                        $lastCurrenRunning = Queue::where('department_id', $request->department_id)
                            ->where('clinic_id', $staffID->clinic_id)
                            // ->where('date', date('Y-m-d'))
                            ->where('date', $request->todayDateUtc)
                            ->orderBy('id', 'DESC')
                            ->get();

                        $lastCurrenRunningGet = Queue::where('department_id', $request->department_id)
                            ->where('clinic_id', $staffID->clinic_id)
                            // ->where('date', date('Y-m-d'))
                            ->where('date', $request->todayDateUtc)
                            ->orderBy('id', 'DESC')
                            ->first();

                        foreach ($lastCurrenRunning as $item) {
                            if ($item->currenNumber == $lastCurrenRunningGet->queue_number) {
                            } else {
                                $item->currenNumber = $lastCurrenRunningGet->currenNumber + 1;
                                $item->is_current = '0';
                                $item->save();
                            }
                        }
                        // $this->queueReferGeneration($request->visit_id, $refer->referred_to,  $request->patietnID, $staffID->clinic_id);
                        // return $this->sendResponseData($patientcheckin, "CheckOut added Successfully.");
                        $modelVisit = VistsModel::where('patientID', $request->patietnID)
                            ->where('id', $request->visit_id)
                            ->where('date', $request->todayDateUtc)
                            ->where('status', '!=', 'complete')
                            ->orderBy('id', 'DESC')
                            ->first();
                        return  $this->ChcekQueueToken($request, $dep, $patientcheckin, $request->visit_id, $modelVisit);
                    }
                }
            } else {
                $vistsModel = VistsModel::where('patientID', $request->patietnID)->where('date', $request->todayDateUtc)->orderBy('id', 'DESC')->first();
                if (!$vistsModel) {
                    return $this->sendError('Visit not exist', []);
                }

                $patientcheckinExistin = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('status', 'checkin')->where('visit_id', $request->visit_id)->where('patient_id', $request->patietnID)->first();
                $patientcheckinExistin1 = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('status', 'checkout')->where('visit_id', $request->visit_id)->where('patient_id', $request->patietnID)->first();
                if ($patientcheckinExistin) {
                    return $this->sendError('You Already checkin in this Department..', []);
                } else if ($patientcheckinExistin1) {
                    return $this->sendError('You Already checkout from this Department.', []);
                }

                $ISpatientcheck = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('status', 'checkin')->where('visit_id', $request->visit_id)->where('patient_id', $request->patietnID)->first();
                if (!$ISpatientcheck) {
                    $patientcheckin = new PatientCheck();
                    $patientcheckin->visit_id =  $request->visit_id;
                    $patientcheckin->status =  "checkin";
                    $patientcheckin->department_id = $dep;
                    $patientcheckin->department = $name;
                    $patientcheckin->patient_id =  $request->patietnID;
                    $patientcheckin->staff_id = $request->staff_id;
                    $patientcheckin->check_in = Carbon::now('UTC');
                    $patientcheckin->type = $name;
                    $patientcheckin->date = $request->todayDateUtc;
                    $patientcheckin->save();

                    if (isset($request->visit_id)) {
                        $staffID = User::where('id', $request->staff_id)->first();
                        $this->queueReferGeneration($request, $request->visit_id, $request->department_id,  $request->patietnID, $staffID->clinic_id);
                    }

                    $modelVisit = VistsModel::where('patientID', $request->patietnID)
                        ->where('id', $request->visit_id)
                        ->where('date', $request->todayDateUtc)
                        ->where('status', '!=', 'complete')
                        ->orderBy('id', 'DESC')
                        ->first();
                    return  $this->ChcekQueueToken2($request, $dep, $patientcheckin, $request->visit_id, $modelVisit);
                    // return $this->sendResponseData($patientcheckin, "Checkin added Successfully.");
                }

                $patientcheckin = new PatientCheck();
                $patientcheckin->visit_id =  $request->visit_id;
                $patientcheckin->status =  "checkin";
                $patientcheckin->department_id = $dep;
                $patientcheckin->department = $name;
                $patientcheckin->patient_id =  $request->patietnID;
                $patientcheckin->staff_id = $request->staff_id;
                $patientcheckin->check_in = Carbon::now('UTC');
                $patientcheckin->type = $name;
                $patientcheckin->date = $request->todayDateUtc;
                $patientcheckin->save();
                if (isset($request->visit_id)) {
                    $staffID = User::where('id', $request->staff_id)->first();
                    $this->queueReferGeneration($request, $request->visit_id, $request->department_id,  $request->patietnID, $staffID->clinic_id);
                }
                $modelVisit = VistsModel::where('patientID', $request->patietnID)
                    ->where('id', $request->visit_id)
                    ->where('date', $request->todayDateUtc)
                    ->where('status', '!=', 'complete')
                    ->orderBy('id', 'DESC')
                    ->first();
                return  $this->ChcekQueueToken2($request, $dep, $patientcheckin, $request->visit_id, $modelVisit);

                // return $this->sendResponseData($patientcheckin, "Checkin added Successfully.");
            }
        } else if ($request->check_in_vital == 'true') {
            if ($request->ischeckout == 'true' && $request->check_in_vital == 'true') {
                $patientcheckinex = PatientCheck::where('department_id', $dep)->where('date', $request->todayDateUtc)->where('visit_id', $request->visit_id)->where('status', 'checkout')->where('patient_id', $request->patietnID)->first();
                $patientcheckinexk = PatientCheck::where('department_id', $dep)->where('date', $request->todayDateUtc)->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();
                if ($patientcheckinex) {
                    return $this->sendError('You Already Checkout from this Department.', []);
                } else if ($patientcheckinexk == null) {
                    return $this->sendError('Checkin first', []);
                }

                if ($patientcheckinex == null && $patientcheckinexk != null) {
                    $patientrefer = PatientCheck::where('department_id', $dep)->where('date', $request->todayDateUtc)->orderBy('id', 'desc')->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();
                    $refer = Referral::where('checkinID', $patientrefer->id)->first();
                    if (!$refer) {
                        return $this->sendError('Please refer first', []);
                    } else {

                        $patientcheckin = PatientCheck::where('department_id', $dep)->where('date', $request->todayDateUtc)->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();
                        $patientcheckin->check_out = Carbon::now('UTC');
                        $patientcheckin->status =  "checkout";
                        $patientcheckin->save();


                        $staffID = User::where('id', $request->staff_id)->first();
                        $lastCurrenRunning = Queue::where('department_id', $request->department_id)
                            ->where('clinic_id', $staffID->clinic_id)
                            ->where('date', $request->todayDateUtc)
                            ->orderBy('id', 'DESC')
                            ->get();

                        $lastCurrenRunningGet = Queue::where('department_id', $request->department_id)
                            ->where('clinic_id', $staffID->clinic_id)
                            ->where('date', $request->todayDateUtc)
                            ->orderBy('id', 'DESC')
                            ->first();

                        foreach ($lastCurrenRunning as $item) {
                            if ($item->currenNumber == $lastCurrenRunningGet->queue_number) {
                            } else {
                                $item->currenNumber = $lastCurrenRunningGet->currenNumber + 1;
                                $item->is_current = '0';
                                $item->save();
                            }
                        }
                        // $this->queueReferGeneration($request->visit_id, $request->department_id,  $request->patietnID, $staffID->clinic_id);
                        // return $this->sendResponseData($patientcheckin, "CheckOut added Successfully.");
                        $modelVisit = VistsModel::where('patientID', $request->patietnID)
                            ->where('id', $request->visit_id)
                            ->where('date', $request->todayDateUtc)
                            ->where('status', '!=', 'complete')
                            ->orderBy('id', 'DESC')
                            ->first();

                        return $this->ChcekQueueToken($request, $dep, $patientcheckin, $request->visit_id, $modelVisit);
                    }
                }
            } else {

                $vistsModel = VistsModel::where('patientID', $request->patietnID)->orderBy('id', 'DESC')->first();
                if (!$vistsModel) {
                    return $this->sendError('Visit not exist', []);
                }

                $patientcheckinExistin = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('status', 'checkin')->where('visit_id', $request->visit_id)->where('patient_id', $request->patietnID)->first();
                $patientcheckinExistin1 = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('status', 'checkout')->where('visit_id', $request->visit_id)->where('patient_id', $request->patietnID)->first();
                if ($patientcheckinExistin) {
                    return $this->sendError('You Already checkin in this Department..', []);
                } else if ($patientcheckinExistin1) {
                    return $this->sendError('You Already checkout from this Department.', []);
                }

                $ISpatientcheck = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('status', 'checkin')->where('visit_id', $request->visit_id)->where('patient_id', $request->patietnID)->first();
                if (!$ISpatientcheck) {
                    $patientcheckin = new PatientCheck();
                    $patientcheckin->visit_id =  $request->visit_id;
                    $patientcheckin->status =  "checkin";
                    $patientcheckin->department_id = $dep;
                    $patientcheckin->department = $name;
                    $patientcheckin->patient_id =  $request->patietnID;
                    $patientcheckin->staff_id = $request->staff_id;
                    $patientcheckin->check_in = Carbon::now('UTC');
                    $patientcheckin->type = $name;
                    $patientcheckin->date = $request->todayDateUtc;
                    $patientcheckin->save();
                    if (isset($request->visit_id)) {
                        $staffID = User::where('id', $request->staff_id)->first();
                        $this->queueReferGeneration($request, $request->visit_id, $request->department_id,  $request->patietnID, $staffID->clinic_id);
                    }

                    $modelVisit = VistsModel::where('patientID', $request->patietnID)
                        ->where('id', $request->visit_id)
                        ->where('date', $request->todayDateUtc)
                        ->where('status', '!=', 'complete')
                        ->orderBy('id', 'DESC')
                        ->first();
                    return  $this->ChcekQueueToken2($request, $dep, $patientcheckin, $request->visit_id, $modelVisit);
                    // return $this->sendResponseData($patientcheckin, "Checkin added Successfully.");
                }


                $patientcheckin = new PatientCheck();
                $patientcheckin->visit_id =  $request->visit_id;
                $patientcheckin->status =  "checkin";
                $patientcheckin->department_id = $dep;
                $patientcheckin->department = $name;
                $patientcheckin->patient_id =  $request->patietnID;
                $patientcheckin->staff_id = $request->staff_id;
                $patientcheckin->check_in = Carbon::now('UTC');
                $patientcheckin->type = $name;
                $patientcheckin->date = $request->todayDateUtc;
                $patientcheckin->save();
                $modelVisit = VistsModel::where('patientID', $request->patietnID)
                    ->where('id', $request->visit_id)
                    ->where('date', $request->todayDateUtc)
                    ->where('status', '!=', 'complete')
                    ->orderBy('id', 'DESC')
                    ->first();
                return  $this->ChcekQueueToken2($request, $dep, $patientcheckin, $request->visit_id, $modelVisit);
                // return $this->sendResponseData($patientcheckin, "Checkin added Successfully.");
            }
        } else if ($request->check_in_refer == 'true') {
            if ($request->ischeckout == 'true' && $request->check_in_refer == 'true') {
                $patientcheckinex = PatientCheck::where('department_id', $dep)->where('date', $request->todayDateUtc)->orderBy('id', 'desc')->where('visit_id', $request->visit_id)->where('status', 'checkout')->where('patient_id', $request->patietnID)->first();
                $patientcheckinexk = PatientCheck::where('department_id', $dep)->where('date', $request->todayDateUtc)->orderBy('id', 'desc')->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();
                if ($patientcheckinex) {
                    return $this->sendError('You Already Checkout from this Department.', []);
                } else if ($patientcheckinexk == null) {
                    return $this->sendError('Checkin first', []);
                }

                $staffID = User::where('id', $request->staff_id)->first();

                $lastCurrenRunning = Queue::where('department_id', $request->department_id)
                    ->where('clinic_id', $staffID->clinic_id)
                    ->where('date', $request->todayDateUtc)
                    ->orderBy('id', 'DESC')
                    ->get();

                $lastCurrenRunningGet = Queue::where('department_id', $request->department_id)
                    ->where('clinic_id', $staffID->clinic_id)
                    ->where('date', $request->todayDateUtc)
                    ->orderBy('id', 'DESC')
                    ->first();

                foreach ($lastCurrenRunning as $item) {
                    if ($item->currenNumber == $lastCurrenRunningGet->queue_number) {
                    } else {
                        $item->currenNumber = $lastCurrenRunningGet->currenNumber + 1;
                        $item->is_current = '0';
                        $item->save();
                    }
                }
                $patientcheckin = PatientCheck::where('department_id', $dep)->where('date', $request->todayDateUtc)->orderBy('id', 'desc')->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();
                $patientcheckin->check_out = Carbon::now('UTC');
                $patientcheckin->status =  "checkout";
                $patientcheckin->save();

                $vistCom = VistsModel::where('id', $request->visit_id)->first();
                $vistCom->status = 'complete';
                $vistCom->patientID = $request->patietnID;
                $vistCom->save();

                // return $this->sendResponseData($patientcheckin, "CheckOut added Successfully.");
                $modelVisit = VistsModel::where('patientID', $request->patietnID)
                    ->where('id', $request->visit_id)
                    ->where('date', $request->todayDateUtc)
                    ->where('status', '!=', 'complete')
                    ->orderBy('id', 'DESC')
                    ->first();
                return $this->ChcekQueueToken($request, $dep, $patientcheckin, $request->visit_id, $modelVisit);
            } else {
                // checkin
                $vistsModel = VistsModel::where('patientID', $request->patietnID)->where('date', $request->todayDateUtc)->orderBy('id', 'DESC')->first();
                if (!$vistsModel) {
                    return $this->sendError('Visit not exist', []);
                }
                $patientcheckinExistin = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('status', 'checkin')->where('visit_id', $request->visit_id)->where('patient_id', $request->patietnID)->first();
                $patientcheckinExistin1 = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('date', $request->todayDateUtc)->where('status', 'checkout')->where('visit_id', $request->visit_id)->where('patient_id', $request->patietnID)->first();
                if ($patientcheckinExistin) {
                    return $this->sendError('You Already checkin in this Department..', []);
                } else if ($patientcheckinExistin1) {
                    return $this->sendError('You Already checkout from this Department..', []);
                }

                $ISpatientcheck = PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('status', 'checkin')->where('date', $request->todayDateUtc)->where('visit_id', $request->visit_id)->where('patient_id', $request->patietnID)->first();
                if (!$ISpatientcheck) {
                    $patientcheckin = new PatientCheck();
                    $patientcheckin->visit_id =  $request->visit_id;
                    $patientcheckin->status =  "checkin";
                    $patientcheckin->department_id = $dep;
                    $patientcheckin->department = $name;
                    $patientcheckin->patient_id =  $request->patietnID;
                    $patientcheckin->staff_id = $request->staff_id;
                    $patientcheckin->check_in = Carbon::now('UTC');
                    $patientcheckin->type = $name;
                    $patientcheckin->date = $request->todayDateUtc;
                    $patientcheckin->save();
                    if (isset($request->visit_id)) {
                        $staffID = User::where('id', $request->staff_id)->first();
                        $this->queueReferGeneration($request, $request->visit_id, $request->department_id,  $request->patietnID, $staffID->clinic_id);
                    } else {
                        $staffID = User::where('id', $request->staff_id)->first();
                        $visit = VistsModel::where('docID', $request->staff_id)->where('date', $request->todayDateUtc)->where('patientID', $request->patietnID)->where('status', '=', 'new')->orderBy('id', 'desc')->first();
                        $this->queueReferGeneration($request, $visit->id, $request->department_id,  $request->patietnID, $staffID->clinic_id);
                    }
                    $modelVisit = VistsModel::where('patientID', $request->patietnID)
                        ->where('id', $request->visit_id)
                        ->where('date', $request->todayDateUtc)
                        ->where('status', '!=', 'complete')
                        ->orderBy('id', 'DESC')
                        ->first();
                    return  $this->ChcekQueueToken2($request, $dep, $patientcheckin, $request->visit_id, $modelVisit);
                    // return $this->sendResponseData($patientcheckin, "Checkin added Successfully.");
                }

                $patientcheckin = new PatientCheck();
                $patientcheckin->visit_id =  $request->visit_id;
                $patientcheckin->status =  "checkin";
                $patientcheckin->department_id = $dep;
                $patientcheckin->department = $name;
                $patientcheckin->patient_id =  $request->patietnID;
                $patientcheckin->staff_id = $request->staff_id;
                $patientcheckin->check_in =  Carbon::now('UTC');
                $patientcheckin->type = $name;
                $patientcheckin->date = $request->todayDateUtc;
                $patientcheckin->save();
                if (isset($request->visit_id)) {
                    $staffID = User::where('id', $request->staff_id)->first();
                    $this->queueReferGeneration($request, $request->visit_id, $request->department_id,  $request->patietnID, $staffID->clinic_id);
                } else {
                    $staffID = User::where('id', $request->staff_id)->first();
                    $visit = VistsModel::where('docID', $request->staff_id)->where('date', $request->todayDateUtc)->where('patientID', $request->patietnID)->where('status', '=', 'new')->orderBy('id', 'desc')->first();
                    $this->queueReferGeneration($request, $visit->id, $request->department_id,  $request->patietnID, $staffID->clinic_id);
                }
                $modelVisit = VistsModel::where('patientID', $request->patietnID)
                    ->where('id', $request->visit_id)
                    ->where('date', $request->todayDateUtc)
                    ->where('status', '!=', 'complete')
                    ->orderBy('id', 'DESC')
                    ->first();
                return  $this->ChcekQueueToken2($request, $dep, $patientcheckin, $request->visit_id, $modelVisit);
                // return $this->sendResponseData($patientcheckin, "Checkin added Successfully.");
            }
        }
    }


    // CheckOUT
    public function CHeckINProcessOUT($request, $dep)
    {
        return PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('visit_id', $request->visit_id)->where('date', $request->todayDateUtc)->where('status', 'checkout')->where('patient_id', $request->patietnID)->first();
    }
    // CheckIN
    public function CHeckINProcessIN($request, $dep)
    {
        return PatientCheck::where('department_id', $dep)->orderBy('id', 'desc')->where('visit_id', $request->visit_id)->where('date', $request->todayDateUtc)->where('status', 'checkin')->where('patient_id', $request->patietnID)->first();
    }

    public function queueReferGeneration($request, $visit_id, $dep, $userId, $clinic)
    {
        try {
            $department = Department::where('id', $dep)->first();
            $lastQueue = Queue::where('department_id', $dep)->where('date', $request->todayDateUtc)->max('queue_number');

            $lastCurren = Queue::where('department_id', $dep)->where('date', $request->todayDateUtc)->where('is_current', '1')->where('clinic_id', $clinic)->orderBy('id', 'DESC')->first();
            $lastCurrenRunning = Queue::where('department_id', $dep)->where('is_current', '0')->where('date', $request->todayDateUtc)->where('clinic_id', $clinic)->orderBy('id', 'DESC')->first();

            if ($lastCurren) {
                if ($lastCurrenRunning) {
                    $lastQueueNumber = (($lastCurrenRunning->queue_number) ? $lastCurrenRunning->queue_number : 0);
                    $latestQueuNumber = ($lastQueueNumber + 1);
                } else {

                    $lastQueueNumber = (($lastCurren->queue_number) ? $lastCurren->queue_number : 0);
                    $latestQueuNumber = ($lastQueueNumber + 1);
                }
                $newQueueEntry = new Queue();
                $newQueueEntry->department_id = $department->id;
                $newQueueEntry->visit_id = $visit_id ?? null;
                $newQueueEntry->department_name = $department->name;
                $newQueueEntry->clinic_id = $clinic;
                $newQueueEntry->user_id = $userId;
                $newQueueEntry->date = $request->todayDateUtc;
                if ($latestQueuNumber == 1) {
                    $newQueueEntry->is_current = true;
                } else {
                    $newQueueEntry->is_current = false;
                }
                $newQueueEntry->queue_number = $latestQueuNumber;
                $newQueueEntry->currenNumber = $lastCurren->queue_number;
                $newQueueEntry->save();
            } else {
                $lastQueueNumber = (($lastQueue) ? $lastQueue : 0);
                $latestQueuNumber = ($lastQueueNumber + 1);
                $newQueueEntry = new Queue();
                $newQueueEntry->department_id = $department->id;
                $newQueueEntry->visit_id = $visit_id ?? null;
                $newQueueEntry->department_name = $department->name;
                $newQueueEntry->clinic_id = $clinic;
                $newQueueEntry->user_id = $userId;
                $newQueueEntry->date = $request->todayDateUtc;
                if ($latestQueuNumber == 1) {
                    $newQueueEntry->is_current = true;
                } else {
                    $newQueueEntry->is_current = false;
                }
                $newQueueEntry->queue_number = $latestQueuNumber;
                $newQueueEntry->currenNumber = $latestQueuNumber;
                $newQueueEntry->save();
            }
            return $this->sendResponse(["queue_number" => $newQueueEntry->queue_number], "Queue generated successfully.");
        } catch (Exception $e) {
            return $this->sendError('Error', [], 500);
        }
    }

    public function ChcekQueueToken($request, $dep, $patientcheckin, $vistID, $modelVisit)
    {
        $userDepartment = User::where('id', $request->staff_id)->first();
        // if (isset($vistID)) {
        //     $staffID = User::where('id', $request->staff_id)->first();
        //     $this->queueReferGeneration($vistID, $userDepartment->department_id,  $request->patietnID, $staffID->clinic_id);
        // }

        $queue = Queue::where('visit_id',  $vistID)
            ->where('department_id', $dep)
            ->where('date', $request->todayDateUtc)
            ->where('clinic_id', $userDepartment->clinic_id)
            ->orderBy('id', 'desc')
            ->first();

        $currenttoken = Queue::where('department_id', $request->department_id)
            ->orderBy('id', 'DESC')
            ->where('date', $request->todayDateUtc)
            ->first();

        $referral = Referral::with(["referredBy", "referredTo"])
            ->where('visit_id', $vistID)
            ->where('clinic_id', $userDepartment->clinic_id)
            ->where('doc_id', $userDepartment->staff_id)
            ->orderBy('id', 'DESC')
            ->first();

        $response = [
            'success' => true,
            'responseData' => $patientcheckin,
            'modelVisit' => $modelVisit,
            'referral' => $referral,
            'queue' => $queue,
            'currentToken' => $currenttoken->currenNumber,
            'message' => 'Checkin added Successfully.',
        ];
        Log::info('Queue' . $queue);
        Log::info('Queue Number' . $currenttoken->currenNumber);
        return response()->json($response, 200);
    }
    public function ChcekQueueToken2($request, $dep, $patientcheckin, $vistID, $modelVisit)
    {
        $userDepartment = User::where('id', $request->staff_id)->first();
        // if (isset($vistID)) {
        //     $staffID = User::where('id', $request->staff_id)->first();
        //     $this->queueReferGeneration($vistID, $userDepartment->department_id,  $request->patietnID, $staffID->clinic_id);
        // }

        $queue = Queue::where('visit_id',  $vistID)
            ->where('department_id', $dep)
            ->where('date', $request->todayDateUtc)
            ->where('clinic_id', $userDepartment->clinic_id)
            ->orderBy('id', 'desc')
            ->first();

        $currenttoken = Queue::where('department_id', $request->department_id)
            ->orderBy('id', 'DESC')
            ->where('date', $request->todayDateUtc)
            ->first();

        $referral = Referral::with(["referredBy", "referredTo"])
            ->where('visit_id', $vistID)
            ->where('clinic_id', $userDepartment->clinic_id)
            ->where('doc_id', $userDepartment->staff_id)
            ->orderBy('id', 'DESC')
            ->first();

        $response = [
            'success' => true,
            'responseData' => $patientcheckin,
            'modelVisit' => $modelVisit,
            'referral' => $referral,
            'queue' => $queue,
            'currentToken' => $currenttoken->queue_number,
            'message' => 'Checkin added Successfully.',
        ];
        Log::info('Queue' . $queue);
        Log::info('Queue Number' . $currenttoken->currenNumber);
        return response()->json($response, 200);
    }
    // public function queueReferGeneration($visit_id, $dep, $userId, $clinic)
    // {
    //     try {
    //         $department = Department::where('id', $dep)->first();
    //         $lastQueue = Queue::where('department_id', $dep)->where('date', date('Y-m-d'))->max('queue_number');

    //         $lastCurren = Queue::where('department_id', $dep)->where('date', date('Y-m-d'))->where('is_current', '1')->where('clinic_id', $clinic)->orderBy('id', 'DESC')->first();
    //         $lastCurrenRunning = Queue::where('department_id', $dep)->where('is_current', '0')->where('clinic_id', $clinic)->orderBy('id', 'DESC')->first();

    //         if ($lastCurren) {
    //             if ($lastCurrenRunning) {
    //                 $lastQueueNumber = (($lastCurrenRunning->queue_number) ? $lastCurrenRunning->queue_number : 0);
    //                 $latestQueuNumber = ($lastQueueNumber + 1);
    //             } else {

    //                 $lastQueueNumber = (($lastCurren->queue_number) ? $lastCurren->queue_number : 0);
    //                 $latestQueuNumber = ($lastQueueNumber + 1);
    //             }
    //             $newQueueEntry = new Queue();
    //             $newQueueEntry->department_id = $department->id;
    //             $newQueueEntry->visit_id = $visit_id ?? null;
    //             $newQueueEntry->department_name = $department->name;
    //             $newQueueEntry->clinic_id = $clinic;
    //             $newQueueEntry->user_id = $userId;
    //             $newQueueEntry->date = $request->todayDateUtc;
    //             if ($latestQueuNumber == 1) {
    //                 $newQueueEntry->is_current = true;
    //             } else {
    //                 $newQueueEntry->is_current = false;
    //             }
    //             $newQueueEntry->queue_number = $latestQueuNumber;
    //             $newQueueEntry->currenNumber = $lastCurren->queue_number;
    //             $newQueueEntry->save();
    //         } else {
    //             $lastQueueNumber = (($lastQueue) ? $lastQueue : 0);
    //             $latestQueuNumber = ($lastQueueNumber + 1);
    //             $newQueueEntry = new Queue();
    //             $newQueueEntry->department_id = $department->id;
    //             $newQueueEntry->visit_id = $visit_id ?? null;
    //             $newQueueEntry->department_name = $department->name;
    //             $newQueueEntry->clinic_id = $clinic;
    //             $newQueueEntry->user_id = $userId;
    //             $newQueueEntry->date = $request->todayDateUtc;
    //             if ($latestQueuNumber == 1) {
    //                 $newQueueEntry->is_current = true;
    //             } else {
    //                 $newQueueEntry->is_current = false;
    //             }
    //             $newQueueEntry->queue_number = $latestQueuNumber;
    //             $newQueueEntry->currenNumber = $latestQueuNumber;
    //             $newQueueEntry->save();
    //         }
    //         return $this->sendResponse(["queue_number" => $newQueueEntry->queue_number], "Queue generated successfully.");
    //     } catch (Exception $e) {
    //         return $this->sendError('Error', [], 500);
    //     }
    // }

    public function PatientVists(Request $request)
    {
        try {
            $res = Validator::make($request->all(), [
                'patietnid' => 'required|exists:users,id',
                // 'docID' => 'required|exists:users,id',
                'date' => 'required',
                'description' => 'required',
            ]);

            foreach ($res->errors()->toArray() as $field => $message) {
                $errors[] = [
                    'message' => $message[0],
                ];
            }
            if (isset($errors)) {
                return $this->sendError('Validation Error.', $errors);
            };

            $vists = new VistsModel();

            $vists->patientID = $request->patietnid;
            $vists->docID = $request->docID;

            $user = User::where('id', $request->docID)->first();
            $vists->date = $request->date;
            $vists->desc = $request->description;
            $vists->doc_name = $user->name;
            $vists->docID = $user->id;
            $vists->status = "new";
            $vists->typeid = $request->visttype;
            if (isset($request->parientID)) {
                $vists->parientID = $request->parientID;
            }
            if ($vists->save()) {
                return $this->sendResponseData($vists, "Patient Vist added Successfully.");
            }
            $this->SendErrorLog("Error PatientVists Not save", "error");
            return $this->sendError("Not save", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error PatientVists exception", "error");
        }
    }

    public function PatientVistsByID(Request $request, $patientID)
    {
        try {
            $checkVists = VistsModel::where('patientID', $patientID)->orderBy('id', 'DESC')->first();
            if (!$checkVists) {
                return $this->sendResponseData(null, "Patient Vists already exist.");
            } else {
                if ($checkVists->status == "complete") {
                    return $this->sendResponseData([], "Patient No Visit Found.");
                } else {
                    return $this->sendResponseData($checkVists, "Continue Appointment.");
                }
            }
        } catch (Exception $e) {
            return $this->Expection($e, "Error PatientVistsByID exception", "error");
        }
    }
    public function PatientVistsStatus(Request $request)
    {
        try {
            $vists =  VistsModel::where('id', $request->vist_id)->first();
            $vists->patientID = $request->patientID;
            $vists->status = $request->status;
            if ($vists->save()) {
                return $this->sendResponseData($vists, "Patient Vist Status Change Successfully.");
            }
            $this->SendErrorLog("Error PatientVistsStatus Not save", "error");
            return $this->sendError("Not Save", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error PatientVistsStatus exception", "error");
        }
    }

    public function GetPatientVists($patientID)
    {
        try {
            $vists =  VistsModel::where('patientID', $patientID)->get();
            if ($vists->count() > 0) {
                return $this->sendResponseData($vists, "Patient Vist List.");
            }

            $this->SendErrorLog("Error GetPatientVists Not Found", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error GetPatientVists exception", "error");
        }
    }

    public function PatientHistory(Request $request, $patientID)
    {
        try {
            $users = User::with(['vists', 'referral', 'checkInAndOut', 'test'])
                ->where('id', $patientID)
                ->first();
            if ($users->count() > 0) {
                return $this->sendResponseData($users, "Patient History.");
            }
            $this->SendErrorLog("Error PatientHistory Not Found", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error PatientHistory exception", "error");
        }
    }

    public function PatientTest(Request $request, $patientID)
    {
        try {
            $users = User::with(['usersdetail', 'test'])
                ->where('id', $patientID)
                ->first();
            if ($users->count() > 0) {
                return $this->sendResponseData($users, "Patient Test History.");
            }
            $this->SendErrorLog("Error PatientTest Not Found", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error PatientTest exception", "error");
        }
    }

    public function Familymedicalhistory($patientID)
    {
        try {
            $users = User::with('usersdetail', 'MedicalFamily')
                ->where('id', $patientID)
                ->first();
            if ($users->count() > 0) {
                return $this->sendResponseData($users, "Patient Family Medical History.");
            }
            $this->SendErrorLog("Error Familymedicalhistory Not Found", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error Familymedicalhistory exception", "error");
        }
    }
    public function AddFamilymedicalhistory(Request $request)
    {
        try {
            $res = Validator::make($request->all(), [
                'disease_id' => 'required',
                'patientID' => 'required',
            ]);
            foreach ($res->errors()->toArray() as $field => $message) {
                $errors[] = [
                    'message' => $message[0],
                ];
            }
            if (isset($errors)) {
                return $this->sendError('Validation Error.', $errors);
            }

            $exist = FamilyMedicalHistory::where('patientID', $request->patientID)->delete();

            foreach ($request->disease_id as $index => $item) {

                $exist = FamilyMedicalHistory::where('patientID', $request->patientID)->where('disease_id', $item)->first();
                if ($exist) {
                    $exist->patientID = $request->patientID;
                    $exist->disease_id = $item;
                    $disease = Disease::where('id', $item)->first();
                    $exist->diseasen_name = $disease->name;
                    $exist->save();
                } else {
                    $familyMedical = new FamilyMedicalHistory();
                    $familyMedical->patientID = $request->patientID;
                    $familyMedical->disease_id = $item;
                    $disease = Disease::where('id', $item)->first();
                    $familyMedical->diseasen_name = $disease->name;
                    $familyMedical->save();
                }
            }

            return $this->sendResponseData([], "Patient Family Medical History.");
        } catch (Exception $e) {
            return $this->Expection($e, "Error Familymedicalhistory exception", "error");
        }
    }

    public function Familymedicalhistorylist()
    {
        try {
            $family = Disease::where('is_familymedical', '1')->select('id', 'name')->get();
            if ($family->count() > 0) {
                return $this->sendResponseData($family, "Family Medical History List.");
            }
            $this->SendErrorLog("Error Familymedicalhistorylist Not Found", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error Familymedicalhistorylist exception", "error");
        }
    }

    // Know Chronic Diseases
    public function KnownChronicDisease()
    {
        try {
            $family = Disease::where('is_familymedical', '2')->select('id', 'name', 'answer')->get();
            if ($family->count() > 0) {
                return $this->sendResponseData($family, "Know Chronic Diseases List.");
            }
            $this->SendErrorLog("Error KnownChronicDisease Not Found", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error KnownChronicDisease exception", "error");
        }
    }

    public function PatientKnownChronicDisease($patientID)
    {
        try {
            return $family = KnownChronicQuestion::with('question')->where('is_knowchronic', '1')->where('user_id', $patientID)->get();
        } catch (Exception $e) {
            return $this->Expection($e, "Error Familymedicalhistorylist exception", "error");
        }
    }
    public function KnownChronicDiseaseAdd(Request $request)
    {
        try {
            foreach ($request->questions_id as $index => $item) {

                $RishFactor = KnownChronicQuestion::where('questions_id', $request->questions_id[$index])->first();
                if ($RishFactor) {
                    $RishFactor->answer = $request->answer[$index];
                    $RishFactor->user_id = $request->user_id;
                    $RishFactor->options = $request->options;
                    $RishFactor->is_knowchronic = 1;
                    $RishFactor->diagonse_year = $request->diagonse_year[$index];
                    $RishFactor->current_medication = $request->current_medication[$index];
                    $RishFactor->save();
                } else {
                    $familyMedical = new KnownChronicQuestion();
                    $familyMedical->questions_id = $request->questions_id[$index];
                    $familyMedical->answer = $request->answer[$index];
                    $familyMedical->user_id = $request->user_id;
                    $familyMedical->options = $request->options;
                    $familyMedical->is_knowchronic = 1;
                    $familyMedical->diagonse_year = $request->diagonse_year[$index];
                    $familyMedical->current_medication = $request->current_medication[$index];
                    $familyMedical->save();
                }
            }

            return $this->sendResponseData([], "Known Chronic Disease Added.");
        } catch (Exception $e) {
            return $this->Expection($e, "Error KnownChronicDiseaseAdd exception", "error");
        }
    }

    public function ViewRiskFactor()
    {
        try {
            $question = RishFactorQuestions::all();
            if (count($question) > 0) {
                return $this->sendResponseData($question, "Risk Factor Question.");
            }
            $this->SendErrorLog("Error ViewRiskFactor Not Found", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error ViewRiskFactor exception", "error");
        }
    }
    public function AddRiskFactor(Request $request)
    {
        try {
            $res = Validator::make($request->all(), [
                'user_id' => 'required|exists:users,id',
                'questions_id' => 'required',
                'ans' => 'required',
            ]);

            foreach ($res->errors()->toArray() as $field => $message) {
                $errors[] = [
                    'message' => $message[0],
                ];
            }
            if (isset($errors)) {
                return $this->sendError('Validation Error.', $errors);
            };


            foreach ($request->questions_id as $key => $value) {
                $RishFactor = RishFactor::where('riskfactor_question', $request->questions_id[$key])->first();
                if ($RishFactor) {
                    $RishFactor->patient_id = $request->user_id;
                    $RishFactor->patient_id = $request->user_id;
                    $RishFactor->riskfactor_question = $request->questions_id[$key];
                    $RishFactor->ans = $request->ans[$key];
                    $question = RishFactorQuestions::where('id', $request->questions_id[$key])->first();
                    $RishFactor->risk_category_id = $question->id;
                    $RishFactor->risk_category_name = $question->questions;
                    $RishFactor->save();
                } else {
                    $riskfactor = new RishFactor();
                    $riskfactor->patient_id = $request->user_id;
                    $riskfactor->riskfactor_question = $request->questions_id[$key];
                    $riskfactor->ans = $request->ans[$key];
                    $question = RishFactorQuestions::where('id', $request->questions_id[$key])->first();
                    $riskfactor->risk_category_id = $question->id;
                    $riskfactor->risk_category_name = $question->questions;
                    $riskfactor->save();
                }
            }

            return $this->sendResponseData([], "Patient Risk Factor Added Successfully.");
        } catch (Exception $e) {
            return $this->Expection($e, "Error AddRiskFactor exception", "error");
        }
    }

    public function UpdateFactorPatient($id)
    {
        try {
        } catch (Exception $e) {
            return $this->Expection($e, "Error Familymedicalhistorylist exception", "error");
        }
        $rish = RishFactor::where('patient_id', $id)->get();
        return $this->sendResponseData($rish, "Patient Risk Factor.");
    }

    public function GetPatientDetail(Request $request)
    {

        try {
            $user = User::where('department_id', $request->department_id)->where('role_id', $request->roleId)->first();
            if ($request->roleId == "4" && $user) {
                $visist1 = VistsModel::where('patientID', $request->patietnID)->where('date', $request->todayDateUtc)->orderBy('id', 'DESC')->where('status', '=', 'new')->first();
                $visist2 = VistsModel::where('patientID', $request->patietnID)->where('date', $request->todayDateUtc)->orderBy('id', 'DESC')->where('status', '=', 'complete')->first();
                if (isset($visist1)) {
                    $visist = $visist1;
                } else if ($visist2) {
                    $visist = $visist2;
                }
            } else {
                $visist = VistsModel::where('patientID', $request->patietnID)->orderBy('id', 'DESC')->where('date', $request->todayDateUtc)->where('status', '!=', 'complete')->where('status', '!=', 'canceled')->first();
            }

            if (isset($visist)) {

                $patient = User::with('usersdetail')
                    ->where('id', $request->patietnID)
                    ->first();

                $referral = Referral::with(["referredBy", "referredTo"])
                    ->where('visit_id', $visist->id)
                    ->orderBy('id', 'DESC')
                    ->first();


                $patinentcheckount = PatientCheck::where('visit_id', $visist->id)
                    ->where('patient_id', $request->patietnID)
                    ->where('department_id', $request->department_id)
                    ->where('status', 'checkout')
                    ->orderBy('id', 'DESC')
                    ->where('date', $request->todayDateUtc)
                    ->first();

                $queue = Queue::where('user_id', $request->patietnID)
                    ->where('department_id', $request->department_id)
                    ->orderBy('id', 'DESC')
                    ->where('date', $request->todayDateUtc)
                    // ->where('visit_id', $visist->id)
                    ->first();

                $currenttoken = Queue::where('department_id', $request->department_id)
                    ->orderBy('id', 'DESC')
                    ->where('date', $request->todayDateUtc)
                    ->first();

                $appointment = Appointment::where('visit_id', $visist->id)->orderBy('id', 'desc')->first();

                return response()->json(
                    [
                        'success' => true,
                        'data' => [
                            'patient' => $patient,
                            'vist' => $visist,
                            'referral' => $referral ?? null,
                            'patinentcheckinount' => $patinentcheckount ?? null,
                            'Queue' => $queue ?? null,
                            'currentToken' => $currenttoken->queue_number ?? null,
                            'appointment' => $appointment ?? null,
                        ],
                        'message' => "Patient Data",
                    ]
                );
            } else {


                $patient = User::with('usersdetail')
                    ->where('id', $request->patietnID)
                    ->first();


                $vists = null;


                $referral = null;

                $patinentcheckount = null;

                $queue =  Queue::where('department_id', $request->department_id)
                    ->orderBy('id', 'DESC')
                    ->where('date', $request->todayDateUtc)
                    ->where('user_id', $request->patietnID)
                    ->first();

                $currenttoken = Queue::where('department_id', $request->department_id)
                    ->orderBy('id', 'DESC')
                    ->where('date', $request->todayDateUtc)
                    ->first();

                $appointment = null;

                return response()->json(
                    [
                        'success' => true,
                        'data' => [
                            'patient' => $patient,
                            'vist' => $vists,
                            'referral' => $referral,
                            'patinentcheckinount' => $patinentcheckount,
                            'Queue' => $queue,
                            'currentToken' => $currenttoken->queue_number ?? null,
                            'appointment' => $appointment ?? null,
                        ],
                        'message' => "Patient Data",
                    ]
                );
            }
        } catch (Exception $e) {
            return $this->Expection($e, "Error getPatientDetail exception", "error");
        }
    }

    public function CHWscreenQuestions()
    {
        try {
            $ques = CHWQuestionScreening::select('*')->get();
            $question =  $ques->groupBy('category');
            if ($question->count() > 0) {
                return $this->sendResponse($question, "Question list");
            }
            $this->SendErrorLog("Error CHWscreenQuestions Not Found", "error");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error CHWscreenQuestions exception", "error");
        }
    }
    public function AddCHWscreenQuestions(Request $request)
    {
        try {
            try {
                $string = $request->input('jsonData');
            } catch (Exception $e) {
                return $this->Expection($e, "Error $request->input('jsonData') exception", "error");
            }
            $data = json_decode($string, true);
            $use = User::where('id', $data['staff_id'])->first();
            if (!$use) {
                return $this->sendError('Staff not exist.', []);
            }
            $modelVisit = new VistsModel();

            $modelVisit->patientID = $data['patient_id'] ?? null;
            $modelVisit->docID = $data['staff_id'] ?? null;
            $modelVisit->date = $data['appointmentDate'] ?? null;
            $modelVisit->status = "new" ?? null;
            $modelVisit->doc_name = $use->name ?? null;
            $modelVisit->isCHW = $data['isChw'] ?? null;
            $modelVisit->appointment = $data['appointmentDate'] ?? null;
            $modelVisit->save();

            $visitID = $modelVisit->id;
            $modelVists = VistsModel::where('id', $visitID)->first();
            $patient = User::with('usersdetail')->where('id', $data['patient_id'])->first();

            if (isset($data['tb_test_assigned'])) {
                if ($data['tb_test_assigned'] == 'Digital Chest X-Ray') {
                    $getTest = TestAll::where('id', '7')->first();
                    $test = new Test();
                    $test->visit_id = $visitID;
                    $test->name = $getTest->name;
                    $test->patient_id = $data['patient_id'];
                    $test->isChw = '1';
                    $test->category = $getTest->name;
                    $test->category_id = $getTest->id;
                    $test->nurse_id = $data['staff_id'];
                    $staffName = User::where('id', $data['staff_id'])->first();
                    $test->doc_name = $staffName->name;


                    if ($request->hasFile('hivData')) {
                        $file = $request->file('hivData');
                        $file = $request->file('tbConsentData');
                        $extension = $file->getClientOriginalExtension();
                        $imagepath = time() . '.' . $extension;
                        $file->move(public_path('images'), $imagepath);
                        $image = asset('images') . "/" . $imagepath;
                        $test->gene_expert = $image;
                    }
                    $test->save();
                }
                if ($data['tb_test_assigned'] == 'GENEXPERT TEST') {
                    $getTest = TestAll::where('id', '7')->first();
                    $test = new Test();
                    $test->visit_id = $visitID;
                    $test->name = $getTest->name;
                    $test->patient_id = $data['patient_id'];
                    $test->isChw = '1';
                    $test->category = $getTest->name;
                    $test->category_id = $getTest->id;
                    $test->nurse_id = $data['staff_id'];
                    $staffName = User::where('id', $data['staff_id'])->first();
                    $test->doc_name = $staffName->name;

                    if ($request->hasFile('tbConsentData')) {
                        $file = $request->file('tbConsentData');
                        $file = $request->file('tbConsentData');
                        $extension = $file->getClientOriginalExtension();
                        $imagepath = time() . '.' . $extension;
                        $file->move(public_path('images'), $imagepath);
                        $image = asset('images') . "/" . $imagepath;
                        $test->x_ray = $image;
                    }
                    $test->save();
                }
            }


            if (isset($data['hiv_rapid_test_answer'])) {
                if ($data['hiv_rapid_test_answer'] == 'Yes') {
                    $this->TestAddCHW($visitID, $data, 9);
                }
            }

            if (isset($data['hiv_pre_test'])) {
                if ($data['hiv_pre_test'] == 'Yes') {
                    $this->TestAddCHW($visitID, $data, 4);
                }
            }
            $this->CallCHW($visitID, $data, $data['questionIdTbYesNoArray'],  $data['answersTbYesNoArray'], 2, $request);
            $this->CallCHW($visitID, $data, $data['questionIdTbCheckedArray'],  $data['answersTbCheckedArray'], 2, $request);
            $this->CallCHW($visitID, $data, $data['questionIdCovidArray'],  $data['answersCovidArray'], 1, $request);
            $this->CallCHW($visitID, $data, $data['questionIdStiArray'],  $data['answersStiArray'], 5, $request);
            $this->CallCHW($visitID, $data, $data['questionIdHivArray'],  $data['answersHivArray'], 4, $request);

            return response()->json(
                [
                    'success' => true,
                    'data' => [
                        'patient' => $patient,
                        'visit' =>  $modelVists,
                    ],
                    'message' => "Patient Data",
                ]
            );
        } catch (Exception $e) {
            return $this->Expection($e, "Error AddCHWscreenQuestions exception", "error");
        }
    }

    public function CallCHW($visitID, $data, $dataList, $answerList, $test, $request)
    {
        try {
            if (isset($dataList)) {
                foreach ($dataList as $key => $value) {
                    $chwQuestion = CHWQuestionScreening::where('id', $value)->first();
                    $checkAdd = new CHWQuestionScreeningAnswer();
                    $checkAdd->date = date('Y-m-d');
                    $checkAdd->category_id = $chwQuestion->category_id;
                    $checkAdd->category = $chwQuestion->category;
                    $checkAdd->staff_id = $data['staff_id'];
                    $checkAdd->patient_id = $data['patient_id'];
                    $checkAdd->visit_id = $visitID;
                    $checkAdd->question_id = $value;
                    $checkAdd->answer =  $answerList[$key] ?? "";
                    $checkAdd->save();
                }
            }
        } catch (Exception $e) {
            return $this->Expection($e, "Error CallCHW exception", "error");
        }
    }

    public function TestAddCHW($visitID, $request, $test)
    {
        try {
            $getTest = TestAll::where('id', $test)->first();
            $test = new Test();
            $test->visit_id = $visitID;
            $test->name = $getTest->name;
            $test->patient_id = $request['patient_id'];
            $test->isChw = '1';
            $test->category = $getTest->name;
            $test->category_id = $getTest->id;
            $test->nurse_id = $request['staff_id'];
            $staffName = User::where('id', $request['staff_id'])->first();
            $test->doc_name = $staffName->name;
            $test->save();
        } catch (Exception $e) {
            return $this->Expection($e, "Error Familymedicalhistorylist exception", "error");
        }
    }

    public function AddCHWRefered(Request $request)
    {
        try {
            $referral = new Referral();
            $referral->visit_id = $request->visit_id;
            $referral->patient_id = $request->patient_id;
            $referral->referred_by = $request->referred_by;
            $referral->referred_to = $request->referred_to;
            $referral->remarks = $request->remarks;
            $referral->status = $request->status;
            if ($request->isCHW == true) {
                $referral->isCHW = '1';
            } else {
                $referral->isCHW = '0';
            }
            if ($referral->save()) {
                $referral = Referral::with(["referredBy", "referredTo"])->where('id', $referral->id)->get();
                return $this->sendResponseData($referral, "Record Reffered Successfully.");
            }
            $this->SendErrorLog("Error AddCHWRefered Not Save", "error");
            return $this->sendError("Not Save", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error AddCHWRefered exception", "error");
        }
    }
    public function sendEmail(Request $request)
    {
        $email=$request->email;
        $message=$request->message;
        $subj=$request->subj;
        try {
            Log::info('Send Email' . $email);
            return Mail::raw($message, function ($message) use ($email, $subj) {
                $message->to($email)
                    ->subject($subj);
            });
        } catch (\Exception $e) {
            return $e;
        }
    }
}
