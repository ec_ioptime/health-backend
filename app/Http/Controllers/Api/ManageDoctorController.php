<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponseTrait;

class ManageDoctorController extends Controller
{
    use ApiResponseTrait;
    public function getDoctors($clinicID)
    {
        $doctors = User::where('is_doctor', true)->where("clinic_id", $clinicID)->get();
        return $this->sendResponse($doctors, []);
    }
}
