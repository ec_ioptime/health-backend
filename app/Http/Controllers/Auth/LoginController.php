<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login(Request $request)
    {
        $input = $request->all();

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if (auth()->attempt(array('email' => $input['email'], 'password' => $input['password']))) {
            if (auth()->user()->deactivated == 1) {
                return redirect()->route('login')
                    ->with('error', 'Email-Address And Password Are Wrong.');
            }
            if (auth()->user()->is_admin == 1) {
                Log::info(auth()->user()->email . " Logged in successfully.");
                return redirect()->route('home');
            } else if (auth()->user()->is_staff == 1) {
                Log::info(auth()->user()->email . " Logged in successfully.");
                return redirect()->route('staffDashboard');
            } else if (auth()->user()->is_doctor == 1) {
                Log::info(auth()->user()->email . " Logged in successfully.");
                return redirect()->route('doctorDashboard');
            } else {
                Auth::logout();
                Log::info(" Incorrect credentials.");
                return redirect()->route('login')
                    ->with('error', 'Email-Address And Password Are Wrong.');
            }
        } else {
            return redirect()->route('login')->with('error', 'Email-Address And Password Are Wrong.');
        }
    }
    public function clearCache()
    {
        Artisan::call('cache:clear');
        return redirect()->route('/');
    }
}
