<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Clinic;
use GuzzleHttp\Client;
use App\Models\PcrTest;
use App\Models\Department;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\DoctorCategory;
use App\Models\PDFtestResult;
use App\Models\Test;
use Aws\Rekognition\RekognitionClient;
use Illuminate\Support\Facades\Config;
use App\Notifications\UserRegistration;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use PhpParser\Node\Stmt\Return_;
use Yajra\DataTables\Facades\DataTables;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $clinics = Clinic::count();
        $departments = Department::count();
        $checkingDesk = User::where('role_id', '1')->count();
        $administration = User::where('role_id', '2')->count();
        $vitalDepartment = User::where('role_id', '3')->count();
        $referDeparts = User::where('role_id', '4')->count();
        $doctDepartment = User::where('role_id', '6')->count();
        $chwStaff = User::where('role_id', '7')->count();
        $patients = User::where('role_id', '8')->count();

        $clincis = Clinic::all();
        $data = User::with('usersdetail')
            ->where('role_id', '=', '8')
            ->whereHas('usersdetail', function ($query) {
                $query->where('patient_id', '!=', "");
            })
            ->get();

        return view('home', compact(
            'clinics',
            'departments',
            'checkingDesk',
            'administration',
            'vitalDepartment',
            'referDeparts',
            'doctDepartment',
            'chwStaff',
            'patients',
            'clincis',
            'data'
        ));
    }

    public function getUsers()
    {
        Log::info(request()->userAgent());

        $data = User::where('is_staff', true)->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '<a href="' . route("user.generateCard", $row->id) . '" class="edit btn btn-primary btn-sm">Print</a>';

                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function addUser(Request $request)
    {
        if ($request->hasFile('image1')) {
            $file = $request->file('image1');
            $extension = $file->getClientOriginalExtension();
            $imagepath = time() . '.' . $extension;
            $file->move('images/', $imagepath);
            $image = asset('images/') . "/" . $imagepath;
        }

        Log::info(request()->userAgent());

        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'clinic' => 'required',
            'department' => 'required',
            'phone_number' => 'required',
        ]);

        $config = Config::get('aws');
        $user = User::where('email', $request->email)->first();
        if ($user) {
            return response()->json(['error' => 'User already exists!'], 404);
        }
        $user = new User();
        $department = Department::where("id", $request->department)->first();
        $clinic = Clinic::where("id", $request->clinic)->first();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->clinic_id = $clinic->id;
        $user->department_id = $department->id;
        $user->phone_number = $request->phone_number;
        $user->profile_pic = $image;
        $pass = Str::random($length = 6);
        $user->password = bcrypt($pass);
        $user->is_staff = true;
        $user->save();
        Log::info($user->email . " added successfully.");
        $user->notify(new UserRegistration("Dear " . $user->name . ", your account has been created. Your login credentials are as follows:" . "\n" . "Username: " . $user->email . "\n" . "Password: " . $pass));
        return response()->json(['status' => 'success']);
    }

    public function deleteUser($id)
    {
        $user = User::where("id", $id)->first();
        Log::info($user->email . " deleted successfully.");

        $user->delete();
        return response()->json(['status' => 'success']);
    }

    public function updatePassword(Request $request)
    {
        Log::info(request()->userAgent());

        $userId = $request->user_id;
        $this->validate($request, [
            'password' => 'required|string|confirmed',
        ]);
        $user = User::where("id", $userId)->first();
        $user->password = bcrypt($request->password);
        $user->save();
        Log::info($user->email . " credentials updated successfully.");
        $user->notify(new UserRegistration("Dear " . $user->name . ", your account has been updated. Your login credentials are as follows:" . "\n" . "Username: " . $user->email . "\n" . "Password: " . $request->password));
        return response()->json(['status' => 'success']);
    }

    public function getDepartments($clinicId)
    {

        $departments = Department::where(["clinic_id" => $clinicId])->get();
        return response()->json([
            'status' => 'success', 'departments' => $departments
        ]);
    }

    public function viewPatients()
    {
        $id = "ADG-12122933-10";
        $data = User::with('usersdetail', 'clinic')
            ->whereHas('usersdetail', function ($query) {
                $query->where('patient_id', '!=', "");
            })
            ->where('is_admin', '!=', '1')
            ->get();

        $tests = Test::all();
        return view('patients', compact('data', 'tests'));
    }

    public function getPatient($id)
    {
        $patients = User::where(['is_staff' => 0, 'is_doctor' => 0, "is_admin" => 0, "deactivated" => 0])->get();
        return response()->json([
            'status' => 'success', 'patients' => $patients
        ]);
    }

    public function deletePatient($id)
    {
        Log::info(request()->userAgent());

        $apiClient = new Client();
        $deleteUrl = "http://3.233.102.40:8098/api/person/delete/" . (int)$id . "?access_token=26E02472AF49D20271778A33B2310373";
        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ];
        $user = User::where("id", $id)->first();
        $config = Config::get('aws');
        $rekognitionClient = new RekognitionClient($config);
        // if (!is_null($user->face_id)) {
        //     Log::info($user->email . " faces are deleted successfully from collection.");
        //     $result = $rekognitionClient->deleteFaces([
        //         'CollectionId' => 'newCollection',
        //         'FaceIds' => [
        //             $user->face_id,
        //         ],
        //     ]);
        // }
        Log::info($user->email . " deleting from ZK DASHBOARD.");
        $response = $apiClient->request('POST', $deleteUrl, [
            'headers' => $headers,
        ]);
        $reponse = json_decode($response->getBody(), true);

        if (isset($reponse['code']) && $reponse['code'] == 0) {
            Log::info($user->email . " deleted from ZK DASHBOARD.");
            $user->deactivated = 1;
            $user->email = Str::random("5") . " + " . bin2hex($user->email);
            $pcr = PcrTest::where("user_id", $user->id)->first();
            if ($pcr) {
                $pcr->delete();
            }
            Log::info($user->email . " deleted successfully.");
            $user->delete();
            return response()->json(['status' => 'success']);
        } else {
            Log::info($user->email . " does not exist on ZK DASHBOARD, deleting from health dashboard.");
            $user->deactivated = 1;
            $user->email = Str::random("5") . " + " . bin2hex($user->email);
            $pcr = PcrTest::where("user_id", $user->id)->first();
            if ($pcr) {
                $pcr->delete();
            }
            Log::info($user->email . " deactivated successfully.");
            $user->delete();
            return response()->json(['status' => 'success']);
        }
    }

    public function category()
    {
        $categorys = DoctorCategory::all();
        return view('doctor.category', compact('categorys'));
    }

    public function categoryD($id)
    {
        $doctors = DoctorCategory::where("id", $id)->first();
        Log::info($doctors->email . " deleted successfully.");

        $doctors->delete();
        return response()->json(['status' => 'success']);
    }

    public function addcategory(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $categoryex = DoctorCategory::where('name', $request->name)->first();
        if ($categoryex) {
            return response()->json(['errorexist' => 'Category already exists!'], 404);
        } else {

            if ($request->hasFile('image1')) {
                $file = $request->file('image1');
                $extension = $file->getClientOriginalExtension();
                $imagepath = time() . '.' . $extension;
                $file->move('images/', $imagepath);
                $image = asset('images/') . "/" . $imagepath;
            }

            if ($request->cate_id) {
                $doctcategory = DoctorCategory::where('id', $request->cate_id)->first();
                $doctcategory->name = $request->name;
                $doctcategory->image1 = $image;
                $doctcategory->save();
                return response()->json(['status' => 'success']);
            } else {
                $doctcategory = new DoctorCategory;
                $doctcategory->name = $request->name;
                $doctcategory->image1 = $image;
                $doctcategory->save();
                return response()->json(['status' => 'success']);
            }
        }
    }
    public function logDynamicContent()
    {
        $folders = []; // If you want to include folders for custom log paths, you can populate this array
        $files = File::files(storage_path('logs'));
        $logs = [];

        $current_file = request()->input('l');
        $current_folder = request()->input('f');

        foreach ($files as $file) {
            if (pathinfo($file, PATHINFO_EXTENSION) === 'log') {
                $logContent = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
                $logs[pathinfo($file, PATHINFO_BASENAME)] = $logContent;
            }
        }

        return view('vendor.laravel-log-viewer.log', compact('folders', 'files', 'logs', 'current_file', 'current_folder'));
    }

    public function deletetest($id)
    {
        $test = PDFtestResult::find($id)->delete();
        if ($test) {
            return response()->json(['status' => 'success']);
        } else {
            return response()->json(['error' => 'error']);
        }
    }
}
