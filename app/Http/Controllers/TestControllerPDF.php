<?php

namespace App\Http\Controllers;

use App\Models\Clinic;
use App\Models\Department;
use App\Models\PDFtestResult;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use League\Csv\Reader;
use Illuminate\Support\Facades\Mail;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


class TestControllerPDF extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $clinic = Clinic::all();
        $test = PDFtestResult::all();
        return view('TestManagement.index', compact('test', 'clinic'));
    }
    public function addTestDashboard(Request $request)
    {

        $pdfFiles = $request->file('pdfTest'); // Assuming you have an array of uploaded PDF files
        $data = $this->processMultiplePDFs($pdfFiles);

        foreach ($data as $data1) {
            foreach ($data1 as $data) {
                $date =  explode(",", $data[27]);
                $time =  $data[28];
                $testType = explode(",", $data[404]);
                $testResult = explode(",", $data[443]);
                $testResult1 = $data[444];
                $testResult2 = explode("|", $data[445]);
                $patientID = explode(",", $data[183]);
                $patient = explode(",", $data[196]);

                $assay1 = explode(",", $data[91]);
                $assay2 =  $data[92];
                $assay3 = $data[93];

                $Test1 =  $data[741];
                $Test2 =  $data[754];
                $Test3 = $data[767];
                $Test4 =  $data[780];
                $Test5 = $data[793];
                $Test6 = $data[806];

                $statusReport = explode(",", $data[651]);
                $statusReportcheck = explode(",", $data[663]);

                $pdfAdd = PDFtestResult::create([
                    'staff_id' => Auth::user()->id,
                    'clinic_id' => AUth::user()->clinic_id,
                    'testdate' => $date[1],
                    'testtime' => $time,
                    'testtype' => $testType[1],
                    'testresult' => $testResult[1] . " " . $testResult1 . " " . $testResult2[0],
                    'sampleid' => $patientID[1],
                    'patientid' => $patient[1],
                    'assay1' => $assay1[1] . " " . $assay2 . " " . $assay3,
                    'testvalues1' => $Test1,
                    'testvalues2' => $Test2,
                    'testvalues3' => $Test3,
                    'testvalues4' => $Test4,
                    'testvalues5' => $Test5,
                    'testvalues6' => $Test6,
                    'statusreport' => $statusReport[0] . " " . $statusReport[1],
                    'statusreportcheck' =>  $statusReportcheck[0] . " " . $statusReportcheck[1],
                ]);
            }
        }
        return redirect()->with("success", 'Test Added Successfully')->back();

        die();



        $date =  explode(",", $data[0][0][27]);
        $time = $data[0][0][28];
        $testType = explode(",", $data[0][0][404]);
        $testResult = explode(",", $data[0][0][443]);
        $testResult1 = $data[0][0][444];
        $testResult2 = explode("|", $data[0][0][445]);
        $patientID = explode(",", $data[0][0][183]);
        $patient = explode(",", $data[0][0][196]);

        $assay1 = explode(",", $data[0][0][91]);
        $assay2 =  $data[0][0][92];
        $assay3 = $data[0][0][93];

        $Test1 =  $data[0][0][741];
        $Test2 =  $data[0][0][754];
        $Test3 =  $data[0][0][767];
        $Test4 =  $data[0][0][780];
        $Test5 =  $data[0][0][793];
        $Test6 =  $data[0][0][806];

        $statusReport = explode(",", $data[0][0][651]);
        $statusReportcheck = explode(",", $data[0][0][663]);

        return response([
            'Testdate' => $date[1],
            'Testtime' => $time,
            'TestType' => $testType[1],
            'TestResult' => $testResult[1] . " " . $testResult1 . " " . $testResult2[0],
            'sampleID' => $patientID[1],
            'patientID' => $patient[1],
            'assay1' => $assay1[1] . " " . $assay2 . " " . $assay3,
            'testValues1' => $Test1,
            'testValues2' => $Test2,
            'testValues3' => $Test3,
            'testValues4' => $Test4,
            'testValues5' => $Test5,
            'testValues6' => $Test6,
            'statusReport' => $statusReport[0] . " " . $statusReport[1],
            'statusReportcheck' => $statusReportcheck[0] . " " . $statusReportcheck[1],
        ]);
    }

    public function processMultiplePDFs($pdfFiles)
    {
        $cleanedDataArray = [];

        foreach ($pdfFiles as $pdfFile) {
            $pdfFilePath = $pdfFile->getRealPath();

            $csv = Reader::createFromPath($pdfFilePath, 'r');
            // Read the CSV data and store it in an array
            $data = [];
            foreach ($csv as $row) {
                $cleanedRow = array_map(function ($value) {
                    return trim($value);
                }, $row);

                // Remove empty values from the cleaned row
                $cleanedRow = $this->removeEmptyValues($cleanedRow);

                $data[] = $cleanedRow;
            }

            // Identify columns with only null values
            $nullColumns = [];
            if (!empty($data)) {
                $numRows = count($data);
                foreach ($data[0] as $columnIndex => $value) {
                    $isNullColumn = true;
                    for ($i = 0; $i < $numRows; $i++) {
                        if ($data[$i][$columnIndex] !== null) {
                            $isNullColumn = false;
                            break;
                        }
                    }
                    if ($isNullColumn) {
                        $nullColumns[] = $columnIndex;
                    }
                }
            }

            // Remove columns with only null values from the data array
            if (!empty($nullColumns)) {
                foreach ($data as &$row) {
                    foreach ($nullColumns as $columnIndex) {
                        unset($row[$columnIndex]);
                    }
                }
            }

            // Store the cleaned data in the array
            $cleanedDataArray[] = $data;
        }

        // Return the array of cleaned data for all CSV files
        return $cleanedDataArray;
    }

    public function removeEmptyValues($array)
    {
        return array_filter($array, function ($value) {
            return $value !== '' && $value !== null;
        });
    }
    public function checkpatientQR(Request $request)
    {
        $users = User::with('usersdetail')->where('id', $request->patientID)->first();
        $patientID = $users->usersdetail->patient_id;
        return  QrCode::size(300)->generate($patientID);
    }
    public function TestEmail()
    {
        $to = 'fawad1824@gmail.com';
        $subject = 'Hello, World!';
        $message = 'This is a simple raw email sent from Laravel.';

        // Use the Mail::raw function to send a raw email
        Mail::raw($message, function ($message) use ($to, $subject) {
            $message->to($to)
                    ->subject($subject)
                    ->from('fawad1824@gmail.com');
        });

        return 'Raw email sent successfully!';
    }
}
