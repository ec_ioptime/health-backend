<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Queue;
use App\Models\Clinic;
use App\Models\Department;
use Aws\Laravel\AwsFacade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class StaffDashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $departments = Department::all();
        $clinics = Clinic::all();
        return view('staff.dashboard', compact('departments', 'clinics'));
    }

    public function getToken($departmentId)
    {
        /* dynamo-db configuration */

        $queue = Queue::where(['department_id' => (int)$departmentId, 'date' => date('Y-m-d')])->orderBy('id', 'desc')->first();

        return view('staff.queue-display', compact('queue'));
    }

    public function manageQueue($departmentId)
    {

        $queue = Queue::where('department_id', $departmentId)->where('date', date('Y-m-d'))->orderBy('id', 'desc')->first();

        if ($queue) {
            $nextNumber = Queue::where(['department_id' => (int) $departmentId, "queue_number" => (int) $queue->queue_number + 1, 'date' => date('Y-m-d')])->first();

            return view('staff.queue-manage', compact('queue', 'nextNumber'));
        } else {
            return view('staff.queue-manage', compact('queue'));
        }
    }



    public function generateQueue($queueID)
    {
        $lastQueue = Queue::where('id', (int)$queueID)->where('date', date('Y-m-d'))->first();
        $nextNumber = Queue::where(['department_id' => (int) $lastQueue->department_id, "queue_number" => (int) $lastQueue->queue_number + 1, 'date' => date('Y-m-d')])->first();

        if ($nextNumber) {

            $queue =  Queue::where('clinic_id', $lastQueue->clinic_id)->where('department_id', $lastQueue->department_id)->get();
            foreach ($queue as $item) {
                $item->currenNumber =  $nextNumber->queue_number;
                $item->save();
            }
            $lastQueue->is_current = 0;
            $lastQueue->save();
            $nextNumber->is_current = 1;
            $nextNumber->currenNumber =  $nextNumber->queue_number;
            $nextNumber->save();
            return response()->json([
                'status' => 'success', 'data' => $nextNumber
            ]);
        } else {
            return response()->json([
                'status' => 'false'
            ]);
        }
    }

    public function resetQueue($queueID)
    {

        $lastQueue = Queue::where('id', (int)$queueID)->where('date', date('Y-m-d'))->first();
        $nextNumber = Queue::where(['department_id' => (int) $lastQueue->department_id, "queue_number" => (int) 1, 'clinic_id' => $lastQueue->clinic_id, 'date' => date('Y-m-d')])->first();
        if ($nextNumber->id != $lastQueue->id) {

            $queue =  Queue::where('clinic_id', $lastQueue->clinic_id)->where('department_id', $lastQueue->department_id)->get();
            foreach ($queue as $item) {
                $item->currenNumber =  $nextNumber->queue_number;
                $item->save();
            }

            $nextNumber->is_current = 1;
            $nextNumber->save();

            $lastQueue->is_current = 0;
            $lastQueue->save();
        }

        return response()->json([
            'status' => 'success'
        ]);
    }
}
