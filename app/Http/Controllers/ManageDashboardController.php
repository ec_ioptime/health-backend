<?php

namespace App\Http\Controllers;

use Aws\Laravel\AwsFacade;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\ModelsAws\UhfCard;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ManageDashboardController extends Controller
{

    public function __construct()
{
    $this->middleware('auth');
}
    public function uhfCards()
    {
        $cards = UhfCard::all();
        return view('uhf-cards' , compact('cards'));
    }

    public function addCard(Request $request)
    {
            $config =  Config::get('aws');
            $client = AwsFacade::createClient('DynamoDb', $config);
          $validator = Validator::make($request->all(), [
            'phone_number' => 'required|max:255',
            'card_number' => 'required|max:255',
        ]);

         if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $card = new UhfCard() ;
        $card->id = (string)rand(1, 1000000);
        $card->card_number = $request->card_number;
        $card->phone_number = "+".$request->phone_number;
        $card->save();
        return response()->json(['status' => 'success']);

    }


    public function deleteCard($id)
    {

        $config =  Config::get('aws');
        $client = AwsFacade::createClient('DynamoDb', $config);
        $card = UhfCard::find($id);
        $card->delete();
        return response()->json(['status' => 'success']);
    }
}
