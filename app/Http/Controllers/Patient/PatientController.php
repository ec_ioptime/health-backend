<?php

namespace App\Http\Controllers\Patient;

use App\Http\Controllers\Controller;
use App\Models\Clinic;
use App\Models\Country;
use App\Models\CovidQuestionRecord;
use App\Models\Department;
use App\Models\KnownChronicQuestion;
use App\Models\PatientCheck;
use App\Models\RishFactorQuestions;
use App\Models\Roles;
use App\Models\User;
use App\Models\Test;
use App\Models\UserDetails;
use App\Models\VitalRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

use function GuzzleHttp\Promise\all;

class PatientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function updateUserPassword(Request $request, $id)
    {
        $request->validate([
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|same:new_password|min:8',
        ], [
            'new_password.required' => 'Please enter a new password.',
            'new_password.min' => 'The new password must be at least :min characters.',
            'confirm_password.required' => 'Please confirm your new password.',
            'confirm_password.same' => 'The new password and confirmation password do not match.',
        ]);
        $user = User::findOrFail($id);
        $user->password = Hash::make($request->input('new_password'));
        $user->save();

        return response()->json(['success' => true]);
    }
    public function PatientsNextKin()
    {
        $data = User::with('usersdetail', 'clinic')
            ->where('role_id', '8')
            ->orderBy('id', 'DESC')
            ->get();

        return view('patientDetail.nextKin', compact('data'));
    }

    public function viewPatients(Request $request)
    {
        $data = User::with('usersdetail', 'clinic')
            ->where('role_id', '8')
            ->orderBy('id', 'DESC')
            ->get();

        $tests = Test::all();
        return view('patients', compact('data', 'tests'));
    }
    public function PatientsNextKinGet(Request $request)
    {
        if ($request->isKinDetail == 'true' && $request->patientID) {

            return $user = User::join('user_details', 'users.id', '=', 'user_details.user_id')
                ->leftjoin('countries', 'user_details.next_of_kin_country_id', '=', 'countries.id')
                ->with('usersdetail')
                ->where('users.id', $request->patientID)
                ->select('users.*', 'countries.name as countryName')
                ->first();

            return $user = User::leftjoin('countries', 'users.next_of_kin_country_id', '=', 'countries.id')->with('usersdetail')->where('users.id', $request->patientID)->select('users.*', 'countries.name as countryName')->first();
        }
        if ($request->id) {

            $data = DB::table('patientcheck')
                ->join('users as staff', 'patientcheck.staff_id', '=', 'staff.id')
                ->join('visitpurpose', 'patientcheck.visit_id', '=', 'visitpurpose.visit_id')
                ->join('clinics', 'staff.clinic_id', '=', 'clinics.id')
                ->where('patientcheck.patient_id', $request->id)
                ->select('patientcheck.*', 'clinics.name as ClinicName', 'patientcheck.date as visitDate', 'visitpurpose.purpose as Purpose', 'patientcheck.department as Dept', 'staff.name AS staffName')
                ->get();

            // return  $data = User::join('clinics', 'users.clinic_id', '=', 'clinics.id')
            //     ->join('patientcheck', 'users.id', '=', 'patientcheck.patient_id')
            //     ->leftJoin('users AS staff', 'patientcheck.staff_id', '=', 'staff.id')
            //     ->join('visitpurpose', 'patientcheck.visit_id', '=', 'visitpurpose.visit_id')
            //     ->where('patientcheck.patient_id', $request->id)
            //     ->select('patientcheck.*','clinics.name as ClinicName','patientcheck.date as visitDate', 'visitpurpose.purpose as Purpose', 'patientcheck.department as Dept', 'staff.name AS staffName')
            //     ->get();

            $testsPending = Test::join('users as Staff', 'tests.nurse_id', '=', 'Staff.id')
                ->where('patient_id', $request->id)
                // ->where('status', 'pending')
                ->select('tests.*', 'Staff.name as staff')
                ->get();

            $testsComplete = Test::where('patient_id', $request->id)->where('status', 'complete')->get();
            $testsVital = VitalRecord::join('users', 'vital_records.nurse_id', '=', 'users.id')
                ->leftjoin('departments', 'users.role_id', '=', 'departments.role_id')
                ->leftjoin('listtest', 'vital_records.questionId', '=', 'listtest.id')
                ->where('vital_records.patient_id', $request->id)
                ->select('*', 'listtest.name as TestType')
                // ->select('vital_records.*', 'departments.department_name')
                ->get();

            $covidQuestion = CovidQuestionRecord::join('covid_questions', 'covid_question_records.covid_question_id', '=', 'covid_questions.id')->where('covid_question_records.user_id', $request->id)->get();

            $knowchrom = KnownChronicQuestion::join('disease', 'knowchronicquestion.questions_id', '=', 'disease.id')->where('knowchronicquestion.user_id', $request->id)->select('*', 'knowchronicquestion.answer as answer')->where('is_knowchronic', '1')->get();
            $familymedical = KnownChronicQuestion::join('disease', 'knowchronicquestion.questions_id', '=', 'disease.id')->where('knowchronicquestion.user_id', $request->id)->select('*', 'knowchronicquestion.answer as answer')->where('is_familymedical', '2')->get();

            $riskfactor =   RishFactorQuestions::join('riskfactorans', 'riskfactor.id', '=', 'riskfactorans.riskfactor_question')->select('*', 'riskfactorans.ans as answer')->get();
            return response()->json([
                'patientID' => $request->id,
                'singlePatientDetail' => $data,
                'testPendingPatient' => $testsPending,
                'testCompletePatient' => $testsComplete,
                'testVital' => $testsVital,
                'ScreenQuestion' => $covidQuestion,
                'knowchrom' => $knowchrom,
                'familymedical' => $familymedical,
                'riskfactor' => $riskfactor,
            ]);
        }

        $dataPatientAll = User::with('usersdetail')
            ->where('is_admin', '!=', '1')
            ->whereHas('usersdetail', function ($query) {
                $query->where('patient_id', '!=', "");
            })
            ->get();

        return response()->json([
            'patientAll' => $dataPatientAll ?? "",
        ]);
    }
    public function UsersManagement()
    {
        $data = User::with('usersdetail', 'clinic', 'depart')
            ->where('is_admin', '!=', '1')
            ->get();
        return view('UserManagement.index', compact('data'));
    }
    public function healthstaffManagement()
    {
        $data = User::with('usersdetail', 'clinic', 'depart')
            ->where('is_staff', '1')
            ->where('role_id', '!=', '7')
            ->orderBy('id', 'DESC')
            ->get();
        $clincis = Clinic::all();
        $departments = Department::all();
        $countrys = Country::all();
        $roles =  $roles = Roles::where('is_department', '=', '1')->get();
        return view('healthStaffManagement.index', compact('data', 'clincis', 'departments', 'countrys', 'roles'));
    }

    public function AddhealthstaffManagement(Request $request)
    {

        $roleID = Department::where('id', '=', $request->dep)->first();
        if ($request->id) {
            $user = User::where('id', $request->id)->first();
            $user->name = $request->name;
            $user->email = $request->email;
            if ($request->password) {
                $user->password = Hash::make($request->password);
            }
            $user->phone_number = $request->phone;
            $user->department_id = $request->dep;
            $user->clinic_id = $request->clinic;
            $user->role_id =  $roleID->role_id;
            $user->gender = $request->gender;


            $country = Country::where('id', 'LIKE', "%{$request->country}%")->first();
            $user->country_id = $country->id;
            $user->dialing_code = "+" . $country->phonecode;
            $user->save();

            $userDet = UserDetails::where('user_id', $request->id)->first();
            $userDetEx = new UserDetails();
            if ($userDet) {
                $userDet->user_id = $request->id;
                $userDet->surname = $request->name2;
                $userDet->phone_number = $request->phone;
                $userDet->department_id = $request->dep;
                $userDet->clinic_id = $request->clinic;
                $userDet->gender = $request->gender;
                $userDet->country_id = $country->id;
                $userDet->dialing_code = "+" . $country->phonecode;
                $userDet->date_of_birth = $request->dob;
                $userDet->home_address_1 = $request->address;
                $userDet->nationality = $request->nationality;
                $userDet->expiry_date = $request->expdate;
                $userDet->document_id = $request->docID;
                $userDet->document_type = $request->doctype;
                $userDet->issue_state_country_id = $request->issuedate;
                $userDet->save();
            } else if ($userDetEx) {
                $userDetEx->user_id = $request->id;
                $userDetEx->surname = $request->name2;
                $userDetEx->phone_number = $request->phone;
                $userDetEx->department_id = $request->dep;
                $userDetEx->clinic_id = $request->clinic;
                $userDetEx->gender = $request->gender;
                $userDetEx->country_id = $country->id;
                $userDetEx->dialing_code = "+" . $country->phonecode;
                $userDetEx->date_of_birth = $request->dob;
                $userDetEx->home_address_1 = $request->address;
                $userDet->nationality = $request->nationality;
                $userDet->expiry_date = $request->expdate;
                $userDet->document_id = $request->docID;
                $userDet->document_type = $request->doctype;
                $userDet->issue_state_country_id = $request->issuedate;
                $userDetEx->save();
            }


            // $this->ZKXUser($user, $request);

            Log::info('Health Staff Add ZK' . $request->email);

            return redirect()->back()->with('message', 'Staff Updated successfully');
        } else {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            if ($request->password) {
                $user->password = Hash::make($request->password);
            }
            $user->phone_number = $request->phone;
            $user->department_id = $request->dep;
            $user->clinic_id = $request->clinic;
            $user->role_id =  $roleID->role_id;
            $user->is_staff = 1;
            $user->gender = $request->gender;

            $country = Country::where('id', 'LIKE', "%{$request->country}%")->first();
            $user->country_id = $country->id;
            $user->dialing_code = "+" . $country->phonecode;
            Log::info('Health Staff Add Database' . $request->email);
            $user->save();

            $userDet = new UserDetails();
            $userDet->user_id = $user->id;
            $userDet->surname = $request->name2;
            $userDet->phone_number = $request->phone;

            $userDet->department_id = $request->dep;
            $userDet->clinic_id = $request->clinic;
            $userDet->gender = $request->gender;
            $userDet->country_id = $country->id;
            $userDet->dialing_code = "+" . $country->phonecode;
            $userDet->date_of_birth = $request->dob;
            $userDet->home_address_1 = $request->address;
            $userDet->nationality = $request->nationality;
            $userDet->expiry_date = $request->expdate;
            $userDet->document_id = $request->docID;
            $userDet->document_type = $request->doctype;
            $userDet->issue_state_country_id = $request->issuedate;
            $this->ZKXUser($user, $request);
            Log::info('Health Staff Add ZK' . $request->email);
            $userDet->save();

            return redirect()->back()->with('success', 'Staff Added Successfully');
        }
    }

    // CHW
    public function AddhealthstaffManagementCHW(Request $request)
    {
        $roleID = '7';
        if ($request->id) {
            $user = User::where('id', $request->id)->first();
            $user->name = $request->name;
            $user->email = $request->email;
            if ($request->password) {
                $user->password = Hash::make($request->password);
            }
            $user->phone_number = $request->phone;
            $user->clinic_id = $request->clinic;
            $user->role_id =  $roleID;
            $departments = Department::where('role_id', '7')->first();

            $user->gender = $request->gender;

            $user->department_id = $departments->id;

            $country = Country::where('id', 'LIKE', "%{$request->country}%")->first();
            $user->country_id = $country->id;
            $user->dialing_code = "+" . $country->phonecode;
            $user->save();

            $userDet = UserDetails::where('user_id', $request->id)->first();
            $userDetEx = new UserDetails();
            if ($userDet) {
                $userDet->user_id = $request->id;
                $userDet->surname = $request->name2;
                $userDet->phone_number = $request->phone;
                $userDet->department_id = $departments->id;
                $userDet->clinic_id = $request->clinic;
                $userDet->gender = $request->gender;
                $userDet->country_id = $country->id;
                $userDet->dialing_code = "+" . $country->phonecode;
                $userDet->date_of_birth = $request->dob;
                $userDet->home_address_1 = $request->address;
                $userDet->save();
            } else if ($userDetEx) {
                $userDetEx->user_id = $request->id;
                $userDetEx->surname = $request->name2;
                $userDetEx->phone_number = $request->phone;
                $userDetEx->department_id = $departments->id;
                $userDetEx->clinic_id = $request->clinic;
                $userDetEx->gender = $request->gender;
                $userDetEx->country_id = $country->id;
                $userDetEx->dialing_code = "+" . $country->phonecode;
                $userDetEx->date_of_birth = $request->dob;
                $userDetEx->home_address_1 = $request->address;
                $userDetEx->save();
            }
            Log::info('Health Staff Add ZK' . $request->email);
            return redirect()->back()->with('message', 'Staff Updated successfully');
        } else {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            if ($request->password) {
                $user->password = Hash::make($request->password);
            }
            $user->phone_number = $request->phone;
            $user->clinic_id = $request->clinic;
            $user->role_id =  $roleID;
            $user->is_staff = 1;
            $user->gender = $request->gender;

            $departments = Department::where('role_id', '7')->first();

            $user->department_id = $departments->id;

            $country = Country::where('id', 'LIKE', "%{$request->country}%")->first();
            $user->country_id = $country->id;
            $user->dialing_code = "+" . $country->phonecode;
            Log::info('Health Staff Add Database' . $request->email);
            $user->save();

            $userDet = new UserDetails();
            $userDet->user_id = $user->id;
            $userDet->surname = $request->name2;
            $userDet->phone_number = $request->phone;

            $userDet->department_id = $request->dep;
            $userDet->clinic_id = $request->clinic;
            $userDet->gender = $request->gender;
            $userDet->country_id = $country->id;
            $userDet->dialing_code = "+" . $country->phonecode;
            $userDet->date_of_birth = $request->dob;
            $userDet->home_address_1 = $request->address;
            $this->ZKXUser($user, $request);
            Log::info('Health Staff Add ZK' . $request->email);
            $userDet->save();

            return redirect()->back()->with('success', 'Staff Added Successfully');
        }
    }

    public function ZKXUser($user, $request)
    {
        $base64 = null;
        if (isset($request->profile_pic)) {
            $data = file_get_contents($request->profile_pic);
            $base64 = base64_encode($data);
        }
        $gender = '';
        $urlAddAcess = "http://3.233.102.40:8098/api/person/add?access_token=26E02472AF49D20271778A33B2310373";
        if ($request->gender == 'male' || $request->gender == 'Male') {
            $gender = 'M';
        } else if ($request->gender == 'female' || $request->gender == 'Female') {
            $gender = 'F';
        }
        $body = array(
            "pin" => $user->id,
            "name" => $request->name,
            "lastName" => $request->surname,
            "email" =>  $request->email,
            "gender" =>   $gender,
            "mobilePhone" =>  $request->phone,
            "deptCode" => 237,
            "cardNo" => $request->cardNo,
            "certNumber" => $request->certNumber,
            "certType" => $request->certType,
            "personPhoto" => $base64,
            "supplyCards" => $request->supplyCards,
        );
        Log::info($user->name . " added on dashboard, Adding User on ZK server");

        $response = Http::post($urlAddAcess, $body);
        $RES = json_decode($response->getBody(), true);
        Log::info($RES);
    }

    public function EditstaffManagement(Request $request)
    {
        if ($request->id) {
            $user = User::with('usersdetail')
                ->where('id', $request->id)
                ->first();

            $clinic = Clinic::all();

            $departments = Department::all();
            $country = Country::all();

            $roles = Roles::where('is_department', '=', '1')->get();
            $countryActive = Country::where('id', $user->country_id)->first();

            return response()->json([
                'user' => $user,
                'clinics' => $clinic,
                'departments' => $departments,
                'country' => $country,
                'countryActive' => $countryActive,
                'roles' => $roles,
            ]);
        } else if ($request->clinic) {

            $user = User::with('usersdetail')
                ->where('id', $request->id)
                ->first();

            $clinic = Clinic::all();
            $departments = Department::where('clinic_id', $request->clinic)->get();
            $country = Country::all();
            return response()->json([
                'user' => $user,
                'clinics' => $clinic,
                'departments' => $departments,
                'country' => $country,
            ]);
        }
    }

    public function EditfacilityManagement(Request $request)
    {
        if ($request->id) {
            $user = User::with('usersdetail')
                ->where('id', $request->id)
                ->first();

            $clinic = Clinic::all();

            $departments = Department::all();
            $country = Country::all();

            $roles = Roles::where('is_department', '=', '2')->get();
            $countryActive = Country::where('id', $user->country_id)->first();

            return response()->json([
                'user' => $user,
                'clinics' => $clinic,
                'departments' => $departments,
                'country' => $country,
                'countryActive' => $countryActive,
                'roles' => $roles,
            ]);
        }
        $user = User::with('usersdetail')
            ->where('id', $request->id)
            ->first();

        $clinic = Clinic::all();

        $departments = Department::all();
        $country = Country::all();
        $roles = Roles::where('is_department', '=', '2')->get();
        // $countryActive = Country::where('id',$user->country_id)->first();

        return response()->json([
            'user' => $user,
            'clinics' => $clinic,
            'departments' => $departments,
            'country' => $country,
            'roles' => $roles,
            // 'countryActive' => $countryActive,
        ]);
    }

    public function Editpractissionarmanagement(Request $request)
    {
        if ($request->id) {
            $user = User::with('usersdetail')
                ->where('id', $request->id)
                ->first();

            $clinic = Clinic::all();

            $departments = Department::all();
            $country = Country::all();

            $roles = Roles::where('is_department', '=', '2')->get();
            $countryActive = Country::where('id', $user->country_id)->first();

            return response()->json([
                'user' => $user,
                'clinics' => $clinic,
                'departments' => $departments,
                'country' => $country,
                'countryActive' => $countryActive,
                'roles' => $roles,
            ]);
        }
        $user = User::with('usersdetail')
            ->where('id', $request->id)
            ->first();

        $clinic = Clinic::all();

        $departments = Department::all();
        $country = Country::all();
        $roles = Roles::where('is_department', '=', '2')->get();
        // $countryActive = Country::where('id',$user->country_id)->first();

        return response()->json([
            'user' => $user,
            'clinics' => $clinic,
            'departments' => $departments,
            'country' => $country,
            'roles' => $roles,
            // 'countryActive' => $countryActive,
        ]);
    }

    public function facilityManagement()
    {
        $data = User::with('usersdetail', 'clinic', 'depart', 'countrylist')
            ->where('role_id', '0')
            ->orderBy('id', 'DESC')
            ->paginate(10);
        $clincis = Clinic::all();
        $departments = Department::all();
        $countrys = Country::all();
        $roles = Roles::where('is_department', '2')->get();

        return view('facilityManagement.index', compact('data', 'clincis', 'departments', 'countrys', 'roles'));
    }

    public function adminData(Request $request)
    {
        $Admin = User::with('usersdetail', 'clinic', 'depart')
            ->where('is_admin', '=', '1')
            ->get();

        return response()->json(
            [
                'Admin' => $Admin,
            ]
        );
    }

    public function reportManagement()
    {
        $clincis = Clinic::all();
        $data = User::with('usersdetail')
            ->where('role_id', '=', '8')
            ->whereHas('usersdetail', function ($query) {
                $query->where('patient_id', '!=', "");
            })
            ->get();
        return view('ReportsManagement.index', compact('data', 'clincis'));
    }
    public function patientDetailReport(Request $request)
    {
        $clinic = $request->clinic;
        $patient = $request->patient;
        $fromdate = $request->fromdate;
        $todate = $request->todate;

        if ($patient == 'all') {
            return $departments = PatientCheck::join('referrals', 'patientcheck.id', '=', 'referrals.checkinID')
                ->where('referrals.clinic_id', $clinic)
                ->where('patientcheck.status', 'checkout')
                ->whereBetween('date',[$fromdate,$todate])
                ->select('department', DB::raw('GROUP_CONCAT(DISTINCT DATE_FORMAT(date, "%Y-%m-%d")) AS dates'), DB::raw('GROUP_CONCAT(DISTINCT TIMEDIFF(check_out, check_in)) AS durations'))
                ->groupBy('department')
                ->get();
        } else {
            return $departments = PatientCheck::join('referrals', 'patientcheck.id', '=', 'referrals.checkinID')
                ->where('referrals.clinic_id', $clinic)
                ->where('patientcheck.patient_id', $patient)
                ->where('patientcheck.status', 'checkout')
                ->whereBetween('date',[$fromdate,$todate])
                ->select('department', DB::raw('GROUP_CONCAT(DISTINCT DATE_FORMAT(date, "%Y-%m-%d")) AS dates'), DB::raw('GROUP_CONCAT(DISTINCT TIMEDIFF(check_out, check_in)) AS durations'))
                ->groupBy('department')
                ->get();
        }
    }
    public function practissionarManagement()
    {
        $data = User::with('usersdetail', 'clinic', 'depart', 'countrylist')
            ->where('role_id', '7')
            ->orderBy('id', 'DESC')
            ->paginate(10);
        $clincis = Clinic::all();
        $departments = Department::all();
        $countrys = Country::all();
        $roles = Roles::where('is_department', '2')->get();
        return view('healthpractissioner.index', compact('data', 'clincis', 'departments', 'countrys', 'roles'));
    }
    public function GetMail(Request $request)
    {
        if ($request->id && $request) {
            $user = User::where('id', $request->id)->where('email', $request->email)->first();
            if ($user) {
                return response()->json(
                    [
                        'status' => 'success',
                    ]
                );
            } else {
                $user = User::where('email', $request->email)->first();
                if ($user) {
                    return response()->json(
                        [
                            'status' => 'warning',
                            'message' => 'Email Already Exists',
                        ]
                    );
                }
            }
        } else {
            $user = User::where('email', $request->email)->first();
            if ($user) {
                return response()->json(
                    [
                        'status' => 'warning',
                        'message' => 'Email Already Exists',
                    ]
                );
            }
        }
    }
    public function GetCounrty(Request $request)
    {
        if ($request->datatype == 'countrycode') {
            return $country = Country::where('id', $request->countryID)->first();
        }
    }
}
