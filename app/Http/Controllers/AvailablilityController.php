<?php

namespace App\Http\Controllers;

use App\Models\Availablility;
use App\Models\TypeAvail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AvailablilityController extends Controller
{
    public function index()
    {
        $docAvaiable = Availablility::all();
        $availTy = TypeAvail::all();
        return view('doctor.avaiblelity', compact('docAvaiable', 'availTy'));
    }

    public function store(Request $request)
    {

        $request->validate([
            'price' => 'required',
            'currency' => 'required',
            'fromdate' => 'required',
            'todate' => 'required',
            'fromtime' => 'required',
            'totime' => 'required',
            'typeof' => 'required',
        ]);

        $Availablility = new Availablility();
        $Availablility->price = $request->price;
        $Availablility->currency = $request->currency;
        $Availablility->fromdate = $request->fromdate;
        $Availablility->todate = $request->todate;
        $Availablility->fromtime = $request->fromtime;
        $Availablility->totime = $request->totime;
        $Availablility->typeof = $request->typeof;
        $Availablility->doc_id = Auth::user()->id;
        $Availablility->save();

        Session::flash('success', 'Availablility Added Succesfully');
        return redirect()->back();
    }
    public function availabledelete($id)
    {
        $Availablility = Availablility::find($id);
        $Availablility->delete();
        Session::flash('success', 'Availablility Deleted Succesfully');
        return redirect()->back();
    }
}
