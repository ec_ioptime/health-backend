<?php

namespace App\Http\Controllers;

use App\Models\Clinic;
use Aws\Laravel\AwsFacade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Models\ModelsAws\MedicalClinic;
use App\Models\User;

class ClinicController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        
        $clinics = Clinic::all();
        return view('clinic.index', compact('clinics'));
    }

    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'search_location' => 'required',
            'contact_number' => 'required',
        ]);



        if ($request->id) {
            $clinic = Clinic::where('id', $request->id)->first();
            $clinic->name = $request->name;
            $clinic->contact = $request->contact_number;
            $clinic->address = $request->search_location;
            $clinic->latitude = $request->latitude;
            $clinic->longitude = $request->longitude;

            if ($request->hasFile('image1')) {
                $file = $request->file('image1');
                $extension = $file->getClientOriginalExtension();
                $imagepath = time() . '.' . $extension;
                $file->move('images/', $imagepath);
                $image = asset('images/') . "/" . $imagepath;
                $clinic->image1 = $image;
            }
            $clinic->save();
            return response()->json(['status' => 'success']);
        } else {
            $clinic = new Clinic();
            $clinic->name = $request->name;
            $clinic->contact = $request->contact_number;
            $clinic->address = $request->search_location;
            $clinic->latitude = $request->latitude;
            $clinic->longitude = $request->longitude;
            if ($request->hasFile('image1')) {
                $file = $request->file('image1');
                $extension = $file->getClientOriginalExtension();
                $imagepath = time() . '.' . $extension;
                $file->move('images/', $imagepath);
                $image = asset('images/') . "/" . $imagepath;
                $clinic->image1 = $image;
            }
            $clinic->save();
            return response()->json(['status' => 'success']);
        }
    }

    public function show($id)
    {
        $clinic = Clinic::where("id", $id)->first();
        return response()->json($clinic);
    }

    public function clinicedit($id)
    {
        $clinics = Clinic::where('id', $id)->first();
        return view('clinic.updateclinic', compact('clinics'));
    }

    public function destroy($id)
    {
        $exists = User::where('clinic_id', $id)->first();
        if ($exists) {
            return response()->json(['errorExist' => 'success']);
        }
        $clinic = Clinic::where("id", $id)->first();
        $clinic->delete();
        return response()->json(['status' => 'success']);
    }


    public function update(Request $request, $id)
    {
        $clinic = Clinic::where("id", $id)->first();
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'contact_number' => 'required',
        ]);
        $clinic->name = $request->name;
        $clinic->contact = $request->contact_number;
        $clinic->address = $request->address;
        $clinic->save();
        return response()->json(['status' => 'success']);
    }

    public function addClinic()
    {
        $clinics = Clinic::all();
        return view('clinic.addclinic', compact('clinics'));
    }
}
