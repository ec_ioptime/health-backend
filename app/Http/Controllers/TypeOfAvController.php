<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TypeOfAvController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('doctor.typeof');
    }
    public function store(Request $request)
    {
        return $request->all();
    }
}
