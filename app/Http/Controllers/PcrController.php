<?php

namespace App\Http\Controllers;

use App\Models\User;
use GuzzleHttp\Client;
use App\Models\PcrTest;
use Illuminate\Support\Facades\Log;

class PcrController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
         Log::info(request()->userAgent());
        $records = PcrTest::all();

        return view('pcr.index', compact('records'));
    }

    public function changeStatus($recordId)
    {
        Log::info(request()->userAgent());
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ];
        $record = PcrTest::find($recordId);
        $user = User::where("id", $record->user_id)->first();
        $urlAddAcess = "http://3.233.102.40:8098/api/accLevel/syncPerson?pin=" . $record->user_id . "&levelIds=2c9fd01a7e581f0d017e5821310f03c9&access_token=26E02472AF49D20271778A33B2310373";
        if ($record->pcr_status == "pending") {
            $user->VaccineVerified = true;
            $response = $client->request('POST', $urlAddAcess, [
                'headers' => $headers,
            ]);

            $RES = json_decode($response->getBody(), true);
            $record->pcr_status = "approved";
            Log::info($user->email." adding access level.");
        } else {
            $user->VaccineVerified = false;
            $record->pcr_status = "pending";
            $deleteLevelURl = "http://3.233.102.40:8098/api/accLevel/deleteLevel?pin=" . $record->user_id . "&levelIds=2c9fd01a7e581f0d017e5821310f03c9&access_token=26E02472AF49D20271778A33B2310373";
            $response = $client->request('POST', $deleteLevelURl, [
                'headers' => $headers,
            ]);
            $RES = json_decode($response->getBody(), true);
             Log::info($user->email." removing access level.");
        }
        if ($RES['code'] == 0) {
            $user->save();
            $record->save();
            Log::info($user->email." access level changed successfully.");
            return response()->json(['status' => 'success']);
        } else {
             Log::info($user->email." error while changing access level.");
            return response()->json(['status' => 'error']);
        }
    }
}
