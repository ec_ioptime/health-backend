<?php

namespace App\Http\Controllers;

use App\Models\Clinic;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DoctorDashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('is_doctor');
    }
    public function index()
    {
        $departments = Department::all();
        $clinics = Clinic::all();
        return view('doctor.dashboard', compact('departments', 'clinics'));
    }

}
