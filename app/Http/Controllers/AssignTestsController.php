<?php

namespace App\Http\Controllers;

use App\Models\AssignTests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Traits\ApiResponseTrait;
use App\Models\PatientCheck;
use App\Models\Permissions;
use App\Models\Roles;
use App\Models\Test;
use App\Models\TestAll;
use App\Models\Testcategory;
use App\Models\User;
use App\Models\VisitPurpose;
use App\Models\VistsModel;
use Exception;
use FFI;
use Illuminate\Support\Facades\DB;
use Laravel\Jetstream\Rules\Role;

class AssignTestsController extends Controller
{

    use ApiResponseTrait;


    // All tests
    public function index($id, $visitid)
    {
        try {
            $tests = VistsModel::with('test')
                ->where('patientID', $id)
                ->where('id', $visitid)
                ->get();
            if ($tests->count() > 0) {
                return $this->sendResponseData($tests, "Visit of Tests.");
            }
            $this->SendErrorLog("Error All Test index Not Found", "info");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error All Test Index exception", "error");
        }
    }

    // Get By Single ID
    public function indexidP($id)
    {
        try {

            $tests = Test::where('patient_id', $id)->where('status', 'pending')->get();
            if ($tests->count() > 0) {
                return $this->sendResponseData($tests, "Pending Patient Tests.");
            }
            $this->SendErrorLog("Error getCountries Not Found", "info");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error indexidP exception", "error");
        }
    }
    public function indexidC($id)
    {
        try {
            $tests = Test::where('patient_id', $id)->where('status', 'complete')->get();
            if ($tests->count() > 0) {
                return $this->sendResponseData($tests, "Complete Patient Tests.");
            }
            $this->SendErrorLog("Error indexidC Not Found", "info");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error indexidC exception", "error");
        }
    }

    // Single test Add
    public function storeS(Request $request)
    {
        try {
            $res = Validator::make($request->all(), [
                'patient_id' => 'required',
                'staff_id' => 'required',
                'status' => 'required',
                'test_id' => 'required',

            ]);
            foreach ($res->errors()->toArray() as $field => $message) {
                $errors[] = [
                    'message' => $message[0],
                ];
            }
            if (isset($errors)) {
                return $this->sendError('Validation Error.', $errors);
            }

            $newRecord = new AssignTests();
            $newRecord->patient_id = $request->patient_id;
            $newRecord->staff_id = $request->staff_id;
            $newRecord->status = $request->status;
            $newRecord->test_id =  $request->test_id;
            if ($newRecord->save()) {
                return $this->sendResponseData($newRecord, "Test Assign Added successfully.");
            }

            $this->SendErrorLog("Error storeS Test Not Found", "info");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error storeS Test exception", "error");
        }
    }


    // Multi test Add
    public function storeM(Request $request)
    {
        $res = Validator::make($request->all(), [
            'patient_id' => 'required',
            'staff_id' => 'required',
            'status' => 'required',
            'test_id' => 'required',

        ]);
        foreach ($res->errors()->toArray() as $field => $message) {
            $errors[] = [
                'message' => $message[0],
            ];
        }
        if (isset($errors)) {
            return $this->sendError('Validation Error.', $errors);
        }

        $newRecord = new AssignTests();
        $newRecord->patient_id = $request->patient_id;
        $newRecord->staff_id = $request->staff_id;
        $newRecord->status = $request->status;
        $newRecord->test_id =  $request->test_id;
        $newRecord->save();

        return $this->sendResponseData($newRecord, "Test Assign Added successfully.");
    }

    public function storeResult(Request $request, $id)
    {
        $tests = Test::where('id', $id)->first();
        if ($tests) {
            $tests->unit = $request->unit;
            $tests->value = $request->value;
            $tests->description = $request->description;
            $tests->status = $request->status;
            $tests->save();

            return $this->sendResponseData($tests, "Add Result Added successfully.");
        } else {
            return $this->sendError('Not Found.', []);
        }
    }

    public function addTestV(Request $request)
    {
        try {
            $res = Validator::make($request->all(), [
                'patient_id' => 'required',
                'nurse_id' => 'required',
                'test_id' => 'required',
                'description' => 'required',
                'unit' => 'required',
                'value' => 'required',

            ]);
            foreach ($res->errors()->toArray() as $field => $message) {
                $errors[] = [
                    'message' => $message[0],
                ];
            }
            if (isset($errors)) {
                return $this->sendError('Validation Error.', $errors);
            }
            foreach ($request->test_id as $key => $value) {
                $test = new Test();
                $testCate = DB::table('listtest')->where('id', $request->test_id[$key])->first();
                $test->name = $testCate['name'];
                $test->patient_id = $request->patient_id;
                $test->nurse_id = $request->nurse_id;

                $test->description = $request->description[$key];
                $test->unit = $request->unit[$key];
                $test->value = $request->value[$key];

                $test->is_vital = 1;
                $test->status = "complete";
                $test->category_id = $testCate['category_id'];
                $test->category = $testCate['category_name'];
                $test->save();
            }

            $tests = Test::where('id', $request->test_id)->where('is_vital', '1')->get();
            if (count($tests) > 0) {
                return $this->sendResponseData($tests, "Test's Added successfully.");
            }

            $this->SendErrorLog("Error addTestV Not Found", "info");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error addTestV exception", "error");
        }
    }

    public function addTestM(Request $request)
    {
        try {
            $res = Validator::make($request->all(), [
                'patient_id' => 'required',
                'nurse_id' => 'required',
                'test_id' => 'required',
                'description' => 'required',
                'unit' => 'required',
                'value' => 'required',

            ]);
            foreach ($res->errors()->toArray() as $field => $message) {
                $errors[] = [
                    'message' => $message[0],
                ];
            }
            if (isset($errors)) {
                return $this->sendError('Validation Error.', $errors);
            }

            $userNurse = User::where('id', $request->nurse_id)->first();

            $patientcheckinexk = PatientCheck::where('department_id', $userNurse->department_id)->where('visit_id', $request->visit_id)->where('status', 'checkin')->where('patient_id', $request->patient_id)->first();
            if ($patientcheckinexk == null) {
                return $this->sendError('Checkin first', []);
            }

            foreach ($request->test_id as $key => $value) {
                $test = Test::where('id', $request->test_id[$key])->first();
                $test->description = $request->description[$key];
                $test->unit = $request->unit[$key];
                $test->value = $request->value[$key];
                $test->status = "complete";
                $test->visit_id = $request->visit_id;
                $test->save();
            }

            return $this->sendResponseData([], "Test's Added successfully.");
        } catch (Exception $e) {
            return $this->Expection($e, "Error addTestM exception", "error");
        }
    }

    public function addTestS(Request $request)
    {
        try {
            $res = Validator::make($request->all(), [
                'patient_id' => 'required',
                'nurse_id' => 'required',
                'test_id' => 'required',
                'description' => 'required',
                'unit' => 'required',
                'value' => 'required',

            ]);
            foreach ($res->errors()->toArray() as $field => $message) {
                $errors[] = [
                    'message' => $message[0],
                ];
            }
            if (isset($errors)) {
                return $this->sendError('Validation Error.', $errors);
            }
            foreach ($request->test_id as $key => $value) {
                $test = new Test();
                $user = User::where('id', $request->nurse_id)->first();
                $testCate = DB::table('listtest')->where('id', $request->test_id[$key])->first();
                $test->name = $testCate['name'];
                $test->patient_id = $request->patient_id;
                $test->nurse_id = $request->nurse_id;
                $test->doc_name = $user->name;

                $test->description = $request->description[$key];
                $test->unit = $request->unit[$key];
                $test->value = $request->value[$key];

                $test->is_screening = 1;
                $test->status = "complete";

                $test->category_id = $testCate['category_id'];
                $test->category = $testCate['category_name'];
                $test->save();
            }

            $tests = Test::where('id', $request->test_id)->where('is_screening', '1')->get();
            if ($tests->count() > 0) {
                return $this->sendResponseData($tests, "Test's Added successfully.");
            }
            $this->SendErrorLog("Error addTestS Not Found", "info");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error addTestS exception", "error");
        }
    }

    public function getTestV()
    {
        try {
            $tests = Testcategory::where('id', '9')->with('tests')->get(); //vital
            if ($tests->count() > 0) {
                return $this->sendResponseData($tests, "Vital Test.");
            }
            $this->SendErrorLog("Error getTestV Not Found", "info");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error getTestV exception", "error");
        }
    }

    public function getTestCHW()
    {
        try {
            $tests = Testcategory::where('id', '11')->with('tests')->get(); //vital
            if ($tests->count() > 0) {
                return $this->sendResponseData($tests, "Vital Test.");
            }
            $this->SendErrorLog("Error getTestCHW Not Found", "info");
            return $this->sendError("Not Found", []);
        } catch (Exception $e) {
            return $this->Expection($e, "Error getTestCHW exception", "error");
        }
    }


    public function getTestS()
    {
        try {
        } catch (Exception $e) {
            return $this->Expection($e, "Error Familymedicalhistorylist exception", "error");
        }
        $tests = Testcategory::where('id', '10')->with('tests')->get(); //screening
        return $this->sendResponseData($tests, "Screening Test.");
    }



    public function addPermissionM(Request $request)
    {
        $res = Validator::make($request->all(), [
            'user_id' => 'required',
            'role_id' => 'required',

        ]);
        foreach ($res->errors()->toArray() as $field => $message) {
            $errors[] = [
                'message' => $message[0],
            ];
        }
        if (isset($errors)) {
            return $this->sendError('Validation Error.', $errors);
        }

        foreach ($request->user_id as $key => $value) {
            $test = new Permissions();
            $test->user_id = $request->user_id[$key];
            $test->role_id = $request->role_id[$key];
            $test->save();
        }

        return $this->sendResponseData([], "Permission Added successfully.");
    }

    public function addRoles(Request $request)
    {
        $roles = new Roles();
        $roles->name = $request->name;
        $roles->save();

        return $this->sendResponseData([], "Role Added successfully.");
    }

    public function Roles()
    {
        $roles = Roles::with('users')->get();
        return $this->sendResponseData($roles, "Roles List");
    }
}
