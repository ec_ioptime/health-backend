<?php

namespace App\Http\Controllers;

use App\Models\Test;
use App\Models\User;
use App\Models\Clinic;
use App\Models\Department;
use Illuminate\Http\Request;

class ManageTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $tests = Test::all();
        return view('test.index', compact('tests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $request->validate([
            'name' => 'required',
            'description' => 'required',
            'value' => 'required',
            'unit' => 'required',
        ]);

        $test = new Test();
        $test->name = $request->name;
        $test->description = $request->description;
        $test->value = $request->value;
        $test->unit = $request->unit;
        $test->save();
        return response()->json(['status' => 'success']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $test = Test::where("id" , $id)->first();
                return response()->json($test);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'value' => 'required',
            'unit' => 'required',
        ]);

        $test = Test::find($id);
        $test->name = $request->name;
        $test->description = $request->description;
        $test->value = $request->value;
        $test->unit = $request->unit;
        $test->save();
        return response()->json(['status' => 'success']);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $test = Test::where("id" , $id)->first();
               $test->delete();
        return response()->json(['status' => 'success']);
    }
}
