<?php

namespace App\Http\Controllers;

use App\Models\Clinic;
use App\Models\Department;
use Aws\Laravel\AwsFacade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Models\ModelsAws\MedicalClinic;
use App\Models\ModelsAws\MedicalDepartment;
use App\Models\Roles;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class DepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::all();
        $clinics = Clinic::all();
        $roles = Roles::where('is_department', '1')->get();
        return view('department.index', compact('departments', 'clinics', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'department_name' => 'required',
            'clinic' => 'required',
            'roles' => 'required',
        ]);
        $clinic = Clinic::where("id", $request->clinic)->first();
        $department = new Department();
        $department->department_name = $request->department_name;
        $department->clinic_id =  $clinic->id;
        $department->role_id =  $request->roles;
        $roless = Roles::where('id', $request->roles)->first();
        $department->role_name =  $roless->name;
        $department->save();

        $this->departmentCreate($department->id, $request,  $request->roles, $clinic);
        return response()->json(['status' => 'success']);
    }

    public function departmentCreate($dep, $request,  $roles, $clinic)
    {
        $client = new Client();
        $response = $client->post('http://3.233.102.40:8098/api/department/add?access_token=26E02472AF49D20271778A33B2310373', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json'
            ],
            'json' => [
                'code' => $dep,
                'name' => $request->department_name . " " . "-" . " " . $clinic->id . " " . "-" . " " . $request->roles,
                'parentCode' => '237',
                'sortNo' => 0
            ]
        ]);

        return $body = $response->getBody();
    }

    public function DeleteDeparttment($id)
    {
        $client = new Client();

        $response = $client->post('http://3.233.102.40:8098/api/department/delete/' . $id . '?access_token=26E02472AF49D20271778A33B2310373', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json'
            ]
        ]);

        return $body = $response->getBody();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $department = Department::where("id", $id)->first();
        $roleSel = Roles::where('id', $request->roleID)->first();
        $role = Roles::all();
        return response()->json(
            [
                'department' => $department,
                'roleSelect' => $roleSel,
                'role' => $role,
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'department_name' => 'required',
            'clinic' => 'required',
        ]);

        $department = Department::where("id", $id)->first();
        $clinic = Clinic::where("id", $request->clinic)->first();
        $department->department_name = $request->department_name;
        $department->clinic_id =  $clinic->id;
        $department->role_id =  $request->role;
        $roless = Roles::where('id', $request->role)->first();
        $department->role_name =  $roless->name;
        $department->save();
        return response()->json(['status' => 'success']);
    }


    public function departmentGet($id)
    {
        $client = new Client();

        $response = $client->get('http://3.233.102.40:8098/api/department/get/' . $id . '?access_token=26E02472AF49D20271778A33B2310373', [
            'headers' => [
                'Accept' => 'application/json'
            ]
        ]);

        return $getDepartmentID = $response->getBody();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $departmentexist = User::where("department_id", $id)->first();
        if ($departmentexist) {
            return response()->json(['status' => 'exists']);
        }
        $department = Department::where("id", $id)->first();
        $department->delete();
        $this->DeleteDeparttment($id);
        return response()->json(['status' => 'success']);
    }
}
