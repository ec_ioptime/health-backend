<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Clinic;
use App\Models\Department;
use App\Models\DoctorCategory;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Notifications\UserRegistration;
use Illuminate\Support\Facades\Log;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('is_admin');
        $this->middleware('auth');
    }

    public function index()
    {
        $data = User::where('is_doctor', true)->get();
        $departments = Department::all();
        $clinics = Clinic::all();
        $categories = DoctorCategory::all();
        return view('doctor.index', compact('data', 'departments', 'clinics', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //D
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        Log::info(request()->userAgent());

        if ($request->hasFile('image1')) {
            $file = $request->file('image1');
            $extension = $file->getClientOriginalExtension();
            $imagepath = time() . '.' . $extension;
            $file->move('images/', $imagepath);
            $image = asset('images/') ."/". $imagepath;
        }


        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'clinic' => 'required',
            'category' => 'required',
            'department' => 'required',
            'phone_number' => 'required',
            'about' => 'required',
            'experience' => 'required',
            'degree' => 'required',
        ]);

        $user = User::where('email', $request->email)->first();
        if ($user) {
            return response()->json(['error' => 'User already exists!'], 404);
        }
        $user = new User();
        $department = Department::where("id", $request->department)->first();
        $clinic = Clinic::where("id", $request->clinic)->first();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->clinic_id = $clinic->id;
        $user->department_id = $department->id;
        $user->doctor_category_id = $request->category;
        $user->phone_number = $request->phone_number;
        $pass = Str::random($length = 6);
        $user->password = bcrypt($pass);
        $user->profile_pic = $image;
        $user->is_doctor = true;
        $user->about = $request->about;
        $user->experience = $request->experience;
        $user->degree = $request->degree;
        $user->save();
        $user->notify(new UserRegistration("Dear " . $user->name . ", your account has been created. Your login credentials are as follows:" . "\n" . "Username: " . $user->email . "\n" . "Password: " . $pass));
        return response()->json(['status' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
