<?php

use App\Http\Controllers\AppoitmentController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\AvailablilityController;
use App\Http\Controllers\ClinicController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\DoctorDashboardController;
use App\Http\Controllers\Patient\PatientController;
use App\Http\Controllers\StaffDashboardController;
use App\Http\Controllers\TestControllerPDF;
use App\Http\Controllers\TypeOfAvController;
use App\Models\TypeofApp;
use Illuminate\Support\Facades\Log;
use Rap2hpoutre\LaravelLogViewer\LaravelLogViewer;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->name('/');


// Auth::routes();

Route::get('/users', [HomeController::class, 'getUsers'])->name('dashboard.users')->middleware('is_admin');;
Route::get('/logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);
Route::post('/login', [LoginController::class, 'login'])->name('login');
Route::get('/clear-cache', [LoginController::class, 'clearCache'])->name('clearcache');
Route::post('users/add-user', [HomeController::class, 'addUser'])->name('user.add')->middleware('is_admin');;
Route::delete('user/delete/{id}', [HomeController::class, 'deleteUser'])->name('delete-user')->middleware('is_admin');;
Route::post('user/update-password', [HomeController::class, 'updatePassword'])->name('user.updatePassword')->middleware('is_admin');;

Route::get('/dashboard',  [HomeController::class, 'index'])->name('dashboard');


// Patient Routes
Route::get('/patients',  [PatientController::class, 'viewPatients'])->name('patients');
Route::get('/home',  [HomeController::class, 'index'])->name('home');
Route::get('/patients_next_kin',  [PatientController::class, 'PatientsNextKin'])->name('Kinpatients');

// Pagination
Route::get('/ajax-Request-patient',  [PatientController::class, 'PatientsNextKinGet'])->name('KinpatientsGet');


//User Management
Route::get('/users_management',  [PatientController::class, 'UsersManagement'])->name('UsersManagement');

//Health Staff Management
Route::get('/healthstaff_management',  [PatientController::class, 'healthstaffManagement'])->name('healthstaffManagement');
Route::get('/get-email',  [PatientController::class, 'GetMail'])->name('GetMail');
Route::get('/get-country',  [PatientController::class, 'GetCounrty'])->name('GetCounrty');
Route::post('/add-staffManagement',  [PatientController::class, 'AddhealthstaffManagement'])->name('AddhealthstaffManagement');

// CHW
Route::post('/add-staffManagementCHW',  [PatientController::class, 'AddhealthstaffManagementCHW'])->name('AddhealthstaffManagementCHW');

Route::get('/Edit-staffManagement',  [PatientController::class, 'EditstaffManagement'])->name('EditstaffManagement');

// Update password
Route::post('/users/{id}/update-password', [PatientController::class, 'updateUserPassword'])->name('updateUserPassword');

//Facility Management
Route::get('/facility_management',  [PatientController::class, 'facilityManagement'])->name('facilityManagement');
Route::get('/Edit-facilityManagement',  [PatientController::class, 'EditfacilityManagement'])->name('Edit.facilityManagement');

//Facility Management
Route::get('/report_management',  [PatientController::class, 'reportManagement'])->name('reportManagement');
Route::get('/patientDetailReport',  [PatientController::class, 'patientDetailReport'])->name('patientDetailReport');

//practissionar Management
Route::get('/practissionar_management',  [PatientController::class, 'practissionarManagement'])->name('practissionarManagement');
Route::get('/Edit-practissionarmanagement',  [PatientController::class, 'Editpractissionarmanagement'])->name('Edit.practissionarmanagement');






















Route::get('/staff-dashboard',  [StaffDashboardController::class, 'index'])->name('staffDashboard');
Route::get('/doctor-dashboard',  [DoctorDashboardController::class, 'index'])->name('doctorDashboard');

// Doctor Categories (CRUD)
Route::get('/doctor-category',  [HomeController::class, 'category'])->name('doctorCategory');
Route::post('/doctor-category',  [HomeController::class, 'addcategory'])->name('adddoctorCategory');
Route::delete('delete-categoryD/{id}',  [HomeController::class, 'categoryD'])->name('doctorCategoryD')->middleware('is_admin');






Route::get('current-token/{departmentId}', [StaffDashboardController::class, 'getToken'])
    ->name('current-token');

Route::get('queue/manage/{departmentId}', [StaffDashboardController::class, 'manageQueue'])->name("manage-queue");

Route::get('generate-queue/{queueId}', [StaffDashboardController::class, 'generateQueue'])
    ->name('generate-queue');

Route::get('reset-queue/{queueId}', [StaffDashboardController::class, 'resetQueue'])
    ->name('reset-queue');

Route::get('/clinics', [ClinicController::class, 'index'])->name('adminclinicsName');
Route::get('/departments', [DepartmentController::class, 'index'])->name('departments');

Route::group(['middleware' => ['auth:sanctum', 'verified'],  'as' => 'admin.'], function () {

    /* clinics */
    Route::resource('clinic', ClinicController::class);

    // Route::get('clinics', [ClinicController::class,'index'])->name('clinics');
    Route::get('/clinicedit/{id}', [ClinicController::class, 'clinicedit']);
    Route::get('addclinic', [ClinicController::class, 'addClinic'])->name('addclinic');

    // availability
    Route::resource('/available', AvailablilityController::class);
    Route::get('/availabledelete/{id}', [AvailablilityController::class, 'availabledelete']);

    Route::get('/appointmnet', [AppoitmentController::class, 'index']);


    Route::resource('medicine', MedicineController::class);
    Route::resource('doctor', DoctorController::class);
    Route::resource('department', DepartmentController::class);
    Route::resource('test', ManageTestController::class);
    Route::get('get-departments/{clinicId}', [HomeController::class, 'getDepartments'])
        ->name('get-departments');
    Route::get('get-patient/{patientId}', [HomeController::class, 'getPatient'])
        ->name('get-patient');
    Route::post('delete/{patientId}', [HomeController::class, 'deletePatient'])
        ->name('delete-patient');

    Route::post('test/{patientId}', [HomeController::class, 'deletetest'])
        ->name('delete-test');
});
Route::get('/pcr-records',  [PcrController::class, 'index'])->name('admin.pcr-records.index');
Route::post('pcr-record/{recordId}', [PcrController::class, 'changeStatus'])
    ->name('record.change-status');
// Route::group(['namespace' => 'Dashboard', 'middleware' => ['auth:web','checkAdmin'], 'prefix' => 'dashboard'], function () {


Route::get('/logDynamicContent', [HomeController::class, 'logDynamicContent']);
Route::get('/testManagement', [TestControllerPDF::class, 'index'])->name('testManagement');
Route::get('/check-patientQR', [TestControllerPDF::class, 'checkpatientQR'])->name('checkpatientQR');
Route::post('/addTestDashboard', [TestControllerPDF::class, 'addTestDashboard'])->name('addTestDashboard');

Route::get('/test_email', [TestControllerPDF::class, 'TestEmail'])->name('TestEmail');
