<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\PcrTestController;
use App\Http\Controllers\Api\ManageApiController;
use App\Http\Controllers\Api\Admin\LoginController;
use App\Http\Controllers\Api\ManageDoctorController;
use App\Http\Controllers\Api\UserRegisterController;
use App\Http\Controllers\Api\GenerateQueueController;
use App\Http\Controllers\Api\ManageParkingController;
use App\Http\Controllers\Api\ManageVaccinesController;
use App\Http\Controllers\Api\ManageQuestionsController;
use App\Http\Controllers\Api\EmergencyContactController;
use App\Http\Controllers\Api\AwsImageProcessingController;
use App\Http\Controllers\Api\ClinicLocationController;
use App\Http\Controllers\Api\ManageReferralController;
use App\Http\Controllers\Api\ManageVitalRecordsController;
use App\Http\Controllers\AssignTestsController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('v1')->group(function () {

    Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
        return $request->user();
    });

    Route::post('login', [LoginController::class, 'login']);
    Route::post('staff-login', [LoginController::class, 'staffLogin']);
    Route::post('doctor-login', [LoginController::class, 'doctorLogin']);


    // Rest API for CHW Modules
    Route::post('chw-login', [LoginController::class, 'chwLogin']);
    Route::post('chw-reset-password', [LoginController::class, 'PasswordResetCHW']);
    Route::post('chw-forget-password', [LoginController::class, 'PasswordForgetCHW']);
    Route::get('chw-user-check', [LoginController::class, 'UserCheckCHW']);

    Route::get('chw-user-screenQuestion', [ManageApiController::class, 'CHWscreenQuestions']);
    Route::post('add-chw-user-screenQuestion', [ManageApiController::class, 'AddCHWscreenQuestions']);
    Route::post('add-chw-user-refer', [ManageApiController::class, 'AddCHWRefered']);




    Route::post('register', [LoginController::class, 'register']);
    Route::get('vaccine-type', [ManageVaccinesController::class, 'getVaccineType']);
    Route::get('get-countries', [ManageApiController::class, 'getCountries']);
    Route::get('get-departments', [ManageApiController::class, 'getDepartments']);
    Route::get('/get-user/', [ManageApiController::class, 'getUser']);
    Route::post('register-user', [UserRegisterController::class, 'register']);
    Route::post('/user/update-pin/', [UserRegisterController::class, 'updatePin']);
    Route::post('aws/create-collection', [AwsImageProcessingController::class, 'insertImage']);
    Route::post('aws/search-image', [AwsImageProcessingController::class, 'searchImage']);
    Route::post('aws/search-image-before-reg', [AwsImageProcessingController::class, 'searchImageBeforeReg']);
    Route::get('/get-zkToken/', [ManageApiController::class, 'getZkToken']);

    /* covid questions routes */
    Route::get('get-questions-CHW', [ManageQuestionsController::class, 'getQuestionsD']);
    Route::get('get-questions-staff', [ManageQuestionsController::class, 'getQuestionsT']);

    Route::get('get-questions-by-category', [ManageQuestionsController::class, 'getQuestionByCategory']);
    Route::get('user/get-questions/{userId}', [ManageQuestionsController::class, 'getUserQuestions']);
    Route::post('add/question-record', [ManageQuestionsController::class, 'addRecord']);


    Route::post('add-multiple/question-record', [ManageQuestionsController::class, 'addMultipleRecord']);
    Route::post('add-multiple/question-recordN', [ManageQuestionsController::class, 'addMultipleRecordN']);

    // Nearest location clinic
    Route::get('near-by-places/{lon}/{lat}', [ClinicLocationController::class, 'index']);
    Route::get('get-clinics', [ManageApiController::class, 'getClinics']);

    /* TOKEN BASED AUTH SANCTUM */
    Route::group(['middleware' => ['auth:sanctum']], function () {
        /* parking routes */
        Route::post('add/parking', [ManageParkingController::class, 'addParkingRecord']);
        Route::get('get/parking/{userId}', [ManageParkingController::class, 'getUserParking']);
        /* contact routes */
        Route::post('add/emergency-contact', [EmergencyContactController::class, 'addEmergencyContact']);
        Route::post('emergency-contact/{contactId}', [EmergencyContactController::class, 'updateContact']);
        Route::get('get-clinic-department/{clinicID}', [ManageApiController::class, 'getDepartmentForClinic']);


        /* vital records routes */
        Route::post("add/vital-record", [ManageVitalRecordsController::class, "addVitalRecord"]);
        Route::get("/get-patient-record/{patientID}", [ManageVitalRecordsController::class, "getPatientRecord"]);
        Route::get("/get-record/{patientID}", [ManageVitalRecordsController::class, "getRecord"]);



        Route::get("/get/latest-pending-record/{patientID}/{visitID}", [ManageVitalRecordsController::class, "getLatestPendingRecord"]);
        Route::post('/close/record/{recordID}', [ManageVitalRecordsController::class, 'closeRecord']);


        // Assign Test
        Route::get('/assigntestall/{id}/{visitid}', [AssignTestsController::class, 'index']);
        Route::get('/assigntestP/{id}', [AssignTestsController::class, 'indexidP']);
        Route::get('/assigntestC/{id}', [AssignTestsController::class, 'indexidC']);

        Route::post('/assigntestS', [AssignTestsController::class, 'storeS']);
        Route::post('/assigntestM', [AssignTestsController::class, 'storeM']);

        // Test
        Route::post('/addtestM', [AssignTestsController::class, 'addtestM']);

        Route::post('/addtestM-vital', [AssignTestsController::class, 'addTestV']);
        Route::post('/addtestM-screening', [AssignTestsController::class, 'addTestS']);

        // Get Test
        Route::get('/get-all-tests-by-screening-question-staff', [AssignTestsController::class, 'getTestS']);
        Route::get('/get-all-tests-by-screening-question-CHW', [AssignTestsController::class, 'getTestCHW']);
        Route::get('/get-all-tests-by-vital-question', [AssignTestsController::class, 'getTestV']);



        // Permissions
        Route::post('/addPermissionM', [AssignTestsController::class, 'addPermissionM']);
        Route::post('/addRoles', [AssignTestsController::class, 'addRoles']);
        Route::post('/Roles', [AssignTestsController::class, 'Roles']);

        // test result add
        Route::post('/add/results/{id}', [AssignTestsController::class, 'storeResult']);
        Route::post('/add/resultsMultiple/{id}', [AssignTestsController::class, 'storeResultM']);


        /* referral reoutes */
        Route::post("refer/record/{recordId}", [ManageReferralController::class, "referRecord"]);


        /* user routees */
        Route::get('/get-guests/{parentUserId}', [ManageApiController::class, 'getGuests']);
        Route::get('/user/get-balance/{userId}', [ManageApiController::class, 'getUserBalance']);
        Route::post('/user/update-balance/{userId}', [ManageApiController::class, 'updateUserBalance']);
        Route::get('/search/user-by-name', [ManageApiController::class, 'getUserByName']);
        Route::post('/user/update/{userId}', [UserRegisterController::class, 'update']);
        Route::post('/user-staff/update/{userId}', [UserRegisterController::class, 'staffupdate']);
        Route::get('/user/{userId}', [UserRegisterController::class, 'getUser']);
        Route::get('/search/{userId}', [UserRegisterController::class, 'getUser']);



        /* vaccines routes */
        Route::get('vaccination-center', [ManageVaccinesController::class, 'getVaccinationCenter']);
        Route::post('delete/vaccine-record/{userId}', [ManageVaccinesController::class, 'deleteVaccineRecord']);
        Route::post('add/vaccination-record', [ManageVaccinesController::class, 'addVaccinationRecord']);
        Route::get('vaccination-record/{userId}', [ManageVaccinesController::class, 'getVaccinationRecordForUser']);
        Route::post('vaccination-record/{recordId}', [ManageVaccinesController::class, 'updateVaccinationRecord']);

        /* pcr tests records */
        Route::get('/get/pcr-record/{userId}', [PcrTestController::class, 'getPcrRecordForUser']);
        Route::post('add/pcr-record', [PcrTestController::class, 'addPcrRecord']);

        // doctores apis
        Route::get("/get-doctor/{clinicID}", [ManageDoctorController::class, "getDoctors"]);
        Route::get("/doctor/{id}", [ManageApiController::class, "getDoctorsbyid"]);
        Route::get("/doctor-online", [ManageApiController::class, "doctoronline"]);
        Route::get("/get-tests", [ManageApiController::class, "getTests"]);

        Route::post("/add-appointment", [ManageApiController::class, "addAppointment"]);
        Route::post("/cancel-appointment/{appointmentID}", [ManageApiController::class, "cancelAppointment"]);
        Route::get("/get-appointments", [ManageApiController::class, "getAppointments"]);
        Route::get("/get-appointment-by-doctor/{doctorID}", [ManageApiController::class, "getAppointmentsByDoctor"]);
        Route::get("/get-appointment-by-patient/{patientID}", [ManageApiController::class, "getAppointmentsByPatient"]);
        Route::get("/gpatient-histryet-time-app", [ManageApiController::class, "gettimeapp"]);
        Route::get("/get-patient-by/{id}/{clinicId}", [ManageApiController::class, "GetPatient"]);


        // Fetch by document
        Route::get("/patient-by-document/{id}/{clinicId}", [ManageApiController::class, "GetPatientDoument"]);
        Route::post("/patient-refer/{recordId}", [ManageApiController::class, "PatientRefer"]);

        // Patient checkin and checkout history
        Route::get("/patient-checkin-checkout", [ManageApiController::class, "getCheckinCheckout"]);


        // Checkin and Checkout
        Route::post("/patient-checkin/{patietnID}", [ManageApiController::class, "PatientCheckin"]);
        Route::post("/patient-checkout", [ManageApiController::class, "PatientCheckout"]);

        // Avaiablity
        Route::get('/availabletypes', [ManageApiController::class, 'availabletypes']);
        Route::post('/availabletype', [ManageApiController::class, 'availabletype']);


        Route::post('/available', [ManageApiController::class, 'available']);

        Route::get("/doctors-listing", [ManageApiController::class, "getDoctors"]);
        Route::get("/get-doctors", [ManageApiController::class, "getDoctors"]);
        Route::get("/get-doctors/{name}", [ManageApiController::class, "getDoctorsName"]);
        Route::get("/get-doctors-by-category/{categoryID}", [ManageApiController::class, "getDoctorByCategory"]);
        Route::get("/doctors-by-category/{categoryID}", [ManageApiController::class, "getDoctorByCategory"]);
        Route::get("/doctors-categories-available", [ManageApiController::class, "doctorscategoriesavailable"]);
        Route::get("/doctors-categories", [ManageApiController::class, "doctorscategories"]);

        /* routes for queue generation */
        Route::post('generateQueue', [GenerateQueueController::class, 'generateQueue']);
        Route::get('getQueue/{userID}', [GenerateQueueController::class, 'getUserQueue']);

        // Ratting
        Route::get('/ratting', [ManageApiController::class, 'ratting']);
        Route::post('/addratting', [ManageApiController::class, 'addratting']);


        // checkin and checkout department
        Route::get('/departments-Clinic', [ManageApiController::class, 'DepartmenClinic']);
        Route::post('/departments-CheckIn', [ManageApiController::class, 'DepartmenCheckInProcess']);

        // Visits
        Route::get('/get-vists/{patientID}', [ManageApiController::class, 'GetPatientVists']);
        Route::post('/create-vists', [ManageApiController::class, 'PatientVists']);
        Route::post('/create-vists-status-change', [ManageApiController::class, 'PatientVistsStatus']);
        Route::post('/get-vists-by-patient/{patientID}', [ManageApiController::class, 'PatientVistsByID']);

        // Patient History
        Route::get('/patient-histry/{patientID}', [ManageApiController::class, 'PatientHistory']);

        // Test History
        Route::get('/patient-test/{patientID}', [ManageApiController::class, 'PatientTest']);

        // Faily Medical History
        Route::get('/family-medical-history', [ManageApiController::class, 'Familymedicalhistorylist']);
        Route::get('/family-medical-history/{patientID}', [ManageApiController::class, 'Familymedicalhistory']);
        Route::post('/family-medical-history', [ManageApiController::class, 'AddFamilymedicalhistory']);

        // Know Chronic Diseases
        Route::get('/known-chronic-disease', [ManageApiController::class, 'KnownChronicDisease']);
        Route::get('/known-chronic-disease/{patientID}', [ManageApiController::class, 'PatientKnownChronicDisease']);
        Route::post('/known-chronic-disease', [ManageApiController::class, 'KnownChronicDiseaseAdd']);

        // Risk Factor History

        Route::get('/risk-factor', [ManageApiController::class, 'ViewRiskFactor']);
        Route::get('/risk-factor/{id}', [ManageApiController::class, 'UpdateFactorPatient']);
        Route::post('/risk-factor', [ManageApiController::class, 'AddRiskFactor']);

        // Patient Detail

        Route::get('/get-patient-detail', [ManageApiController::class, 'GetPatientDetail']);
    });


    Route::get('utc-date', [ManageApiController::class, 'UTCDate']);
    Route::get('/email_send', [ManageApiController::class, 'sendEmail']);
    Route::post('/queueGenerate/{dep}/{clinic}', [ManageApiController::class, 'queueReferGeneration2']);
    Route::get('/email_send', [ManageApiController::class, 'sendEmail']);
});
