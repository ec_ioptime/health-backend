<x-app-layout>
    <x-slot name="header">
        <div class="page-header">
            <div>
                {{-- <h2 class="main-content-title tx-24 mg-b-5">Welcome To Dashboard</h2> --}}
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Next of kin details</li>
                </ol>
            </div>
        </div>
    </x-slot>
    <style>
        .modal-backdrop {
            opacity: 0.5 !important;
        }

        .btn-width2 {
            width: 100px;
        }

        .btn-width {
            width: 134px;
        }

        .button-style {
            float: right;
            margin-bottom: inherit
        }

        .paginate_button {
            width: 120px
        }
    </style>
    <div class="py-12">
            <div class="card p-3">
                <div class="card-body">


                    <div class="form-group mt-5">
                        <label for="">Select Patient Name</label>
                        <select class="form-control"id="UserGet">
                            <option value="">Please Select Patient Name</option>
                            @foreach ($data as $item)
                                <option value="{{ $item->id }}" data-id="{{ $item->id }}">{{ $item->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>


                    <style>
                        .table td,
                        .table th {
                            padding: 0.75rem;
                            vertical-align: top;
                            border-top: 1px solid white;
                        }
                    </style>
                    <div class="form pt-5 ml-5 mr-4">
                        <div class="card p-3">
                                <table class="table table-bordered table-sm" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td style="width: 40%; background:#F2F2F2;"><b>Categories</b></td>
                                            <td style="background:#F2F2F2;"><b>Details</b></td>
                                        </tr>
                                        <tr>
                                            <td><b>Kin Surname</b></td>
                                            <td id="kinsurename"></td>
                                        </tr>
                                        <tr>
                                            <td><b>Next Of Kin Number</b></td>
                                            <td id="nextofkinN"></td>
                                        </tr>
                                        <tr>
                                            <td><b>kin Relation</b></td>
                                            <td id="kinrelaion"></td>
                                        </tr>
                                        <tr>
                                            <td><b>Next Of Kin Country</b></td>
                                            <td id="nextofkinCountry"></td>
                                        </tr>
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {

            function changeValue(name, kin, next, relation, country) {
                if (name || kin || next || relation || country) {
                    $('#nextkIn').html(name);
                    $('#kinsurename').html(kin);
                    $('#nextofkinN').html(next);
                    $('#kinrelaion').html(relation);
                    $('#nextofkinCountry').html(country);
                } else {
                    $('#nextkIn').empty();
                    $('#kinsurename').empty();
                    $('#nextofkinN').empty();
                    $('#kinrelaion').empty();
                    $('#nextofkinCountry').empty();
                }
            }


            $("#UserGet").change(function() {


                data = {
                    'isKinDetail': 'true',
                    'patientID': $(this).find(':selected').data("id"),
                };

                url = "{{ url('/ajax-Request-patient') }}";
                type = "get";

                $.ajax({
                    url: url,
                    type: type,
                    data: data,
                    success: function(data) {
                        if (data.usersdetail.next_of_kin === null || data.usersdetail
                            .next_of_kin === "") {
                            data.usersdetail.next_of_kin = "";
                        }
                        if (data.usersdetail.kin_surname === null || data.usersdetail
                            .kin_surname === "") {
                            data.usersdetail.kin_surname = "";
                        }
                        if (data.usersdetail.next_of_kin_number === null || data.usersdetail
                            .next_of_kin_number === "") {
                            data.usersdetail.next_of_kin_number = "";
                        }
                        if (data.usersdetail.kinRelation === null || data.usersdetail
                            .kinRelation === "") {
                            data.usersdetail.kinRelation = "";
                        }
                        if (data.countryName === null || data.countryName === "") {
                            data.countryName = "";
                        }
                        changeValue();
                        changeValue(
                            data.usersdetail.next_of_kin,
                            data.usersdetail.kin_surname,
                            "+" + data.usersdetail.next_of_kin_country_id + data.usersdetail
                            .next_of_kin_number,
                            data.usersdetail.kinRelation,
                            data.countryName
                        );
                    }
                });
            });
        });
    </script>
</x-app-layout>
