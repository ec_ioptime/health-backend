<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Page</title>
    <!-- Add Bootstrap CSS link -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Add your custom CSS if needed -->
    <style>
        /* Add custom CSS here */
    </style>
</head>

<body>
    <div class="container">
        <div class="row justify-content-center mt-4">
            <div class="col-md-5">
                <div class="card p-5">
                    @if (session('error'))
                        <div class="mb-4 font-medium text-sm text-green-600">
                            {{ session('error') }}
                        </div>
                    @endif

                        <style>
                            .mb-4.font-medium.text-sm.text-green-600 {
                                color: red;
                            }
                        </style>
                        <div style="text-align: center;" class="preloader flex-column justify-content-center align-items-center mb-5">
                            <img class="animation__shake" src="{{ asset('assets/dist/img/AdminLTELogo.svg') }}" alt="AdminLTELogo"
                                height="70px" width="200">
                        </div>

                    @if (Auth::check())
                        <h5 style="text-align: center">
                            You’re already logged in
                            <br>
                            <br>
                            Go to
                            <a style="text-decoration: underline" href="{{ url('patients') }}">Dashboard</a>
                        </h5>
                    @else
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div>
                                <x-jet-label for="email" value="{{ __('Email') }}" />
                                <x-jet-input id="email" class="form-control block mt-1 w-full" type="email" name="email"
                                    :value="old('email')" required autofocus />
                            </div>

                            <div class="mt-4">
                                <x-jet-label for="password" value="{{ __('Password') }}" />
                                <x-jet-input id="password" class="form-control block mt-1 w-full" type="password" name="password"
                                    required autocomplete="current-password" />
                            </div>

                            <div class="block mt-4">
                                <label for="remember_me" class="flex items-center">
                                    <x-jet-checkbox id="remember_me" name="remember" />
                                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                                </label>
                            </div>

                            <div class="flex items-center justify-end mt-4">
                                <x-jet-button class="btn btn-primary">
                                    {{ __('Log in') }}
                                </x-jet-button>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- Add Bootstrap JS and jQuery scripts (optional) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>

</html>
