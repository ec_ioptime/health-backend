<x-app-layout>

    <style>
        .card-body {
            flex: 1 1 auto;
        }

        p.mb-1.tx-inverse1 {
            color: white;
            font-size: 17px;
            text-align: left;
            margin-left: 23px;
        }

        .custom-card {
            margin-bottom: 20px;
            border: 0;
            background: #e6e6e6;
            border-radius: 23px;
            box-shadow: 0 8px 16px 0 rgb(232 232 237 / 24%);
        }

        .custom-card1 {
            margin-bottom: 20px;
            border: 0;
            background: #c41034;
            border-radius: 23px;
            box-shadow: 0 8px 16px 0 rgb(232 232 237 / 24%);
        }

        span.text-muted1 {
            color: white;
        }

        .expansion-label {
            font-weight: 900;
            text-align: right;
        }

        .row-sm>div {
            padding-left: 6px;
            padding-right: 6px;
        }

        .card-body.dash1 {
            text-align: center;
        }

        p.mb-1.tx-inverse {
            padding-top: 9px;
            font-size: 14px;
        }

        .mr-auto,
        .mx-auto {
            margin-right: inherit;
        }

        b,
        strong {
            font-weight: bolder;
            color: white;
            font-size: 21px;
            padding: 2px 10px;
            text-align: right;
        }

        span.text-muted {
            font-size: 16px;
            font-weight: 500;
        }

        .row.hiddne {
            padding: 15px 77px;
        }

        .card-body.dash1 {
            text-align: center;
            padding: 40px 35px;
        }

        .card-body.dash {
            text-align: center;
        }

        .col-xl-2,
        .col-sm-6,
        .col-lg-6 {
            position: relative;
            width: 100%;
            padding-right: 7px;
            padding-left: 7px;
        }
    </style>
    <x-slot name="header">
        <div class="page-header">
            <div>
                <h1 class="main-content-title tx-40 mg-b-5">Welcome To Dashboard</h1>
            </div>
        </div>
    </x-slot>



    <div class="row hiddne">
        <div class="col-sm-6 col-xl-4 col-lg-6">
            <div class="card custom-card1">
                <div class="card-body dash1">
                    <div class="d-flex">
                        <div class="mr-auto">
                            <img style="width: 62px;" src="{{ asset('assets/icon/NoOfClinics.svg') }}" alt="clinics">
                        </div>
                        <p class="mb-1 tx-inverse1">Number of <br> clinics </p>
                        {{-- <b>{{ $clinics }}</b> --}}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xl-4 col-lg-6">
            <div class="card custom-card1">
                <div class="card-body dash1">
                    <div class="d-flex">
                        <div class="mr-auto">
                            <img style="width: 62px;" src="{{ asset('assets/icon/NoOfDepts.svg') }}" alt="clinics">
                        </div>
                        <p class="mb-1 tx-inverse1">Number of <br>  departments</p>
                        {{-- <b>{{ $departments }}</b> --}}

                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xl-4 col-lg-6">
            <div class="card custom-card1">
                <div class="card-body dash1">
                    <div class="d-flex">
                        <div class="mr-auto">
                            <img style="width: 45px;" src="{{ asset('assets/icons/noofpatients.svg') }}" alt="clinics">
                        </div>
                        <p class="mb-1 tx-inverse1">Number of <br> patients</p>
                        {{-- <b>{{ $patients }}</b> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div style="margin-left: 20px; margin-right: 20px;"class="row">
        <div class="col-sm-6 col-xl-2 col-lg-6">
            <div class="card custom-card">
                <div class="card-body dash">
                    <div class="ml-auto">
                        <img style="width: 45px;" src="{{ asset('assets/icons/Asset6.svg') }}" alt="clinics">
                    </div>
                    <p class="mb-1 tx-inverse">Checking Desk <br>Staff</p>
                    {{-- <span class="text-muted">{{ $checkingDesk }}</span> --}}

                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-2 col-lg-6">
            <div class="card custom-card">
                <div class="card-body dash">
                    <div class="ml-auto">
                        <img style="width: 45px;" src="{{ asset('assets/icons/Asset7.svg') }}" alt="clinics">
                    </div>
                    <p class="mb-1 tx-inverse">Administration<br> Staff</p>
                    {{-- <span class="text-muted">{{ $administration }}</span> --}}
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-2 col-lg-6">
            <div class="card custom-card">
                <div class="card-body dash">
                    <div class="ml-auto">
                        <img style="width: 45px;" src="{{ asset('assets/icons/Asset8.svg') }}" alt="clinics">
                    </div>
                    <p class="mb-1 tx-inverse">Vital Dep <br> Staff</p>
                    {{-- <span class="text-muted">{{ $vitalDepartment }}</span> --}}
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-2 col-lg-6">
            <div class="card custom-card">
                <div class="card-body dash">
                    <div class="ml-auto">
                        <img style="width: 45px;" src="{{ asset('assets/icons/checkingdeskstaff.svg') }}"
                            alt="clinics">
                    </div>
                    <p class="mb-1 tx-inverse"> Refer Station<br> Staff</p>
                    {{-- <span class="text-muted">{{ $referDeparts }}</span> --}}
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-2 col-lg-6">
            <div class="card custom-card">
                <div class="card-body dash">
                    <div class="ml-auto">
                        <img style="width: 45px;" src="{{ asset('assets/icons/Asset8.svg') }}" alt="clinics">
                    </div>
                    <p class="mb-1 tx-inverse">Doc Dep <br> Staff</p>
                    {{-- <span class="text-muted">{{ $doctDepartment }}</span> --}}
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-2 col-lg-6">
            <div class="card custom-card">
                <div class="card-body dash">
                    <div class="ml-auto">
                        <img style="width: 45px;" src="{{ asset('assets/icons/Asset7.svg') }}" alt="clinics">
                    </div>
                    <p class="mb-1 tx-inverse">CHW <br>Staff</p>
                    {{-- <span class="text-muted">{{ $chwStaff }}</span> --}}
                </div>
            </div>
        </div>

    </div>
    <!--End  Row -->


    <div class="row row-sm">
        <div class="col-sm-12 col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div hidden class="row mb-5">
                        <div class="col-lg-12">
                            <div class="form-group ">
                                <a href="#"style="float: right;" class="refreshBtn" id="refreshBtn">Refesh</a>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group ">
                                <label for="">Select Clinic</label>
                                <select name="clinic" id="clinic" class="form-control clinic">
                                    @foreach ($clincis as $item)
                                        <option {{ $item->id == '1015' ? 'selected' : '' }}
                                            value="{{ $item->id }}">
                                            {{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group ">
                                <label for="">Select Patient</label>
                                <select name="patient" id="patient" class="form-control patient">
                                    <option value="all">All</option>
                                    @foreach ($data as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @php
                            $subtractedDate = date('Y-m-d', strtotime('-30 days'));

                        @endphp
                        <div class="col-lg-3">
                            <div class="form-group ">
                                <label for="">From Date</label>
                                <input type="date" value="{{ $subtractedDate }}" class="form-control fromdate"
                                    name="fromdate" id="fromdate">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group ">
                                <label for="">To Date</label>
                                <input type="date" value="{{ date('Y-m-d') }}" class="form-control todate"
                                    name="todate" id="todate">
                            </div>
                        </div>
                    </div>

                    <div id="text">
                    </div>
                    {{-- <div id="chart-container" style="width: 100%;height:400px;"></div>
                    <div id="chart-container1" style="width: 100%;height:400px;"></div> --}}
                </div>
            </div>
        </div>
    </div>

    <script src="https://fastly.jsdelivr.net/npm/echarts@5.4.2/dist/echarts.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#text').html("");
            ajaxCall();

            function ajaxCall() {
                var clinic = $('#clinic').val();
                var patient = $('#patient').val();
                var fromdate = $('#fromdate').val();
                var todate = $('#todate').val();
                $.ajax({
                    type: 'get',
                    url: '{{ url('/patientDetailReport') }}',
                    data: {
                        clinic: clinic,
                        patient: patient,
                        fromdate: fromdate,
                        todate: todate,
                    },
                    success: function(data) {
                        if (data.length === 0) {
                            console.log(data);
                            $('#text').html("No data found");
                            $('#chart-container').html('');
                            $('#chart-container1').html('');

                        } else {
                            $('#text').html("");
                            $('#chart-container').html('');
                            $('#chart-container1').html('');
                            table = '';
                            table += '<div class="row">';
                            table += '<div class="col-lg-6">';
                            table +=
                                '<div id="chart-container" style="width: 100%;height:400px;"></div>';
                            table += '</div>';

                            table += '<div class="col-lg-6">';
                            table +=
                                '<div id="chart-container1" style="width: 100%;height:400px;"></div>';
                            table += '</div">';
                            table += '</div">';

                            $('#text').append(table);
                            linechart(data);
                            barchart(data);
                        }
                    }
                });
            }

            $('#refreshBtn').on('click', function() {
                ajaxCall();
            });
            $('#clinic').on('change', function() {
                ajaxCall();
            });
            $('#patient').on('change', function() {
                ajaxCall();
            });
            $('#fromdate').on('change', function() {
                ajaxCall();
            });
            $('#todate').on('change', function() {
                ajaxCall();
            });
        });

        function barchart(data) {
            var pieChartElement = document.getElementById('chart-container');
            // Get the DOM element to render the chart

            // Create an ECharts instance
            const chart = echarts.init(pieChartElement);

            // Process the data to calculate total durations for each department
            const departmentDurations = data.map(item => {
                const durations = item.durations.split(',').map(duration => {
                    const [hours, minutes, seconds] = duration.split(':').map(Number);
                    return hours * 3600 + minutes * 60 + seconds;
                });
                const totalDuration = durations.reduce((total, duration) => total + duration, 0);
                const hours = Math.floor(totalDuration / 3600);
                const minutes = Math.floor((totalDuration % 3600) / 60);
                const seconds = totalDuration % 60;
                return {
                    department: item.department,
                    totalDuration,
                    hours,
                    minutes,
                    seconds
                };
            });

            // Create the options object with dynamic data
            const options = {
                tooltip: {
                    trigger: 'item',
                    formatter: function(params) {
                        const item = departmentDurations.find(item => item.department === params.name);
                        return `${item.department}: ${item.hours}h ${item.minutes}m ${item.seconds}s (${params.percent}%)`;
                    }
                },
                series: [{
                    name: 'Department',
                    type: 'pie',
                    radius: '60%',
                    label: {
                        padding: [8, 10],
                        formatter: function(params) {
                            const item = departmentDurations.find(item => item.department === params.name);
                            return (
                                `{a|${item.department}}{abg|}\n{hr|}\n` +
                                `{b|Duration: ${item.hours}h ${item.minutes}m ${item.seconds}s}`
                            );
                        },
                        backgroundColor: '#F6F8FC',
                        borderColor: '#8C8D8E',
                        borderWidth: 1,
                        borderRadius: 4,
                        rich: {
                            a: {
                                color: '#6E7079',
                                lineHeight: 22,
                                align: 'center'
                            },
                            hr: {
                                borderColor: '#8C8D8E',
                                width: '100%',
                                borderWidth: 1,
                                height: 0
                            },
                            b: {
                                color: '#4C5058',
                                fontSize: 14,
                                fontWeight: 'bold',
                                lineHeight: 33
                            },
                            per: {
                                color: '#fff',
                                backgroundColor: '#4C5058',
                                padding: [3, 4],
                                borderRadius: 4
                            }
                        }
                    },
                    data: departmentDurations.map(item => ({
                        name: item.department,
                        value: item.totalDuration
                    }))
                }]
            };


            // Set the options and render the chart
            chart.setOption(options);
        }


        function linechart(data) {
            var chartContainer = document.getElementById('chart-container1');
            var myChart = echarts.init(chartContainer, null, {
                renderer: 'canvas',
                useDirtyRect: false
            });

            var option = {
                // tooltip: {
                //     trigger: 'axis',
                //     axisPointer: {
                //         type: 'shadow'
                //     }
                // },
                legend: {
                    orient: 'vertical',
                    left: 'left',
                    data: [] // Placeholder for dynamic legend data
                },
                xAxis: {
                    type: 'category',
                    data: [] // Placeholder for dynamic x-axis data
                },
                yAxis: {
                    type: 'value',
                    axisLabel: {
                        formatter: function(value) {
                            var hours = Math.floor(value / 3600);
                            var minutes = Math.floor((value % 3600) / 60);
                            var seconds = value % 60;
                            return hours + 'h ' + minutes + 'm ' + seconds + 's';
                        }
                    }
                },
                series: [{
                    name: 'Duration',
                    type: 'bar',
                    barWidth: 20, // Set the bar width here
                    data: [], // Placeholder for dynamic bar chart data
                    label: {
                        show: true,
                        position: 'top',
                        formatter: function(params) {
                            var duration = params.data;
                            var hours = Math.floor(duration / 3600);
                            var minutes = Math.floor((duration % 3600) / 60);
                            var seconds = duration % 60;
                            return params.name + '\n' + hours + 'h ' + minutes + 'm ' + seconds + 's';
                        }
                    },
                    emphasis: {
                        label: {
                            show: false,
                        }
                    },
                }]
            };

            var legendData = [];
            var barChartData = [];
            var xAxisData = [];

            $.each(data, function(index, row) {
                legendData.push(row.department);

                var durations = row.durations.split(',');
                var totalDuration = 0;
                $.each(durations, function(i, duration) {
                    var timeParts = duration.split(':');
                    var hours = parseInt(timeParts[0]);
                    var minutes = parseInt(timeParts[1]);
                    var seconds = parseInt(timeParts[2]);
                    totalDuration += hours * 3600 + minutes * 60 + seconds;
                });

                barChartData.push(totalDuration);
                xAxisData.push(row.department);
            });

            option.legend.data = legendData;
            option.series[0].data = barChartData;
            option.xAxis.data = xAxisData;

            if (option && typeof option === 'object') {
                myChart.setOption(option);
            }

            window.addEventListener('resize', myChart.resize);


        }
    </script>
</x-app-layout>
