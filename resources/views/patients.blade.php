<x-app-layout>

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>

    <style>
        button:not(:disabled),
        [type="button"]:not(:disabled),
        [type="reset"]:not(:disabled),
        [type="submit"]:not(:disabled) {
            cursor: pointer;
            color: black;
        }

        button.btn.btn-primary.btn-sm.m-1 {
            color: white;
        }

        span {
            color: black;
        }
    </style>

    <x-slot name="header">
        <div class="page-header">
            <div>
                {{-- <h2 class="main-content-title tx-24 mg-b-5">Welcome To Patient Detail</h2> --}}
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Patient</li>
                </ol>
            </div>
        </div>
    </x-slot>
    <style>
        table.dataTable>thead .sorting:before,
        table.dataTable>thead .sorting_asc:before,
        table.dataTable>thead .sorting_desc:before,
        table.dataTable>thead .sorting_asc_disabled:before,
        table.dataTable>thead .sorting_desc_disabled:before,
        table.dataTable>thead .sorting:after,
        table.dataTable>thead .sorting_asc:after,
        table.dataTable>thead .sorting_desc:after,
        table.dataTable>thead .sorting_asc_disabled:after,
        table.dataTable>thead .sorting_desc_disabled:after {
            display: none;
        }

        .table td,
        .table th {
            padding: 4px;
            vertical-align: middle;
            line-height: 1;
            border-top: 1px solid #dee2e6;
        }
    </style>

    <div class="card p-3">

        {{-- <div class="row m-3">
            <div class="col-lg-3">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="vaccinated" name="Vaccinated" value="Vaccinated">
                    <label class="form-check-label ">Vaccinated</label>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="unVaccinated" name="unVaccinated"
                        value="unVaccinated">
                    <label class="form-check-label">Not Vaccinated</label>
                </div>
            </div>
        </div> --}}


        <div class="table-responsive mb-3">

            <table class="table table-striped table-bordered" id="patientSearch">
                <thead>
                    <tr>
                        <th>ID<br>/Passport number</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Biometric Verified</th>
                        <th>Patient Number</th>
                        <th>Patient Type</th>
                        <th>Contact Number</th>
                        <th style="padding-right: 237px;">Address</th>
                        <th>Health Facility</th>
                        <th>Vacinate</th>
                        <th>Action</th>
                    </tr>

                </thead>
                <tbody id="ajax-pagination">
                    @foreach ($data as $index => $patient)
                        @php
                            $chronic = DB::table('disease')
                                ->join('knowchronicquestion', 'disease.id', '=', 'knowchronicquestion.questions_id')
                                ->where('is_familymedical', '2')
                                ->where('knowchronicquestion.user_id', $patient->id)
                                ->where('knowchronicquestion.answer', 'yes')
                                ->first();
                        @endphp
                        <tr>
                            <td>{{ $patient->id }}</td>
                            <td>{{ $patient->name }}</td>
                            <td>{{ $patient->usersdetail->surname ?? '' }}</td>
                            <td>Yes</td>
                            <td>{{ $patient->usersdetail->patient_id ?? '' }}</td>
                            @if ($chronic)
                                <td>Chronic</td>
                            @else
                                <td>Non-Chronic</td>
                            @endif

                            @if (str_contains($patient->phone_number, $patient->dialing_code))
                                <td>{{ $patient->phone_number }}</td>
                            @else
                                <td>{{ $patient->dialing_code . $patient->phone_number }}</td>
                            @endif
                            <td>{{ $patient->usersdetail->home_address_1 ?? '' }}</td>
                            <td>
                                @if (isset($patient->clinic->name))
                                    {{ $patient->clinic->name }}
                                @endif
                            </td>
                            <td>
                                @if ($patient->VaccineVerified == 'true')
                                    Vaccinated
                                @else
                                    Not vaccinated
                                @endif
                            </td>
                            <td class="d-flex">
                                <button type="button" onclick="funcQr({{ $patient->id }})"
                                    class="btn btn-primary btn-sm m-1">
                                    <i class="fa fa-qrcode" aria-hidden="true"></i>
                                </button>

                                <a href="#patientDetail" onclick="funclick({{ $patient->id }})"
                                    class="btn btn-primary btn-sm m-1">
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>

                                <button type="button" class="delete-patient btn btn-danger btn-sm m-1"
                                    data-patient-id="{{ $patient->id }}"
                                    onclick="deletePatient({{ $patient->id }})">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>


            </table>
        </div>

        {{-- {{ $data->links('pagination::bootstrap-4') }} --}}
    </div>

    {{-- <div class="card mt-5">
        <div class="card-body mt-2">
            <div style="float: right;" class="row">
                <div class="form-group">
                    <input type="date" style="margin-left: 12px;" class="form-control fromtimeFilter"
                        name="fromtimeFilter" value="{{ date('Y-m-d') }}" id="fromtimeFilter">
                </div>
                <div class="form-group">
                    <input type="date" style="margin-left: 22px;" class="form-control totimeFilter"
                        name="totimeFilter" value="{{ date('Y-m-d') }}" id="totimeFilter">
                </div>
                <div class="form-group">
                    <button type="button" style="margin-left: 32px;" class="btn btn-primary searchvisit"
                        id="searchvisit">Search</button>
                </div>
            </div>
        </div>
    </div> --}}

    <div id="patientDetail">
        <div class="row mt-5">
            <div class="col-lg-12 col-sm-12">
                <div class="card card-primary card-tabs">
                    <div class="card-header p-0 pt-1">
                        <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="custom-tab-facility" data-toggle="pill"
                                    href="#custom-facility" role="tab" aria-controls="custom-facility"
                                    aria-selected="true">
                                    FACTILITY
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="custom-tabs-labtest" data-toggle="pill" href="#custom-labtest"
                                    role="tab" aria-controls="custom-labtest" aria-selected="false">LAB
                                    TESTS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="custom-tabs-tb-xray-result" data-toggle="pill"
                                    href="#custom-tb-xray-result" role="tab" aria-controls="custom-tb-xray-result"
                                    aria-selected="false">TB-X-RAY</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="custom-tab-screening" data-toggle="pill"
                                    href="#custom-screening" role="tab" aria-controls="custom-screening"
                                    aria-selected="false">SCREENING</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="custom-tabs-testresult-tab" data-toggle="pill"
                                    href="#custom-testresult-tab" role="tab" aria-controls="custom-testresult-tab"
                                    aria-selected="false">TEST
                                    RESULTS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="custom-tabs-vital" data-toggle="pill" href="#custom-vital"
                                    role="tab" aria-controls="custom-vital" aria-selected="false">VITALS</a>
                            </li>
                        </ul>
                    </div>


                    <div class="card-body">
                        <div class="tab-content" id="custom-tabs-one-tabContent">

                            {{-- Facility Visits --}}
                            <div class="tab-pane fade show active" id="custom-facility" role="tabpanel"
                                aria-labelledby="custom-tab-facility">
                                <table class="table table-striped table-bordered table-sm" cellspacing="0"
                                    width="100%">
                                    <thead>
                                        <tr>
                                            <th class="th-sm">Facility name</th>
                                            <th class="th-sm">Visit Date</th>
                                            <th class="th-sm">Check-In Time</th>
                                            <th class="th-sm">Check-Out Time</th>
                                            <th class="th-sm">Duration of Visit</th>
                                            <th class="th-sm">Purpose of Visit</th>
                                            <th class="th-sm">Department</th>
                                            <th class="th-sm">Staff</th>
                                        </tr>

                                    </thead>
                                    <tbody id="checkin">

                                    </tbody>
                                </table>
                            </div>


                            <div class="tab-pane fade" id="custom-labtest" role="tabpanel"
                                aria-labelledby="custom-tabs-labtest">
                                <table class="table table-striped table-bordered table-sm" cellspacing="0"
                                    width="100%">
                                    <thead>
                                        <tr>
                                            <th class="th-sm">Test Name</th>
                                            <th class="th-sm">Type</th>
                                            <th class="th-sm">Staff Name</th>
                                            <th class="th-sm">Status</th>
                                        </tr>

                                    </thead>
                                    <tbody id="Testcheckin">

                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="custom-tb-xray-result" role="tabpanel"
                                aria-labelledby="custom-tabs-tb-xray-result">
                                <table class="table table-striped table-bordered table-sm" cellspacing="0"
                                    width="100%">
                                    <thead>
                                        <tr>
                                            <th class="th-sm">#</th>
                                            <th class="th-sm">Image</th>
                                            <th class="th-sm">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody id="xraytest">

                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="custom-screening" role="tabpanel"
                                aria-labelledby="custom-tab-screening">

                                <table class="table table-striped table-bordered table-sm" cellspacing="0"
                                    width="100%">
                                    <thead>
                                        <tr>
                                            <th class="th-sm">Question</th>
                                            <th class="th-sm">Category</th>
                                            {{-- <th class="th-sm">Options</th> --}}
                                            <th class="th-sm">Answer</th>
                                        </tr>

                                    </thead>
                                    <tbody id="screenQuestions">

                                    </tbody>
                                </table>

                            </div>
                            <div class="tab-pane fade" id="custom-testresult-tab" role="tabpanel"
                                aria-labelledby="custom-tabs-testresult-tab">

                                <table class="table table-striped table-bordered table-sm" cellspacing="0"
                                    width="100%">
                                    <thead>
                                        <tr>
                                            <th class="th-sm">Name</th>
                                            <th class="th-sm">Value</th>
                                            <th class="th-sm">Unit</th>
                                            <th class="th-sm">Status</th>
                                            <th class="th-sm">Action</th>
                                        </tr>

                                    </thead>
                                    <tbody id="AllTest">


                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="custom-vital" role="tabpanel"
                                aria-labelledby="custom-tabs-vital">
                                <table class="table table-striped table-bordered table-sm" cellspacing="0"
                                    width="100%">
                                    <thead>
                                        <tr>
                                            <th class="th-sm">Name</th>
                                            <th class="th-sm">Test Type</th>
                                            <th class="th-sm">Value</th>
                                            <th class="th-sm">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody id="testResultData">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>

        </div>
    </div>

    {{-- update patient --}}
    <div class="modal" tabindex="-1" id="update_patientModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Patient</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="update_patient" type="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" min="0" required name="name" class="form-control"
                                id="patient_name">
                            <span class="text-danger" id="updatenameError"></span>
                        </div>
                        <input type="hidden" value="" name="patient_id" id="patient_id">
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" min="0" required name="address" class="form-control"
                                id="age">
                            <span class="text-danger" id="updateaddressError"></span>
                        </div>
                        <div class="form-group">
                            <label for="contact_number">Contact Number</label>
                            <input type="number" min="0" required name="contact_number" class="form-control"
                                id="clinic_contact_number">
                            <span class="text-danger" id="updatephoneError"></span>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="update-clinic"
                                class="btn btn-primary btn-width2">Update</button>
                            <button type="button" class="btn btn-secondary btn-width2"
                                data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="QRcode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">QR code</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div style="text-align: center;" class="modal-body">
                    <div id="qrcodeView" class="qrcodeView"></div>
                    <button type="button" class="btn btn-primary qrcodeDownload mt-3">Print
                        QR code</button>
                </div>
            </div>
        </div>
    </div>



    <script>
        $('.close').on('click', function() {
            $('#QRcode').modal('hide');
            $('#update_patientModal').modal('hide');
        });

        $('.qrcodeDownload').on('click', function() {
            var printContents = $('.qrcodeView').html();
            // var originalContents = $('body').html();
            // $('body').html(printContents);
            // window.print();
            // $('body').html(originalContents);


            var printWindow = window.open('', '_blank');
            printWindow.document.open();
            printWindow.document.write('<html><head><title>Print</title></head><body>');
            printWindow.document.write(printContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });

        function funcQr(patientID) {
            $('.qrcodeView').html('');
            $('#QRcode').modal('show');
            $.ajax({
                url: "{{ url('/check-patientQR') }}",
                type: 'get',
                data: {
                    '_token': "{{ csrf_token() }}",
                    'patientID': patientID,
                },
                success: function(data) {
                    $('.qrcodeView').append(data);
                    console.log(data);
                },
            });
        }

        function deletePatient(patientId) {
            var patient = patientId;
            let url = "{{ route('admin.delete-patient', ':id') }}";
            url = url.replace(':id', patient);
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the Patient!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: {
                                '_token': "{{ csrf_token() }}",
                            },
                            success: function(data) {
                                if (data['status'] == 'success') {
                                    swal("Patient has been deleted successfully!", {
                                        icon: "success",
                                    });
                                    setTimeout(() => {
                                        location.reload();
                                    }, 1000);
                                } else {
                                    swal("Oops", "Something went wrong!", "error")

                                }
                            },
                            error: function(response) {
                                swal("Oops", "Something went wrong!", "error")

                            }
                        });
                    }
                });

        }

        $(function() {
            $('#dtBasicExample').DataTable({
                "pagingType": "simple",
                // "simple" option for 'Previous' and 'Next' buttons only
            });
            $('#vaccinated').on('change', function() {

                if ($(this).is(':checked')) {
                    if ($('#unVaccinated').is(":checked")) {
                        $.fn.dataTable.ext.search.pop()
                    } else {
                        $.fn.dataTable.ext.search.push(
                            function(settings, data, dataIndex) {

                                return data[9] == "vaccinated"
                            }
                        )
                    }
                } else {
                    if ($('#unVaccinated').is(":checked")) {

                        $.fn.dataTable.ext.search.push(
                            function(settings, data, dataIndex) {
                                return data[9] == "not vaccinated"
                            }
                        )
                    } else {
                        $.fn.dataTable.ext.search.pop()
                    }
                }
                $('#dtBasicExample').DataTable().draw()
            });
            $('#unVaccinated').on('change', function() {
                if ($(this).is(':checked')) {
                    if ($('#vaccinated').is(":checked")) {
                        $.fn.dataTable.ext.search.pop()
                    } else {
                        $.fn.dataTable.ext.search.push(
                            function(settings, data, dataIndex) {
                                return data[9] == "not vaccinated"
                            }
                        )
                    }

                } else {
                    if ($('#vaccinated').is(":checked")) {
                        $.fn.dataTable.ext.search.push(
                            function(settings, data, dataIndex) {
                                return data[9] == "vaccinated"
                            }
                        )
                    } else {
                        $.fn.dataTable.ext.search.pop()
                    }
                }
                $('#dtBasicExample').DataTable().draw()
            });
            $('.dataTables_length').addClass('bs-select');

        });


        function funclick(id) {
            data = {
                'id': id,
            };

            url = "{{ url('/ajax-Request-patient') }}";
            type = "get";

            $.ajax({
                url: url,
                type: type,
                data: data,
                success: function(data) {

                    if (data.ScreenQuestion[0] && data.singlePatientDetail[0] && data.testCompletePatient[0] &&
                        data.testPendingPatient[0] && data.testVital[0]) {
                        toastrCallSuccess(message = "Patient Detail Found")
                    }

                    if (!data.ScreenQuestion[0]) {
                        toastrCallError(message = "Screening Questions Not found")
                    }
                    if (!data.singlePatientDetail[0]) {
                        toastrCallError(message = "Patient Visit Not found")
                    }
                    if (!data.testCompletePatient[0]) {
                        toastrCallError(message = "Complete Tests Not found")
                    }
                    if (!data.testPendingPatient[0]) {
                        toastrCallError(message = "Pending Tests Not found")
                    }
                    if (!data.testVital[0]) {
                        toastrCallError(message = "Vital Tests Not found")
                    }

                    console.log(data);
                    $('#checkin').empty();
                    $('#Testcheckin').empty();
                    $('#testResultData').empty();
                    $('#AllTest').empty();
                    $('#screenQuestions').empty();
                    $('#xraytest').empty();

                    var table;
                    var table1;
                    var table2;
                    var table3;
                    var table4;
                    var table5;

                    // Patient Detail
                    if (data.singlePatientDetail.length > 1) {

                        $.each(data.singlePatientDetail, function(index, value) {

                            var fromTimestamp = new Date(value.check_in);
                            var toTimestamp = new Date(value.check_out);

                            var timeDiff = Math.abs(toTimestamp - fromTimestamp);

                            // Convert milliseconds to hours, minutes, and seconds
                            var hours = Math.floor(timeDiff / (1000 * 60 * 60));
                            var minutes = Math.floor((timeDiff % (1000 * 60 * 60)) / (1000 * 60));
                            var seconds = Math.floor((timeDiff % (1000 * 60)) / 1000);

                            console.log('Hours:', hours);
                            console.log('Minutes:', minutes);
                            console.log('Seconds:', seconds);


                            table += '<tr>';
                            table += '<td>' + value.ClinicName + '</td>';
                            table += '<td>' + value.visitDate + '</td>';
                            table += '<td>' + value.check_in + '</td>';
                            table += '<td>' + value.check_out + '</td>';
                            table += '<td>' + 'Hours' + hours + ': Minutes' + minutes + ': Seconds' +
                                seconds + '</td>';
                            table += '<td>' + value.Purpose + '</td>';
                            table += '<td>' + value.Dept + '</td>';
                            table += '<td>' + value.staffName + '</td>';
                            table += '</tr>';
                            $('#checkin').append(table);
                        });
                    } else {
                        table += '<tr>';
                        table += '<td>Not Found</td>';
                        table += '</tr>';
                        $('#checkin').append(table);

                    }


                    if (data.testPendingPatient) {
                        $.each(data.testPendingPatient, function(index, value) {
                            table1 += '<tr>';
                            table1 += '<td>' + value.name + '</td>';
                            table1 += '<td>' + 'Screening Test' + '</td>';
                            table1 += '<td>' + value.staff + '</td>';
                            table1 += '<td>' + value.status + '</td>';
                            table1 += '</tr>';
                        });
                        $('#Testcheckin').append(table1);
                    } else {
                        table += '<tr>';
                        table += '<td>Not Found</td>';
                        table += '</tr>';
                        $('#checkin').append(table1);

                    }



                    // TestResult Data
                    if (data.testVital) {
                        $.each(data.testVital, function(index, value) {
                            table2 += '<tr>';
                            table2 += '<td>' + value.symptoms + '</td>';
                            table2 += '<td>' + value.TestType + '</td>';
                            // table2 += '<td>' + value.unit + '</td>';
                            table2 += '<td>' + value.value + '</td>';
                            table2 += '<td>' + value.status + '</td>';
                            table2 += '</tr>';
                        });
                    }
                    $('#testResultData').append(table2);



                    // TestResultComplete Data
                    console.log(data.patientID);
                    if (data.testCompletePatient) {
                        $.each(data.testCompletePatient, function(index, value) {
                            table3 += '<tr>';
                            table3 += '<td>' + value.name + '</td>';
                            table3 += '<td>' + value.value + '</td>';
                            table3 += '<td>' + value.unit + '</td>';
                            table3 += '<td>' + value.status + '</td>';
                            table3 +=
                                '<td> <button type="button" onclick="funcQr({{ $patient->id }})" class="btn btn-primary btn-sm m-1"><i class="fa fa-qrcode" aria-hidden="true"></i></button></td>';
                            table3 += '</tr>';
                        });

                    }
                    $('#AllTest').append(table3);


                    // screenQuestions


                    $.each(data.ScreenQuestion, function(index, value) {
                        var str = value.answers;
                        str = str.replace(/[^a-zA-Z]/g, ' ');
                        table4 += '<tr>';
                        table4 += '<td>' + value.question + '</td>';
                        table4 += '<td>' + value.category + '</td>';
                        // table4 += '<td>' + str + '</td>';
                        table4 += '<td>' + value.answer + '</td>';
                        table4 += '</tr>';
                    });
                    $.each(data.riskfactor, function(index, value) {
                        var str1 = value.answer;
                        // str1 = str1.replace(/[^a-zA-Z]/g, ' ');
                        table4 += '<tr>';
                        table4 += '<td>' + value.questions + '</td>';
                        table4 += '<td>' + value.question_name + '</td>';
                        // table4 += '<td>' + str1 + '</td>';
                        table4 += '<td>' + value.answer + '</td>';
                        table4 += '</tr>';
                    });
                    $.each(data.knowchrom, function(index, value) {
                        var str12 = value.answer;
                        // str1 = str1.replace(/[^a-zA-Z]/g, ' ');
                        table4 += '<tr>';
                        table4 += '<td>' + value.name + '</td>';
                        table4 += '<td>' + "Known Chronic" + '</td>';
                        // table4 += '<td>' + str12 + '</td>';
                        table4 += '<td>' + value.answer + '</td>';
                        table4 += '</tr>';
                    });

                    $.each(data.familymedical, function(index, value) {
                        var str123 = value.answer;
                        // str1 = str1.replace(/[^a-zA-Z]/g, ' ');
                        table4 += '<tr>';
                        table4 += '<td>' + value.name + '</td>';
                        table4 += '<td>' + "Family Medical History" + '</td>';
                        // table4 += '<td>' + str123 + '</td>';
                        table4 += '<td>' + value.answer + '</td>';
                        table4 += '</tr>';
                    });
                    $('#screenQuestions').append(table4);



                    // Xray Test
                    table5 += '<tr>';
                    table5 += '<td>' + '1' + '</td>';
                    table5 += '<td>' + 'TB' + '</td>';
                    table5 += '</tr>';

                    $('#xraytest').append(table5);



                },
            });
        }

        // success toaster
        function toastrCallSuccess(message) {
            toastr.options = {
                "closeButton": true,
                "progressBar": true
            }
            toastr.success(message);
        }
        // success error
        function toastrCallError(message) {
            toastr.options = {
                "closeButton": true,
                "progressBar": true
            }
            toastr.error(message);
        }

        oTable = $('#patientSearch')
            .DataTable(); //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said
        $('#searchPatientField').keyup(function() {
            console.log("hello");
            oTable.search($(this).val()).draw();
        })
    </script>
</x-app-layout>
