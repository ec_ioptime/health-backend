<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <div id="dynamic-content"></div>
    <script>
        // Function to update the dynamic content
        function updateDynamicContent() {
            // Make an AJAX request to your Laravel route or API endpoint
            // Modify the URL as per your requirements
            fetch('/')
                .then(response => {
                    if (response.status == '200') {
                        console.log("Server is up");
                    } else {
                        console.log("Server is down");
                    }
                })
        }

        // Set the interval to call the updateDynamicContent function every 5 seconds (5000 milliseconds)
        setInterval(updateDynamicContent, 5000);

        // Call the updateDynamicContent function immediately to fetch the initial content
        updateDynamicContent();
    </script>
</body>

</html>
