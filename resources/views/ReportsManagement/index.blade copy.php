<x-app-layout>
    <x-slot name="header">
        <style>
            * {
                margin: 0;
                padding: 0;
            }

            #chart-container {
                position: relative;
                height: 100vh;
                overflow: hidden;
            }
        </style>

        <div class="row">
            <div class="col-lg-6">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    Report Dashboard
                </h2>
            </div>
            {{-- <div class="col-lg-6">
                <a href="" class="btn btn-primary btn-sm pull-right">Add New</a>
            </div> --}}
        </div>


    </x-slot>


    {{-- Admin --}}
    <div class="card">
        <div class="card-header">
        </div>
        <div class="card-body">

            <div class="row mb-5">

                <div class="col-lg-12">
                    <div class="form-group ">
                        <a href="#"style="float: right;" class="refreshBtn" id="refreshBtn">Refesh</a>
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="form-group ">
                        <label for="">Select Clinic</label>
                        <select name="clinic" id="clinic" class="form-control clinic">
                            @foreach ($clincis as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group ">
                        <label for="">Select Patient</label>
                        <select name="patient" id="patient" class="form-control patient">
                            <option value="all">All</option>
                            @foreach ($data as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group ">
                        <label for="">From Date</label>
                        <input type="date" value="{{ date('Y-m-d') }}" class="form-control fromdate" name="fromdate"
                            id="fromdate">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group ">
                        <label for="">To Date</label>
                        <input type="date" value="{{ date('Y-m-d') }}" class="form-control todate" name="todate"
                            id="todate">
                    </div>
                </div>
            </div>

            <div id="chart-container"></div>
        </div>
    </div>

    <script src="https://fastly.jsdelivr.net/npm/echarts@5.4.2/dist/echarts.min.js"></script>
    <script>
        $(document).ready(function() {
            ajaxCall();

            function ajaxCall() {
                var clinic = $('#clinic').val();
                var patient = $('#patient').val();
                var fromdate = $('#fromdate').val();
                var todate = $('#todate').val();
                $.ajax({
                    type: 'get',
                    url: '/patientDetailReport',
                    data: {
                        clinic: clinic,
                        patient: patient,
                        fromdate: fromdate,
                        todate: todate,
                    },
                    success: function(data) {
                        var dom = document.getElementById('chart-container');
                        var myChart = echarts.init(dom, null, {
                            renderer: 'canvas',
                            useDirtyRect: false
                        });
                        var app = {};

                        var option;

                        option = {
                            title: {
                                text: 'Report Management',
                                left: 'center'
                            },
                            tooltip: {
                                trigger: 'axis',
                                axisPointer: {
                                    type: 'shadow'
                                }
                            },
                            legend: {
                                orient: 'vertical',
                                left: 'left',
                                data: [] // Placeholder for dynamic legend data
                            },
                            grid: {
                                top: '20%',
                                left: '10%',
                                right: '10%',
                                bottom: '15%',
                                containLabel: true
                            },
                            xAxis: {
                                type: 'category',
                                data: [] // Placeholder for dynamic x-axis data
                            },
                            yAxis: {
                                type: 'value',
                                axisLabel: {
                                    formatter: function(value) {
                                        var hours = Math.floor(value / 3600);
                                        var minutes = Math.floor((value % 3600) / 60);
                                        var seconds = value % 60;
                                        return hours + 'h ' + minutes + 'm ' + seconds + 's';
                                    }
                                }
                            },
                            series: [{
                                    name: 'Access From',
                                    type: 'bar',
                                    data: [] // Placeholder for dynamic bar chart data
                                },
                                {
                                    name: 'Access Duration',
                                    type: 'pie',
                                    radius: ['30%', '50%'],
                                    center: ['50%', '75%'],
                                    label: {
                                        formatter: '{b}: {@value} ({d}%)'
                                    },
                                    encode: {
                                        itemName: 'department',
                                        value: 'value',
                                        tooltip: 'value'
                                    },
                                    data: [] // Placeholder for dynamic pie chart data
                                }
                            ]
                        };

                        var legendData = [];
                        var xAxisData = [];
                        var barChartData = [];
                        var pieChartData = [];

                        $.each(data, function(index, row) {
                            legendData.push(row.department);
                            xAxisData.push(row.department);

                            var durations = row.durations.split(',');
                            var totalDuration = 0;
                            $.each(durations, function(i, duration) {
                                var timeParts = duration.split(':');
                                var hours = parseInt(timeParts[0]);
                                var minutes = parseInt(timeParts[1]);
                                var seconds = parseInt(timeParts[2]);
                                totalDuration += hours * 3600 + minutes * 60 + seconds;
                            });

                            barChartData.push(totalDuration);
                            pieChartData.push({
                                value: totalDuration,
                                name: row.department
                            });
                        });

                        option.legend.data = legendData;
                        option.xAxis.data = xAxisData;
                        option.series[0].data = barChartData;
                        option.series[1].data = pieChartData;

                        if (option && typeof option === 'object') {
                            myChart.setOption(option);
                        }

                        window.addEventListener('resize', myChart.resize);

                    }
                });
            }
            $('#refreshBtn').on('click', function() {
                ajaxCall();
            });
        });
    </script>
</x-app-layout>
