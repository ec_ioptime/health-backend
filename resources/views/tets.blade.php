<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Add Venue</title>

    <style>
        ul#ui-id-1 {
            background: #f3f3f3;
            height: 45px;
        }

        #geomap {
            width: 100%;
            height: 300px;
            margin-top: 1%;
        }

        .ui-helper-hidden-accessible {
            display: none;
        }
    </style>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzf7KnzVx3iLASRh25OP_bYgTpUD-dIW8&libraries=places">
    </script>
</head>

<body class="theme-red">


    <!-- Top Bar -->

    <section class="content" style="margin-top: 0px !important;">
        <div class="container-fluid">
            <div class="block-header">

                <div id="result"></div>
                <form id="add_menue_form">
                    <span id="form_result1"></span>
                    <span id="form_result2"></span>
                    @csrf

                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" id="search_location" class="form-control"
                                placeholder="Search location">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div id="geomap"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" name="search_addr" class="search_addr" size="45">
                            <input type="hidden" name="latitude" class="search_latitude" size="30">
                            <input type="hidden" name="longitude" class="search_longitude" size="30">
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </section>

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzf7KnzVx3iLASRh25OP_bYgTpUD-dIW8&libraries=places">
    </script>


    <script>
        var geocoder;
        var map;
        var marker;

        /*
         * Google Map with marker
         */
        function initialize() {
            var initialLat = $('.search_latitude').val();
            var initialLong = $('.search_longitude').val();
            initialLat = initialLat ? initialLat : 36.169648;
            initialLong = initialLong ? initialLong : -115.141000;


            const center = {
                lat: 50.064192,
                lng: -130.605469
            };

            var latlng = new google.maps.LatLng(initialLat, initialLong);
            var options = {
                zoom: 16,
                center: latlng,
                componentRestrictions: {
                    country: "us"
                },
                // bounds: defaultBounds,
                strictBounds: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            };

            map = new google.maps.Map(document.getElementById("geomap"), options);

            geocoder = new google.maps.Geocoder();

            marker = new google.maps.Marker({
                map: map,
                draggable: false,
                position: latlng
            });

            google.maps.event.addListener(marker, "dragend", function() {
                var point = marker.getPosition();
                map.panTo(point);
                geocoder.geocode({
                    'latLng': marker.getPosition()
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        map.setCenter(results[0].geometry.location);
                        marker.setPosition(results[0].geometry.location);
                        $('.search_addr').val(results[0].formatted_address);
                        $('.search_latitude').val(marker.getPosition().lat());
                        $('.search_longitude').val(marker.getPosition().lng());
                    }
                });
            });

        }

        $(document).ready(function() {
            //load google map
            initialize();

            /*
             * autocomplete location search
             */
            var PostCodeid = '#search_location';


            $(function() {

                $(PostCodeid).autocomplete({

                    source: function(request, response) {
                        geocoder.geocode({
                            'address': request.term
                        }, function(results, status) {
                            response($.map(results, function(item) {
                                return {
                                    label: item.formatted_address,
                                    value: item.formatted_address,
                                    lat: item.geometry.location.lat(),
                                    lon: item.geometry.location.lng()
                                };
                            }));
                        });
                    },
                    select: function(event, ui) {
                        $('.search_addr').val(ui.item.value);
                        $('.search_latitude').val(ui.item.lat);
                        $('.search_longitude').val(ui.item.lon);
                        var latlng = new google.maps.LatLng(ui.item.lat, ui.item.lon);
                        marker.setPosition(latlng);
                        initialize();
                    }
                });
            });

            /*
             * Point location on google map
             */
            $('.get_map').click(function(e) {
                var address = $(PostCodeid).val();
                geocoder.geocode({
                    'address': address
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        map.setCenter(results[0].geometry.location);
                        marker.setPosition(results[0].geometry.location);
                        $('.search_addr').val(results[0].formatted_address);
                        $('.search_latitude').val(marker.getPosition().lat());
                        $('.search_longitude').val(marker.getPosition().lng());
                    } else {
                        alert("Geocode was not successful for the following reason: " + status);
                    }
                });
                e.preventDefault();
            });

            //Add listener to marker for reverse geocoding
            google.maps.event.addListener(marker, 'drag', function() {
                geocoder.geocode({
                    'latLng': marker.getPosition()
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('.search_addr').val(results[0].formatted_address);
                            $('.search_latitude').val(marker.getPosition().lat());
                            $('.search_longitude').val(marker.getPosition().lng());
                        }
                    }
                });
            });
        });


        $('#add_menue_form').on('submit', function(event) {

            event.preventDefault();
            $.ajax({
                url: "/add/venue",
                method: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                dataType: "json",
                success: function(data) {

                    var html = '';
                    if (data.errors) {
                        html = '<div class="alert alert-danger">';
                        for (var count = 0; count < data.errors.length; count++) {
                            html += '<p>' + data.errors[count] + '</p>';
                        }
                        html += '</div>';

                        $('#form_result2').html(null);
                        $('#form_result1').html(null);
                        $('#form_result2').html(html);

                    }
                    if (data.success) {
                        window.location.href = '/venues/list';
                    }
                }
            })
        });
    </script>
</body>

</html>
