 <!--Main Sidebar -->
 <div class="sticky" style="margin-bottom: -65px;">
     <aside class="app-sidebar ps horizontal-main ps--active-y">
         <div class="app-sidebar__header"> <a class="main-logo" href="/patients"> <img
                     src="{{ asset('assets/dist/img/AdminLTELogo.png') }}" class="desktop-logo desktop-logo-dark"
                     alt="HealthAppLogo"> <img src="{{ asset('assets/dist/img/AdminLTELogo.png') }}" class="desktop-logo"
                     alt="HealthAppLogo"> <img src="{{ asset('assets/dist/img/AdminLTELogo.png') }}"
                     class="mobile-logo mobile-logo-dark" alt="HealthAppLogo"> <img
                     src="{{ asset('assets/dist/img/AdminLTELogo.png') }}" class="mobile-logo" alt="HealthAppLogo">
             </a> </div>

         <div class="main-sidemenu">
             <div class="slide-left disabled d-none" id="slide-left">
                 <svg xmlns="http://www.w3.org/2000/svg" fill="#7b8191" width="24" height="24"
                     viewBox="0 0 24 24">
                     <path d="M13.293 6.293 7.586 12l5.707 5.707 1.414-1.414L10.414 12l4.293-4.293z">
                     </path>
                 </svg>
             </div>


             <ul class="side-menu" style="margin-left: 0px;">
                 <li class="side-item side-item-category">Patient Management</li>
                 <li class="slide">
                     <a class="side-menu__item {{ Route::is('patients') ? 'active' : '' }}" data-bs-toggle="slide"
                         href="{{ route('patients') }}">
                         <span class="side-menu__icon">
                             <i class="fa fa-user side_menu_img"></i>
                         </span>
                         <span class="side-menu__label">Patient Details</span>
                     </a>
                     <a class="side-menu__item {{ Route::is('Kinpatients') ? 'active' : '' }}" data-bs-toggle="slide"
                         href="{{ route('Kinpatients') }}">
                         <span class="side-menu__icon">
                             <i class="fa fa-users side_menu_img"></i>
                         </span>
                         <span class="side-menu__label">Next Of Kin</span>
                     </a>
                 </li>
             </ul>


             <ul class="side-menu" style="margin-left: 0px;">
                 <li class="side-item side-item-category">Health Staff Management</li>
                 <li class="slide">
                     <a class="side-menu__item {{ Route::is('healthstaffManagement') ? 'active' : '' }}" data-bs-toggle="slide"
                         href="{{ route('healthstaffManagement') }}">
                         <span class="side-menu__icon">
                             <i class="fa fa-user side_menu_img"></i>
                         </span>
                         <span class="side-menu__label">Staff Details</span>
                     </a>
                     <a class="side-menu__item {{ Route::is('practissionarManagement') ? 'active' : '' }}" data-bs-toggle="slide"
                         href="{{ route('practissionarManagement') }}">
                         <span class="side-menu__icon">
                             <i class="fa fa-users side_menu_img"></i>
                         </span>
                         <span class="side-menu__label">Practitioner Details</span>
                     </a>
                     <a class="side-menu__item {{ Route::is('facilityManagement') ? 'active' : '' }}" data-bs-toggle="slide"
                         href="{{ route('facilityManagement') }}">
                         <span class="side-menu__icon">
                             <i class="fa fa-users side_menu_img"></i>
                         </span>
                         <span class="side-menu__label">Facility Details</span>
                     </a>
                 </li>
             </ul>




             <ul class="side-menu {{ Route::is('staffDashboard') ? 'active' : '' }}" style="margin-left: 0px;">
                 <li class="slide">
                     <a class="side-menu__item {{ Route::is('staffDashboard') ? 'active' : '' }}"
                         data-bs-toggle="slide" href="/staff-dashboard">
                         <span class="side-menu__icon">
                             <i class="fa fa-user-md side_menu_img"></i>
                         </span>
                         <span class="side-menu__label">Queue Management</span>
                     </a>
                 </li>
             </ul>
             <ul class="side-menu  {{ Route::is('reportManagement') ? 'active' : '' }}" style="margin-left: 0px;">
                 <li class="slide">
                     <a class="side-menu__item  {{ Route::is('reportManagement') ? 'active' : '' }}"
                         data-bs-toggle="slide" href="/report_management">
                         <span class="side-menu__icon">
                             <i class="fa fa-user-md side_menu_img"></i>
                         </span>
                         <span class="side-menu__label">Report Management</span>
                     </a>
                 </li>
             </ul>
             <ul class="side-menu {{ Route::is('adminclinicsName') ? 'active' : '' }}" style="margin-left: 0px;">
                 <li class="slide">
                     <a class="side-menu__item {{ Route::is('adminclinicsName') ? 'active' : '' }}"
                         data-bs-toggle="slide" href="/clinics">
                         <span class="side-menu__icon">
                             <i class="fa fa-user-md side_menu_img"></i>
                         </span>
                         <span class="side-menu__label">Clinics</span>
                     </a>
                 </li>
             </ul>
             <ul class="side-menu {{ Route::is('departments') ? 'active' : '' }}" style="margin-left: 0px;">
                 <li class="slide">
                     <a class="side-menu__item {{ Route::is('departments') ? 'active' : '' }}" data-bs-toggle="slide"
                         href="/departments">
                         <span class="side-menu__icon">
                             <i class="fa fa-user-md side_menu_img"></i>
                         </span>
                         <span class="side-menu__label">Departments</span>
                     </a>
                 </li>
             </ul>
             <ul class="side-menu {{ Route::is('testManagement') ? 'active' : '' }}" style="margin-left: 0px;">
                 <li class="slide">
                     <a class="side-menu__item {{ Route::is('testManagement') ? 'active' : '' }}"
                         data-bs-toggle="slide" href="/testManagement">
                         <span class="side-menu__icon">
                             <i class="fa fa-user-md side_menu_img"></i>
                         </span>
                         <span class="side-menu__label">Test Management</span>
                     </a>
                 </li>
             </ul>
             <ul class="side-menu" style="margin-left: 0px;">
                 <li class="slide">
                     <form method="POST" action="{{ route('logout') }}">
                         @csrf

                         <a class="side-menu__item" href="{{ route('logout') }}"
                             onclick="event.preventDefault();
                                            this.closest('form').submit();"data-bs-toggle="slide"
                             href="/staff-dashboard">
                             <span class="side-menu__icon">
                                 <i class="fa fa-user-md side_menu_img"></i>
                             </span>
                             <span class="side-menu__label">Logout</span>
                         </a>
                     </form>
                 </li>
             </ul>
         </div>
         <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
             <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
         </div>
         <div class="ps__rail-y" style="top: 0px; height: 375px; right: 0px;">
             <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 174px;"></div>
         </div>
     </aside>
 </div>
 <!--Main sidebar -->
