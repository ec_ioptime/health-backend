<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Token number</title>
    <!-- bootstrap 5 css CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css"
        integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
</head>
<style>
    body {
        min-height: 100vh;
        display: flex;
        align-items: center;
        background: rgb(255, 255, 255);
        background: linear-gradient(90deg, rgba(255, 255, 255, 1) 0%, rgba(184, 216, 254, 1) 100%);
    }

    .token_number_box {
        width: 550px;
        height: 300px;
        max-width: 100%;
        margin: 0 auto;
        border-radius: 16px;
        color: #0f4c9b;
    }

    .text {
        font-size: 20px;
    }

    .num {
        font-size: 90px;
        margin-top: 0;
        line-height: 100%;
        font-weight: 600;
    }
</style>

<body class="px-3">
    <div class="token_number_box bg-white py-4 d-flex flex-column shadow align-items-center justify-content-center">
        <span class="text">Token Number</span>
        @if(isset($queue))
        <h1 class="num">0{{ $queue->currenNumber }}</h1>
        @else
        <h1 class="num">NA</h1>
        @endif
    </div>
</body>

</html>
