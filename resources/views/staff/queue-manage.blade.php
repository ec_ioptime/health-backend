<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manage Queue</title>
    <!-- bootstrap 5 css CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css"
        integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
</head>
<style>
    .head {
        padding: 35px 20px;
        background-color: #d7e5ff;
        font-size: 22px;
    }

    .current_token {
        /* font-size: 18px; */
        margin: 20px;
        color: #0f4c9b;
    }

    .num {
        margin-bottom: 20px;
        color: #0f4c9b;
    }

    .btn {
        padding: 6px 18px;
        border-radius: 26px;
        width: 102px;
    }

    .btn.reset_queue {
        border: 1px solid #808080;
        color: #808080;
    }

    .btn.next_token {
        background-color: #094d97;
        color: #fff;
    }

    .time {
        color: #0f4c9b;
    }
</style>

<body class="text-center">
    <div class="head text-center">
        Manage Queue
    </div>
    <h4 class="current_token">Current Token</h4>
    @if (isset($nextNumber))
        <h1 class="num">0{{ $queue->currenNumber }}</h1>
    @elseif(isset($queue->currenNumber))
        <h1 class="num">0{{ $queue->currenNumber }}</h1>
    @else
        <h4 class="num">Queue Empty</h4>
    @endif
    @if (isset($nextNumber))
        <h4 class="time">Next: 0{{ $nextNumber->queue_number }}</h4>
    @else
        <h4 hi class="time">Next: Queue Ended </h4>
    @endif
    <div class="btns mt-5">
        {{-- <a href="#" class="btn  reset_queue mx-lg-4 mx-2"
            @if (!isset($queue)) style="pointer-events: none" @endif>Reset</a> --}}
        <a href="#" id="next_token" class="btn next_token mx-lg-4 mx-2"
            @if (!isset($nextNumber)) style="pointer-events: none" @endif>Call Next</a>
    </div>
</body>

</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
    integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/gh/AmagiTech/JSLoader/amagiloader.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


@if (isset($nextNumber) || isset($queue->queue_number))
    <script>
        $(document).ready(function(){
            $('.next_token').on('click',function(){
                console.log("next_token");
            });
        });
        $('.next_token').click(function() {
            console.log("data ");
            AmagiLoader.show();
            const queueId = "{{ $queue->id }}";
            var newURL = "{{ route('generate-queue', ':id') }}";
            newURL = newURL.replace(':id', queueId);
            return $.ajax({
                url: newURL,
                type: 'GET',
                success: function(data) {
                    AmagiLoader.hide();
                    if (data.status == "success") {
                        /* retrieve new results from callback */
                        location.reload();
                    } else {

                    }
                },
                error: function(response) {
                    AmagiLoader.hide();
                    swal("Oops", "Something went wrong!", "error")
                },
            });

        });

        $(".reset_queue").click(function() {

            AmagiLoader.show();
            const queueId = "{{ $queue->id }}";
            var newURL = "{{ route('reset-queue', ':id') }}";
            newURL = newURL.replace(':id', queueId);
            return $.ajax({
                url: newURL,
                type: 'GET',
                success: function(data) {
                    AmagiLoader.hide();
                    if (data.status == "success") {
                        /* retrieve new results from callback */
                        location.reload();
                    } else {

                    }
                },
                error: function(response) {
                    AmagiLoader.hide();
                    swal("Oops", "Something went wrong!", "error")
                },
            });

        });
    </script>
@endif
