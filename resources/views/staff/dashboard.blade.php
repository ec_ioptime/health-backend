<x-app-layout>
    <style>
        .row.flexHeader {
            padding: 20px 20px 10px 10px;
        }
    </style>
    <x-slot name="header">
        <div class="page-header">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Queue Management System</li>
            </ol>
        </div>
    </x-slot>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <div class="row bg-white pt-5 pb-5">
        <div class="col-lg-12">

            <form id="queue-form" type="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="sel1">Select Clinic:</label>
                    <select required class="form-control" id="clinic_name" name="clinic">
                        <option value="" disabled selected> Select Clinic </option>
                        @foreach ($clinics as $clinic)
                            <option value="{{ $clinic->id }}"> {{ $clinic->name }} </option>
                        @endforeach
                    </select>
                    <span class="text-danger" id="clinicError"></span>
                </div>
                <div class="form-group">
                    <label for="department_name">Select Department:</label>
                    <select required class="form-control" name="department" id="department_name">
                        <option value="" disabled selected> Select Department </option>

                        @foreach ($departments as $department)
                            <option value="{{ $department->id }}"> {{ $department->department_name }} </option>
                        @endforeach
                    </select>
                    <span class="text-danger" id="departmentError"></span>
                </div>
                <div class="modal-footer">
                    <button type="button" id="current-token" class="btn btn-primary">Display</button>
                    <button type="button" id="manage-queue" class="btn btn-primary">Manage</button>
                    {{-- <a href="{{ route("current-token") }}" class="button" target="_blank" rel="noopener noreferrer">asd</a> --}}
                </div>
            </form>
        </div>
    </div>

</x-app-layout>
<script>
    $(function() {

        $('#clinic_name').change(function() {
            const clinic_id = $(this).val();
            getTriggers(clinic_id);
        });
        $('#department_name').change(function() {
            const department_id = $(this).val();
        });

        function getTriggers(clinic_id) {
            /* get departmenmts for selected clinic. */
            var triggerUrl = "{{ route('admin.get-departments', ':id') }}";
            triggerUrl = triggerUrl.replace(':id', clinic_id);
            return $.ajax({
                url: triggerUrl,
                type: 'GET',
                dataType: "application/json",
                contentType: "application/json",
                statusCode: {
                    200: function(data) {
                        let response = JSON.parse(data.responseText);
                        console.log(response.status == "success");
                        /* add triggers to view.*/
                        if (response.status == "success") {
                            /* retrieve new results from callback */
                            let newDepartments = response.departments;
                            var departments;
                            newDepartments.forEach(function(item) {
                                departments +=
                                    `<option value=${item.id}>${item.department_name}</option>`;
                            });
                            trigger_departments = departments;
                            let userTriggers = {
                                "totalDepartments": departments,
                            }
                            let $departmentsSelect = $("#department_name");
                            $departmentsSelect.empty().append(userTriggers.totalDepartments);
                        }
                    }
                }
            });
        }
        $('#current-token').click(function() {
            const department_id = $('#department_name').val();
            const clinic_id = $('#clinic_name').val();
            if (!department_id || !clinic_id) {
                alert('Please select Department and Clinic')
            } else {
                var triggerUrl = "{{ route('current-token', ':id') }}";
                triggerUrl = triggerUrl.replace(':id', department_id);
                window.open(
                    triggerUrl,
                    '_blank' // <- This is what makes it open in a new window.
                );
            }


        });
        $('#manage-queue').click(function() {
            const department_id = $('#department_name').val();
            const clinic_id = $('#clinic_name').val();
            if (!department_id || !clinic_id) {
                alert('Please select Department and Clinic')
            } else {
                var newURL = "{{ route('manage-queue', ':id') }}";
                newURL = newURL.replace(':id', department_id);
                window.open(
                    newURL,
                    '_blank' // <- This is what makes it open in a new window.
                );
            }

        });
    });
</script>
