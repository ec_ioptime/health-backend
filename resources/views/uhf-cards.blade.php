<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            UHF CARDS
        </h2>
    </x-slot>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <style>
        .modal-backdrop {
            opacity: 0.5 !important;
        }

        .button-style {
            float: right;
            margin-bottom: inherit
        }
    </style>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="container" style="margin:20px; padding:20px">
                    <div class="button-style">
                        <button type="button" data-toggle="modal" data-target="#addUhfModal" id="addUhfCardBtn"
                            class="btn btn-primary btn-sm m-0">Add Card</button>
                    </div>

                    <table id="uhfCardsTable" class="table table-striped table-bordered table-sm" cellspacing="0"
                        width="100%">
                        <thead>
                            <tr>
                                <th class="th-sm">Card ID</th>
                                <th class="th-sm">Phone Number</th>
                                <th class="th-sm">Card Number</th>
                                <th style="col-md-1">Action</th>
                            </tr>

                        </thead>
                        <tbody>
                            @foreach ($cards as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->phone_number }}</td>
                                <td>{{ $item->card_number }}</td>
                                <td>
                                    <button type="button" class="delete-card btn btn-primary btn-sm m-0"
                                        data-user-id="{{ $item->id }}" onclick="deleteCard({{  $item->id }})">Delete Card</button>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" id="addUhfModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add UHF Card</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="add_card_form" type="post">
                        {{ csrf_field() }}
                        <input type="hidden" id="user_id" name="user_id">
                        <div class="form-group">
                            <label for="phone_number">Phone No</label>
                            <input type="number" min="0" required name="phone_number" class="form-control"
                                id="phone_number">
                        </div>
                        <div class="form-group">
                            <label for="card_number">Card No</label>
                            <input type="number" min="0" required name="card_number" class="form-control"
                                id="card_number">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" id="add-uhf" class="btn btn-primary">Add Card</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</x-app-layout>

<script>

    function deleteCard(card_id) {
         var card_id = card_id;
        console.log(card_id);
        let url = "{{ route('delete-uhf-card',':id') }}";
        url = url.replace(':id', card_id);
        swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this Card!",
    type: "warning",
    icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnClickOutside: false,
            closeOnCancel: true,
                dangerMode: true,
            },
            function (isConfirm) {
                console.log(isConfirm);
                if (isConfirm) {
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        data: {
                            '_token': "{{ csrf_token() }}",

                        },
                        success: function (data) {
                            if (data['status'] == 'success') {
                                alert('Card Deleted Successfully');
                                location.reload();
                            } else {
                                alert('Something went wrong');
                            }
                        },
                        error: function (response) {
                            alert('Something went wrong');
                        }
                    });
                }
            });
    }
    $(function () {
    $('#uhfCardsTable').DataTable({
        "pagingType": "simple" // "simple" option for 'Previous' and 'Next' buttons only
    });
    $('.dataTables_length').addClass('bs-select');

    $('#addUhfModal').on('shown.bs.modal', function () {
        $('#phone_number').trigger('focus')
    })
    $('#add-uhf').click(function () {
        var url = "{{ route('add-uhf-cards') }}";
        $.ajax({
            type: "POST",
            url: url,
            data: $('#add_card_form').serialize(),
            success: function (data) {
                if (data['status'] == 'success') {
                    alert('Card Added Successfully');
                    location.reload();
                } else {
                    alert('Something went wrong');
                }
            },
            error: function (response) {
                alert('Something went wrong');
            }
        });
    });
    });
</script>
