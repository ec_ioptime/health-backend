<x-app-layout>
    <x-slot name="header">

        <div class="row">
            <div class="col-lg-8">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    PCR Records
                </h2>
            </div>
            {{-- <div class="col-lg-4">
                <button style="float: right" type="button" data-toggle="modal" data-target="#addDepartment"
                    class="btn btn-primary btn-sm m-0">Add Department</button>
            </div> --}}
        </div>

    </x-slot>
    <style>
        .swal-button {
            width: 90px
        }

        .modal-backdrop {
            opacity: 0.5 !important;
        }

        .btn-width2 {
            width: 100px;
        }

        .btn-width {
            width: 134px;
        }

        .button-style {
            float: right;
            margin-bottom: inherit
        }

        .paginate_button {
            width: 120px
        }
    </style>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="container" style="margin:20px; padding:20px">
                    {{-- ID document, type of id , name age gender , vaccination status --}}
                    {{-- <div class="form-check">
  <input class="form-check-input" type="checkbox" id="vaccinated" name="Vaccinated" value="Vaccinated" >
  <label class="form-check-label ">Vaccinated</label>
</div>
<div class="form-check">
  <input class="form-check-input" type="checkbox" id="unVaccinated" name="unVaccinated" value="unVaccinated" >
  <label class="form-check-label">Not Vaccinated</label>
</div> --}}


                    <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0"
                        width="100%">
                        <thead>
                            <tr>
                                <th class="th-sm">Name</th>
                                <th class="th-sm">PCR Status</th>
                                <th style=" text-align:center">Action</th>

                                {{-- <th>Email</th> --}}
                            </tr>

                        </thead>
                        <tbody>
                            @if (isset($records))
                                @foreach ($records as $record)
                                    <tr>
                                        <td>{{ $record->user_name }}</td>
                                        <td>{{ $record->pcr_status }}</td>
                                        <td style="text-align: center;">
                                            {{-- <button type="button" data-toggle="modal" data-target="#update_patientModal"
                                      id="addBtn" class="btn btn-primary btn-sm m-0 addBalance btn-width"
                                      onclick="stars({{  $patient->id }})" data-patient-id="{{ $patient->id }}">Update
                                      </button> --}}
                                            <button type="button"
                                                class="delete-patient btn btn-primary btn-sm m-0 btn-width"
                                                onclick="changeStatus({{ $record->id }})">Change Status </button>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    function changeStatus(recordId) {
        var record = recordId;
        let url = "{{ route('record.change-status', ':id') }}";
        url = url.replace(':id', record);
        swal({
                title: "Are you sure?",
                text: "The Status will be changed!",
                icon: "info",
                buttons: true,
                dangerMode: false
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            '_token': "{{ csrf_token() }}",
                        },
                        success: function(data) {
                            if (data['status'] == 'success') {
                                swal("Status has been changed successfully!", {
                                    icon: "success",
                                });
                                setTimeout(() => {
                                    location.reload();
                                }, 1000);
                            } else {
                                swal("Oops", "Something went wrong!", "error")

                            }
                        },
                        error: function(response) {
                            swal("Oops", "Something went wrong!", "error")

                        }
                    });
                }
            });

    }

    $(function() {
        $('#dtBasicExample').DataTable({
            "pagingType": "simple",
            // "simple" option for 'Previous' and 'Next' buttons only
        });
        $('.dataTables_length').addClass('bs-select');

    });
</script>
