<x-app-layout>

    <x-slot name="header">
        <div class="row">
            <div class="col-lg-8">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    Doctor Appointments
                </h2>
            </div>

            {{-- <div class="col-lg-4">

                <button style="float: right" type="button" data-toggle="modal" data-target="#addCategory"
                    class="btn btn-primary float-right btn-sm m-0">Add Availability</button>


            </div> --}}
        </div>

    </x-slot>

    <style>
        .modal-backdrop {
            opacity: 0.5 !important;
        }

        img {
            vertical-align: middle;
            border-style: none;
            max-width: 35px;
        }
    </style>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

    @if (Session::has('success'))
        <div class="alert alert-success">
            <strong>Success: </strong>{{ Session::get('success') }}

        </div>
    @endif

    <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="th-sm">id</th>
                <th class="th-sm">Doctor</th>
                <th class="th-sm">Patient</th>
                <th class="th-sm">Status</th>
                <th class="th-sm">Type</th>
                <th class="th-sm">Description</th>
                <th class="th-sm">Date</th>
                <th class="th-sm">Time</th>
                {{-- <th style=" text-align:center;width: 285px">Action</th> --}}
            </tr>
        </thead>
        <tbody>
            @foreach ($appointments as $index => $list)
                @if ($list->doc_id == Auth::user()->id || (Auth::user()->is_admin = '1'))
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>
                            @php
                                $doc = DB::table('users')
                                    ->where('id', $list->doctor_id)
                                    ->first();
                            @endphp
                            {{ $doc->name }}
                        </td>
                        <td>
                            @php
                                $doc = DB::table('users')
                                    ->where('id', $list->patient_id)
                                    ->first();
                            @endphp
                            {{ $doc->name }}
                        </td>
                        <td> {{ $list->status }} </td>
                        <td> {{ $list->type }} </td>
                        <td> {{ $list->description }} </td>
                        <td> {{ $list->date }} </td>
                        <td> {{ $list->time }} </td>
                        {{-- <td>
                            <a href="/availabledelete/{{ $list->id }}" style="float: right;"
                                class="btn btn-danger">Delete</a>
                        </td> --}}
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>

    {{-- Add Category --}}
    <div class="modal" tabindex="-1" id="addCategory">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Doctor Availability</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="/available" enctype="multipart/form-data">
                        @csrf

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="price">Price</label>
                                    <input required type="number" min="1" name="price"
                                        class="form-control price" id="price">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="currency">Currency</label>
                                    <select required class="form-control" name="currency" id="currency">
                                        <option value="">Select Currency</option>
                                        <option value="$">Dollar $</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="fromdate">From Date</label>
                                    <input required type="date" min="1" name="fromdate"
                                        class="form-control fromdate" id="fromdate">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="todate">To Date</label>
                                    <input required type="date" min="1" name="todate"
                                        class="form-control todate" id="todate">
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="fromtime">From Time</label>
                                    <input required type="time" min="1" name="fromtime"
                                        class="form-control fromtime" id="fromtime">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="totime">To Time</label>
                                    <input required type="time" min="1" name="totime"
                                        class="form-control totime" id="totime">
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="typeof">Type of Availability</label>
                            <select required name="typeof" class="form-control typeof" id="typeof">
                                <option value="">Please Select</option>
                                <option value="video">Video Call</option>
                                <option value="audio">Audio Call</option>
                                <option value="clinic">Clinic</option>
                            </select>
                        </div>

                        <div class="modal-footer">
                            <input type="submit" class=" btn btn-primary" value="Save">

                            <button type="button" class="btn btn-secondary" style="width: 100px;"
                                data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</x-app-layout>
