<x-app-layout>

    <x-slot name="header">
        <div class="row">
            <div class="col-lg-8">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    Doctor Categories
                </h2>
            </div>
            <div class="col-lg-4">
                <button style="float: right" type="button" data-toggle="modal" data-target="#addCategory"
                    class="btn btn-primary float-right btn-sm m-0">Add Doctor</button>
            </div>
        </div>
    </x-slot>

    <style>
        .modal-backdrop {
            opacity: 0.5 !important;
        }

        img {
            vertical-align: middle;
            border-style: none;
            max-width: 35px;
        }
    </style>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>



    <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="th-sm">id</th>
                <th class="th-sm">Image</th>
                <th class="th-sm">Name</th>
                <th style=" text-align:center;width: 285px">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($categorys as $index => $cate)
                <tr>
                    <td>{{ $cate->id }}</td>
                    <td><img src="{{ $cate->image1 }}" alt="{{ $cate->name }}"></td>
                    <td>{{ $cate->name }}</td>
                    <td style="text-align: center;">
                        <button type="button" data-toggle="modal" data-target="#updateCategory" id="addBtn"
                            class="btn btn-primary btn-sm m-0  btn-width addBtn" data-id="{{ $cate->id }}"
                            data-name="{{ $cate->name }}">Edit</button>
                        <button type="button"class="delete-categoryD btn btn-danger btn-sm m-0"
                            onclick="deleteCategory({{ $cate->id }})">Delete</button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>


    {{-- update Category --}}
    <div class="modal" tabindex="-1" id="updateCategory">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Doctor Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" class="sample_form" class="form-horizontal" enctype="multipart/form-data">
                        @csrf

                        <input type="hidden" class="cate_id" name="cate_id">

                        <div class="form-group">
                            <label for="category">Name</label>
                            <input required type="text" min="1" name="name"
                                class="form-control updatecategory" id="updatecategory">
                            <span class="text-danger updatecategoryerror" id="updatecategoryerror"></span>
                        </div>


                        <div class="form-group">
                            <label for="image1">Icon Image:</label>
                            <input class="form-control image1" type="file" name="image1" id="image1">
                            <span class="text-danger" id="profileError"></span>
                        </div>


                        <div class="modal-footer">

                            <input type="submit" class=" btn btn-primary" value="Save">

                            {{-- <button type="button" id="update-category" style="width: 100px;"
                                class=" btn    btn-primary">Update</button> --}}
                            <button type="button" class="btn btn-secondary" style="width: 100px;"
                                data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    {{-- Add Category --}}
    <div class="modal" tabindex="-1" id="addCategory">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Doctor Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" class="sample_form" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="category">Name</label>
                            <input required type="text" min="1" name="name" class="form-control"
                                id="addcategory">
                            <span class="text-danger" id="addcategoryerror"></span>
                        </div>


                        <div class="form-group">
                            <label for="image1">Icon Image:</label>
                            <input required class="form-control image1" type="file" name="image1"
                                id="image1">
                            <span class="text-danger" id="profileError"></span>
                        </div>

                        <div class="modal-footer">
                            <input type="submit" class=" btn btn-primary" value="Save">
                            {{-- <button type="button" id="add-category" style="width: 100px;"
                               >Save</button> --}}
                            <button type="button" class="btn btn-secondary" style="width: 100px;"
                                data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>



    <script>
        $('.sample_form').on('submit', function(event) {
            event.preventDefault();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var url = "{{ route('adddoctorCategory') }}";
            $.ajax({
                type: "POST",
                url: url,
                // data: $('#update_category').serialize(),
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    if (data['status'] == 'success') {
                        $('#updatecategoryerror').text("");
                        swal("Doctor Category Updated successfully!", {
                            icon: "success",
                        });
                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    } else {
                        swal("Oops", "Something went wrong!", "error")
                    }
                },
                error: function(response) {
                    var res = JSON.parse(response.responseText);
                    if (res.errorexist) {
                        swal("Oops", res.errorexist, "error")
                    } else {
                        $('#updatecategoryerror').text(res.errors.name);
                    }
                },
            });

        });


        $('.addBtn').click(function() {

            id = $(this).data('id');
            name = $(this).data('name');

            $('.cate_id').val(id);
            $('.updatecategory').val(name);
        });


        // Update Category Code
        $('#update-category').click(function() {

            $('.cate_id').text('');
            $('.updatecategory').text('');

            var url = "{{ route('adddoctorCategory') }}";
            $.ajax({
                type: "POST",
                url: url,
                data: $('#update_category').serialize(),
                success: function(data) {
                    console.log(data);
                    if (data['status'] == 'success') {
                        $('#updatecategoryerror').text("");
                        swal("Doctor Category Updated successfully!", {
                            icon: "success",
                        });
                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    } else {
                        swal("Oops", "Something went wrong!", "error")
                    }
                },
                error: function(response) {
                    var res = JSON.parse(response.responseText);
                    if (res.errorexist) {
                        swal("Oops", res.errorexist, "error")
                    } else {
                        $('#updatecategoryerror').text(res.errors.name);
                    }
                },
            });
        });


        // Add Category Code
        $('#add-category').click(function() {
            $('#addcategory').text("");
            var url = "{{ route('adddoctorCategory') }}";
            $.ajax({
                type: "POST",
                url: url,
                data: $('#add_category').serialize(),
                success: function(data) {
                    console.log(data);
                    if (data['status'] == 'success') {
                        $('#addcategoryerror').text("");
                        swal("Doctor Category Added successfully!", {
                            icon: "success",
                        });
                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    } else {
                        swal("Oops", "Something went wrong!", "error")
                    }
                },
                error: function(response) {
                    var res = JSON.parse(response.responseText);
                    console.log(res.errorexist);
                    if (res.errorexist) {
                        swal("Oops", res.errorexist, "error")
                    } else {
                        $('#addcategoryerror').text(res.errors.name);
                    }
                },
            });
        });

        // Delete Category
        function deleteCategory(userId) {
            var user = userId;
            let url = "{{ route('doctorCategoryD', ':id') }}";
            url = url.replace(':id', user);
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the Category Doctor!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: "DELETE",
                            url: url,
                            data: {
                                '_token': "{{ csrf_token() }}",
                            },
                            success: function(data) {
                                if (data['status'] == 'success') {
                                    swal("Category Deleted successfully!", {
                                        icon: "success",
                                    });
                                    setTimeout(() => {
                                        location.reload();
                                    }, 1000);
                                } else {
                                    swal("Oops", "Something went wrong!", "error")

                                }
                            },
                            error: function(response) {
                                swal("Oops", "Something went wrong!", "error")

                            }
                        });
                    }
                });
        }
    </script>

</x-app-layout>
