<x-app-layout>
    <x-slot name="header">

        <div class="row">
            <div class="col-lg-8">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    Doctors
                </h2>
            </div>
            <div class="col-lg-4">
                <button style="float: right" type="button" data-toggle="modal" data-target="#addDoctor"
                    class="btn btn-primary btn-sm m-0">Add
                    Doctor</button>
            </div>
        </div>

    </x-slot>
    <style>
        .modal-backdrop {
            opacity: 0.5 !important;
        }

        .btn-width2 {
            width: 100px;
        }

        .btn-width {
            width: 134px;
        }

        .button-style {
            float: right;
            margin-bottom: inherit
        }

        .paginate_button {
            width: 120px
        }
    </style>
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 mt-5">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
            <div class="container" style="margin:20px; padding:20px">
                <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0"
                    width="100%">
                    <thead>
                        <tr>
                            <th class="th-sm">Profile</th>
                            <th class="th-sm">Name</th>
                            <th class="th-sm">Contact Number</th>
                            <th class="th-sm">Email</th>
                            <th class="th-sm">Category</th>
                            <th class="th-sm">Clinic</th>
                            <th class="th-sm">Department</th>
                            {{-- <th>Email</th> --}}
                            <th style=" text-align:center;width: 285px">Action</th>
                        </tr>

                    </thead>
                    <tbody>
                        @if (isset($data))
                            @foreach ($data as $user)
                                <tr>
                                    <td> <img style="max-width: 50px;" src="{{ $user->profile_pic }}"
                                            alt="{{ $user->name }}"></td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->phone_number }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        @if (isset($user->category))
                                            {{ $user->category->name }}
                                        @endif
                                    </td>
                                    <td>
                                        @if (isset($user->clinic->name))
                                            {{ $user->clinic->name }}
                                        @endif
                                    </td>
                                    <td>
                                        @if (isset($user->department->department_name))
                                            {{ $user->department->department_name }}
                                        @endif
                                    </td>
                                    <td style="text-align: center;">
                                        <button type="button" data-toggle="modal" data-target="#updatePassModal"
                                            id="addBtn" class="btn btn-primary btn-sm m-0  btn-width"
                                            onclick="setUserId({{ $user->id }})"
                                            data-user-id="{{ $user->id }}">Update Password</button>
                                        <button type="button" class="delete-user btn btn-primary btn-sm m-0 btn-width"
                                            data-user-id="{{ $user->id }}"
                                            onclick="deleteUser({{ $user->id }})">Delete Doctor</button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{-- update password --}}
    <div class="modal" tabindex="-1" id="updatePassModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Doctor Password</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="update_password_form" type="post">
                        {{ csrf_field() }}
                        <input type="hidden" id="user_id" name="user_id">
                        <div class="form-group">
                            <label for="password">New Password</label>
                            <input type="password" min="1" name="password" class="form-control" id="password">
                            <span class="text-danger" id="passwordError"></span>
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Confirm Password</label>
                            <input type="password" min="1" name="password_confirmation" class="form-control"
                                id="password_confirmation">
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="update-password" style="width: 100px;"
                                class=" btn    btn-primary">Update</button>
                            <button type="button" class="btn btn-secondary" style="width: 100px;"
                                data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    {{-- add staff modal  --}}
    <div class="modal" tabindex="-1" id="addDoctor">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Doctor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" min="0" required name="name" class="form-control"
                                id="name">
                            <span class="text-danger" id="nameError"></span>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" min="0" required name="email" class="form-control"
                                id="email">
                            <span class="text-danger" id="emailError"></span>
                        </div>
                        <div class="form-group">
                            <label for="phone_number">Phone Number</label>
                            <input type="number" min="0" required name="phone_number" class="form-control"
                                id="phone_number">
                            <span class="text-danger" id="phoneError"></span>
                        </div>

                        <div class="form-group">
                            <label for="image1">Profile Image:</label>
                            <input required class="form-control image1" type="file" name="image1"
                                id="image1">
                            <span class="text-danger" id="profileError"></span>
                        </div>

                        <div class="form-group">
                            <label for="sel1">Select Category:</label>
                            <select class="form-control" id="category_name" name="category">
                                <option value="" disabled selected> Select Category </option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}"> {{ $category->name }} </option>
                                @endforeach
                            </select>
                            <span class="text-danger" id="categoryError"></span>
                        </div>
                        <div class="form-group">
                            <label for="sel1">Select Clinic:</label>
                            <select class="form-control" id="clinic_name" name="clinic">
                                <option value="" disabled selected> Select Clinic </option>
                                @foreach ($clinics as $clinic)
                                    <option value="{{ $clinic->id }}"> {{ $clinic->name }} </option>
                                @endforeach
                            </select>
                            <span class="text-danger" id="clinicError"></span>
                        </div>
                        <div class="form-group">
                            <label for="department_name">Select Department:</label>
                            <select class="form-control" name="department" id="department_name">
                                <option value="" disabled selected> Select Department </option>
                                @foreach ($departments as $department)
                                    <option value="{{ $department->id }}"> {{ $department->department_name }}
                                    </option>
                                @endforeach
                            </select>
                            <span class="text-danger" id="departmentError"></span>
                        </div>


                        <div class="form-group">
                            <label for="experience">Experience</label>
                            <input type="text" min="0" required name="experience" class="form-control"
                                id="experience">
                            <span class="text-danger" id="experienceError"></span>
                        </div>
                        <div class="form-group">
                            <label for="degree">Degree</label>
                            <input type="text" min="0" required name="degree" class="form-control"
                                id="degree">
                            <span class="text-danger" id="degreeError"></span>
                        </div>
                        <div class="form-group">
                            <label for="about">About Detail</label>
                            <textarea name="about" id="about" class="form-control" cols="30" rows="6"></textarea>
                            <span class="text-danger" id="aboutError"></span>
                        </div>


                        <div class="modal-footer">
                            <input type="submit" value="Add" class="btn btn-primary">
                            {{-- <button type="button" id="add-doctor" class="btn btn-primary btn-width2">Add</button> --}}
                            <button type="button" class="btn btn-secondary btn-width2"
                                data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


</x-app-layout>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $('#addUhfModal').on('shown.bs.modal', function() {
        $('#phone_number').trigger('focus')
    })
    $('#updatePassModal').on('shown.bs.modal', function() {
        $('#myInput').trigger('focus')
    })

    function deleteUser(userId) {
        var user = userId;
        console.log(user);
        let url = "{{ route('delete-user', ':id') }}";
        url = url.replace(':id', user);
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover the Doctor!",
                icon: "warning",
                buttons: true,
                dangerMode: true
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        data: {
                            '_token': "{{ csrf_token() }}",
                        },
                        success: function(data) {
                            if (data['status'] == 'success') {
                                swal("Doctor Deleted successfully!", {
                                    icon: "success",
                                });
                                setTimeout(() => {
                                    location.reload();
                                }, 1000);
                            } else {
                                swal("Oops", "Something went wrong!", "error")

                            }
                        },
                        error: function(response) {
                            swal("Oops", "Something went wrong!", "error")

                        }
                    });
                }
            });
    }


    $('#sample_form').on('submit', function(event) {
        event.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $('#nameError').text("");
        $('#emailError').text("");
        $('#phoneError').text("");
        $('#categoryError').text("");
        $('#clinicError').text("");
        $('#departmentError').text("");
        var url = "{{ route('admin.doctor.store') }}";
        $.ajax({
            type: "POST",
            url: url,
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType: "json",
            success: function(data) {
                console.log(data);
                if (data['status'] == 'success') {
                    swal("Doctor Added successfully!", {
                        icon: "success",
                    });
                    setTimeout(() => {
                        location.reload();
                    }, 1000);
                } else {
                    swal("Oops", "Something went wrong!", "error")
                }
            },
            error: function(response) {
                var res = JSON.parse(response.responseText);
                if (res.error) {
                    $('#emailError').text(res.error);
                } else {
                    $('#nameError').text(res.errors.name);
                    $('#emailError').text(res.errors.email);
                    $('#phoneError').text(res.errors.phone_number);
                    $('#categoryError').text(res.errors.category);
                    $('#clinicError').text(res.errors.clinic);


                    $('#departmentError').text(res.errors.department);
                    $('#experienceError').text(res.errors.experience);
                    $('#degreeError').text(res.errors.degree);
                    $('#aboutError').text(res.errors.about);
                }
            },
        });



    });

    function setUserId(param) {
        var userId = param;
        $("#user_id").val(userId);
    }
    $(function() {
        $('#dtBasicExample').DataTable({
            "pagingType": "simple",
            // "simple" option for 'Previous' and 'Next' buttons only
        });
        $('.dataTables_length').addClass('bs-select');

        $('#updatePassModal').on('shown.bs.modal', function() {
            $('#myInput').trigger('focus')
        })
        /* add UHF JS-Code */

        $('#add-doctor').click(function() {
            $('#nameError').text("");
            $('#emailError').text("");
            $('#phoneError').text("");
            $('#categoryError').text("");
            $('#clinicError').text("");
            $('#departmentError').text("");
            var url = "{{ route('admin.doctor.store') }}";
            $.ajax({
                type: "POST",
                url: url,
                data: $('#add_doctor').serialize(),
                success: function(data) {
                    console.log(data);
                    if (data['status'] == 'success') {
                        swal("Doctor Added successfully!", {
                            icon: "success",
                        });
                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    } else {
                        swal("Oops", "Something went wrong!", "error")
                    }
                },
                error: function(response) {
                    var res = JSON.parse(response.responseText);
                    if (res.error) {
                        $('#emailError').text(res.error);
                    } else {
                        $('#nameError').text(res.errors.name);
                        $('#emailError').text(res.errors.email);
                        $('#phoneError').text(res.errors.phone_number);
                        $('#categoryError').text(res.errors.category);
                        $('#clinicError').text(res.errors.clinic);


                        $('#departmentError').text(res.errors.department);
                        $('#experienceError').text(res.errors.experience);
                        $('#degreeError').text(res.errors.degree);
                        $('#aboutError').text(res.errors.about);
                    }
                },
            });
        });

        /* update password */
        $('#update-password').click(function() {
            $('#passwordError').text("");

            var id = $('#user_id').val();
            var url = "{{ route('user.updatePassword') }}";
            $.ajax({
                type: "POST",
                url: url,
                data: $('#update_password_form').serialize(),
                success: function(data) {
                    if (data['status'] == 'success') {
                        swal("Password Updated successfully!", {
                            icon: "success",
                        });
                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                        location.reload();
                    } else {
                        console.log(data);
                        swal("Oops", "Something went wrong!", "error")
                    }
                },
                error: function(response) {
                    var res = JSON.parse(response.responseText);
                    $('#passwordError').text(res.errors.password);
                },
            });
        });


        $('#clinic_name').change(function() {
            const clinic_id = $(this).val();
            getTriggers(clinic_id);
        });

        function getTriggers(clinic_id) {
            /* get departmenmts for selected clinic. */
            var triggerUrl = "{{ route('admin.get-departments', ':id') }}";
            triggerUrl = triggerUrl.replace(':id', clinic_id);
            return $.ajax({
                url: triggerUrl,
                type: 'GET',
                dataType: "application/json",
                contentType: "application/json",
                statusCode: {
                    200: function(data) {
                        let response = JSON.parse(data.responseText);
                        console.log(response.status == "success");
                        /* add triggers to view.*/
                        if (response.status == "success") {
                            /* retrieve new results from callback */
                            let newDepartments = response.departments;
                            var departments;
                            newDepartments.forEach(function(item) {
                                departments +=
                                    `<option value=${item.id}>${item.department_name}</option>`;
                            });
                            trigger_departments = departments;
                            let userTriggers = {
                                "totalDepartments": departments,
                            }
                            let $departmentsSelect = $("#department_name");
                            $departmentsSelect.empty().append(userTriggers.totalDepartments);
                        }
                    }
                }
            });
        }

    });
</script>
