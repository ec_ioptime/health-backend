<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Patients
        </h2>
    </x-slot>
    <style>
        .modal-backdrop {
            opacity: 0.5 !important;
        }

        .alert {
            padding: 20px;
            background-color: #04AA6D;
            color: white;
        }

        .closebtn {
            margin-left: 15px;
            color: white;
            font-weight: bold;
            float: right;
            font-size: 22px;
            line-height: 20px;
            cursor: pointer;
            transition: 0.3s;
        }

        .closebtn:hover {
            color: black;
        }

        .button-style {
            float: right;
            margin-bottom: inherit
        }

        h1 {
            margin: 0;
            font-size: 1.4em;
            text-align: center;
        }

        #counter {
            height: $font-size;
            width: 180px;
            overflow: hidden;
            position: relative;
            font-size: $font-size;
            line-height: $font-size;
            text-align: center;
            margin: 20px auto;
            display: block;
            font-family: Menlo, Andale Mono, monospace;
            background: #f0f0f0;
            color: #444;
            text-shadow: 1px 1px 0px white;
            width: 50%;

            &::before,
            &::after {
                position: absolute;
                white-space: pre;
                width: $font-size / 1.75;
            }

            // tens
            &::before {
                content: "0 \a 1 \a 2 \a 3 \a 4 \a 5";
                left: 10px;
                animation: count_tens 60s linear infinite;
            }

            // ones
            &::after {
                content: "0 \a 1 \a 2 \a 3 \a 4 \a 5 \a 6 \a 7 \a 8 \a 9";
                right: 10px;
                animation: count_secs 10s linear infinite;
            }

        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="container" style="margin:20px; padding:20px">
                    <form id="queue-form" type="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="sel1">Select Clinic:</label>
                            <select class="form-control" id="clinic_name" name="clinic">
                                <option value="" disabled selected> Select Clinic </option>
                                @foreach ($clinics as $clinic)
                                    <option value="{{ $clinic->id }}"> {{ $clinic->name }} </option>
                                @endforeach
                            </select>
                            <span class="text-danger" id="clinicError"></span>
                        </div>
                        <div class="form-group">
                            <label for="department_name">Select Department:</label>
                            <select class="form-control" name="department" id="department_name">
                                <option value="" disabled selected> Select Department </option>

                                @foreach ($departments as $department)
                                    <option value="{{ $department->id }}"> {{ $department->department_name }} </option>
                                @endforeach
                            </select>
                            <span class="text-danger" id="departmentError"></span>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="current-token" class="btn btn-primary">Display</button>
                            <button type="button" id="manage-queue" class="btn btn-primary">Manage</button>
                            {{-- <a href="{{ route("current-token") }}" class="button" target="_blank" rel="noopener noreferrer">asd</a> --}}
                        </div>
                    </form>
                </div>
                <div class="container" id="alert-msg" style="display: none">
                    <h1 id="alert-msg1">A Pure CSS Counter is probably completely useless...</h1>
                    <span id="counter"style="font-size: 100px;"> 12</span>

                </div>
            </div>
        </div>
    </div>

</x-app-layout>
<script>
    $(function() {

        $('#clinic_name').change(function() {
            const clinic_id = $(this).val();
            getTriggers(clinic_id);
        });
        $('#department_name').change(function() {
            const department_id = $(this).val();
        });

        function getTriggers(clinic_id) {
            /* get departmenmts for selected clinic. */
            var triggerUrl = "{{ route('admin.get-departments', ':id') }}";
            triggerUrl = triggerUrl.replace(':id', clinic_id);
            return $.ajax({
                url: triggerUrl,
                type: 'GET',
                dataType: "application/json",
                contentType: "application/json",
                statusCode: {
                    200: function(data) {
                        let response = JSON.parse(data.responseText);
                        console.log(response.status == "success");
                        /* add triggers to view.*/
                        if (response.status == "success") {
                            /* retrieve new results from callback */
                            let newDepartments = response.departments;
                            var departments;
                            newDepartments.forEach(function(item) {
                                departments +=
                                    `<option value=${item.id}>${item.department_name}</option>`;
                            });
                            trigger_departments = departments;
                            let userTriggers = {
                                "totalDepartments": departments,
                            }
                            let $departmentsSelect = $("#department_name");
                            $departmentsSelect.empty().append(userTriggers.totalDepartments);
                        }
                    }
                }
            });
        }
        $('#current-token').click(function() {
            const department_id = $('#department_name').val();
            const clinic_id = $('#clinic_name').val();
            console.log();
            var triggerUrl = "{{ route('current-token', ':id') }}";
            triggerUrl = triggerUrl.replace(':id', department_id);
            window.open(
                triggerUrl,
                '_blank' // <- This is what makes it open in a new window.
            );

        });
        $('#manage-queue').click(function() {
            const department_id = $('#department_name').val();
            const clinic_id = $('#clinic_name').val();
            var newURL = "{{ route('manage-queue', ':id') }}";
            newURL = newURL.replace(':id', department_id);
            window.open(
                newURL,
                '_blank' // <- This is what makes it open in a new window.
            );
        });
    });
</script>
