<x-app-layout>

    <x-slot name="header">

        <div class="row">
            <div class="col-lg-8">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    Type of Availability
                </h2>
            </div>

            <div class="col-lg-4">

                <button style="float: right" type="button" data-toggle="modal" data-target="#addCategory"
                    class="btn btn-primary float-right btn-sm m-0">Add Availability</button>

            </div>
        </div>

    </x-slot>

    <style>
        .modal-backdrop {
            opacity: 0.5 !important;
        }

        img {
            vertical-align: middle;
            border-style: none;
            max-width: 35px;
        }
    </style>


    <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="th-sm">id</th>
                <th class="th-sm">Image</th>
                <th class="th-sm">Name</th>
                <th style=" text-align:center;width: 285px">Action</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    {{-- Add Category --}}
    <div class="modal" tabindex="-1" id="addCategory">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Type of Availability</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label for="name">name</label>
                            <input required type="text" min="1" name="name" class="form-control name"
                                id="name">
                        </div>


                        <div class="form-group">
                            <label for="image1">Image</label>
                            <input required type="file" min="1" name="image1" class="form-control image1"
                                id="image1">
                        </div>

                        <div class="modal-footer">
                            <input type="submit" class=" btn btn-primary" value="Save">

                            <button type="button" class="btn btn-secondary" style="width: 100px;"
                                data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</x-app-layout>
