<x-app-layout>

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>


    <x-slot name="header">
        <div class="page-header">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Test Management Dashboard</li>
            </ol>
        </div>


    </x-slot>


    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 mt-5">
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 mt-5">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="container" style="margin:10px; padding:10px">

                    <div class="row">
                        <div class="col-lg-8">
                            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                                Test Management Dashboard
                            </h2>
                        </div>
                        <div class="col-lg-4">
                            <button style="float: right" type="button" id="addTest"
                                class="btn btn-primary btn-sm mr-2">Add
                                Test</button>
                        </div>
                    </div>
                    <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0"
                        width="100%">
                        <thead>
                            <tr>
                                <th class="th-sm">#</th>
                                <th class="th-sm">Staff Name</th>
                                <th class="th-sm">Clinic Name</th>
                                <th class="th-sm">Patient Name</th>
                                <th class="th-sm">SampleID</th>
                                <th class="th-sm">Test Type</th>
                                <th class="th-sm">Result</th>
                                <th class="th-sm">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($test as $index => $item)
                                @php
                                    $user = DB::table('users')
                                        ->where('id', $item->staff_id)
                                        ->first();
                                    $userP = DB::table('users')
                                        ->where('id', $item->patientid)
                                        ->first();
                                    $clinic = DB::table('clinics')
                                        ->where('id', $item->clinic_id)
                                        ->first();
                                @endphp
                                <tr>
                                    <td>{{ $index + 1 }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $clinic->name }}</td>
                                    <td>{{ $userP->name }}</td>
                                    <td>{{ $item->sampleid }}</td>
                                    <td>{{ $item->testtype }}</td>
                                    <td>{{ $item->testresult }}</td>
                                    <td>
                                        <button type="button" class="delete-patient btn btn-danger btn-sm m-1"
                                            onclick="deletePatient({{ $item->id }})">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <style>
        /* Custom CSS to set the modal width */
        .custom-modal .modal-dialog {
            /* max-width: 900px; */
            /* Set the desired width here */
        }
    </style>



    <!-- Modal -->
    <div class="modal fade custom-modal" id="testModelPOP" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Add Test</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/addTestDashboard" method="post" id="myForm" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div id="dynamic-fields">
                            <table>
                                <thead>
                                    <tr>
                                        <th style="width: 40%;" class="th-sm">Clinic Name</th>
                                        <th style="width: 256px;" class="th-sm">Select Test</th>
                                        <th style="width: 122px; text-align: center;" class="th-sm">Add new </th>
                                        <th style="text-align: center;" class="th-sm">Remove</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Initial row -->
                                    <tr class="dynamic-row">
                                        <td>
                                            <select name="clinic[]" required class="form-control user-select">
                                                @php
                                                    $clinics = DB::table('clinics')->get();
                                                @endphp
                                                @foreach ($clinics as $item)
                                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>

                                        <td>
                                            <input type="file" required multiple name="pdfTest[]"
                                                class="form-control">
                                        </td>
                                        <td style="text-align: center;">
                                            <button type="button" class="btn btn-sm btn-primary add-field">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </button>
                                        </td>
                                        <td style="text-align: center;">
                                            <button type="button" class="btn btn-sm btn-danger remove-field">
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>

            </div>
        </div>
    </div>



    <script>
        $(document).ready(function() {
            $('#addTest').on('click', function() {
                $('#testModelPOP').modal('show');
            });
        });

        function deletePatient(patientId) {
            var patient = patientId;
            let url = "{{ route('admin.delete-test', ':id') }}";
            url = url.replace(':id', patient);
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this record!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: {
                                '_token': "{{ csrf_token() }}",
                            },
                            success: function(data) {
                                if (data['status'] == 'success') {
                                    swal("Test result deleted successfully!", {
                                        icon: "success",
                                    });
                                    setTimeout(() => {
                                        location.reload();
                                    }, 1000);
                                } else {
                                    swal("Oops", "Something went wrong!", "error")

                                }
                            },
                            error: function(response) {
                                swal("Oops", "Something went wrong!", "error")

                            }
                        });
                    }
                });

        }
    </script>


    <script>
        // Function to add a new row
        function addRow() {
            var newRow = $(".dynamic-row:last").clone();

            // Clear the values of the cloned form elements
            newRow.find("input, select").val('');

            // Enable all options in the cloned row's dropdowns
            newRow.find("option").prop("disabled", false);

            // Append the new row to the table
            $(".dynamic-row:last").after(newRow);
        }

        // Function to remove the last row
        function removeRow() {
            var rowCount = $(".dynamic-row").length;
            if (rowCount > 1) {
                var removedUser = $(".dynamic-row:last").find(".user-select").val();
                $(".dynamic-row:last").remove();
                var index = selectedUsers.indexOf(removedUser);
                if (index !== -1) {
                    selectedUsers.splice(index, 1);
                }
                updateDisabledOptions();
            }
        }

        // Handle the click event of the "add-field" button
        $(document).on("click", ".add-field", function() {
            addRow();
            updateDisabledOptions();
        });

        // Handle the click event of the "remove-field" button
        $(document).on("click", ".remove-field", function() {
            removeRow();
            updateDisabledOptions(); // Update the disabled options after removing a row
        });

        // Array to keep track of selected user IDs
        var selectedUsers = [];

        // Update disabled options in dropdowns
        function updateDisabledOptions() {
            selectedUsers = [];
            $(".user-select").each(function() {
                var selectElement = $(this);
                selectElement.find("option").show(); // Show all options first
                selectElement.find("option:selected").each(function() {
                    var selectedUserId = $(this).val();
                    selectedUsers.push(selectedUserId);
                });
            });

            $(".user-select").not(":disabled").find("option").each(function() {
                var optionValue = $(this).val();
                if (selectedUsers.includes(optionValue)) {
                    $(this).hide(); // Hide options that have already been selected in other dropdowns
                }
            });
        }

        // Disable selected options in subsequent dropdowns based on user selection
        $(document).on("change", ".user-select", function() {
            var selectedOptionValue = $(this).val();
            if (!selectedUsers.includes(selectedOptionValue)) {
                selectedUsers.push(selectedOptionValue);
            }
            updateDisabledOptions();
        });

        // Call the updateDisabledOptions function on initial page load
        updateDisabledOptions();
    </script>

</x-app-layout>
