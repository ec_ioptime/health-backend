<x-app-layout>
    <x-slot name="header">
        <div class="row">
            <div class="col-lg-8">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    Test Management Dashboard
                </h2>
            </div>
            <div class="col-lg-4">
                <button style="float: right" type="button" id="addTest" class="btn btn-primary btn-sm m-0">Add
                    Test</button>
            </div>
        </div>
    </x-slot>


    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 mt-5">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 mt-5">
            <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0"
                width="100%">
                <thead>
                    <tr>
                        <th class="th-sm">#</th>
                        <th class="th-sm">ID</th>
                        <th class="th-sm">Name</th>
                        <th class="th-sm">Test Name</th>
                        <th class="th-sm">Clinic</th>
                        <th class="th-sm">Department</th>
                        <th style=" text-align:center;width: 285px">Action</th>
                    </tr>
                </thead>
                <tbody>


                </tbody>
            </table>
        </div>
    </div>

    <style>
        /* Custom CSS to set the modal width */
        .custom-modal .modal-dialog {
            max-width: 900px;
            /* Set the desired width here */
        }
    </style>



    <!-- Modal -->
    <div class="modal fade custom-modal" id="testModelPOP" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Add Test</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/addTestDashboard" method="POST" id="myForm" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div id="dynamic-fields">
                            <table>
                                <tbody>
                                    <!-- Initial row -->
                                    <tr class="dynamic-row">
                                        <td>
                                            <select name="user[]" required class="user-select form-control">
                                                @foreach ($user as $item)
                                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select name="clinic[]" required class="form-control">
                                                @foreach ($clinic as $item)
                                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select name="department[]" required class="form-control">
                                                @foreach ($department as $item)
                                                    <option value="{{ $item->id }}">{{ $item->department_name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input type="file" required multiple name="pdfTest[]"
                                                class="form-control">
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-primary add-field">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </button>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-danger remove-field">
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#addTest').on('click', function() {
                $('#testModelPOP').modal('show');
            });
        });


        $("#myForm").submit(function(event) {
            event.preventDefault();
            var userDe = $("select[name='user[]").val();
            var formData = [];
            $("#dynamic-fields .dynamic-row").each(function() {
                var row = $(this);
                var user = row.find("select[name='user[]']").val();
                var clinic = row.find("select[name='clinic[]']").val();
                var department = row.find("select[name='department[]']").val();
                var pdfTest = [];

                // Get all selected files from the file input element
                row.find("input[name='pdfTest[]']").each(function() {
                    var files = this.files;
                    for (var i = 0; i < files.length; i++) {
                        pdfTest.push(files[i]);
                    }
                });

                formData.push({
                    user: user,
                    clinic: clinic,
                    department: department,
                    pdfTest: pdfTest
                });
            });
            console.log(formData);
        });
    </script>


    <script>
        // Function to add a new row
        function addRow() {
            var newRow = $(".dynamic-row:last").clone();

            // Clear the values of the cloned form elements
            newRow.find("input, select").val('');

            // Enable all options in the cloned row's dropdowns
            newRow.find("option").prop("disabled", false);

            // Append the new row to the table
            $(".dynamic-row:last").after(newRow);
        }

        // Function to remove the last row
        function removeRow() {
            var rowCount = $(".dynamic-row").length;
            if (rowCount > 1) {
                var removedUser = $(".dynamic-row:last").find(".user-select").val();
                $(".dynamic-row:last").remove();
                var index = selectedUsers.indexOf(removedUser);
                if (index !== -1) {
                    selectedUsers.splice(index, 1);
                }
                updateDisabledOptions();
            }
        }

        // Handle the click event of the "add-field" button
        $(document).on("click", ".add-field", function() {
            addRow();
            updateDisabledOptions();
        });

        // Handle the click event of the "remove-field" button
        $(document).on("click", ".remove-field", function() {
            removeRow();
            updateDisabledOptions(); // Update the disabled options after removing a row
        });

        // Array to keep track of selected user IDs
        var selectedUsers = [];

        // Update disabled options in dropdowns
        function updateDisabledOptions() {
            selectedUsers = [];
            $(".user-select").each(function() {
                var selectElement = $(this);
                selectElement.find("option").show(); // Show all options first
                selectElement.find("option:selected").each(function() {
                    var selectedUserId = $(this).val();
                    selectedUsers.push(selectedUserId);
                });
            });

            $(".user-select").not(":disabled").find("option").each(function() {
                var optionValue = $(this).val();
                if (selectedUsers.includes(optionValue)) {
                    $(this).hide(); // Hide options that have already been selected in other dropdowns
                }
            });
        }

        // Disable selected options in subsequent dropdowns based on user selection
        $(document).on("change", ".user-select", function() {
            var selectedOptionValue = $(this).val();
            if (!selectedUsers.includes(selectedOptionValue)) {
                selectedUsers.push(selectedOptionValue);
            }
            updateDisabledOptions();
        });

        // Call the updateDisabledOptions function on initial page load
        updateDisabledOptions();
    </script>

</x-app-layout>
