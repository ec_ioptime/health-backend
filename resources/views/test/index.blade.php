<x-app-layout>
    <x-slot name="header">

        <div class="row">
            <div class="col-lg-8">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    Tests
                </h2>
            </div>

            <div class="col-lg-4">

                <button style="float: right" type="button" data-toggle="modal" data-target="#addTests"
                    class="btn btn-primary btn-sm m-0">Add Test</button>

            </div>
        </div>

    </x-slot>
    <style>
        .modal-backdrop {
            opacity: 0.5 !important;
        }

        .btn-width2 {
            width: 100px;
        }

        .btn-width {
            width: 134px;
        }

        .button-style {
            float: right;
            margin-bottom: inherit
        }

        .paginate_button {
            width: 120px
        }
    </style>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="container" style="margin:20px; padding:20px">

                    <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0"
                        width="100%">
                        <thead>
                            <tr>
                                <th class="th-sm">Name</th>
                                <th class="th-sm">Value</th>
                                <th class="th-sm">Unit</th>
                                {{-- <th>Email</th> --}}
                                <th style=" text-align:center;width: 285px">Action</th>
                            </tr>

                        </thead>
                        <tbody>
                            @if (isset($tests))
                                @foreach ($tests as $test)
                                    <tr>
                                        <td>{{ $test->name }}</td>
                                        <td>{{ $test->value }}</td>
                                        <td>{{ $test->unit }}</td>
                                        <td style="text-align: center;">
                                            <button type="button" data-toggle="modal" data-target="#update_test_modal"
                                                id="addBtn" class="btn btn-primary btn-sm m-0  btn-width"
                                                onclick="stars({{ $test->id }})"
                                                data-test-id="{{ $test->id }}">Update Test</button>
                                            <button type="button"
                                                class="delete-test btn btn-primary btn-sm m-0 btn-width"
                                                data-test-id="{{ $test->id }}"
                                                onclick="deleteTest({{ $test->id }})">Delete Test</button>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    {{-- add staff modal  --}}
    <div class="modal" tabindex="-1" id="addTests">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Test</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="add_test" type="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" min="0" required name="name" class="form-control"
                                id="name">
                            <span class="text-danger" id="nameError"></span>
                        </div>
                        <div class="form-group">
                            <label for="phone_number">Description</label>
                            <textarea class="form-control" id="description" style="  resize: none;" name="description" rows="3"></textarea>
                            <span class="text-danger" id="descError"></span>
                        </div>
                        <div class="form-group">
                            <label for="value">Value</label>
                            <input type="text" min="0" required name="value" class="form-control"
                                id="value">
                            <span class="text-danger" id="valueError"></span>
                        </div>
                        <div class="form-group">
                            <label for="unit">Unit</label>
                            <input type="text" min="0" required name="unit" class="form-control"
                                id="unit">
                            <span class="text-danger" id="unitError"></span>
                        </div>


                        <div class="modal-footer">
                            <button type="button" id="add-test" class="btn btn-primary btn-width2">Add</button>
                            <button type="button" class="btn btn-secondary btn-width2"
                                data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    {{-- UPDATE TEST MODAL --}}
    <div class="modal" tabindex="-1" id="update_test_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Test</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="update_test" type="post">
                        <input type="hidden" name="test_id" id="test_id">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" min="0" required name="name" class="form-control"
                                id="updatedTestName">
                            <span class="text-danger" id="updateNameError"></span>
                        </div>
                        <div class="form-group">
                            <label for="phone_number">Description</label>
                            <textarea class="form-control" id="updatedTestDescription" style="  resize: none;" name="description"
                                rows="3"></textarea>
                            <span class="text-danger" id="updateDescError"></span>
                        </div>
                        <div class="form-group">
                            <label for="value">Value</label>
                            <input type="text" min="0" required name="value" class="form-control"
                                id="updatedTestValue">
                            <span class="text-danger" id="updateValueError"></span>
                        </div>
                        <div class="form-group">
                            <label for="unit">Unit</label>
                            <input type="text" min="0" required name="unit" class="form-control"
                                id="updatedTestUnit">
                            <span class="text-danger" id="updateUnitError"></span>
                        </div>


                        <div class="modal-footer">
                            <button type="button" id="update-test"
                                class="btn btn-primary btn-width2">Update</button>
                            <button type="button" class="btn btn-secondary btn-width2"
                                data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</x-app-layout>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $('#updatePassModal').on('shown.bs.modal', function() {
        $('#myInput').trigger('focus')
    })

    function deleteTest(testId) {
        var test = testId;
        let url = "{{ route('admin.test.destroy', ':id') }}";
        url = url.replace(':id', test);
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover the Test!",
                icon: "warning",
                buttons: true,
                dangerMode: true
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        data: {
                            '_token': "{{ csrf_token() }}",
                        },
                        success: function(data) {
                            if (data['status'] == 'success') {
                                swal("Test has been deleted successfully!", {
                                    icon: "success",
                                });
                                setTimeout(() => {
                                    location.reload();
                                }, 1000);
                            } else {
                                swal("Oops", "Something went wrong!", "error")

                            }
                        },
                        error: function(response) {
                            swal("Oops", "Something went wrong!", "error")

                        }
                    });
                }
            });
    }

    function stars(testId) {
        $('#updateNameError').text("");
        $('#updateDescError').text("");
        $('#updateValueError').text("");
        $('#updateUnitError').text("");
        var test = testId;
        let url = "{{ route('admin.test.show', ':id') }}";
        url = url.replace(':id', test);
        $.ajax({
            type: "GET",
            url: url,
            success: function(data) {

                $("#updatedTestName").val(data.name);
                $("#updatedTestDescription").val(data.description);
                $("#updatedTestValue").val(data.value);
                $("#updatedTestUnit").val(data.unit);
                $("#test_id").val(data.id);
            },
        });
    }

    function setUserId(param) {
        var userId = param;
        $("#user_id").val(userId);
    }
    $(function() {
        $('#dtBasicExample').DataTable({
            "pagingType": "simple",
            // "simple" option for 'Previous' and 'Next' buttons only
        });
        $('.dataTables_length').addClass('bs-select');

        $('#updatePassModal').on('shown.bs.modal', function() {
            $('#myInput').trigger('focus')
        })
        /* add UHF JS-Code */

        $('#add-test').click(function() {
            console.log('asd');
            $('#nameError').text("");
            $('#valueError').text("");
            $('#unitError').text("");
            $('#descError').text("");
            var url = "{{ route('admin.test.store') }}";
            $.ajax({
                type: "POST",
                url: url,
                data: $('#add_test').serialize(),
                success: function(data) {
                    if (data['status'] == 'success') {
                        swal("Test has been deleted successfully!", {
                            icon: "success",
                        });
                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    } else {
                        swal("Oops", "Something went wrong!", "error")
                    }
                },
                error: function(response) {
                    var res = JSON.parse(response.responseText);
                    if (res.error) {
                        $('#emailError').text(res.error);
                    } else {
                        $('#nameError').text(res.errors.name);
                        $('#descError').text(res.errors.description);
                        $('#unitError').text(res.errors.unit);
                        $('#valueError').text(res.errors.value);
                    }
                },
            });
        });

        /* update test */

        $('#update-test').click(function() {
            $("#update-test").prop("disabled", true);
            $('#updateNameError').text("");
            $('#updateDescError').text("");
            $('#updateValueError').text("");
            $('#updateUnitError').text("");
            var test_id = $("#test_id").val();
            let url = "{{ route('admin.test.update', ':id') }}";
            url = url.replace(':id', test_id);
            $.ajax({
                type: "PUT",
                url: url,
                data: $('#update_test').serialize(),
                success: function(data) {
                    if (data['status'] == 'success') {
                        swal("Test has been updated successfully!", {
                            icon: "success",
                        });
                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    } else {
                        $("#update-test").prop("disabled", false);
                        swal("Oops", "Something went wrong!", "error")
                    }
                },
                error: function(response) {
                    $("#update-test").prop("disabled", false);
                    var res = JSON.parse(response.responseText);
                    console.log(res);
                    if (res.error) {} else {
                        $('#updateNameError').text(res.errors.name);
                        $('#updateDescError').text(res.errors.description);
                        $('#updateValueError').text(res.errors.value);
                        $('#updateUnitError').text(res.errors.unit);
                    }
                },
            });
        });


        $('#clinic_name').change(function() {
            const clinic_id = $(this).val();
            getTriggers(clinic_id);
        });

        function getTriggers(clinic_id) {
            /* get departmenmts for selected clinic. */
            var triggerUrl = "{{ route('admin.get-departments', ':id') }}";
            triggerUrl = triggerUrl.replace(':id', clinic_id);
            return $.ajax({
                url: triggerUrl,
                type: 'GET',
                dataType: "application/json",
                contentType: "application/json",
                statusCode: {
                    200: function(data) {
                        let response = JSON.parse(data.responseText);
                        console.log(response.status == "success");
                        /* add triggers to view.*/
                        if (response.status == "success") {
                            /* retrieve new results from callback */
                            let newDepartments = response.departments;
                            var departments;
                            newDepartments.forEach(function(item) {
                                departments +=
                                    `<option value=${item.id}>${item.department_name}</option>`;
                            });
                            trigger_departments = departments;
                            let userTriggers = {
                                "totalDepartments": departments,
                            }
                            let $departmentsSelect = $("#department_name");
                            $departmentsSelect.empty().append(userTriggers.totalDepartments);
                        }
                    }
                }
            });
        }

    });
</script>
