<!DOCTYPE html>
<html>
<head>
  <title>Auto-Refreshing Data Example</title>
  <script>
    // Function to fetch and update data
    function fetchData() {
      fetch('https://dummyjson.com/products') // Replace with your API endpoint
        .then(response => response.json())
        .then(data => {
          // Update a specific part of the page with the fetched data
          document.getElementById('data-container').innerHTML = data.message;
        })
        .catch(error => {
          console.error('Error fetching data:', error);
        });
    }

    // Fetch data initially when the page loads
    document.addEventListener('DOMContentLoaded', function() {
      fetchData();

      // Fetch data every 5 seconds (5000 milliseconds)
      setInterval(fetchData, 5000);
    });
  </script>
</head>
<body>
  <h1>Auto-Refreshing Data Example</h1>
  <div id="data-container"></div>
</body>
</html>
