<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <meta name="description" content="Dashlead - Admin Panel HTML Dashboard Template">
    <meta name="author" content="Spruko Technologies Private Limited">
    <meta name="keywords" content="">

    <link rel="stylesheet" href="https://cdn.tutorialjinni.com/intl-tel-input/17.0.8/css/intlTelInput.css" />
    <script src="https://cdn.tutorialjinni.com/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>

    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>



    <!-- Favicon -->
    <link rel="icon" href="https://www.spruko.com/demo/dashlead/dashlead/assets/img/brand/favicon.ico"
        type="image/x-icon"> <!-- Title -->
    <title>Health App</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style>
        button.close {
            border: none;
            background: white;
            font-size: 20px;
        }
        .row.flexHeader {
            padding: 20px 20px 10px 10px;
        }
    </style>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
        integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />

    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.29.0/feather.min.js"
        integrity="sha512-24XP4a9KVoIinPFUbcnjIjAjtS59PUoxQj3GNVpWc86bCqPuy3YxAcxJrxFCxXe4GHtAumCbO2Ze2bddtuxaRw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!--- bootstrap css -->
    <link id="style"
        href="https://www.spruko.com/demo/dashlead/dashlead/assets/plugins/bootstrap/css/bootstrap.min.css"
        rel="stylesheet">
    <!--- FONT-ICONS CSS -->
    <link href="https://www.spruko.com/demo/dashlead/dashlead/assets/css/icons.css" rel="stylesheet">
    <!--- Style css -->
    <link href="https://www.spruko.com/demo/dashlead/dashlead/assets/css/style.css" rel="stylesheet">
    <!--- Plugins css -->
    <link href="https://www.spruko.com/demo/dashlead/dashlead/assets/css/plugins.css" rel="stylesheet">
    <!-- Switcher css -->
    <link href="https://www.spruko.com/demo/dashlead/dashlead/assets/switcher/css/switcher.css" rel="stylesheet">
    <link href="https://www.spruko.com/demo/dashlead/dashlead/assets/switcher/demo.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    @livewireStyles

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">


</head>

<body class="sidebar-mini">
    <div class="horizontalMenucontainer">
        <div class="page">
            <div>
                <!--Main Header -->
                <div class="main-header side-header sticky" style="margin-bottom: -65px;">
                    <div class="container-fluid main-container">
                        <div class="main-header-left sidemenu">
                            <a class="main-header-menu-icon"
                                href="javascript:void(0);" data-bs-toggle="sidebar"
                                id="mainSidebarToggle"><span></span>
                            </a>
                        </div>
                        <div class="main-header-left horizontal"> <a class="main-logo" href="index.html"> <img
                                    src="{{ asset('assets/dist/img/AdminLTELogo.png') }}"
                                    class="desktop-logo desktop-logo-dark" alt="HealthAppLogo"> <img
                                    src="{{ asset('assets/dist/img/AdminLTELogo.png') }}"
                                    class="desktop-logo theme-logo" alt="HealthAppLogo"> </a> </div>
                        <div class="main-header-right"> <button
                                class="navbar-toggler navresponsive-toggler d-lg-none ms-auto collapsed" type="button"
                                data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent-4"
                                aria-controls="navbarSupportedContent-4" aria-expanded="false"
                                aria-label="Toggle navigation"> <span
                                    class="navbar-toggler-icon fe fe-more-vertical"></span> </button>
                            <div class="navbar navbar-expand-lg navbar-collapse responsive-navbar p-0">
                                <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
                                    <ul class="nav nav-item header-icons navbar-nav-right ms-auto">

                                        <!-- FULL SCREEN -->

                                        <li class="dropdown main-profile-menu"> <a class="main-img-user"
                                                href="javascript:void(0);" data-bs-toggle="dropdown"><img alt="avatar"
                                                    src="https://www.spruko.com/demo/dashlead/dashlead/assets/img/users/1.jpg"></a>
                                            <div class="dropdown-menu">
                                                <div class="header-navheading">
                                                    <h6 class="main-notification-title">{{ Auth::user()->name }}</h6>
                                                    {{-- <p class="main-notification-text">Web Designer</p> --}}
                                                </div>
                                                {{-- <a class="dropdown-item border-top text-wrap"
                                                    href="profile.html"> <i class="fe fe-user"></i> My Profile </a> <a
                                                    class="dropdown-item text-wrap" href="profile.html"> <i
                                                        class="fe fe-edit"></i> Edit Profile </a> <a
                                                    class="dropdown-item text-wrap" href="settings.html"> <i
                                                        class="fe fe-settings"></i> Settings </a> <a
                                                    class="dropdown-item text-wrap" href="timeline.html"> <i
                                                        class="fe fe-activity"></i> Activity </a>  --}}
                                                <a class="dropdown-item text-wrap" href="signin.html"> <i
                                                        class="fe fe-power"></i> Sign Out </a>
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!--Main Header -->

                @if (!request()->routeIs('get-queue'))
                    @livewire('navigation-menu')
                @endif


            </div>

            <div class="jumps-prevent" style="padding-top: 15px;"></div>
            <!-- Main Content-->
            <div class="main-content side-content pt-0">
                <div class="side-app">
                    <div class="main-container container-fluid">

                        <!-- Page Header -->
                        <div class="card" style="margin-bottom: 18px;">
                            @if (isset($header))
                                {{ $header }}
                            @endif
                        </div>
                        <!-- Page Header -->

                        {{ $slot }}
                    </div>
                </div>
            </div>
            <!-- End Main Content-->


            <!-- Main Footer-->
            <div class="main-footer text-center">
                <div>
                    <div class="row">
                        <div class="col-md-12"> <span>Health App Copyright © 2023 All rights reserved.</span> </div>
                    </div>
                </div>
            </div>
            <!--End Footer-->
        </div>
    </div>

    @livewireScripts
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.29.0/feather.min.js"
        integrity="sha512-24XP4a9KVoIinPFUbcnjIjAjtS59PUoxQj3GNVpWc86bCqPuy3YxAcxJrxFCxXe4GHtAumCbO2Ze2bddtuxaRw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://www.spruko.com/demo/dashlead/dashlead/assets/plugins/bootstrap/popper.min.js"></script>
    <script src="https://www.spruko.com/demo/dashlead/dashlead/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://www.spruko.com/demo/dashlead/dashlead/assets/plugins/jquery.flot/jquery.flot.js"></script>
    <script src="https://www.spruko.com/demo/dashlead/dashlead/assets/plugins/jquery.flot/jquery.flot.resize.js"></script>
    <script src="https://www.spruko.com/demo/dashlead/dashlead/assets/js/chart.flot.sampledata.js"></script>
    <script src="https://www.spruko.com/demo/dashlead/dashlead/assets/plugins/chart.js/Chart.bundle.min.js"></script>
    <script src="https://www.spruko.com/demo/dashlead/dashlead/assets/plugins/peity/jquery.peity.min.js"></script>
    <script src="https://www.spruko.com/demo/dashlead/dashlead/assets/plugins/jquery-ui/ui/widgets/datepicker.js">
        < script src = "https://www.spruko.com/demo/dashlead/dashlead/assets/plugins/select2/js/select2.min.js" >
    </script>
    <script src="https://www.spruko.com/demo/dashlead/dashlead/assets/plugins/multipleselect/multiple-select.js"></script>
    <script src="https://www.spruko.com/demo/dashlead/dashlead/assets/plugins/multipleselect/multi-select.js"></script>
    <script src="https://www.spruko.com/demo/dashlead/dashlead/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js">
        < script src = "https://www.spruko.com/demo/dashlead/dashlead/assets/plugins/perfect-scrollbar/p-scroll-1.js" >
    </script>
    <script src="https://www.spruko.com/demo/dashlead/dashlead/assets/plugins/sidemenu/sidemenu.js"></script>
    <script src="https://www.spruko.com/demo/dashlead/dashlead/assets/plugins/sidebar/sidebar.js"></script>
    <script src="https://www.spruko.com/demo/dashlead/dashlead/assets/js/sticky.js"></script>
    <script src="https://www.spruko.com/demo/dashlead/dashlead/assets/js/index.js"></script>
    <script src="https://www.spruko.com/demo/dashlead/dashlead/assets/js/custom-switcher.js">
        < script src = "https://www.spruko.com/demo/dashlead/dashlead/assets/js/custom.js" >
    </script>
    <script src="https://www.spruko.com/demo/dashlead/dashlead/assets/switcher/js/switcher.js"></script>

    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- jQuery -->
    <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $("#example2").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example2_wrapper .col-md-6:eq(0)');
            $("#example3").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example3_wrapper .col-md-6:eq(0)');
            $("#example4").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example4_wrapper .col-md-6:eq(0)');
            $('#exampleSimple').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>

</body>

</html>
