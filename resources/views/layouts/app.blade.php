<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <meta name="description" content="Dashlead -  Admin Panel HTML Dashboard Template">
    <meta name="author" content="Spruko Technologies Private Limited">
    <meta name="keywords"
        content="sales dashboard, admin dashboard, bootstrap 4 admin template, html admin template, admin panel design, admin panel design, bootstrap 4 dashboard, admin panel template, html dashboard template, bootstrap admin panel, sales dashboard design, best sales dashboards, sales performance dashboard, html5 template, dashboard template">
    <!-- Favicon -->
    <link rel="icon" href="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/img/brand/favicon.ico"
        type="image/x-icon" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />


    <link rel="stylesheet" href="https://cdn.tutorialjinni.com/intl-tel-input/17.0.8/css/intlTelInput.css" />
    <script src="https://cdn.tutorialjinni.com/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>

    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>


    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Title -->
    <title>Health App</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!---Ionicons css-->
    <link href="{{ asset('assets/assets/Admin/css/ionicons.min.css') }}" rel="stylesheet">
    <!---Typicons css-->
    <link href="{{ asset('assets/assets/Admin/css/typicons.css') }}" rel="stylesheet">
    <!---Feather css-->
    <link href="{{ asset('assets/assets/Admin/css/feather.css') }}" rel="stylesheet">
    <!---Falg-icons css-->
    <link href="{{ asset('assets/assets/Admin/css/flag-icon.min.css') }}" rel="stylesheet">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


    <link href="{{ asset('assets/assets/Admin/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/assets/Admin/css/custom-style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/assets/Admin/css/skins.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/assets/Admin/css/dark-style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/assets/Admin/css/custom-dark-style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/assets/Admin/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/assets/Admin/css/multiple-select.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/assets/Admin/css/flag-icon.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/assets/Admin/css/mCustomScrollbar.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/assets/Admin/css/sidebar.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/assets/Admin/css/sidemenu.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/assets/Admin/css/switcher.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/assets/Admin/css/demo.css') }}" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <style>
        .main-sidebar.main-sidebar-sticky.side-menu.ps {
            z-index: 0;
        }
    </style>
    @livewireStyles
    <style>
        /* .main-sidebar-sticky {
            position: fixed;
            top: 0px;
            left: 0;
            bottom: 0;
            background: #fff;
            box-shadow: 0 8px 24px rgba(61, 119, 180, .12);
            transition: left 0.3s ease, width 0.3s ease;
            border-right: 1px solid #e1e6f1;
            z-index: 1;
        } */

        .side-header .main-profile-menu .dropdown-menu {
            width: 10px;
            top: 53px;
        }

        button:not(:disabled),
        [type="button"]:not(:disabled),
        [type="reset"]:not(:disabled),
        [type="submit"]:not(:disabled) {
            cursor: pointer;
            color: white;
        }

        button#saveRecord {
            color: white;
        }

        .main-img-user img {
            width: 100%;
            border: 1px solid #cb115e;
            height: 100%;
            object-fit: cover;
            border-radius: 100%;
        }

        span.headMain {
            font-size: 19px;
            color: black;
        }

        .sidemenu-logo {
            padding: 8px;
            margin: 0;
        }

        .header-brand-img {
            height: 6rem;
            line-height: 2.5rem;
            vertical-align: middle;
            width: auto;
            margin-top: 4px;
        }

        main-content .container,
        .main-content .container-fluid {
            background: white;
            padding-left: 20px;
            padding-right: 20px;
        }

        .main-sidebar-body {
            border-top: none;
        }

        .page-header {
            display: flex;
            -ms-flex-align: center;
            align-items: center;
            padding: 2rem 0 1.5rem;
            -ms-flex-wrap: wrap;
            justify-content: space-between;
            /* padding: 0; */
            position: relative;
            min-height: 58px;
        }
    </style>
    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">


</head>

<body>

    <!-- Loader -->
    <div id="global-loader">
        <img src="{{ asset('assets/icons/loader.svg') }}" class="loader-img" alt="Loader">
    </div>
    <!-- End Loader -->

    <!-- Page -->
    <div class="page">
        <!-- Sidemenu -->
        <div class="main-sidebar main-sidebar-sticky side-menu">
            <div class="sidemenu-logo">
                <a class="main-logo" href="{{ url('home') }}">
                    <img src="{{ asset('assets/dist/img/AdminLTELogo.svg') }}" class="header-brand-img desktop-logo"
                        alt="logo">
                    <img src="{{ asset('assets/dist/img/AdminLTELogo.svg') }}" class="header-brand-img icon-logo"
                        alt="logo">
                    <img src="{{ asset('assets/dist/img/AdminLTELogo.svg') }}"
                        class="header-brand-img desktop-logo theme-logo" alt="logo">
                    <img src="{{ asset('assets/dist/img/AdminLTELogo.svg') }}"
                        class="header-brand-img icon-logo theme-logo" alt="logo">
                </a>
            </div>
            @if (!request()->routeIs('get-queue'))
                @livewire('navigation-menu')
            @endif
        </div>
        <!-- End Sidemenu -->
        <!-- Main Content-->
        <div class="main-content side-content pt-0">
            <!-- Main Header-->
            <div class="main-header side-header sticky">
                <div class="container-fluid">
                    <div class="main-header-left">
                        <a class="main-logo d-lg-none" href="{{ url('/home') }}">
                            <img src="{{ asset('assets/dist/img/AdminLTELogo.svg') }}"
                                class="header-brand-img desktop-logo" alt="logo">
                            <img src="{{ asset('assets/dist/img/AdminLTELogo.svg') }}"
                                class="header-brand-img icon-logo" alt="logo">
                            <img src="{{ asset('assets/dist/img/AdminLTELogo.svg') }}"
                                class="header-brand-img desktop-logo theme-logo" alt="logo">
                            <img src="{{ asset('assets/dist/img/AdminLTELogo.svg') }}"
                                class="header-brand-img icon-logo theme-logo" alt="logo">
                        </a>
                    </div>
                    <div class="main-header-right">

                        {{-- <div class="dropdown d-md-flex">
                            <a class="nav-link icon full-screen-link">
                                <i class="fa fa-desktop fullscreen-button"></i>
                            </a>
                        </div> --}}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="dropdown main-profile-menu">
                                    <a class="main-img-user" href=""><img alt="avatar"
                                            src="{{ asset('images/126044187-isolated-object-of-avatar-and-dummy-symbol-collection-of-avatar-and-image-stock-symbol-for-web.jpeg') }}"></a>
                                    <div class="dropdown-menu">
                                        {{-- <div class="header-navheading">
                                            <h6 class="main-notification-title">{{ Auth::user()->name }}</h6>
                                            <p class="main-notification-text">
                                                @if (Auth::user()->role_id == '0')
                                                    Admin
                                                @else
                                                    Staff
                                                @endif
                                            </p>
                                        </div> --}}
                                        <form method="POST" action="{{ route('logout') }}">
                                            @csrf
                                            <a class="dropdown-item border-top" href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                           this.closest('form').submit();">
                                                <i class="fa fa-sign-out"></i> Log Out
                                            </a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div style="line-height: 0;" class="col-lg-6 mt-2">
                                <h6>{{ Auth::user()->name }}</h6>
                                <p style="font-size: 12px;">
                                    @if (Auth::user()->role_id == '0')
                                        Admin
                                    @else
                                        Staff
                                    @endif
                                </p>
                            </div>
                        </div>
                        <a style="justify-content: flex-end;width: 71px;" class="main-header-menu-icon"
                            href="" id="mainSidebarToggle"><span></span></a>

                    </div>
                </div>
            </div>
            <!-- End Main Header-->
            <div class="container-fluid">
                <!-- Page Header -->

                @if (isset($header))
                    {{ $header }}
                @endif

                {{ $slot }}

            </div>
        </div>
        <!-- End Main Content-->



        <!-- Main Footer-->
        <div class="main-footer text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <span>Copyright © 2023 All rights reserved.</span>
                    </div>
                </div>
            </div>
        </div>
        <!--End Footer-->
    </div>
    <!-- End Page -->
    <!-- Back-to-top -->
    <a href="#top" id="back-to-top"><i class="fa fa-arrow-up"></i></a>

    <!-- Jquery js-->
    <script src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap js-->
    <script
        src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/plugins/bootstrap/js/bootstrap.bundle.min.js">
    </script>

    <!-- Ionicons js-->
    <script src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/plugins/ionicons/ionicons.js"></script>

    <!-- Rating js-->
    <script src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/plugins/rating/jquery.rating-stars.js">
    </script>

    <!-- Flot Chart js-->
    <script src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/plugins/jquery.flot/jquery.flot.js">
    </script>
    <script src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/plugins/jquery.flot/jquery.flot.resize.js">
    </script>
    <script src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/js/chart.flot.sampledata.js"></script>
    <!-- Chart.Bundle js-->
    <script src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/plugins/chart.js/Chart.bundle.min.js">
    </script>
    <!-- Peity js-->
    <script src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/plugins/peity/jquery.peity.min.js"></script>
    <!-- Jquery-Ui js-->
    <script src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/plugins/jquery-ui/ui/widgets/datepicker.js">
    </script>
    <!-- Select2 js-->
    <script src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/plugins/select2/js/select2.min.js"></script>
    <!--MutipleSelect js-->
    <script src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/plugins/multipleselect/multiple-select.js">
    </script>
    <script src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/plugins/multipleselect/multi-select.js">
    </script>
    <!-- Jquery.mCustomScrollbar js-->
    <script
        src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/plugins/jquery.mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js">
    </script>
    <!-- index -->
    <script src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/js/index.js"></script>

    <!-- Perfect-scrollbar js-->
    <script
        src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js">
    </script>

    <!-- Sidemenu js-->
    <script src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/plugins/sidemenu/sidemenu.js"></script>

    <!-- Sidebar js-->
    <script src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/plugins/sidebar/sidebar.js"></script>

    <!-- Sticky js-->
    <script src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/js/sticky.js"></script>

    <!-- Switcher js-->
    <script src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/switcher/js/switcher.js"></script>

    <!-- Custom js-->
    <script src="https://laravel.spruko.com/dashlead/Leftmenu-Icon-Light/assets/js/custom.js"></script>


</body>

</html>
