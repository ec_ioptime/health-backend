<x-app-layout>
    <x-slot name="header">
        <div class="page-header">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Departments</li>
            </ol>
        </div>
    </x-slot>
    <style>
        .modal-backdrop {
            opacity: 0.5 !important;
        }

        .paginate_button {
            width: 120px
        }

        .btn-width2 {
            width: 100px;
        }

        .button-style {
            float: right;
            margin-bottom: inherit
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="container" style="margin:10px; padding:10px">
                    <div class="row">
                        <div class="col-lg-8">
                            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                                Departments
                            </h2>
                        </div>
                        <div class="col-lg-4">
                            <button style="float: right" type="button" id="addDepartment" class="btn btn-primary btn-sm m-0 mr-2">Add
                                Department</button>
                        </div>
                    </div>

                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="th-sm">Department Name</th>
                                <th class="th-sm">Clinic</th>
                                <th class="th-sm">Role Name</th>
                                <th style=" text-align:center">Action</th>
                            </tr>

                        </thead>
                        <tbody>
                            @if (asset($departments))
                                @foreach ($departments as $department)
                                    <tr>
                                        <td>{{ $department->department_name ?? '' }}</td>
                                        <td>{{ $department->clinic->name ?? '' }}</td>
                                        <td>{{ $department->role_name ?? '' }}</td>
                                        <td style="text-align: center;">
                                            <button type="button" id="update_department"
                                                class="btn btn-primary btn-sm m-0 update_department  btn-width"
                                                onclick="stars({{ $department->id }},{{ $department->role_id }})"
                                                data-department-id="{{ $department->id }}"><i
                                                    class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            </button>
                                            <button type="button"
                                                class="delete-clinic btn btn-danger btn-sm m-0 btn-width"
                                                data-clinic-id="{{ $department->id }}"
                                                onclick="deleteDepartment({{ $department->id }})">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- update department modal --}}
    <div class="modal" tabindex="-1" id="update_department_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Department</h5>
                    {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> --}}
                </div>
                <div class="modal-body">
                    <form id="updateDepartment" type="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="department_id" id="department_id">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" min="0" id="department_name" required name="department_name"
                                class="form-control" id="name">
                            <span class="text-danger" id="updatenameError"></span>
                        </div>
                        <div class="form-group">
                            <label for="sel1">Select Clinic:</label>
                            <select class="form-control" id="clinic_update" name="clinic" id="clinic_name">
                                @foreach ($clinics as $clinic)
                                    <option value="{{ $clinic->id }}"> {{ $clinic->name }} </option>
                                @endforeach
                            </select>
                            <span class="text-danger" id="updateclinicError"></span>
                        </div>
                        <div class="form-group">
                            <label for="sel1">Select Role:</label>
                            <select class="form-control" id="role_update" name="role" id="role_name">
                                @foreach ($roles as $clinic)
                                    <option value="{{ $clinic->id }}"> {{ $clinic->name }} </option>
                                @endforeach
                            </select>
                            <span class="text-danger" id="updateroleError"></span>

                        </div>
                        <div class="modal-footer">
                            <button type="button" id="update-department"
                                class="btn btn-primary  btn-width2">Update</button>
                            {{-- <button type="button" class="btn btn-secondary btn-width2"
                                data-dismiss="modal">Close</button> --}}
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    {{-- add department modal  --}}
    <div class="modal" tabindex="-1" id="addDepartmentModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Department</h5>
                    {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> --}}
                </div>
                <div class="modal-body">
                    <form id="add_department" type="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" min="0" required name="department_name" class="form-control"
                                id="name">
                            <span class="text-danger" id="nameError"></span>
                        </div>
                        <div class="form-group">
                            <label for="clinic">Select Clinic:</label>
                            <select class="form-control" id="clinic_" name="clinic">
                                @foreach ($clinics as $clinic)
                                    <option value="{{ $clinic->id }}"> {{ $clinic->name }} </option>
                                @endforeach
                            </select>
                            <span class="text-danger" id="clinicError"></span>
                        </div>
                        <div class="form-group">
                            <label for="roles">Select Roles:</label>
                            <select class="form-control" id="roles" name="roles">
                                @foreach ($roles as $item)
                                    <option value="{{ $item->id }}"> {{ $item->name }} </option>
                                @endforeach
                            </select>
                            <span class="text-danger" id="rolesError"></span>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="add-department"
                                class="btn btn-primary  btn-width2">Add</button>
                            {{-- <button type="button" class="btn btn-secondary  btn-width2"
                                data-dismiss="modal">Close</button> --}}
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</x-app-layout>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $('#addDepartment').on('click', function() {
        $('#addDepartmentModel').modal('toggle');
        clearField();
        $('#myInput').trigger('focus')
    });
    $('.update_department').on('click', function() {
        $('#update_department_modal').modal('toggle');
    });


    clearField();

    function clearField() {
        $('#name').val('');
        $('#clinic_').val('');
        $('#roles').val('');
    }

    function deleteDepartment(departmentId) {
        var department = departmentId;
        let url = "{{ route('admin.department.destroy', ':id') }}";
        url = url.replace(':id', department);
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover the Department!",
                icon: "warning",
                buttons: true,
                dangerMode: true
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        data: {
                            '_token': "{{ csrf_token() }}",
                        },
                        success: function(data) {
                            console.log(data);
                            if (data['status'] == 'success') {
                                swal("Department has been deleted successfully!", {
                                    icon: "success",
                                });
                                setTimeout(() => {
                                    location.reload();
                                }, 1000);
                            } else if (data['status'] == 'exists') {
                                swal("Oops", "Staff exist against this department!", "error")
                            } else {
                                swal("Oops", "Something went wrong!", "error")

                            }
                        },
                        error: function(response) {
                            swal("Oops", "Something went wrong!", "error")
                            console.log(response);
                        }
                    });
                }
            });
    }

    function stars(departmentId, roleID) {
        // $('#clinic_update').text("");
        $('#department_name').text("");
        var department = departmentId;
        let url = "{{ route('admin.department.show', ':id') }}";
        url = url.replace(':id', department);
        $.ajax({
            type: "GET",
            url: url,
            data: data = {
                'roleID': roleID,
            },
            success: function(data) {

                //  $("#clinic_name").val(data.clinic);
                $("#department_name").val(data.department.department_name);
                $("#clinic_update").val(data.department.clinic_id);
                $("#department_id").val(data.department.id);
                $("#role_update").val(data.roleSelect.id);
            },
        });
    }
    $(function() {
        $('#dtBasicExample').DataTable({
            "pagingType": "simple",
            // "simple" option for 'Previous' and 'Next' buttons only
        });
        $('.dataTables_length').addClass('bs-select');

        $('#update_department_modal').on('shown.bs.modal', function() {
            $('#myInput').trigger('focus')
        })


        /* add department JS-Code */
        $('#add-department').click(function() {
            $("#add-department").prop("disabled", true);
            $('#updatenameError').text("");
            $('#updateclinicError').text("");
            var url = "{{ route('admin.department.store') }}";
            $.ajax({
                type: "POST",
                url: url,
                data: $('#add_department').serialize(),
                success: function(data) {
                    if (data['status'] == 'success') {
                        swal("Department has been added successfully!", {
                            icon: "success",
                        });
                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    } else {
                        $("#add-department").prop("disabled", false);
                        swal("Oops", "Something went wrong!", "error")
                    }
                },
                error: function(response) {
                    $("#add-department").prop("disabled", false);
                    var res = JSON.parse(response.responseText);
                    console.log(res);
                    if (res.error) {} else {
                        $('#updatenameError').text(res.errors.department_name);
                        $('#updateclinicError').text(res.errors.clinic);

                    }
                },
            });
        });


        $('#update-department').click(function() {
            $("#update-department").prop("disabled", true);
            $('#updatenameError').text("");
            $('#updateclinicError').text("");
            var department_id = $("#department_id").val();
            let url = "{{ route('admin.department.update', ':id') }}";
            url = url.replace(':id', department_id);
            $.ajax({
                type: "PUT",
                url: url,
                data: $('#updateDepartment').serialize(),
                success: function(data) {
                    if (data['status'] == 'success') {
                        swal("Department has been updated successfully!", {
                            icon: "success",
                        });
                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    } else {
                        $("#update-department").prop("disabled", false);
                        swal("Oops", "Something went wrong!", "error")
                    }
                },
                error: function(response) {
                    $("#update-department").prop("disabled", false);
                    var res = JSON.parse(response.responseText);
                    console.log(res);
                    if (res.error) {} else {
                        $('#updatenameError').text(res.errors.department_name);
                        $('#clinicError').text(res.errors.clinic);
                    }
                },
            });
        });
    });
</script>
