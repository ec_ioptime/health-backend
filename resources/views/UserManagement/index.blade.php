<x-app-layout>
    <x-slot name="header">

        <div class="row">
            <div class="col-lg-6">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    User Management
                </h2>
            </div>
            <div class="col-lg-6">
                <a href="" class="btn btn-primary btn-sm pull-right">Add New User</a>
            </div>
        </div>

    </x-slot>
    <style>
        .modal-backdrop {
            opacity: 0.5 !important;
        }

        .btn-width2 {
            width: 100px;
        }

        .btn-width {
            width: 134px;
        }

        .button-style {
            float: right;
            margin-bottom: inherit
        }

        .paginate_button {
            width: 120px
        }
    </style>
    <div class="card">
        <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                    <div class="container">
                        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0"
                            width="100%">
                            <thead>
                                <tr>
                                    <th class="th-sm">ID</th>
                                    <th class="th-sm">First Name</th>
                                    <th class="th-sm">Second Name</th>
                                    <th class="th-sm">Email</th>
                                    <th class="th-sm">Clinic</th>
                                    <th class="th-sm">Department</th>
                                    <th class="th-sm">Contact Number</th>
                                    <th class="th-sm">Address</th>
                                    <th style=" text-align:center">Action</th>
                                </tr>

                            </thead>
                            <tbody>

                                @if (isset($data))
                                    @foreach ($data as $index => $patient)
                                        <tr>
                                            <td>{{ $patient->id ?? '' }}</td>
                                            <td>{{ $patient->usersdetail->surname ?? '' }}</td>
                                            <td>{{ $patient->name }}</td>
                                            <td>{{ $patient->email ?? '' }}</td>
                                            <td>
                                                @if (isset($patient->clinic->name))
                                                    {{ $patient->clinic->name }}
                                                @endif
                                            </td>
                                            <td>
                                                @if (isset($patient->depart->department_name))
                                                    {{ $patient->depart->department_name }}
                                                @endif
                                            </td>
                                            <td>{{ $patient->dialing_code . $patient->phone_number ?? '' }}</td>
                                            <td>{{ $patient->home_address ?? '' }}</td>

                                            <td class="d-flex">
                                                <button type="button" class="btn btn-primary btn-sm m-1">
                                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                                </button>
                                                <button type="button" class="btn btn-success btn-sm m-1">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                </button>
                                                <button type="button" class="btn btn-danger btn-sm m-1">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</x-app-layout>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    function deletePatient(patientId) {
        var patient = patientId;
        let url = "{{ route('admin.delete-patient', ':id') }}";
        url = url.replace(':id', patient);
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover the Patient!",
                icon: "warning",
                buttons: true,
                dangerMode: true
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            '_token': "{{ csrf_token() }}",
                        },
                        success: function(data) {
                            if (data['status'] == 'success') {
                                swal("Patient has been deleted successfully!", {
                                    icon: "success",
                                });
                                setTimeout(() => {
                                    location.reload();
                                }, 1000);
                            } else {
                                swal("Oops", "Something went wrong!", "error")

                            }
                        },
                        error: function(response) {
                            swal("Oops", "Something went wrong!", "error")

                        }
                    });
                }
            });

    }

    $(function() {
        $('#dtBasicExample').DataTable({
            "pagingType": "simple",
            // "simple" option for 'Previous' and 'Next' buttons only
        });
        $('.dataTables_length').addClass('bs-select');
    });
</script>
