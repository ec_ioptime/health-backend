<x-app-layout>

    <style>
        .card-body {
            flex: 1 1 auto;
        }

        p.mb-1.tx-inverse1 {
            color: white;
            font-size: 17px;
            text-align: left;
            margin-left: 23px;
        }

        .custom-card {
            margin-bottom: 20px;
            border: 0;
            background: #e6e6e6;
            border-radius: 23px;
            box-shadow: 0 8px 16px 0 rgb(232 232 237 / 24%);
        }

        .custom-card1 {
            margin-bottom: 20px;
            border: 0;
            background: #c41034;
            border-radius: 23px;
            box-shadow: 0 8px 16px 0 rgb(232 232 237 / 24%);
        }

        span.text-muted1 {
            color: white;
        }

        .expansion-label {
            font-weight: 900;
            text-align: right;
        }

        .row-sm>div {
            padding-left: 6px;
            padding-right: 6px;
        }

        .card-body.dash1 {
            text-align: center;
        }

        p.mb-1.tx-inverse {
            padding-top: 9px;
            font-size: 14px;
        }

        .mr-auto,
        .mx-auto {
            margin-right: inherit;
        }

        b,
        strong {
            font-weight: bolder;
            color: white;
            font-size: 21px;
            padding: 2px 10px;
            text-align: right;
        }

        span.text-muted {
            font-size: 16px;
            font-weight: 500;
        }

        .row.hiddne {
            padding: 15px 77px;
        }

        .card-body.dash1 {
            text-align: center;
            padding: 40px 35px;
        }

        .card-body.dash {
            text-align: center;
        }

        .col-xl-2,
        .col-sm-6,
        .col-lg-6 {
            position: relative;
            width: 100%;
            padding-right: 7px;
            padding-left: 7px;
        }
    </style>
    <x-slot name="header">
        <div class="page-header">
            <div>
                <h1 class="main-content-title tx-40 mg-b-5">Welcome to Health Clinic</h1>
            </div>
        </div>
    </x-slot>



    <div class="row hiddne">
        <div class="col-sm-6 col-xl-4 col-lg-6">
            <div class="card custom-card1">
                <div class="card-body dash1">
                    <div class="d-flex">
                        <div class="mr-auto">
                            <img style="width: 52px;" src="{{ asset('assets/icon//NoOfClinics.svg') }}" alt="clinics">
                        </div>
                        <p class="mb-1 tx-inverse1">Number of <br> clinics </p>
                        {{-- <b>{{ $clinics }}</b> --}}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xl-4 col-lg-6">
            <div class="card custom-card1">
                <div class="card-body dash1">
                    <div class="d-flex">
                        <div class="mr-auto">
                            <img style="width: 52px;" src="{{ asset('assets/icon/NoOfDepts.svg') }}" alt="clinics">
                        </div>
                        <p class="mb-1 tx-inverse1">Number of <br> departments</p>
                        {{-- <b>{{ $departments }}</b> --}}

                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xl-4 col-lg-6">
            <div class="card custom-card1">
                <div class="card-body dash1">
                    <div class="d-flex">
                        <div class="mr-auto">
                            <img style="width: 52px;" src="{{ asset('assets/icon/NoOfPatients.svg') }}" alt="clinics">
                        </div>
                        <p class="mb-1 tx-inverse1">Number of <br> patients</p>
                        {{-- <b>{{ $patients }}</b> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div style="margin-left: 20px; margin-right: 20px;"class="row">
        <div class="col-sm-6 col-xl-2 col-lg-6">
            <div class="card custom-card">
                <div class="card-body dash">
                    <div class="ml-auto">
                        <img style="width: 40px;" src="{{ asset('assets/icon/CheckingDeskStaff.svg') }}" alt="clinics">
                    </div>
                    <p class="mb-1 tx-inverse">Checking Desk <br>Staff</p>
                    {{-- <span class="text-muted">{{ $checkingDesk }}</span> --}}

                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-2 col-lg-6">
            <div class="card custom-card">
                <div class="card-body dash">
                    <div class="ml-auto">
                        <img style="width: 40px;" src="{{ asset('assets/icon/AdminStaff.svg') }}" alt="clinics">
                    </div>
                    <p class="mb-1 tx-inverse">Administration<br> Staff</p>
                    {{-- <span class="text-muted">{{ $administration }}</span> --}}
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-2 col-lg-6">
            <div class="card custom-card">
                <div class="card-body dash">
                    <div class="ml-auto">
                        <img style="width: 40px;" src="{{ asset('assets/icon/VitalDeptStaff.svg') }}" alt="clinics">
                    </div>
                    <p class="mb-1 tx-inverse">Vital Dep <br> Staff</p>
                    {{-- <span class="text-muted">{{ $vitalDepartment }}</span> --}}
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-2 col-lg-6">
            <div class="card custom-card">
                <div class="card-body dash">
                    <div class="ml-auto">
                        <img style="width: 40px;" src="{{ asset('assets/icon/ReferStationStaff.svg') }}" alt="clinics">
                    </div>
                    <p class="mb-1 tx-inverse"> Refer Station<br> Staff</p>
                    {{-- <span class="text-muted">{{ $referDeparts }}</span> --}}
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-2 col-lg-6">
            <div class="card custom-card">
                <div class="card-body dash">
                    <div class="ml-auto">
                        <img style="width: 52px;" src="{{ asset('assets/icon/DocDeptStaff.svg') }}" alt="clinics">
                    </div>
                    <p class="mb-1 tx-inverse">Doc Dep <br> Staff</p>
                    {{-- <span class="text-muted">{{ $doctDepartment }}</span> --}}
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-2 col-lg-6">
            <div class="card custom-card">
                <div class="card-body dash">
                    <div class="ml-auto">
                        <img style="width: 42px;" src="{{ asset('assets/icon/CHWStaff.svg') }}" alt="clinics">
                    </div>
                    <p class="mb-1 tx-inverse">CHW <br>Staff</p>
                    {{-- <span class="text-muted">{{ $chwStaff }}</span> --}}
                </div>
            </div>
        </div>

    </div>
    <!--End  Row -->


    <div class="row row-sm">
        <div class="col-sm-12 col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-5">
                        <div class="col-lg-12">
                            <div class="form-group ">
                                <a href="#"style="float: right;" class="refreshBtn" id="refreshBtn">Refesh</a>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group ">
                                <label for="">Select Clinic</label>
                                <select name="clinic" id="clinic" class="form-control clinic">
                                    @foreach ($clincis as $item)
                                        <option {{ $item->id == '1015' ? 'selected' : '' }}
                                            value="{{ $item->id }}">
                                            {{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group ">
                                <label for="">Select Patient</label>
                                <select name="patient" id="patient" class="form-control patient">
                                    <option value="all">All</option>
                                    @foreach ($data as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @php
                            $subtractedDate = date('Y-m-d', strtotime('-30 days'));

                        @endphp
                        <div class="col-lg-3">
                            <div class="form-group ">
                                <label for="">From Date</label>
                                <input type="date" value="{{ $subtractedDate }}" class="form-control fromdate"
                                    name="fromdate" id="fromdate">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group ">
                                <label for="">To Date</label>
                                <input type="date" value="{{ date('Y-m-d') }}" class="form-control todate"
                                    name="todate" id="todate">
                            </div>
                        </div>
                    </div>


                    <div class="d-flex">
                        <div id="barChart" style="width: 700px; height: 400px; float: left;"></div>
                        <div id="pieChart" style="width: 300px; height: 400px; float: left;"></div>
                    </div>

                    {{-- <div id="chart-container" style="width: 100%;height:400px;"></div>
                    <div id="chart-container1" style="width: 100%;height:400px;"></div> --}}
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/echarts/5.0.0/echarts.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#text').html("");
            ajaxCall();

            function ajaxCall() {
                var clinic = $('#clinic').val();
                var patient = $('#patient').val();
                var fromdate = $('#fromdate').val();
                var todate = $('#todate').val();
                $.ajax({
                    type: 'get',
                    url: '{{ url('/patientDetailReport') }}',
                    data: {
                        clinic: clinic,
                        patient: patient,
                        fromdate: fromdate,
                        todate: todate,
                    },
                    success: function(data) {
                        if (data.length === 0) {
                            console.log(data);
                            $('#text').html("No data found");
                            $('#chart-container').html('');
                            $('#chart-container1').html('');

                        } else {
                            $('#text').html("");
                            $('#chart-container').html('');
                            $('#chart-container1').html('');

                            linechart(data);
                        }
                    }
                });
            }

            $('#refreshBtn').on('click', function() {
                ajaxCall();
            });
            $('#clinic').on('change', function() {
                ajaxCall();
            });
            $('#patient').on('change', function() {
                ajaxCall();
            });
            $('#fromdate').on('change', function() {
                ajaxCall();
            });
            $('#todate').on('change', function() {
                ajaxCall();
            });
        });



        function linechart(dataStat) {
            // Your existing option object for the bar chart
            var barOption = {
                color: ['#4339F2', '#FFB200', '#CF1034'], // Specify custom colors here
                grid: [{
                        left: '5%',
                        width: '70%',
                        containLabel: true,
                    },
                    {
                        left: '75%',
                        width: '20%',
                    },
                ],
                xAxis: {
                    type: 'category',
                    data: [], // Change this to an array
                },
                yAxis: {
                    type: 'value',
                    axisLabel: {
                        formatter: function(value) {
                            return formatDuration(value);
                        },
                    },
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    },
                    formatter: function(params) {
                        var tooltipContent = '';
                        var minDuration = Infinity;
                        var maxDuration = -Infinity;

                        params.forEach(function(param) {
                            var department = param.seriesName;
                            tooltipContent += department + ': ' + formatDuration(param.value) + '<br>';
                        });

                        return tooltipContent;
                    }
                },
                legend: {
                    data: ['Administration', 'Checking Desk', 'Vital Department'],
                    bottom: 10,
                    itemStyle: {
                        borderRadius: 5,
                    },
                },
                series: [
                    // New series for the additional data
                    {
                        name: 'Administration',
                        type: 'bar',
                        stack: 'dataStack',
                        data: [], // This will be updated with the durations
                        barWidth: 10,
                        itemStyle: {
                            borderRadius: [5, 5, 0, 0],
                        },
                    },
                    {
                        name: 'Checking Desk',
                        type: 'bar',
                        stack: 'dataStack',
                        data: [], // This will be updated with the durations
                        barWidth: 10,
                        itemStyle: {
                            borderRadius: [5, 5, 0, 0],
                        },
                    },
                    {
                        name: 'Vital Department',
                        type: 'bar',
                        stack: 'dataStack',
                        data: [], // This will be updated with the durations
                        barWidth: 10,
                        itemStyle: {
                            borderRadius: [5, 5, 0, 0],
                        },
                    },
                ],
            };

            // Your additional data
            var additionalData = dataStat;

            // Extract the durations from the additional data and map them to the corresponding series
            var administrationDurations = additionalData[0].durations.split(',').map(function(duration) {
                return parseDuration(duration); // Parse the duration string
            });

            var checkingDeskDurations = additionalData[1].durations.split(',').map(function(duration) {
                return parseDuration(duration); // Parse the duration string
            });

            var vitalDepartmentDurations = additionalData[2].durations.split(',').map(function(duration) {
                return parseDuration(duration); // Parse the duration string
            });

            // Sort the data in descending order to bring larger values to the back
            administrationDurations.sort(function(a, b) {
                return b - a;
            });

            checkingDeskDurations.sort(function(a, b) {
                return b - a;
            });

            vitalDepartmentDurations.sort(function(a, b) {
                return b - a;
            });

            // Update the data for the new series in the barOption object
            barOption.series[0].data = administrationDurations;
            barOption.series[1].data = checkingDeskDurations;
            barOption.series[2].data = vitalDepartmentDurations;


    // Extract the dates from the additional data and set them as x-axis data
    var dates = additionalData[0].dates.split(',');
    barOption.xAxis.data = dates;

            // Create an instance of echarts for the bar chart and set the option
            var barChart = echarts.init(document.getElementById('barChart'));
            barChart.setOption(barOption);

            // Function to parse duration string and convert it to seconds
            function parseDuration(duration) {
                var parts = duration.split(':');
                var hours = parseInt(parts[0]);
                var minutes = parseInt(parts[1]);
                var seconds = parseFloat(parts[2]);
                return hours * 3600 + minutes * 60 + seconds;
            }

            // Function to format duration in hours, minutes, and seconds
            function formatDuration(durationInSeconds) {
                var hours = Math.floor(durationInSeconds / 3600);
                var minutes = Math.floor((durationInSeconds % 3600) / 60);
                var seconds = durationInSeconds % 60;
                return hours + 'h ' + minutes + 'm ' + seconds + 's';
            }

            // Your pie chart data
            var pieData = additionalData.map(function(data) {
                return {
                    value: data.durations.split(',').reduce(function(total, duration) {
                        return total + parseDuration(duration);
                    }, 0),
                    name: data.department,
                };
            });

            // Create an instance of echarts for the pie chart and set the option
            var pieOption = {
                color: ['#4339F2', '#FFB200', '#CF1034'], // Specify custom colors here

                tooltip: {
                    trigger: 'item',
                    formatter: function(params) {
                        return params.name + '<br/>' + 'Duration: ' + formatDuration(params.value);
                    },
                },

                series: [{
                    name: 'Department Duration',
                    type: 'pie',
                    avoidLabelOverlap: false,
                    label: {
                        show: false,
                        position: 'center',
                    },
                    emphasis: {
                        label: {
                            show: true,
                            fontSize: '20',
                            fontWeight: 'bold',
                        },
                    },
                    labelLine: {
                        show: false,
                    },
                    data: pieData,
                }, ],
            };

            var pieChart = echarts.init(document.getElementById('pieChart'));
            pieChart.setOption(pieOption);
        }
    </script>
</x-app-layout>
