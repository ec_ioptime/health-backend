


<table id="dtBasicExample" class="table table-striped table-bordered table-sm mt-5" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th style="width:50px;">#</th>
            <th class="th-sm">Visit Date</th>
            <th class="th-sm">Check-In Time</th>
            <th class="th-sm">Check-Out Time</th>
            <th class="th-sm">Duration of Visit</th>
            <th class="th-sm">Purpose of Visit</th>
            <th class="th-sm">Department</th>
            <th class="th-sm">Staff</th>
            <th class="th-sm">Token Number</th>
        </tr>

    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>12-02-2023</td>
            <td>11.00 PM</td>
            <td>1.00 AM</td>
            <td>2 Hours</td>
            <td>General Purpose</td>
            <td>CGP</td>
            <td>DR Ali</td>
            <td>31</td>
        </tr>
        <tr>
            <td>2</td>
            <td>12-02-2023</td>
            <td>11.00 PM</td>
            <td>1.00 AM</td>
            <td>2 Hours</td>
            <td>General Purpose</td>
            <td>CGP</td>
            <td>DR Ali</td>
            <td>31</td>
        </tr>
        <tr>
            <td>3</td>
            <td>12-02-2023</td>
            <td>11.00 PM</td>
            <td>1.00 AM</td>
            <td>2 Hours</td>
            <td>General Purpose</td>
            <td>CGP</td>
            <td>DR Ali</td>
            <td>31</td>
        </tr>
        <tr>
            <td>4</td>
            <td>12-02-2023</td>
            <td>11.00 PM</td>
            <td>1.00 AM</td>
            <td>2 Hours</td>
            <td>General Purpose</td>
            <td>CGP</td>
            <td>DR Ali</td>
            <td>31</td>
        </tr>
        <tr>
            <td>5</td>
            <td>12-02-2023</td>
            <td>11.00 PM</td>
            <td>1.00 AM</td>
            <td>2 Hours</td>
            <td>General Purpose</td>
            <td>CGP</td>
            <td>DR Ali</td>
            <td>31</td>
        </tr>
        <tr>
            <td>6</td>
            <td>12-02-2023</td>
            <td>11.00 PM</td>
            <td>1.00 AM</td>
            <td>2 Hours</td>
            <td>General Purpose</td>
            <td>CGP</td>
            <td>DR Ali</td>
            <td>31</td>
        </tr>
        <tr>
            <td>7</td>
            <td>12-02-2023</td>
            <td>11.00 PM</td>
            <td>1.00 AM</td>
            <td>2 Hours</td>
            <td>General Purpose</td>
            <td>CGP</td>
            <td>DR Ali</td>
            <td>31</td>
        </tr>
    </tbody>
</table>
