@extends('layouts.app')

@section('content')
    <!-- End Page Header -->


    <div class="row row-sm">
        <div class="col-sm-6 col-xl-3 col-lg-6">
            <div class="card custom-card">
                <div class="card-body dash1">
                    <div class="d-flex">
                        <p class="mb-1 tx-inverse">Number Of Sales</p>
                        <div class="ml-auto">
                            <i class="fas fa-chart-line fs-20 text-primary"></i>
                        </div>
                    </div>
                    <div>
                        <h3 class="dash-25">568</h3>
                    </div>
                    <div class="progress mb-1">
                        <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="70"
                            class="progress-bar progress-bar-xs wd-70p" role="progressbar"></div>
                    </div>
                    <div class="expansion-label d-flex">
                        <span class="text-muted">Last Month</span>
                        <span class="ml-auto"><i class="fas fa-caret-up mr-1 text-success"></i>0.7%</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3 col-lg-6">
            <div class="card custom-card">
                <div class="card-body dash1">
                    <div class="d-flex">
                        <p class="mb-1 tx-inverse">New Revenue</p>
                        <div class="ml-auto">
                            <i class="fab fa-rev fs-20 text-secondary"></i>
                        </div>
                    </div>
                    <div>
                        <h3 class="dash-25">$12,897</h3>
                    </div>
                    <div class="progress mb-1">
                        <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="70"
                            class="progress-bar progress-bar-xs wd-60p bg-secondary" role="progressbar">
                        </div>
                    </div>
                    <div class="expansion-label d-flex">
                        <span class="text-muted">Last Month</span>
                        <span class="ml-auto"><i
                                class="fas fa-caret-down mr-1 text-danger"></i>0.43%</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3 col-lg-6">
            <div class="card custom-card">
                <div class="card-body dash1">
                    <div class="d-flex">
                        <p class="mb-1 tx-inverse">Total Cost</p>
                        <div class="ml-auto">
                            <i class="fas fa-dollar-sign fs-20 text-success"></i>
                        </div>
                    </div>
                    <div>
                        <h3 class="dash-25">$11,234</h3>
                    </div>
                    <div class="progress mb-1">
                        <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="70"
                            class="progress-bar progress-bar-xs wd-50p bg-success" role="progressbar">
                        </div>
                    </div>
                    <div class="expansion-label d-flex text-muted">
                        <span class="text-muted">Last Month</span>
                        <span class="ml-auto"><i
                                class="fas fa-caret-down mr-1 text-danger"></i>1.44%</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3 col-lg-6">
            <div class="card custom-card">
                <div class="card-body dash1">
                    <div class="d-flex">
                        <p class="mb-1 tx-inverse">Profit By Sale</p>
                        <div class="ml-auto">
                            <i class="fas fa-signal fs-20 text-info"></i>
                        </div>
                    </div>
                    <div>
                        <h3 class="dash-25">$789</h3>
                    </div>
                    <div class="progress mb-1">
                        <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="70"
                            class="progress-bar progress-bar-xs wd-40p bg-info" role="progressbar"></div>
                    </div>
                    <div class="expansion-label d-flex text-muted">
                        <span class="text-muted">Last Month</span>
                        <span class="ml-auto"><i class="fas fa-caret-up mr-1 text-success"></i>0.9%</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End  Row -->


    <div class="row row-sm">
        <div class="col-sm-12 col-xl-8 col-lg-8">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="card-option d-flex">
                        <div>
                            <h6 class="card-title mb-1">Overview of Sales Win/Lost</h6>
                            <p class="text-muted card-sub-title">Comapred to last month sales.</p>
                        </div>
                        <div class="card-option-title ml-auto">
                            <div class="btn-group p-0">
                                <button class="btn btn-light btn-sm" type="button">Month</button>
                                <button class="btn btn-outline-light btn-sm" type="button">Year</button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="chartjs-size-monitor"
                            style="position: absolute; inset: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                            <div class="chartjs-size-monitor-expand"
                                style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                <div
                                    style="position:absolute;width:1000000px;height:1000000px;left:0;top:0">
                                </div>
                            </div>
                            <div class="chartjs-size-monitor-shrink"
                                style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                            </div>
                        </div>
                        <canvas id="sales" height="385"
                            style="display: block; height: 350px; width: 592px;" width="651"
                            class="chartjs-render-monitor"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-xl-4 col-lg-4">
            <div class="card custom-card">
                <div class="card-body">
                    <div>
                        <h6 class="card-title mb-1">Cost BreakDown</h6>
                        <p class="text-muted card-sub-title">Excepteur sint occaecat cupidatat non
                            proident.</p>
                    </div>
                    <div class="row">
                        <div class="col-6 col-md-6 text-center">
                            <div class="mb-2">
                                <span class="peity-donut"
                                    data-peity="{ &quot;fill&quot;: [&quot;#eb6f33&quot;, &quot;#77778e33&quot;], &quot;innerRadius&quot;: 14, &quot;radius&quot;: 20 }"
                                    style="display: none;">4/7</span><svg class="peity" height="40"
                                    width="40">
                                    <path
                                        d="M 20 0 A 20 20 0 1 1 11.322325217648839 38.01937735804839 L 13.925627652354187 32.61356415063387 A 14 14 0 1 0 20 6"
                                        data-value="4" fill="#eb6f33"></path>
                                    <path
                                        d="M 11.322325217648839 38.01937735804839 A 20 20 0 0 1 19.999999999999996 0 L 19.999999999999996 6 A 14 14 0 0 0 13.925627652354187 32.61356415063387"
                                        data-value="3" fill="#77778e33"></path>
                                </svg>
                            </div>
                            <p class="mb-1 tx-inverse">Marketing</p>
                            <h4 class="mb-1"><span>$</span>67,927</h4>
                            <span class="text-muted fs-12"><i
                                    class="fas fa-caret-up mr-1 text-success"></i>54% last month</span>
                        </div>
                        <div class="col-6 col-md-6 text-center">
                            <div class="mb-2">
                                <span class="peity-donut"
                                    data-peity="{ &quot;fill&quot;: [&quot;#01b8ff&quot;, &quot;#77778e33&quot;], &quot;innerRadius&quot;: 14, &quot;radius&quot;: 20 }"
                                    style="display: none;">2/7</span><svg class="peity" height="40"
                                    width="40">
                                    <path
                                        d="M 20 0 A 20 20 0 0 1 39.498558243636474 24.450418679126287 L 33.64899077054553 23.1152930753884 A 14 14 0 0 0 20 6"
                                        data-value="2" fill="#01b8ff"></path>
                                    <path
                                        d="M 39.498558243636474 24.450418679126287 A 20 20 0 1 1 19.999999999999996 0 L 19.999999999999996 6 A 14 14 0 1 0 33.64899077054553 23.1152930753884"
                                        data-value="5" fill="#77778e33"></path>
                                </svg>
                            </div>
                            <p class="mb-1 tx-inverse">Sales</p>
                            <h4 class="mb-1"><span>$</span>24,789</h4>
                            <span class="text-muted fs-12"><i
                                    class="fas fa-caret-down mr-1 text-danger"></i>33% last month</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card custom-card">
                <div class="card-body">
                    <div>
                        <h6 class="card-title mb-1">Monthly Profits</h6>
                        <p class="text-muted card-sub-title">Excepteur sint occaecat cupidatat non
                            proident.</p>
                    </div>
                    <h3><span>$</span>22,534</h3>
                    <div class="clearfix mb-3">
                        <div class="clearfix">
                            <span class="float-left text-muted">This Month</span>
                            <span class="float-right">75%</span>
                        </div>
                        <div class="progress mt-1">
                            <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="70"
                                class="progress-bar progress-bar-xs wd-70p bg-primary" role="progressbar">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="clearfix">
                            <span class="float-left text-muted">Last Month</span>
                            <span class="float-right">50%</span>
                        </div>
                        <div class="progress mt-1">
                            <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="50"
                                class="progress-bar progress-bar-xs wd-50p bg-success" role="progressbar">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
