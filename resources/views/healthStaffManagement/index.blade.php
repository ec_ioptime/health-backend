<x-app-layout>
    <style>
                button:not(:disabled), [type="button"]:not(:disabled), [type="reset"]:not(:disabled), [type="submit"]:not(:disabled) {
    cursor: pointer;
    color: white;
}
        label {
            width: 100%;
        }

        .iti.iti--allow-dropdown {
            width: 100%;
        }

        .row.flexHeader {
            padding: 20px 20px 10px 10px;
        }
        .fa-trash:before,
        .fa-qrcode:before,
        .fa-eye:before,
        .fa-key:before,
        .fa-edit:before,
        .fa-pencil-square-o:before,
        .fa-trash:before {
            color: white;
        }
    </style>

<x-slot name="header">
    <div class="page-header">
            <link rel="stylesheet" href="https://cdn.tutorialjinni.com/intl-tel-input/17.0.8/css/intlTelInput.css" />
        <script src="https://cdn.tutorialjinni.com/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>

        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>

        <link rel="stylesheet" type="text/css"
            href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

        <div>
            {{-- <h2 class="main-content-title tx-24 mg-b-5">Welcome To Dashboard</h2> --}}

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Health Staff management</li>
            </ol>
        </div>
    </div>
</x-slot>

    {{-- <x-slot name="header">

        <div class="row flexHeader mt-4">
            <div class="col-lg-6">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    Health Practitioner management
                </h2>
            </div>
            <div class="col-lg-6">
                <a href="#" id="addnewbtn" class="btn btn-primary btn-sm pull-right">Add New</a>
            </div>
        </div>

    </x-slot> --}}
    <style>
        .modal-backdrop {
            opacity: 0.5 !important;
        }

        .btn-width2 {
            width: 100px;
        }

        .btn-width {
            width: 134px;
        }

        .button-style {
            float: right;
            margin-bottom: inherit
        }

        .paginate_button {
            width: 120px
        }
    </style>
    <style>
        /* .table-bordered td,
        .table-bordered th {
            border: 1px solid #dee2e6;
            line-height: 2;
        } */

        #addnewModel .modal-dialog {
            max-width: 80%;
            /* margin: 0; */
        }

        .iti.iti--allow-dropdown.iti--separate-dial-code {
            width: 100%;
        }
         button:not(:disabled), [type="button"]:not(:disabled), [type="reset"]:not(:disabled), [type="submit"]:not(:disabled) {
    cursor: pointer;
    color: black;
}

        .modal-content {
            position: relative;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            width: 100%;
            pointer-events: auto;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid rgba(0, 0, 0, .2);
            border-radius: 0.3rem;
            box-shadow: 0 0.25rem 0.5rem rgba(0, 0, 0, .5);
            outline: 0;
        }
    </style>

    <div class="card">
        <div class="py-12">
            @if (session('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 m-1">
                <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-2">
                    <div class="table-responsive">
                        <div class="row">
                            <div class="col-lg-6">
                                <h4>Health Staff management</h4>
                            </div>
                            <div class="col-lg-6">
                                <a href="#" id="addnewbtn" class="btn btn-primary btn-sm pull-right">Add New</a>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered m-2" id="patientSearch">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Clinic</th>
                                    <th>Department</th>
                                    <th>Contact Number</th>
                                    <th style=" text-align:center">Action</th>
                                </tr>

                            </thead>
                            <tbody>
                                @if (isset($data))
                                    @foreach ($data as $index => $patient)
                                        <tr>
                                            <td>{{ $patient->id ?? '' }}</td>
                                            <td>{{ $patient->name }}</td>
                                            <td>{{ $patient->usersdetail->surname ?? '' }}</td>
                                            <td>{{ $patient->email ?? '' }}</td>
                                            <td>
                                                @if (isset($patient->clinic->name))
                                                    {{ $patient->clinic->name }}
                                                @endif
                                            </td>
                                            <td>
                                                @if (isset($patient->depart->department_name))
                                                    {{ $patient->depart->department_name }}
                                                @endif
                                            </td>
                                            <td>{{ $patient->phone_number ?? '' }}</td>
                                            <td class="d-flex">
                                                <button type="button" class="btn btn-primary btn-sm m-1"
                                                    data-toggle="modal" data-target="#updatePassword"
                                                    onclick="passwordChange({{ $patient->id }})">
                                                    <i class="fa fa-key" aria-hidden="true"></i>
                                                </button>
                                                <button type="button" onclick="editPatient({{ $patient->id }})"
                                                    class="btn btn-success btn-sm m-1">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                </button>
                                                <button type="button" onclick="deletePatient({{ $patient->id }})"
                                                    class="btn btn-danger btn-sm m-1">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal Password -->
    <div class="modal fade" id="updatePassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog mw-50 w-75" role="document">
            <form id="password-update-form">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Update Password</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="new_password">New Password:</label>
                            <input type="password" name="new_password" id="new_password" class="form-control password">
                            <div id="password-update-errors1" style="color: red;"></div>
                        </div>
                        <div class="form-group">
                            <label for="confirm_password">Confirm Password:</label>
                            <input type="password" name="confirm_password" id="confirm_password"
                                class="form-control password">
                            <div id="password-update-errors2" style="color: red;"></div>
                        </div>
                        <input type="hidden" name="user_id" id="useredit_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- Add Health Staff --}}
    <div class="modal fade" id="addnewModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form action="/add-staffManagement" method="post">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">
                            Add Health Staff Management
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        {{-- biological information detail --}}
                        <div class="card">
                            <div class="card-header">
                                <h4>Biographical information</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Enter Name</label>
                                            <input type="text" class="form-control" id="name" name="name"
                                                required>
                                            <input type="hidden" class="form-control" id="id" name="id"
                                                required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Enter Surename</label>
                                            <input type="text" class="form-control" id="name2" name="name2"
                                                required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Enter Document ID</label>
                                            <input type="text" class="form-control" id="docID" name="docID"
                                                required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Enter Document type</label>
                                            <select name="doctype" id="doctype" required class="form-control">
                                                <option value="ID">ID</option>
                                                <option value="Driver's license">Driver's license</option>
                                                <option value="Passport">Passport</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Select Issue State</label>
                                            <select class="form-control" id="issuedate" required name="issuedate">
                                                @foreach ($countrys as $count)
                                                    @if ($count->id == 162)
                                                        <option selected value="{{ $count->id }}">
                                                            {{ $count->nicename }}
                                                        </option>
                                                    @else
                                                        <option value="{{ $count->id }}">{{ $count->nicename }}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    @php
                                        $minDate = date('Y-m-d');
                                    @endphp
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Enter Expiry Date</label>
                                            <input type="date" class="form-control" min="<?php echo $minDate; ?>"
                                                id="expdate" name="expdate" required>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Gender</label>
                                            <select name="gender" id="gender" required class="form-control">
                                                <option value="">Select Gender</option>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                                <option value="other">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <h4>Personal details</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Enter Email</label>
                                            <input type="email" autocomplete="off" class="form-control"
                                                id="email" name="email" required>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Select Nationality</label>
                                            <select class="form-control" id="nationality" required
                                                name="nationality">
                                                @foreach ($countrys as $count)
                                                    @if ($count->id == 162)
                                                        <option selected value="{{ $count->id }}">
                                                            {{ $count->nicename }}
                                                        </option>
                                                    @else
                                                        <option value="{{ $count->id }}">{{ $count->nicename }}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Select Country</label>
                                            <select class="form-control" id="country" required name="country">
                                                @foreach ($countrys as $count)
                                                    @if ($count->id == 162)
                                                        <option selected value="{{ $count->id }}">
                                                            {{ $count->nicename }}
                                                        </option>
                                                    @else
                                                        <option value="{{ $count->id }}">{{ $count->nicename }}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Enter Phone</label>
                                            <input type="tel" class="form-control" id="phone" name="phone"
                                                required>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Enter Home Address</label>
                                            <input type="text" class="form-control" id="address" name="address"
                                                required>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Clinic Name</label>
                                            <select name="clinic" id="clinic" required class="form-control">
                                                <option value="">Select Clinic</option>
                                                @foreach ($clincis as $item)
                                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    @php
                                        $today = date('Y-m-d');
                                        $minDate = '1970-06-01';
                                    @endphp
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Enter DOB</label>
                                            <input type="date" class="form-control" min="<?php echo $minDate; ?>"
                                                max="<?php echo $today; ?>" id="dob" name="dob" required>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for=""> Department</label>
                                            <select name="dep" id="department" required class="form-control">
                                                <option value="">Select Department</option>
                                                @foreach ($departments as $item)
                                                    <option value="{{ $item->id }}">{{ $item->department_name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div id="passwordField" class="col-lg-4">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="saveRecord" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        function deletePatient(patientId) {
            var patient = patientId;
            let url = "{{ route('admin.delete-patient', ':id') }}";
            url = url.replace(':id', patient);
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the Staff!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: {
                                '_token': "{{ csrf_token() }}",
                            },
                            success: function(data) {
                                if (data['status'] == 'success') {
                                    swal("Data deleted successfully!", {
                                        icon: "success",
                                    });
                                    setTimeout(() => {
                                        location.reload();
                                    }, 1000);
                                } else {
                                    swal("Oops", "Something went wrong!", "error")

                                }
                            },
                            error: function(response) {
                                swal("Oops", "Something went wrong!", "error")

                            }
                        });
                    }
                });

        }
    </script>

    <script>
        $('#clinic').change(function() {
            var clinic = $('#clinic').val();

            $.ajax({
                type: "GET",
                url: "{{ url('/Edit-staffManagement') }}",
                async: true,
                data: {
                    'clinic': clinic,
                },
                success: function(data) {
                    $('#department').html('');

                    array = data.departments;
                    option = "";
                    array.forEach(element => {
                        option += '<option value="' + element.id + '">' + element
                            .department_name +
                            '</option>';
                    });
                    console.log(array);
                    $('#department').append(option);
                },
            });
        });

        function ajaxCallPhone() {
            var input = document.querySelector("#phone");
            window.intlTelInput(input);
        }
        $(document).ready(function() {

            $('#addnewModel').on('hidden.bs.modal', function() {
                // Reset the form when the modal is closed
                $('#addnewModel form')[0].reset();
            });

            $('#country').on('change', function() {
                var country = $('#country').val();
                $('#phone').html("");

                console.log(country);
                $.ajax({
                    url: "{{ url('/get-country') }}",
                    type: 'GET',
                    data: data = {
                        'datatype': 'countrycode',
                        'countryID': country,
                    },
                    success: function(res) {
                        $('#phone').val("+" + res.phonecode);
                        ajaxCallPhone();
                    }
                });

            });

            $('#email').on("keyup", function() {
                var email = $('#email').val();
                var id = $('#id').val();
                $.ajax({
                    type: "GET",
                    url: "{{ url('/get-email') }}",
                    async: true,
                    data: {
                        'id': id,
                        'email': email,
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.status == "warning") {
                            toastr.options = {
                                "closeButton": true,
                                "progressBar": true
                            }
                            toastr.warning(data.message);
                            $('.modal-footer').html('');

                            value =
                                '<button type="submit" id="saveRecord" disabled class="btn btn-primary">Save</button>';
                            $('.modal-footer').append(value);
                        } else if (data.status == "success") {
                            $('.modal-footer').html('');
                            value =
                                '<button type="submit" id="saveRecord" class="btn btn-primary">Save</button>';
                            $('.modal-footer').append(value);
                        } else {
                            $('.modal-footer').html('');
                            value =
                                '<button type="submit" id="saveRecord" class="btn btn-primary">Save</button>';
                            $('.modal-footer').append(value);
                        }
                    }
                });

            });
            $('.close').on('click', function() {
                $('#addnewModel').modal('hide');
            });

            function appCountry() {
                var country = $('#country').val();
                $('#phone').html("");

                console.log(country);
                $.ajax({
                    url: "{{ url('/get-country') }}",
                    type: 'GET',
                    data: data = {
                        'datatype': 'countrycode',
                        'countryID': country,
                    },
                    success: function(res) {
                        $('#phone').val("+" + res.phonecode);
                        ajaxCallPhone();
                    }
                });
            }

            $('#addnewbtn').on('click', function() {
                clearData();

                appCountry();
                $('#passwordField').html('');
                password = '';
                password += '<div class="form-group">';
                password += '<label for="">Enter Password</label>';
                password +=
                    '<input type="password"  minlength="8" id="passwordAddEdit" class="form-control" name="password" required>';
                password += '<div style="color: red" id="error-container"></div>';
                password += '</div>';

                $('#passwordField').append(password);

                $('#addnewModel').modal('show');
                $('#saveRecord').text("Save");
                $('#exampleModalCenterTitle').text("Add Health Practitioner management");
            })
        });

        function clearData() {
            $('#id').val('');
            // $('#country').val('');
            $('#name').val('');
            $('#name2').val('');
            $('#role').val('');
            $('#gender').val('');
            $('#email').val('');
            $('#dob').val('');
            $('#gender').val('');
            $('#phone').text('');
            $('#address').val('')
            $('#department').val('');
            $('#clinic').val('');
        }

        function editPatient(id) {
            clearData();
            $.ajax({
                type: "GET",
                url: "{{ url('/Edit-staffManagement') }}",
                data: {
                    id: id,
                },
                success: function(response) {

                    $('#passwordField').html("");
                    $('#id').val(response.user.id);

                    $('#name').val(response.user.name);
                    $('#name2').val(response.user.usersdetail.surname);

                    $('#email').val(response.user.email);
                    $('#dob').val(response.user.usersdetail.date_of_birth);

                    $('#docID').val(response.user.usersdetail.document_id);
                    $('#doctype').val(response.user.usersdetail.document_type);
                    $('#expdate').val(response.user.usersdetail.expiry_date);




                    // Gender
                    $('#gender').html('');
                    option1 = "";
                    option1 += '<option ' + (response.user.gender == "male" ? "selected" : "") +
                        '  value="male">Male</option>';
                    option1 += '<option ' + (response.user.gender == "female" ? "selected" : "") +
                        '  value="female">Female</option>';
                    option1 += '<option ' + (response.user.gender == "other" ? "selected" : "") +
                        '  value="other">Other </option>';

                    $('#gender').append(option1);


                    // clinic
                    $('#clinic').html('');
                    array1 = response.clinics;
                    option3 = "";
                    array1.forEach(element => {
                        option3 += '<option ' + (response.user.clinic_id == element.id ? "selected" :
                                "") +
                            '  value="' + element.id + '">' + element.name +
                            '</option>';
                    });
                    $('#clinic').append(option3);

                    // department
                    $('#department').html('');
                    array2 = response.departments;
                    option4 = "";
                    array2.forEach(element => {
                        option4 += '<option ' + (response.user.department_id == element.id ?
                                "selected" : "") +
                            '  value="' + element.id + '">' + element.department_name +
                            '</option>';
                    });
                    $('#department').append(option4);

                    // department
                    $('#country').html('');
                    array3 = response.country;
                    option4 = "";
                    array3.forEach(element => {
                        option4 += '<option ' + (response.user.country_id == element.id ?
                            "selected" :
                            "") + '  value="' + element.id + '">' + element.name + '</option>';
                    });
                    $('#country').append(option4);

                    $('#phone').val(response.user.phone_number);
                    $('#address').val(response.user.usersdetail.home_address_1)

                    // Log Files
                    console.log(response.user.phone_number);
                    ajaxCallPhone();
                },
                error: function(error) {
                    console.error(error);
                }
            });

            $('#addnewModel').modal('show');
            $('#saveRecord').text("Update");
            $('#exampleModalCenterTitle').text("Update Health Staff Management");
        }
        oTable = $('#patientSearch')
            .DataTable(); //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said
        $('#searchPatientField').keyup(function() {
            console.log("hello");
            oTable.search($(this).val()).draw();
        })

        function passwordChange(id) {
            $('#useredit_id').val(id);
            console.log(id);
        }

        $('#password-update-form').on('submit', function(e) {
            e.preventDefault();
            $('#password-update-errors1').html("");
            $('#password-update-errors2').html("");
            var formData = $(this).serialize();
            var userId=$(this).find('[name=user_id]').val();

            $.ajax({
                url: '{{ url("/users") }}/' + userId + '/update-password',
                method: 'POST',
                data: formData,
                success: function(response) {
                    toastr.options = {
                        "closeButton": true,
                        "progressBar": true
                    }
                    toastr.success("Password Updated Sucessfully");
                    location.reload();
                },
                error: function(xhr) {

                    $('#password-update-errors1').html("");
                    $('#password-update-errors2').html("");

                    var errors = xhr.responseJSON.errors;

                    $('#password-update-errors1').html(errors.new_password);
                    $('#password-update-errors2').html(errors.confirm_password);
                }
            });
        });
    </script>
</x-app-layout>
