<x-app-layout>
    <style>
        ul#ui-id-1 {
            background: #f3f3f3;
            height: 45px;
        }

        #geomap {
            width: 100%;
            height: 300px;
            margin-top: 1%;
        }

        .ui-helper-hidden-accessible {
            display: none;
        }
    </style>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <x-slot name="header">

        <div class="page-header">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/clinics') }}">Clinics</a></li>
                <li class="breadcrumb-item active" aria-current="page">Add Clinics</li>
            </ol>
        </div>
    </x-slot>
    <style>
        .modal-backdrop {
            opacity: 0.5 !important;
        }
    </style>


    <form method="post" class="sample_form" class="form-horizontal" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" min="0" placeholder="Enter Your Name" required name="name"
                class="form-control" id="name">
            <span class="text-danger" id="nameError"></span>
        </div>
        <div class="form-group">
            <label for="contact_number">Contact Number</label>
            <input type="number" min="0" placeholder="Enter Your Contact Number" required name="contact_number"
                class="form-control" id="contact_number">
            <span class="text-danger" id="phoneError"></span>
        </div>

        <div class="form-group">
            <label for="image1">Image</label>
            <input required class="form-control image1" type="file" name="image1" id="image1">
            <span class="text-danger" id="profileError"></span>
        </div>

        <span id="form_result1"></span>
        <span id="form_result2"></span>

        <div class="form-group">
            <label for="search_location">Address</label>
            <input type="text" name="search_location" id="search_location" class="form-control"
                placeholder="Search location">
            <span class="text-danger" id="addresserror"></span>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div id="geomap"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <input type="hidden" name="search_addr" class="search_addr" size="45">
                <input type="hidden" name="latitude" class="search_latitude" size="30">
                <input type="hidden" name="longitude" class="search_longitude" size="30">
            </div>
        </div>

        <div class="modal-footer">
            <input type="submit" class=" btn btn-primary btn-width2" value="Add">

            {{-- <button type="button" id="add-clinic" class="btn btn-primary">Add</button> --}}
        </div>
    </form>


    {{-- update Clinic --}}
    <div class="modal" tabindex="-1" id="update_clinicModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Clinic</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="update_clinic" type="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" min="0" required name="name" class="form-control"
                                id="clinic_name">
                            <span class="text-danger" id="updatenameError"></span>
                        </div>
                        <input type="hidden" value="" name="clinic_id" id="clinic_id">
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" min="0" required name="address" class="form-control"
                                id="clinic_address">
                            <span class="text-danger" id="updateaddressError"></span>
                        </div>
                        <div class="form-group">
                            <label for="contact_number">Contact Number</label>
                            <input type="number" min="0" required name="contact_number" class="form-control"
                                id="clinic_contact_number">
                            <span class="text-danger" id="updatephoneError"></span>
                        </div>


                        <div class="modal-footer">
                            <button type="button" id="update-clinic"
                                class="btn btn-primary btn-width2">Update</button>
                            <button type="button" class="btn btn-secondary btn-width2"
                                data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


</x-app-layout>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(function() {
        $('#dtBasicExample').DataTable({
            "pagingType": "simple",
            // "simple" option for 'Previous' and 'Next' buttons only
        });
        $('.dataTables_length').addClass('bs-select');

        $('#update_clinicModal').on('shown.bs.modal', function() {
            $('#myInput').trigger('focus')
        })
        /* add UHF JS-Code */

        function clearfield() {
            $('#nameError').text("");
            $('#phoneError').text("");
            $('#addressError').text("");
            $('#search_location').text("");


            $('.search_addr').text("");
            $('.latitude').text("");
            $('.search_longitude').text("");
        }

        $('.sample_form').on('submit', function(event) {
            event.preventDefault();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var url = "{{ route('admin.clinic.store') }}";
            $.ajax({
                type: "POST",
                url: url,
                // data: $('#add_clinic').serialize(),
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                dataType: "json",

                success: function(data) {
                    console.log(data);
                    if (data['status'] == 'success') {
                        clearfield();
                        swal("Clinic Added successfully!", {
                            icon: "success",
                        });
                        setTimeout(() => {
                            location.reload();
                            window.location = '/clinic';

                        }, 1000);
                    } else {
                        $("#add-clinic").prop("disabled", false);
                        swal("Oops", "Something went wrong!", "error")
                    }
                },
                error: function(response) {
                    $("#add-clinic").prop("disabled", false);
                    var res = JSON.parse(response.responseText);
                    console.log(res);
                    if (res.error) {} else {
                        $('#nameError').text(res.errors.name);
                        $('#addressError').text(res.errors.address);
                        $('#phoneError').text(res.errors.contact_number);
                        $('#addresserror').text(res.errors.search_location);

                    }
                },
            });
        });




        $('#add-clinic').click(function() {
            $("#add-clinic").prop("disabled", true);
            $('#nameError').text("");
            $('#phoneError').text("");
            $('#addressError').text("");
            $('#search_location').text("");


            $('.search_addr').text("");
            $('.latitude').text("");
            $('.search_longitude').text("");

            var url = "{{ route('admin.clinic.store') }}";
            $.ajax({
                type: "POST",
                url: url,
                data: $('#add_clinic').serialize(),
                success: function(data) {
                    console.log(data);
                    if (data['status'] == 'success') {
                        clearfield();
                        swal("Clinic Added successfully!", {
                            icon: "success",
                        });
                        setTimeout(() => {
                            location.reload();

                            window.location = '/clinic';
                        }, 1000);
                    } else {
                        $("#add-clinic").prop("disabled", false);
                        swal("Oops", "Something went wrong!", "error")
                    }
                },
                error: function(response) {
                    $("#add-clinic").prop("disabled", false);
                    var res = JSON.parse(response.responseText);
                    console.log(res);
                    if (res.error) {} else {
                        $('#nameError').text(res.errors.name);
                        $('#addressError').text(res.errors.address);
                        $('#phoneError').text(res.errors.contact_number);
                        $('#addresserror').text(res.errors.search_location);

                    }
                },
            });
        });


    });
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzf7KnzVx3iLASRh25OP_bYgTpUD-dIW8&libraries=places">
</script>


<script>
    var geocoder;
    var map;
    var marker;

    /*
     * Google Map with marker
     */
    function initialize() {
        var initialLat = $('.search_latitude').val();
        var initialLong = $('.search_longitude').val();
        initialLat = initialLat ? initialLat : 36.169648;
        initialLong = initialLong ? initialLong : -115.141000;


        const center = {
            lat: 50.064192,
            lng: -130.605469
        };

        var latlng = new google.maps.LatLng(initialLat, initialLong);
        var options = {
            zoom: 16,
            center: latlng,
            componentRestrictions: {
                country: "us"
            },
            // bounds: defaultBounds,
            strictBounds: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        };

        map = new google.maps.Map(document.getElementById("geomap"), options);

        geocoder = new google.maps.Geocoder();

        marker = new google.maps.Marker({
            map: map,
            draggable: false,
            position: latlng
        });

        google.maps.event.addListener(marker, "dragend", function() {
            var point = marker.getPosition();
            map.panTo(point);
            geocoder.geocode({
                'latLng': marker.getPosition()
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    marker.setPosition(results[0].geometry.location);
                    $('.search_addr').val(results[0].formatted_address);
                    $('.search_latitude').val(marker.getPosition().lat());
                    $('.search_longitude').val(marker.getPosition().lng());
                }
            });
        });

    }

    $(document).ready(function() {
        //load google map
        initialize();

        /*
         * autocomplete location search
         */
        var PostCodeid = '#search_location';


        $(function() {

            $(PostCodeid).autocomplete({

                source: function(request, response) {
                    geocoder.geocode({
                        'address': request.term
                    }, function(results, status) {
                        response($.map(results, function(item) {
                            return {
                                label: item.formatted_address,
                                value: item.formatted_address,
                                lat: item.geometry.location.lat(),
                                lon: item.geometry.location.lng()
                            };
                        }));
                    });
                },
                select: function(event, ui) {
                    $('.search_addr').val(ui.item.value);
                    $('.search_latitude').val(ui.item.lat);
                    $('.search_longitude').val(ui.item.lon);
                    var latlng = new google.maps.LatLng(ui.item.lat, ui.item.lon);
                    marker.setPosition(latlng);
                    initialize();
                }
            });
        });

        /*
         * Point location on google map
         */
        $('.get_map').click(function(e) {
            var address = $(PostCodeid).val();
            geocoder.geocode({
                'address': address
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    marker.setPosition(results[0].geometry.location);
                    $('.search_addr').val(results[0].formatted_address);
                    $('.search_latitude').val(marker.getPosition().lat());
                    $('.search_longitude').val(marker.getPosition().lng());
                } else {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
            e.preventDefault();
        });

        //Add listener to marker for reverse geocoding
        google.maps.event.addListener(marker, 'drag', function() {
            geocoder.geocode({
                'latLng': marker.getPosition()
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('.search_addr').val(results[0].formatted_address);
                        $('.search_latitude').val(marker.getPosition().lat());
                        $('.search_longitude').val(marker.getPosition().lng());
                    }
                }
            });
        });
    });


    $('#add_menue_form').on('submit', function(event) {

        event.preventDefault();
        $.ajax({
            url: "/add/venue",
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType: "json",
            success: function(data) {

                var html = '';
                if (data.errors) {
                    html = '<div class="alert alert-danger">';
                    for (var count = 0; count < data.errors.length; count++) {
                        html += '<p>' + data.errors[count] + '</p>';
                    }
                    html += '</div>';

                    $('#form_result2').html(null);
                    $('#form_result1').html(null);
                    $('#form_result2').html(html);

                }
                if (data.success) {
                    window.location.href = '/venues/list';
                }
            }
        })
    });
</script>
