<x-app-layout>
    <style>
        ul#ui-id-1 {
            background: #f3f3f3;
            height: 45px;
        }
        a#addnewbtn,
        a.btn.btn-primary.btn-sm.m-0.addBalance.btn-width{
            padding: 0.25rem 0.5rem;
            font-size: .875rem;
            line-height: 1.5;
            border-radius: 0.2rem;
            background: #cb115e;
            border: none;
        }

        #geomap {
            width: 100%;
            height: 300px;
            margin-top: 1%;
        }

        .ui-helper-hidden-accessible {
            display: none;
        }
    </style>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzf7KnzVx3iLASRh25OP_bYgTpUD-dIW8&libraries=places">
    </script>
    <x-slot name="header">

        <div class="page-header">
            <link rel="stylesheet" href="https://cdn.tutorialjinni.com/intl-tel-input/17.0.8/css/intlTelInput.css" />
            <script src="https://cdn.tutorialjinni.com/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>

            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
            <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>

            <link rel="stylesheet" type="text/css"
                href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
            <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

            <div>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Clinics</li>
                </ol>
            </div>
        </div>
    </x-slot>
    <style>
        .modal-backdrop {
            opacity: 0.5 !important;
        }

        .btn-width2 {
            width: 100px;
        }

        .button-style {
            float: right;
            margin-bottom: inherit
        }

        .paginate_button {
            width: 120px
        }

        #btnser {
            width: 180px;
            text-align: center;
        }


    </style>


    <div class="card">
        <div class="py-12">
            @if (session('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 m-1">
                <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-2">
                    <div class="table-responsive">
                        <div class="row">
                            <div class="col-lg-6">
                                <h4>Clinics</h4>
                            </div>
                            <div class="col-lg-6">
                                <a href="{{ url('addclinic') }}" id="addnewbtn"
                                    class="btn btn-primary btn-sm pull-right">Add New</a>
                            </div>
                        </div>
                        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0"
                            width="100%">
                            <thead>
                                <tr>
                                    <th style="width: 50px;" class="th-sm">#</th>
                                    <th style="width: 110px;" class="th-sm">Image</th>
                                    <th style="width: 126px;" class="th-sm">Clinic Name</th>
                                    <th style="width: 600px;" class="th-sm">Address</th>
                                    <th class="th-sm">Contact Number</th>
                                    {{-- <th>Email</th> --}}
                                    <th style=" text-align:center">Action</th>
                                </tr>

                            </thead>
                            <tbody>
                                @foreach ($clinics as $index => $clinic)
                                    <tr>
                                        <td style="width:30px;">{{ $index + 1 }}</td>
                                        <td style="width:30px;"><img src="{{ $clinic->image1 }}"
                                                alt="{{ $clinic->name }}">
                                        </td>
                                        <td style="width:30px;">{{ $clinic->name }}</td>
                                        <td>{{ $clinic->address }}</td>
                                        <td>{{ $clinic->contact }}</td>
                                        <td id="btnser">
                                            <a href="/clinicedit/{{ $clinic->id }}" type="button"
                                                class="btn btn-primary btn-sm m-0 addBalance btn-width">Update</a>

                                            {{-- <button  data-toggle="modal" data-target="#update_clinicModal"
                                            id="addBtn"
                                            onclick="stars({{ $clinic->id }})"
                                            data-clinic-id="{{ $clinic->id }}">
                                        </button> --}}

                                            <button type="button"
                                                class="delete-clinic btn btn-danger btn-sm m-0 btn-width"
                                                data-clinic-id="{{ $clinic->id }}"
                                                onclick="deleteClinic({{ $clinic->id }})">Delete
                                            </button>
                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



    {{-- update Clinic --}}
    <div class="modal" tabindex="-1" id="update_clinicModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Clinic</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="update_clinic" type="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" min="0" required name="name" class="form-control"
                                id="clinic_name">
                            <span class="text-danger" id="updatenameError"></span>
                        </div>
                        <input type="hidden" value="" name="clinic_id" id="clinic_id">
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" min="0" required name="address" class="form-control"
                                id="clinic_address">
                            <span class="text-danger" id="updateaddressError"></span>
                        </div>
                        <div class="form-group">
                            <label for="contact_number">Contact Number</label>
                            <input type="number" min="0" required name="contact_number" class="form-control"
                                id="clinic_contact_number">
                            <span class="text-danger" id="updatephoneError"></span>
                        </div>


                        <div class="modal-footer">
                            <button type="button" id="update-clinic"
                                class="btn btn-primary btn-width2">Update</button>
                            <button type="button" class="btn btn-secondary btn-width2"
                                data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    {{-- add card modal --}}
    <div class="modal" tabindex="-1" id="addClinic">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Clinic</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="add_clinic" type="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" min="0" required name="name" class="form-control"
                                id="name">
                            <span class="text-danger" id="nameError"></span>
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" min="0" required name="address" class="form-control"
                                id="address">
                            <span class="text-danger" id="addressError"></span>
                        </div>
                        <div class="form-group">
                            <label for="contact_number">Contact Number</label>
                            <input type="number" min="0" required name="contact_number" class="form-control"
                                id="contact_number">
                            <span class="text-danger" id="phoneError"></span>
                        </div>

                        <span id="form_result1"></span>
                        <span id="form_result2"></span>

                        <div class="form-group">
                            <input type="text" id="search_location" class="form-control"
                                placeholder="Search location">
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div id="geomap"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="search_addr" class="search_addr" size="45">
                                <input type="hidden" name="latitude" class="search_latitude" size="30">
                                <input type="hidden" name="longitude" class="search_longitude" size="30">
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" id="add-clinic" class="btn btn-primary  btn-width2">Add</button>
                            <button type="button" class="btn btn-secondary  btn-width2"
                                data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $('#update_clinicModal').on('shown.bs.modal', function() {
        $('#myInput').trigger('focus')
    })

    function deleteClinic(clinicId) {
        var clinic = clinicId;
        let url = "{{ route('admin.clinic.destroy', ':id') }}";
        url = url.replace(':id', clinic);
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover the Clinic!",
                icon: "warning",
                buttons: true,
                dangerMode: true
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        data: {
                            '_token': "{{ csrf_token() }}",
                        },
                        success: function(data) {
                            console.log(data);

                            if (data['status'] == 'success') {
                                swal("Clinic Deleted successfully!", {
                                    icon: "success",
                                });
                                setTimeout(() => {
                                    location.reload();
                                }, 1000);
                            } else if (data['errorExist'] == 'success') {
                                swal("Oops", "Patient's exist against this Clinic!", "error")
                            } else {
                                swal("Oops", "Something went wrong!", "error")

                            }
                        },
                        error: function(response) {
                            console.log(response);
                            swal("Oops", "Something went wrong!", "error")

                        }
                    });
                }
            });

    }

    function stars(clinicId) {
        var clinic = clinicId;
        let url = "{{ route('admin.clinic.show', ':id') }}";
        url = url.replace(':id', clinic);
        $.ajax({
            type: "GET",
            url: url,
            success: function(data) {
                $("#clinic_name").val(data.name);
                $("#clinic_address").val(data.address);
                $("#clinic_contact_number").val(data.contact);
                $("#clinic_id").val(data.id);

            },
        });
    }
    $(function() {
        $('#dtBasicExample').DataTable({
            "pagingType": "simple",
            // "simple" option for 'Previous' and 'Next' buttons only
        });
        $('.dataTables_length').addClass('bs-select');

        $('#update_clinicModal').on('shown.bs.modal', function() {
            $('#myInput').trigger('focus')
        })
        /* add UHF JS-Code */

        $('#add-clinic').click(function() {
            $("#add-clinic").prop("disabled", true);
            $('#nameError').text("");
            $('#phoneError').text("");
            $('#addressError').text("");
            var url = "{{ route('admin.clinic.store') }}";
            $.ajax({
                type: "POST",
                url: url,
                data: $('#add_clinic').serialize(),
                success: function(data) {

                    if (data['status'] == 'success') {
                        swal("Clinic has been added successfully!", {
                            icon: "success",
                        });
                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    } else {
                        $("#add-clinic").prop("disabled", false);
                        swal("Oops", "Something went wrong!", "error")
                    }
                },
                error: function(response) {
                    $("#add-clinic").prop("disabled", false);
                    var res = JSON.parse(response.responseText);
                    console.log(res);
                    if (res.error) {} else {
                        $('#nameError').text(res.errors.name);
                        $('#phoneError').text(res.errors.contact_number);
                        $('#addressError').text(res.errors.address);

                    }
                },
            });
        });
        $('#update-clinic').click(function() {
            $("#update-clinic").prop("disabled", true);
            $('#updatenameError').text("");
            $('#updatephoneError').text("");
            $('#updateaddressError').text("");
            var clinic_id = $("#clinic_id").val();
            let url = "{{ route('admin.clinic.update', ':id') }}";
            url = url.replace(':id', clinic_id);
            $.ajax({
                type: "PUT",
                url: url,
                data: $('#update_clinic').serialize(),
                success: function(data) {
                    if (data['status'] == 'success') {
                        swal("Department has been updated successfully!", {
                            icon: "success",
                        });
                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                        location.reload();
                    } else {
                        $("#update-clinic").prop("disabled", false);

                        swal("Oops", "Something went wrong!", "error")
                    }
                },
                error: function(response) {
                    $("#update-clinic").prop("disabled", false);

                    var res = JSON.parse(response.responseText);
                    console.log(res);
                    if (res.error) {} else {
                        $('#updatenameError').text(res.errors.name);
                        $('#updatephoneError').text(res.errors.contact_number);
                        $('#updateaddressError').text(res.errors.address);

                    }
                },
            });
        });
    });
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzf7KnzVx3iLASRh25OP_bYgTpUD-dIW8&libraries=places">
</script>


<script>
    var geocoder;
    var map;
    var marker;

    /*
     * Google Map with marker
     */
    function initialize() {
        var initialLat = $('.search_latitude').val();
        var initialLong = $('.search_longitude').val();
        initialLat = initialLat ? initialLat : 36.169648;
        initialLong = initialLong ? initialLong : -115.141000;


        const center = {
            lat: 50.064192,
            lng: -130.605469
        };

        var latlng = new google.maps.LatLng(initialLat, initialLong);
        var options = {
            zoom: 16,
            center: latlng,
            componentRestrictions: {
                country: "us"
            },
            // bounds: defaultBounds,
            strictBounds: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        };

        map = new google.maps.Map(document.getElementById("geomap"), options);

        geocoder = new google.maps.Geocoder();

        marker = new google.maps.Marker({
            map: map,
            draggable: false,
            position: latlng
        });

        google.maps.event.addListener(marker, "dragend", function() {
            var point = marker.getPosition();
            map.panTo(point);
            geocoder.geocode({
                'latLng': marker.getPosition()
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    marker.setPosition(results[0].geometry.location);
                    $('.search_addr').val(results[0].formatted_address);
                    $('.search_latitude').val(marker.getPosition().lat());
                    $('.search_longitude').val(marker.getPosition().lng());
                }
            });
        });

    }

    $(document).ready(function() {
        //load google map
        initialize();

        /*
         * autocomplete location search
         */
        var PostCodeid = '#search_location';


        $(function() {

            $(PostCodeid).autocomplete({

                source: function(request, response) {
                    geocoder.geocode({
                        'address': request.term
                    }, function(results, status) {
                        response($.map(results, function(item) {
                            return {
                                label: item.formatted_address,
                                value: item.formatted_address,
                                lat: item.geometry.location.lat(),
                                lon: item.geometry.location.lng()
                            };
                        }));
                    });
                },
                select: function(event, ui) {
                    $('.search_addr').val(ui.item.value);
                    $('.search_latitude').val(ui.item.lat);
                    $('.search_longitude').val(ui.item.lon);
                    var latlng = new google.maps.LatLng(ui.item.lat, ui.item.lon);
                    marker.setPosition(latlng);
                    initialize();
                }
            });
        });

        /*
         * Point location on google map
         */
        $('.get_map').click(function(e) {
            var address = $(PostCodeid).val();
            geocoder.geocode({
                'address': address
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    marker.setPosition(results[0].geometry.location);
                    $('.search_addr').val(results[0].formatted_address);
                    $('.search_latitude').val(marker.getPosition().lat());
                    $('.search_longitude').val(marker.getPosition().lng());
                } else {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
            e.preventDefault();
        });

        //Add listener to marker for reverse geocoding
        google.maps.event.addListener(marker, 'drag', function() {
            geocoder.geocode({
                'latLng': marker.getPosition()
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('.search_addr').val(results[0].formatted_address);
                        $('.search_latitude').val(marker.getPosition().lat());
                        $('.search_longitude').val(marker.getPosition().lng());
                    }
                }
            });
        });
    });


    $('#add_menue_form').on('submit', function(event) {

        event.preventDefault();
        $.ajax({
            url: "/add/venue",
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType: "json",
            success: function(data) {

                var html = '';
                if (data.errors) {
                    html = '<div class="alert alert-danger">';
                    for (var count = 0; count < data.errors.length; count++) {
                        html += '<p>' + data.errors[count] + '</p>';
                    }
                    html += '</div>';

                    $('#form_result2').html(null);
                    $('#form_result1').html(null);
                    $('#form_result2').html(html);

                }
                if (data.success) {
                    window.location.href = '/venues/list';
                }
            }
        })
    });
</script>
