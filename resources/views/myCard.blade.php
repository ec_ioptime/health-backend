<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href=
"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="style.css">
    <title>
      {{ $name }}
    </title>
</head>
    <style>
        body {
    font-family: Arial, Helvetica, sans-serif;
    margin: 0;
    padding-top: 50px;
}

.card {
    box-shadow: 0 5px 5px 5px #e1e1e1;
    max-width: 350px;
    padding: 15px;
    border-radius: 5px;
    margin: auto;
    text-align: center;
}

img {
    width: 50%;
}
    .stars{
    width: 50%;
    border: 5px solid #e1e1e1;
    border-radius: 50%;
    }
.social {
    margin: 15px 0;
}

a {
    font-size: 26px;
    padding: 7px 12px;
    text-decoration: none;
    color: #585858;
    border: 1px solid #e1e1e1;
    border-radius: 10px;
}

a:hover {
    background-color: #585858;
    color: #ffffff;
}
table, table tr td {
    width: 290px;
    margin: 0 auto;
    border: 1px solid #e1e1e1;
    text-align: left;
}
    </style>
<body>
    <div class="card">
             <img src="data:image/jpeg;base64,{{ $image1 }}" alt="image" />

        <img src="data:image/jpeg;base64,{{ $image }}"  alt="image" />
        <h4>{{ $name }}</h4>
        <img src="data:image/jpeg;base64,{{ $qrCode }}"  alt="image" />
    </div>
</body>

</html>

