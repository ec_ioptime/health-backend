<style>
    .main-sidebar-body .nav-item+.nav-item {
        margin-top: 8px;
    }

    .side-menu .nav-link {
        transition: all .25s;
        padding-top: 4px;
    }

    .main-sidebar-body .nav-item.active .nav-link,
    .main-sidebar-body .nav-sub-item.active>.nav-sub-link,
    .main-sidebar-body .nav-sub-item.hover>.nav-sub-link,
    .side-menu .nav-link:hover,
    .page-header .breadcrumb-item.active {
        color: #cb115e;
    }

    .btn-primary {
        background: #cb115e;
    }

    button.btn.btn-danger.btn-sm.m-1 {
        background: red;
    }

    button:not(:disabled),
    [type="button"]:not(:disabled),
    [type="reset"]:not(:disabled),
    [type="submit"]:not(:disabled) {
        cursor: pointer;
        color: white;
    }

    .main-sidebar-body .nav-link {
        height: 40px;
        display: flex;
        align-items: center;
        padding: 0;
        font-weight: 400;
        font-size: 14px;
        color: #334151;
        margin-left: 0px;
    }

    img.header-brand-img.icon-logo {
        height: 40px;
    }

    .main-sidebar.main-sidebar-sticky.side-menu.ps.ps--active-y {
        z-index: 1000;
    }

    .header-brand-img {
        height: 4rem;
        line-height: 2.5rem;
        vertical-align: middle;
        width: auto;
        padding: 8px;
        margin-top: 4px;
    }
</style>
<div class="main-sidebar-body">
    <ul class="nav">
        {{-- <li class="nav-label">Dashboard</li> --}}
        <li class="nav-item {{ Route::is('home') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('home') }}">
                <i class="fa fa-tachometer"></i>
                <span class="sidemenu-label">Dashboard</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link with-sub" href="#">
                <i class="fa fa-users"></i>
                <span class="sidemenu-label">Patient Management</span>
                <i class="angle fa fa-chevron-right"></i>
            </a>
            <ul class="nav-sub">
                <li class="nav-sub-item {{ Route::is('patients') ? 'active' : '' }}">
                    <a class="nav-sub-link" href="{{ route('patients') }}">Patients Details</a>
                </li>
                <li class="nav-sub-item {{ Route::is('Kinpatients') ? 'active' : '' }}">
                    <a class="nav-sub-link" href="{{ route('Kinpatients') }}">Next of Kin</a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a class="nav-link with-sub" href="#">
                <i class="fa fa-user-md"></i>
                <span class="sidemenu-label">Staff Management</span>
                <i class="angle fa fa-chevron-right"></i>
            </a>
            <ul class="nav-sub">
                <li class="nav-sub-item {{ Route::is('practissionarManagement') ? 'active' : '' }}">
                    <a class="nav-sub-link" href="{{ route('practissionarManagement') }}">Health Practitioner</a>
                </li>
                <li class="nav-sub-item {{ Route::is('facilityManagement') ? 'active' : '' }}">
                    <a class="nav-sub-link" href="{{ route('facilityManagement') }}">Health Facility</a>
                </li>
                <li class="nav-sub-item {{ Route::is('healthstaffManagement') ? 'active' : '' }}">
                    <a class="nav-sub-link" href="{{ route('healthstaffManagement') }}">Health Staff</a>
                </li>
            </ul>
        </li>

        <li class="nav-item {{ Route::is('staffDashboard') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('staffDashboard') }}">
                <i class="fa fa-home"></i>
                <span class="sidemenu-label">Queue Management</span>
            </a>
        </li>
        <li class="nav-item {{ Route::is('reportManagement') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('reportManagement') }}">
                <i class="fa fa-line-chart"></i>
                <span class="sidemenu-label">Report Management</span>
            </a>
        </li>
        <li class="nav-item {{ Route::is('adminclinicsName') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('adminclinicsName') }}">
                <i class="fa fa-hospital-o"></i>
                <span class="sidemenu-label">Clinics</span>
            </a>
        </li>
        <li class="nav-item {{ Route::is('departments') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('departments') }}">
                <i class="fa fa-medkit"></i>
                <span class="sidemenu-label">Departments</span>
            </a>
        </li>
        <li class="nav-item {{ Route::is('testManagement') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('testManagement') }}">
                <i class="fa fa-print"></i>
                <span class="sidemenu-label">Test Management</span>
            </a>
        </li>

    </ul>
</div>
