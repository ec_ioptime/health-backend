<?php

namespace Database\Seeders;

use App\Models\VaccinationCenter;
use Illuminate\Database\Seeder;

class VaccineCenterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          $data = [
            [
            'name' => 'ec Rabula Clinic',
            'address' => 'Rabula Location Keiskammahoek',
            "type" => ""

        ],
        [
            'name' => 'ec Philani Clinic (King Williams Town)',
            'address' => 'Kei Road',
                     "type" => ""
        ],
        [
            'name' => 'ec Zalara Clinic',
            'address' => 'Zalara Village King Williamstown',
            "type" => ""
        ],
    ];

    VaccinationCenter::insert($data);
    }
}

