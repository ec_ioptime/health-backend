<?php

namespace Database\Seeders;

use App\Models\VaccinationType;
use Illuminate\Database\Seeder;

class VaccineTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
            'name' => 'Pfizer-BioNTech',
            'dose' => '2',
        ],
        [
            'name' => 'Moderna',
            'dose' => '2',
        ],
        [
            'name' => 'Johnson & Johnson',
            'dose' => '1',
        ],
    ];

    VaccinationType::insert($data);
    }
}

