<?php

namespace Database\Seeders;

use App\Models\CovidQuestion;
use Illuminate\Database\Seeder;

class CovidQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $data = [
            [
            'question' => 'Do you have fever?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'covid'

        ],
        [
            'question' => 'Do you have covid recently?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'covid'

        ],
        [
            'question' => 'Do you have symptoms of cough?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'covid'
        ],
        [
            'question' => 'Have you travelled to a covid19 area?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'covid'
        ],
        [
            'question' => 'Have you been in contact with anyone who tested positive for
covid19?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'covid'
        ],

        [
            'question' => 'Do you have symptoms of sneezing?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'covid'
        ],
        [
            'question' => 'Do you have symptoms of HIV?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'heart_condition'
        ],
        [
            'question' => 'Do you have symptoms of TB?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'heart_condition'
        ],
        [
            'question' => 'Do you have symptoms of Hypertension?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'heart_condition'
        ],
        [
            'question' => 'Do you have symptoms of Ischaemic Heart Disease?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'heart_condition'
        ],
        [
            'question' => 'Do you have symptoms of Diabetes?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'heart_condition'
        ],
        [
            'question' => 'Do you have symptoms of Epilepsy?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'heart_condition'
        ],
        [
            'question' => 'Do you have symptoms of Asthma/COPD?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'heart_condition'
        ],
        [
            'question' => 'Do you have symptoms of Rheumatic Heart Disease?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'heart_condition'
        ],
        [
            'question' => 'Do you have symptoms of Physical Disability?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'heart_condition'
        ],
        [
            'question' => 'Do you have symptoms of Liver Disease?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'heart_condition'
        ],
        [
            'question' => 'Do you have symptoms of Chemotherapy?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'heart_condition'
        ],
        [
            'question' => 'Do you have symptoms of Kidney Disease?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'heart_condition'
        ],
        [
            'question' => 'Do you have symptoms of Mental Health?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'heart_condition'
        ],
        [
            'question' => 'Do you take Alcohol?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'risk_factors'
        ],
        [
            'question' => 'Do you do Smoking/Tobacco?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'risk_factors'
        ],
        [
            'question' => 'Do you do Physical Activity?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'risk_factors'
        ],
        [
            'question' => 'Do you have enough food in your home?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'eating_factors'
        ],
        [
            'question' => 'Do you eat a heaped plate of food?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'eating_factors'
        ],
        [
            'question' => 'Do you eat food high in Salt?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'eating_factors'
        ],
        [
            'question' => 'Do you eat food high in Fat?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'eating_factors'
        ],
        [
            'question' => 'Do you eat food high in Sugar?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'eating_factors'
        ],
        [
            'question' => 'Number of current Sexual partners?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'sexual_behaviour'
        ],
        [
            'question' => 'Have you had more than one Sexual partner in the past 6 months?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'sexual_behaviour'
        ],
        [
            'question' => 'Do you protect yourself and your partner when having sex?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'sexual_behaviour'
        ],
        [
            'question' => 'Have You Been in Contact with Anyone with Infectious TB?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'tb'
        ],
        [
            'question' => 'Have you had a Cough of 2 weeks/more, not improving on treatment?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'tb'
        ],
        [
            'question' => 'Have you had a Persistent fever of more than two weeks?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'tb'
        ],
        [
            'question' => 'Do you have Unexplained weight loss for more than 1.5 in a month?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'tb'
        ],
        [
            'question' => 'Do you get Drenching night sweats?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'tb'
        ],
        [
            'question' => 'Do you get Fatigue (less painful/ but always tired)?',
            'answers' => '{"No","Yes"}',
            "is_optional" => false,
            "category" => 'tb'
        ],
    ];

    CovidQuestion::insert($data);
    }
}

