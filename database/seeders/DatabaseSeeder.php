<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\VaccineTypeSeeder;
use Database\Seeders\CovidQuestionSeeder;
use Database\Seeders\VaccineCenterSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
         $this->call(VaccineTypeSeeder::class);
         $this->call(VaccineCenterSeeder::class);
         $this->call(CovidQuestionSeeder::class);
    }
}
