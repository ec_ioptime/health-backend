<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKnowchronicquestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('knowchronicquestion', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('questions_id');
            $table->unsignedBigInteger('user_id');
            $table->string('answer');
            $table->string('options');
            $table->string('is_knowchronic');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('knowchronicquestion');
    }
}
