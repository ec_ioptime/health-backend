<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string("phone_number")->nullable();
            $table->string("age")->nullable();
            $table->string("document_id")->nullable();
            $table->string("gender")->nullable();
            $table->string("pin")->nullable();
            $table->unsignedBigInteger('country_id')->nullable();
            $table->string("dialing_code")->nullable();
            $table->boolean('is_social_login')->default(0)->nullable();
            $table->string("profile_pic")->nullable();
            $table->string("document_type")->nullable();
            $table->timestamp("expiry_date")->nullable();
            $table->string("parent_user")->nullable();
            $table->string("card")->nullable();
            $table->boolean("VaccineVerified")->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

        });
    }
}
