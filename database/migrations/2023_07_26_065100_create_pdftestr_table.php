<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePdftestrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pdftestr', function (Blueprint $table) {
            $table->id();
            $table->string('staff_id');
            $table->string('clinic_id');
            $table->string('testdate');
            $table->string('testtime');
            $table->string('testtype');
            $table->string('testresult');
            $table->string('sampleid');
            $table->string('patientid');
            $table->string('assay1');
            $table->string('testvalues1');
            $table->string('testvalues2');
            $table->string('testvalues3');
            $table->string('testvalues4');
            $table->string('testvalues5');
            $table->string('testvalues6');
            $table->string('statusreport');
            $table->string('statusreportcheck');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pdftestr');
    }
}
