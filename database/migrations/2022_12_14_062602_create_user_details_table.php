<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('surname')->nullable();
            $table->string('age')->nullable();
            $table->string('gender')->nullable();
            $table->string('country_id')->nullable();
            $table->string('dialing_code')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('nationality')->nullable();
            $table->string('language')->nullable();
            $table->string('race')->nullable();
            $table->string('martial_status')->nullable();
            $table->string('personal_detail_country_id')->nullable();
            $table->string('home_address_1')->nullable();
            $table->string('home_address_2')->nullable();
            $table->string('home_address_3')->nullable();
            $table->string('issue_state_country_id')->nullable();

            $table->string('occupation_type')->nullable();
            $table->string('designation')->nullable();

            $table->string('employer_name')->nullable();
            $table->string('employer_country_id')->nullable();
            $table->string('work_number')->nullable();
            $table->string('work_address')->nullable();

            $table->unsignedBigInteger('clinic_id')->nullable();
            $table->foreign('clinic_id')->references('id')->on('clinics')->onDelete('cascade');
            $table->unsignedBigInteger('department_id')->nullable();
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade');

            $table->string('document_type')->nullable();
            $table->string('document_id')->nullable();
            $table->string('expiry_date')->nullable();

            $table->string('next_of_kin')->nullable();
            $table->string('kin_surname')->nullable();
            $table->string('next_of_kin_number')->nullable();
            $table->string('kinRelation')->nullable();
            $table->string('next_of_kin_country_id')->nullable();

            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();

            $table->string('identity_type')->nullable();
            $table->string('covidTestId')->nullable();
            $table->string('VaccineVerified')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
